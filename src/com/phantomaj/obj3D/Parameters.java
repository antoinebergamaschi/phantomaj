/**
* Parameters.java
* 02/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.obj3D;

import javax.swing.tree.DefaultMutableTreeNode;

/**
* Class use as an basic Structur model to save parameters data for each NodeForm/NodeObject
* NodeForm/NodeObject can have multiple Parameters
**/
public class Parameters{
	
	//0 == form , 1 == object , 2 == phantom
	private int _type;
	
	private int ID_form = -1;
	private int ID_obj = -1;
	private int ID_param = -1;
	private int number_of_transformation=1;
	
	//NodeObject specific
	private int[][] distFromCenter = null;
	
	private DefaultMutableTreeNode node = null;
	
	//constructor for Form
	public Parameters(int positionF,int positionO, DefaultMutableTreeNode node, int type,int id_param){
		this.node = node ;
		this._type = type ;
		this.ID_param = id_param ;
		this.ID_form = positionF ;
		this.ID_obj = positionO ;
	}
	
	//constructor for Object
	public Parameters(int positionF,int positionO, DefaultMutableTreeNode node, int type,int id_param, int[][] distFromCenter){
		this.node = node ;
		this._type = type ;
		this.ID_param = id_param ;
		this.ID_form = positionF ;
		this.ID_obj = positionO ;
		this.distFromCenter = distFromCenter ;
	}
	

	// Public methode 
	//setter
	
	public void setNode(DefaultMutableTreeNode node){
		this.node = node ;
	}
	
	public void setPosForm(int id){
		this.ID_form = id;
	}
	
	public void setPosObj(int id){
		this.ID_obj = id ;
	}
	
	public void setIdParam(int id){
		this.ID_param = id ;
	}
	
	public void setNumbofTransformation(int nb){
		this.number_of_transformation = nb ;
	}
	
	public void setCenterObj(int[][] center){
		this.distFromCenter = center;
	}
	
	//getter
	
	public DefaultMutableTreeNode getNode(){
		return this.node;
	}
	
	public int getPosForm(){
		return this.ID_form;
	}
	
	public int getPosObj(){
		return this.ID_obj;
	}
	
	public int getIdParam(){
		return this.ID_param;
	}
	
	public int getNumberOfTransformation(){
		return this.number_of_transformation;
	}
	
	public int getType(){
		return this._type;
	}
	
	
	public boolean isParamtersFromThisForm(int posObj, int posForm){
		if ( posObj == this.ID_obj ){
			if ( this._type == 1 ){
				return true;
			}
			else if ( posForm == this.ID_form ){
				return true;
			}
		}
		return false;
	}
	
	public String getTitle(){
		String str = "" ;
		str += "paramID_"+ID_param;
		return str;
	}
	
	public int[][] getCenterDistObj(){
		return this.distFromCenter;
	}
	
	/**
	* Debug
	**/
	public String toString(){
		String str = "";
		str += "Type : "+this._type+"\n";
		str += "PosObj : "+this.ID_obj+"\n";
		str += "PosForm : "+this.ID_form+"\n";
		str += "Param : "+this.ID_param+"\n";
		str += "Numb of repetition : "+this.number_of_transformation+"\n";
		return str;
	}
	
}