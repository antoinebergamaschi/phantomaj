/**
* Object3D.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.obj3D;

//Local import 
import com.phantomaj.form.CreateForm;
import com.phantomaj.tree.NodeForm;
import com.phantomaj.tree.NodeObject;
import com.phantomaj.tree.TreeDragAndDrop;
import com.phantomaj.utils.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

//Java imports


/**
* Object3D is the class regrouping the differente algorithm responsable of the drawing and banks creation.
* Interaction between Thread and CreatForm is the crucial objective of this class.
**/
public class Object3D {

	private static int idObj = 10;
	private ArrayList<ArrayList<CreateForm>> phantom = null;
	private ArrayList<ArrayList<CreateForm>> phantom_toString = null;
	private int sizePrephantom=-1;
	private int[] dimStack;
	private String savePath;
	private int saveType;
	private int numberOfPhantom;
	private double noiseSD;
	private TreeDragAndDrop tree;
	
	private boolean isStackUp = false;
	
	private Compute_Intensity  Cint = new Compute_Intensity();
	private Thread_option Toption;
	private boolean first_line=true;
	private String scriptVisilog = "";
	private boolean _isStructure = false;
	
	private String loadingT = "";
	private int loadingX = 0;

	public enum DataID{
        PHANTOM("P"),OBJECT("O"),FORM("NAME"),
        PHANTOM_DIMENSION("DS"),
        OBJECT_PIXEL_VALUE("OPV"),OBJECT_LAYER("LAY"),
        PIXEL_VALUE("PV"),BACKGROUND_VALUE("BV"),
        ORIGIN("ORI"),ANGLE("ANG"),DIMENSION("DIM");

        private String name;

        DataID(String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }

        public static DataID getType(String str){
            boolean bo = false;
            int i = -1;
            while (!bo && i < DataID.values().length ){
                i++;
                if ( DataID.values()[i].name.equalsIgnoreCase(str) ){
                    bo = true;
                }
            }
            if ( !bo ){
                System.err.println("Unknown Type String "+str);
            }

            return DataID.values()[i];
        }
    }

//######################## CONSTRUCTOR ######################################

	/**
	* Constructor
	**/
	public Object3D(ArrayList<ArrayList<CreateForm>> phantom, TreeDragAndDrop tree){
		this.phantom = phantom;
		this.tree = tree;
		this.phantom_toString = new ArrayList<ArrayList<CreateForm>>();
	}


	/**
	* Constructor use in RandomGenerator
	**/
	public Object3D(ArrayList<ArrayList<CreateForm>> phantom){
		this.phantom = phantom;
	}
	
//########################## METHOD ########################################

	/**
	* Draw the Phantom, use private ArrayList<ArrayList<CreateForm>> phantom.
	**/
	public void draw(){
		final Chrono c = new Chrono();
		c.start();

		final LoadingBar l = new LoadingBar();
		l.setInfo_obj(1,this);
		l.start();
		l.setLoadTitle("Allocating...");
		int time=0;
		
		try{
		
			final Thread g = new Thread(){
				public void run(){
				
					loadingT = "Allocating memory... please wait";
				
					CreateForm.creatStack(dimStack[0], dimStack[1], dimStack[2], Cint.getBackgroundValue());
					CreateForm.setComputeIntensity(Cint);
					CreateForm.setThreadOption(Toption);
					
					
					c.stop();
					
					long temps = c.delay();
					
					int count = 0;
					int maxCount = 0;
					
					for ( int k=0;k<phantom.size();k++){
						maxCount += phantom.get(k).size();
					}
					
					l.setLoadTitle("Drawing...");
					loadingX = (int)(((((float)count)/((float)maxCount))*2.0f)*100.0f);
					loadingT = "Remaing object :: "+count+" / "+maxCount;
					
					c.start();
					
					Iterator<ArrayList<CreateForm>> objects = phantom.iterator();

					//for every objects in the phantom
					while (objects.hasNext()) {
						ArrayList<CreateForm> object = objects.next();
						Iterator<CreateForm> forms = object.iterator();
						//Give a specific Identifier for this Object
						
						//for every form constituing this object
						while(forms.hasNext()){
							CreateForm form = forms.next();
							form.setLayerObject(idObj);
							form.init_parameters();
							
							
							loadingX = (int)(((((float)count)/((float)maxCount))*2.0f)*100.0f);
							loadingT = "Remaing object :: "+count+" / "+maxCount;
							count++;
						}
						idObj+=10;
					}
					
					c.stop();
					
					System.out.println(temps+"\t"+c.delay());
					
					l.setLoadTitle("Displaying");
					loadingX = -1;
					loadingT = "Stack display in process";
					
					// Affiche le stack construit une image plus test
					phantom.get(0).get(0).build_ImagePlus("Draw",noiseSD);
					
					
					//reset stack in CreateForm
					CreateForm.reset();	
				}
			};
			g.start();

			while ( g.isAlive() ){
			}
			l.dispose();

		}
		catch (Exception e){
			System.out.println("Test Error");
			l.setLoadTitle("Error while drawing");
			loadingT = "";
			loadingX = -1;
			while(time<10000) {
				l.dispose();
				time++;
			}
		}
	}
	
	
	/**
	* Draw the Phantom, use private ArrayList<ArrayList<CreateForm>> phantom.
	* @param first_time, true if first time else false.
	* @return int the number of remaining form
	**/
	public int drawWithoutCollision(boolean first_time){
		
		int i = 0;
		int z = 0;
		int objectPos = 0;
		
		ArrayList<CreateForm> form_toAdd = new ArrayList<CreateForm>();
		Iterator<ArrayList<CreateForm>> objects = phantom.iterator();
		if ( first_time && !isStackUp ){
			CreateForm.setComputeIntensity(this.Cint);
			CreateForm.setThreadOption(this.Toption);
			CreateForm.creatStack(dimStack[0], dimStack[1], dimStack[2], Cint.getBackgroundValue());
		}
		
		//for every objects in the phantom
		while (objects.hasNext()) {
			ArrayList<CreateForm> object = objects.next();
			System.out.print("Array form :"+object.size()+"\n");
			Iterator<CreateForm> forms = object.iterator();
			
			int count = 0;
			
			//for every form constituing this object
			while(forms.hasNext()){
				CreateForm form = forms.next();
				
				form.setLayerObject(idObj);
				
				// Test whener this form can be draw or not
				if ( form.try_init_parameters() ){
				
					//If there isn't any structure to draw
					if(!_isStructure){
						form.init_parameters();
						form_toAdd.add(form);
						z++;
						idObj+=10;
					}
					count++;
				}
				if ( !_isStructure ){
					i++;
				}
			}
			
			if ( _isStructure ){
				i++;
				//Add every form selected who have past the try function
				if ( count == phantom.get(objectPos).size()){
					z++;
					forms = object.iterator();
					while(forms.hasNext()){
						CreateForm form = forms.next();
						form_toAdd.add(form);
						form.init_parameters();
					}
					idObj+=10;
				}
			}
			
			objectPos++;
		}
		
		if( first_time && this.phantom_toString == null ){
			this.phantom_toString = new ArrayList<ArrayList<CreateForm>>();
			this.phantom_toString.add(form_toAdd);
		}
		else{
			this.phantom_toString.get(0).addAll(form_toAdd);
		}

		return ( i - z );
	}	
	
	/**
	* Force the phantom in the Object3D to be draw
	**/
	public void drawForced(){
		phantom.get(0).get(0).build_ImagePlus("Draw",this.noiseSD);
	}
	
	/**
	* Create a Register File use to proced Statistical analysis. Works only with the RandomGeneratorFrame
	* @param path, String the path where to save the Register File.
	**/
	public void creaRegisterFile(String path){
		String toWrite ="";
		int posForm = -1;
		BufferedWriter output = null;
		
		this.savePath = path;
		try{
			FileWriter fw = new FileWriter(this.savePath+"registry.data", true);
			output = new BufferedWriter(fw);
			
			output.write("###############################################################################\n");
			output.write("####### DATE :\t"+(new Date()).toString()+"\n") ;
			output.write("RegistryName\t"+savePath+"\n") ;
			
			output.write("###############################################################################\n");
			output.write("File\t"+savePath+"\n");
			output.write("###############################################################################\n");
			
			
			Iterator<ArrayList<CreateForm>> objects = phantom_toString.iterator();
			//for every objects in the phantom
			while (objects.hasNext()) {
			
				ArrayList<CreateForm> object = objects.next();
				Iterator<CreateForm> forms = object.iterator();
				
				
				//for every form constituing this object
				while(forms.hasNext()){
					CreateForm form = forms.next();
					posForm++;
					
					toWrite = "";
					toWrite += "############################### FORM DESCRIPTION ##############################\n";
					toWrite += "#######\tPOSITION :\t"+posForm+"\n"; 
					toWrite += form.toString();
					
					output.write(toWrite);
				}
			}

		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
		finally{
			try{
				output.flush();
				output.close();
			}
			catch(IOException e){
				System.out.print("Erreur : ");
				e.printStackTrace();
			}
		}
	}
	
	/**
	* Create an Object File, used to load randomGenerator Result in phanto_maJ
	* @param path, String where to save the object file.
	**/
	public void creatObjectFile(int[] dimStack, String path){
		BufferedWriter output = null;
		try{
			path += "randomStructure.obj";
			
			FileWriter fw = new FileWriter(path, false);
			output = new BufferedWriter(fw);
			
			int size = -1;
			int i = 0;
			Iterator<ArrayList<CreateForm>> objects = phantom_toString.iterator();
			
			
			if ( _isStructure ){
				size = this.phantom.get(0).size();
			}
			// System.out.println("Array forms :"+phantom.size()+"\n");
			System.out.println(sizePrephantom);
			//Phantom info
			String phanString = "";
			phanString += "P\tPhantom\nBV\t"+(int)this.Cint.getBackgroundValue()+"\n";
			phanString += "DS\t"+this.dimStack[0]+"\t"+this.dimStack[1]+"\t"+this.dimStack[2]+"\n";
			output.write(phanString);
			//for every objects in the phantom
			while (objects.hasNext()) {
				ArrayList<CreateForm> object = objects.next();
				Iterator<CreateForm> forms = object.iterator();
				//for every form constituing this object
				while(forms.hasNext()){
					CreateForm form = forms.next();
					
					if ( !_isStructure ){
						String objString = "O\tObject "+i+"\n";
						objString += "OPV\t"+form.getPixel_value()+"\n";
						objString += "LAY\t0\n";
						output.write(objString);
					}
					else if ( i == 0 ){
						String objString = "O\tObject "+i+"\n";
						objString += "OPV\t"+form.getPixel_value()+"\n";
						objString += "LAY\t0\n";
						output.write(objString);
					}
					else if ( (i-sizePrephantom)%size == 0 && i >= sizePrephantom){
						String objString = "O\tObject "+((i/size)+1)+"\n";
						objString += "OPV\t"+form.getPixel_value()+"\n";
						objString += "LAY\t0\n";
						output.write(objString);
					}
					String formString = form.toString();
					output.write(formString);
					
					i += 1;
				}
			}

		}
		catch(IOException ieo){
			System.out.print("Erreur : ");
			ieo.printStackTrace();
		}
		finally{
			try{
				output.flush();
				output.close();
			}
			catch(IOException e){
				System.out.print("Erreur : ");
				e.printStackTrace();
			}
		}
	}
	
	public void creatObjectFile_multi(String filename){
		BufferedWriter output = null;
		try{

			FileWriter fw = new FileWriter(this.savePath+filename, false);
			output = new BufferedWriter(fw);

			int i = 0;
			Iterator<ArrayList<CreateForm>> objects = phantom_toString.iterator();
			//Phantom info
			String phanString = "";
			phanString += "P\tPhantom\nBV\t0\n";
			phanString += "DS\t"+this.dimStack[0]+"\t"+this.dimStack[1]+"\t"+this.dimStack[2]+"\n";
			output.write(phanString);
			//for every objects in the phantom
			while (objects.hasNext()) {
				ArrayList<CreateForm> object = objects.next();
				
				String objString = "O\tObject "+i+"\n";
				objString += "OPV\t"+object.get(0).getPixel_value()+"\n";
				objString += "LAY\t"+object.get(0).getLayer_value()+"\n";
				output.write(objString);
				
				Iterator<CreateForm> forms = object.iterator();
				//for every form constituing this object
				while(forms.hasNext()){
					CreateForm form = forms.next();
					String formString = form.toString();
					output.write(formString);

				}
				i += 1;
			}
			
		}
		catch(IOException ieo){
			System.out.print("Erreur : ");
			ieo.printStackTrace();
		}
		finally{
			try{
				output.flush();
				output.close();
			}
			catch(IOException e){
				System.out.print("Erreur : ");
				e.printStackTrace();
			}
		}
	}
	
	public void multiDraw(ArrayList<Parameters> parameters, boolean isPaired, String path , int saveType ){
		
		path += File.separator;
		int num = 0;
		
		try{
			this.savePath = path+"MultiPhantom_"+num;
			File f = new File(this.savePath);
			while ( f.exists() && f.isDirectory()){
				num++;
				this.savePath = path+"MultiPhantom_"+num;
				f = new File(this.savePath);
			}
			// Create one directory
			f.mkdir();
			this.savePath += File.separator;
		}
		catch (Exception e){
			System.err.println("Error: " + e.getMessage());
		}
		
		
		this.saveType = saveType ;
		// this.savePath = path ;
		Parameters newParam = parameters.get(parameters.size() -1 );
		parameters.remove(parameters.size() - 1);
		multiDraw_recurcif(parameters,newParam );
	}
	
	/**
	* Recurcif call of the draw_modif_doubleparam fonction
	**/
	public void multiDraw_recurcif(ArrayList<Parameters> parameters_toclone, Parameters parameter_principal){
		ArrayList<Parameters> parameters = new ArrayList<Parameters>(parameters_toclone);
		// for the number of asked repetition
		for ( int i = 0 ; i < parameter_principal.getNumberOfTransformation()  ; i++ ){
			Iterator<Parameters> param_iterator = parameters.iterator();
			// for each Parameter exepte the parameter_principal
			while ( param_iterator.hasNext() ){
				Parameters param_current = param_iterator.next();
				Parameters[] param_array = {parameter_principal,param_current};
				for ( int j = 0 ; j < param_current.getNumberOfTransformation()  ; j++ ){
					int[] value_array = { i , j };
					draw_modif_Multiparam(param_array,value_array);
				}
			}
		}
		//recurcif call
		if ( parameters.size() >= 2 ){
			// System.out.println(parameters.size());
			Parameters newParam = parameters.get(parameters.size() - 1 );
			parameters.remove(parameters.size() - 1);
			multiDraw_recurcif(parameters,newParam );
		}
		else{
			//Added Script Visilog
			try{
				FileWriter fw = new FileWriter(this.savePath+"registry.data", true);
				BufferedWriter output = new BufferedWriter(fw);
				
				output.write(this.scriptVisilog);
		
				output.flush();
				output.close();
			}
			catch(IOException ioe){
				System.out.print("Erreur : ");
				ioe.printStackTrace();
			}
			//Fin add
			
			System.out.println("Fin Appelle recurcif multiDraw_recurcif ");
		}
	}
	
	
	/**
	* Draw the Phantom, use private ArrayList<ArrayList<CreateForm>> phantom. Fonction used in the recurcive fonction multiDraw_recurcif
	**/
	public void draw_modif_Multiparam(Parameters[] parameter, int[] posValue){
		ArrayList<CreateForm> phantom_form = new ArrayList<CreateForm>();
		phantom_toString = new ArrayList<ArrayList<CreateForm>>();
	
		int i = 0;
		int posObj= 0;
		int posForm = -1;
		int number = 0;
		String tosave ="";
		String scriptVisilog_int="";
		
		//Create a new file name for this object
		String finalPath = "";
		String nameObj ="";
		for ( int z = 0 ; z < parameter.length ; z ++ ){
			finalPath += parameter[z].getTitle();
			nameObj+= parameter[z].getTitle();
		}
		
		finalPath = this.savePath + finalPath + "_" + (posValue[0]+1) +"_"+ (posValue[1]+1) + ".tif";
		nameObj += (posValue[0]+1) +"_"+ (posValue[1]+1) +".obj";
		
		if ( first_line ){
			//Erase the file
			try{
				FileWriter fw = new FileWriter(this.savePath+"registry.data", false);
				BufferedWriter output = new BufferedWriter(fw);
				
				output.write("");
		
				output.flush();
				output.close();
			}
			catch(IOException ioe){
				System.out.print("Erreur : ");
				ioe.printStackTrace();
			}
			
			tosave += "###############################################################################\n";
			tosave += "####### DATE :\t"+(new Date()).toString()+"\n" ;
			tosave += "RegistryName\t"+this.savePath+"\n" ;
			first_line = false ;
		}
		// to save in the registry
		tosave += "###############################################################################\n";
		tosave += "File\t"+finalPath+"\n";
		tosave += "############################### FORM DESCRIPTION ##############################\n";
		Iterator<ArrayList<CreateForm>> objects = phantom.iterator();
		
		
		///********************************************************************
		//ADDED FOR VISILOG SCRIPT
		scriptVisilog_int += "########################## Script Visilog #####################################\n";
		this.scriptVisilog += "\nbErr = Data.LoadData(\""+finalPath+"\",DataType,ImgName)\nCall analyse()\n";
		scriptVisilog_int += "bErr = Data.LoadData(\""+finalPath+"\",DataType,ImgName)\nCall analyse()\n\n";
		//********************************************************************
		
		CreateForm.creatStack(dimStack[0], dimStack[1], dimStack[2], Cint.getBackgroundValue());
		CreateForm.setComputeIntensity(this.Cint);
		idObj = 10;
		
		//for every objects in the phantom
		while (objects.hasNext()) {
		
			ArrayList<CreateForm> object = objects.next();
			// System.out.print("Array form :"+object.size()+"\n");
			Iterator<CreateForm> forms = object.iterator();
			
			posForm = 0;
		
			//for every form constituing this object
			while(forms.hasNext()){
				CreateForm form = forms.next();
				
				int[] arrayMinMax = new int[2];
				boolean test_sameForm = false ;
				
				CreateForm copy_form = form.copy() ;
				phantom_form = new ArrayList<CreateForm>();
				
				copy_form.setLayerObject(idObj);
				
				// for every parameters
				for ( int posParam = 0 ; posParam < parameter.length ; posParam++ ){
					
					//Ajout for le nombre de repetition du parametre (1/2 ?) si 2 retire le for dans la fonction precedente
					if ( parameter[posParam].isParamtersFromThisForm(posObj,posForm) ){
						if ( parameter[posParam].getIdParam() < NodeForm.DIM){
							switch(parameter[posParam].getIdParam()){
								case NodeObject.TRANSLATION_X:
									// System.out.println("X origine Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getXoriTransfo();
									// add the specific distance from the Center of this object
									copy_form.setOriXInitial(arrayMinMax[0]+parameter[posParam].getCenterDistObj()[posForm][0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToXori(number);
									break;
								case NodeObject.TRANSLATION_Y:
									// System.out.println("Y origine Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getYoriTransfo();
									// add the specific distance from the Center of this object
									copy_form.setOriYInitial(arrayMinMax[0]+parameter[posParam].getCenterDistObj()[posForm][1]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToYori(number);
									break;
								case NodeObject.TRANSLATION_Z:
									// System.out.println("Z origine Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getZoriTransfo();
									// add the specific distance from the Center of this object
									copy_form.setOriZInitial(arrayMinMax[0]+parameter[posParam].getCenterDistObj()[posForm][2]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToZori(number);
									break;
								case NodeObject.ROTATION_PHI:
									// System.out.println("Phi Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getPhiTransfo();
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									// System.out.println("Phi modif = "+number);
									copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),0,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),0,posObj,posForm));
									break;
								case NodeObject.ROTATION_THETA:
									// System.out.println("Theta Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getThetaTransfo();
									copy_form.setThetaInitial(arrayMinMax[0]);
									number = (int)(((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									// System.out.println("Theta modif = "+number);
									copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),1,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),1,posObj,posForm));
									break;
								case NodeObject.ROTATION_PSI:
									// System.out.println("Psi Transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getPsiTransfo();
									copy_form.setPsiInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									// System.out.println("Psi modif = "+number);
									copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),2,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),2,posObj,posForm));

									break;
								case NodeObject.DIM:
									// System.out.println("Dimension X transformation Object");
									arrayMinMax = ((NodeObject)parameter[posParam].getNode()).getDimXTransfo();
									// copy_form.setPsiInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									number += arrayMinMax[0];
									//coef is in percent
									float coef = ((float)number)/100.0f;
									
									copy_form.setObjectChange(this.tree.computeObjectSize_typeT(coef,posObj,posForm));
									break;
								case NodeForm.TRANSLATION_X:
									// System.out.println("X origine Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getXoriTransfo();
									copy_form.setOriXInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToXori(number);
									break;
								case NodeForm.TRANSLATION_Y:
									// System.out.println("Y origine Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getYoriTransfo();
									copy_form.setOriYInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToYori(number);
									break;
								case NodeForm.TRANSLATION_Z:
									// System.out.println("Z origine Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getZoriTransfo();
									copy_form.setOriZInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToZori(number);
									break;
								case NodeForm.ROTATION_PHI:
									// System.out.println("Phi Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getPhiTransfo();
									copy_form.setPhiInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToPhi(number);
									break;
								case NodeForm.ROTATION_THETA:
									// System.out.println("Theta Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getThetaTransfo();
									copy_form.setThetaInitial(arrayMinMax[0]);
									number = (int)(((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToTheta(number);
									break;
								case NodeForm.ROTATION_PSI:
									// System.out.println("Psi Transformation Form");
									arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getPsiTransfo();
									copy_form.setPsiInitial(arrayMinMax[0]);
									number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() - 1)) * posValue[posParam]);
									copy_form.addToPsi(number);
									break;
								default:
									System.out.println("UnIdentified Modification :: Object3D.draw_modif_Multiparam L.727");
									break;
							}
						}
						//Case Dimension form
						else {
							arrayMinMax = ((NodeForm)parameter[posParam].getNode()).getDimensionTransfo()[parameter[posParam].getIdParam() - 100];
							copy_form.setDimensionInit(arrayMinMax[0],parameter[posParam].getIdParam() - 100);
							number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter[posParam].getNumberOfTransformation() -1)) * posValue[posParam]);
							copy_form.addToDim(number,parameter[posParam].getIdParam() - 100);
						}
					}
				}
				copy_form.init_parameters();
				
				phantom_form.add(copy_form);
				
				//to be save in the registry
				tosave += "#######\tPOSITION :\t"+posForm+"\n"; 
				tosave += copy_form.toString();
				posForm += 1 ;
			}
			idObj+=10;
			phantom_toString.add(phantom_form);
			
			posObj += 1;
			
		}
		
		//save the stack
		phantom_toString.get(0).get(0).save(finalPath,this.saveType);
		
		//Save objFile
		creatObjectFile_multi(nameObj);
		
		//save the registry for this object3D
		try{
			FileWriter fw = new FileWriter(this.savePath+"registry.data", true);
			BufferedWriter output = new BufferedWriter(fw);
			
			output.write(tosave);
			output.write(scriptVisilog_int);
			
			output.flush();
			output.close();
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
		
	}


	// private CreateForm ChangeParameters(CreateForm copy_form,Parameters parameter,int posForm, int posObj){
		// int[] arrayMinMax = null ;
		// int number = 0;
	
		// for (int posValue = 0 ; posValue < parameter.getNumberOfTransformation() ; posValue ++ ){
			// if ( parameter.getIdParam() >= 100){
				// switch(parameter.getIdParam()){
					// case 0:
						// System.out.println("X origine Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getXoriTransfo();
						// // add the specific distance from the Center of this object
						// copy_form.setOriXInitial(arrayMinMax[0]+parameter.getCenterDistObj()[posForm][0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToXori(number);
						// break;
					// case 1:
						// System.out.println("Y origine Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getYoriTransfo();
						// // add the specific distance from the Center of this object
						// copy_form.setOriYInitial(arrayMinMax[0]+parameter.getCenterDistObj()[posForm][1]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToYori(number);
						// break;
					// case 2:
						// System.out.println("Z origine Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getZoriTransfo();
						// // add the specific distance from the Center of this object
						// copy_form.setOriZInitial(arrayMinMax[0]+parameter.getCenterDistObj()[posForm][2]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToZori(number);
						// break;
					// case 3:
						// System.out.println("Phi Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getPhiTransfo();
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// System.out.println("Phi modif = "+number);
						// copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),0,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),0,posObj,posForm));
						// break;
					// case 4:
						// System.out.println("Theta Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getThetaTransfo();
						// copy_form.setThetaInitial(arrayMinMax[0]);
						// number = (int)(((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// System.out.println("Theta modif = "+number);
						// copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),0,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),0,posObj,posForm));
						// break;
					// case 5:
						// System.out.println("Psi Transformation Object");
						// arrayMinMax = ((NodeObject)parameter.getNode()).getPsiTransfo();
						// copy_form.setPsiInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// System.out.println("Psi modif = "+number);
						// copy_form.setObjectChange(this.tree.computeObjectRotation_vObject((arrayMinMax[0]+number),0,posObj,posForm),this.tree.computeObjectRotation_typeT_vObject3D((arrayMinMax[0]+number),0,posObj,posForm));

						// break;
					// case 6:
						// System.out.println("Dimension X transformation Object Pas Implemente");
						// break;
					// case 7:
						// System.out.println("Dimension Y transformation Object Pas Implemente");
						// break;
					// case 8:
						// System.out.println("Dimension Z transformation Object Pas Implemente");
						// break;
					// case 10:
						// System.out.println("X origine Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getXoriTransfo();
						// copy_form.setOriXInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToXori(number);
						// break;
					// case 11:
						// System.out.println("Y origine Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getYoriTransfo();
						// copy_form.setOriYInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToYori(number);
						// break;
					// case 12:
						// System.out.println("Z origine Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getZoriTransfo();
						// copy_form.setOriZInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToZori(number);
						// break;
					// case 13:
						// System.out.println("Phi Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getPhiTransfo();
						// copy_form.setPhiInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToPhi(number);
						// break;
					// case 14:
						// System.out.println("Theta Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getThetaTransfo();
						// copy_form.setThetaInitial(arrayMinMax[0]);
						// number = (int)(((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToTheta(number);
						// break;
					// case 15:
						// System.out.println("Psi Transformation Form");
						// arrayMinMax = ((NodeForm)parameter.getNode()).getPsiTransfo();
						// copy_form.setPsiInitial(arrayMinMax[0]);
						// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// copy_form.addToPsi(number);
						// break;
					// // case 16:
						// // System.out.println("Dimension X transformation Form");
						// // arrayMinMax = ((NodeForm)parameter.getNode()).getDimXTransfo();
						// // copy_form.setDimXInitial(arrayMinMax[0]);
						// // number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// // copy_form.addToXdim(number);
						// // break;
					// // case 17:
						// // System.out.println("Dimension Y transformation Form");
						// // arrayMinMax = ((NodeForm)parameter.getNode()).getDimYTransfo();
						// // copy_form.setDimYInitial(arrayMinMax[0]);
						// // number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// // copy_form.addToYdim(number);
						// // break;
					// // case 18:
						// // System.out.println("Dimension Z transformation Form");
						// // arrayMinMax = ((NodeForm)parameter.getNode()).getDimZTransfo();
						// // copy_form.setDimZInitial(arrayMinMax[0]);
						// // number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
						// // copy_form.addToZdim(number);
						// // break;
					// default:
						// System.out.println("UnIdentified Modification :: Object3D.java L.151");
						// break;
				// }
			// }
			// //Case Dimension form
			// else {
				// arrayMinMax = ((NodeForm)parameter.getNode()).getDimensionTransfo()[parameter.getIdParam() - 100];
				// copy_form.setDimensionInit(arrayMinMax[0],parameter.getIdParam() - 100);
				// number = (int)((float)((float)(arrayMinMax[1] - arrayMinMax[0])/(float)(parameter.getNumberOfTransformation() + 1)) * posValue);
				// copy_form.addToDim(number,parameter.getIdParam() - 100);
			// }
		// }
		// return copy_form;
	// }
	
	

	
	
	/**
	* Set the Dimension of the Stack to be Draw.
	* @param dimStack, int[] {xDim,yDim,zDim}
	**/
	public void setDimStack(int[] dimStack){
		this.dimStack = dimStack;
	}

	/**
	* Set the noise Standard Deviation Value
	**/
	public void setNoiseSD(double sd){
		 this.noiseSD = sd ;
	}
	
	
	/**
	* Save the phantom corresponding to the currents TreeDragAndDrop in a new file obj.
	* @param filename, String the filename of the obj file to be creat
	**/
	public void saveFileObj(String filename, ArrayList<NodeObject> objNode){
		try{
			FileWriter fw = new FileWriter(filename, false);
			BufferedWriter output = new BufferedWriter(fw);

			int i = 0;
			Iterator<ArrayList<CreateForm>> objects = phantom.iterator();

			//Phantom info
			String phanString = "";
			phanString += DataID.PHANTOM.getName()+"\tPhantom\n"+DataID.BACKGROUND_VALUE.getName()+"\t"+(int)this.Cint.getBackgroundValue()+"\n";
			phanString += DataID.PHANTOM_DIMENSION.getName()+"\t"+this.dimStack[0]+"\t"+this.dimStack[1]+"\t"+this.dimStack[2]+"\n";
			output.write(phanString);
			//for every objects in the phantom
			while (objects.hasNext()) {
				String objString = DataID.OBJECT.getName()+"\tObject "+i+"\n";
				objString += DataID.OBJECT_PIXEL_VALUE.getName()+"\t"+objNode.get(i).getPixelValue()+"\n";
				objString += DataID.OBJECT_LAYER.getName()+"\t"+objNode.get(i).getLayer()+"\n";
				output.write(objString);
				ArrayList<CreateForm> object = objects.next();
				Iterator<CreateForm> forms = object.iterator();
				//for every form constituing this object
				while(forms.hasNext()){
					CreateForm form = forms.next();		
					String formString = form.toString();
					output.write(formString);
				}
				i += 1;
			}
	
			output.flush();
			output.close();
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
	}		


	/**
	* Open an obj file, and extract from it a new ArrayList<ArrayList<NodeForm>> to be add in the TreeDragAndDrop.
	* @param filename, String the filename of the file to open.
	* @return phantom_load, ArrayList<ArrayList<NodeForm>> the new List of Node to implement in the TreeDragAndDrop.
	**/
	public ArrayList<ArrayList<NodeForm>> openFileObj(String filename){
		Chrono c = new Chrono();
		c.start();
		
		NodeForm node = null;
		String line ="";
		String name ="";
		int [] ori = {0,0,0};
		int [] dim = {0,0,0};
		int [] angle= {0,0,0};
		float pixelVal =0;
		float angleFlex = 0;
		// int i = 0;
		ArrayList<ArrayList<NodeForm>> phantom_load = new ArrayList<ArrayList<NodeForm>>();
		ArrayList<NodeForm> obj = new ArrayList<NodeForm>();
		int fullLenght=0;
		try{
			FileReader fr = new FileReader(filename);
			BufferedReader input = new BufferedReader(fr);
			//While there is line in this file
			while((line = input.readLine()) != null){
                String [] lineTable = line.split("\\s+");

                DataID current = DataID.getType(lineTable[0]);

                switch (current){
                    case OBJECT:
                        //premiere initialisation
                        if(obj.size() != 0){
                            phantom_load.add(obj);
                            obj = new ArrayList<NodeForm>();
                            fullLenght = 0;
                        }
                        break;
                    case FORM:
                        name = lineTable[1];
                        fullLenght +=1;
                        break;
                    case ORIGIN:
                        ori[0] = Integer.parseInt(lineTable[1]);
                        ori[1] = Integer.parseInt(lineTable[2]);
                        ori[2] = Integer.parseInt(lineTable[3]);
                        fullLenght +=1;
                        break;
                    case DIMENSION:
                        dim = new int[lineTable.length - 1];
                        for ( int i = 0 ; i < dim.length ; i++ ){
                            dim[i] = Integer.parseInt(lineTable[i+1]);
                        }
                        fullLenght +=1;
                        break;
                    case ANGLE:
                        angle[0] = Integer.parseInt(lineTable[1]);
                        angle[1] = Integer.parseInt(lineTable[2]);
                        angle[2] = Integer.parseInt(lineTable[3]);
                        fullLenght +=1 ;
                        break;
                    case PIXEL_VALUE:
                        pixelVal = Float.parseFloat(lineTable[1]);
                        fullLenght += 1;
                        break;
                }

				//0 = box , 1 = cone , 2 = pyramide , 3 = sphere , 4 = tube , 5 = ellipse , 6 =fiber
				// The lenght of a form is 5 parameters.

				if(fullLenght == 5){
					fullLenght = 0;
					String name_T = name.toLowerCase();
					for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
						String nameComp = (StaticField_Form.FORM_NAME[i]).toLowerCase();
						if ( name_T.compareTo(nameComp)==0 ){
							node = new NodeForm(name,dim,ori[0],ori[1],ori[2],angle[0],angle[1],angle[2],i);
						}
					}
					obj.add(node);		
				}
			}
			//add the last Object
			if ( obj.size() != 0 ){
				phantom_load.add(obj);
			}
			
		}
		catch(IOException ieo){
			System.out.print("Erreur : ");
			ieo.printStackTrace();
		}
		
		c.stop();
		
		System.out.println("Time passer dans le chargement ::"+c.delayString());
		
		return phantom_load;
	}
	
	/**
	* Extract an ArrayList of String containing the Object and Phantom information
	* @param filename, the file Path.
	**/
	public ArrayList<String> openFile(String filename){
		String line ="";
		String str ="";
		ArrayList<String> info_load = new ArrayList<String>();
		int i=0;
		
		try{
			
			FileReader fr = new FileReader(filename);
			BufferedReader input = new BufferedReader(fr,1000000);
			
			while((line = input.readLine()) != null){
				String [] lineTable = line.split("\t");
				if( lineTable[0].compareTo("P")==0 ){
					//TODO rajout des parametres relatif au phantom
				}
				else if( lineTable[0].compareTo("BV")==0 ){
					//add in info_load
					info_load.add(lineTable[1]);
				}
				else if ( lineTable[0].compareTo("DS")==0 ){
					str = "";
					str += lineTable[1];
					str += "\t"+lineTable[2];
					str += "\t"+lineTable[3];
					info_load.add(str);
				}
				else if ( lineTable[0].compareTo("OPV")==0 ){
					info_load.add(lineTable[1]);
				}
				else if ( lineTable[0].compareTo("LAY")==0 ){
					info_load.add(lineTable[1]);
				}
			}
		}
		catch(IOException ieo){
			System.out.print("Erreur : ");
			ieo.printStackTrace();
		}
		return info_load;
	}
	
	public void setNumberOfPhantom(int number){
		this.numberOfPhantom = number ;
	}
	
	public void setPhantom(ArrayList<ArrayList<CreateForm>> phantom){
		this.phantom = phantom;
	}
	
	
	/**
	* draw in the stack preleminarie from the creation of he randomGenerator Algorithm (drawWithoutCollision).
	* @param phantom, the phantom to draw.
	* @param dimStack, the stack dimension
	**/
	public void setPhantom_toString(ArrayList<ArrayList<CreateForm>> phantom,int[] dimStack){
	
		ArrayList<ArrayList<CreateForm>> phantom_ = new ArrayList<ArrayList<CreateForm>>();
		phantom_ = phantom;
		
		setSizePrePhantom(phantom_.get(0).size());
		this.isStackUp = true;
		
		Iterator<ArrayList<CreateForm>> objects = phantom_.iterator();
		CreateForm.creatStack(dimStack[0], dimStack[1], dimStack[2], 0);
		
		//for every objects in the phantom
		while (objects.hasNext()) {
			ArrayList<CreateForm> object = objects.next();
			Iterator<CreateForm> forms = object.iterator();

			//for every form constituing this object
			while(forms.hasNext()){
				CreateForm form = forms.next();
				form.setLayerObject(idObj);				
				form.init_parameters();
			}
			idObj+=10;
		}

	}
	
	public void setComputeIntensity(Compute_Intensity Cint){
		this.Cint = Cint;
	}
	
	public void setThreadOption(Thread_option Toption){
		this.Toption = Toption;
	}
	
	/**
	* Return information about the Drawing progress status
	**/
	public void ask_update_loading(LoadingBar bar){
		bar.updateThread(this.loadingX,this.loadingT);
	}
	
	/**
	* @param saveType, int 0->Tiff
	**/
	public void saveImage(String path,int saveType){
		phantom.get(0).get(0).save(path+"RandomStack.tif",0);
	}
	
	public void setIsStructure(boolean bol){
		this._isStructure = bol;
	}
	
	public static void resetIdObj(){
		idObj = 10;
	}
	
	public void setSizePrePhantom(int size){
		this.sizePrephantom = size;
	}
}
