/**
 * NodeForm.java
 * 07/03/2012
 * @author Antoine Bergamaschi
 **/

package com.phantomaj.tree;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;

import com.phantomaj.form.CreateForm;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.form.CreateForm;
import com.phantomaj.utils.StaticField_Form;

@SuppressWarnings("serial")
public class NodeForm extends DefaultMutableTreeNode {

	public final static int ROTATION_PHI = 10;
	public final static int ROTATION_PSI = 11;
	public final static int ROTATION_THETA = 12;
	public final static int TRANSLATION_X = 13;
	public final static int TRANSLATION_Y = 14;
	public final static int TRANSLATION_Z = 15;
	public final static int DIM = 100;

	private int[] origine = new int[3];
	private int[] dimension = new int[3];
	private float pixelValue;
	private int form;
	private String name;
	private int[] angle = new int[3];

	private int[][] dimensionTransfo = null;
	private int[] phiTransfo = null;
	private int[] thetaTransfo = null;
	private int[] psiTransfo = null;
	private int[] XoriTransfo = null;
	private int[] YoriTransfo = null;
	private int[] ZoriTransfo = null;
	private boolean isXoriSelected = false;
	private boolean isYoriSelected = false;
	private boolean isZoriSelected = false;
	private boolean isPhiTransfoSelected = false;
	private boolean isThetaTransfoSelected = false;
	private boolean isPsiTransfoSelected = false;
	private boolean[] isSelectedParam = null;
	private int selectedCenter = 0;

	private float angleFlex = 0;

	// ############################# CONSTRUCTOR
	// ####################################################

	public NodeForm(NodeForm node) {
		// System.out.println("Copy "+node.getUserObject() );
		setUserObject(node.getUserObject());
		copy(node);

		this.isSelectedParam = node.isSelectedParam();

		this.isPhiTransfoSelected = node.getisPhiTransfoSelected();
		this.isThetaTransfoSelected = node.getisThetaTransfoSelected();
		this.isPsiTransfoSelected = node.getisPsiTransfoSelected();

		this.isXoriSelected = node.getisXoriSelected();
		this.isYoriSelected = node.getisYoriSelected();
		this.isZoriSelected = node.getisZoriSelected();

		this.dimensionTransfo = node.getDimensionTransfo();

		this.XoriTransfo = node.getXoriTransfo();
		this.YoriTransfo = node.getYoriTransfo();
		this.ZoriTransfo = node.getZoriTransfo();
		this.phiTransfo = node.getPhiTransfo();
		this.thetaTransfo = node.getThetaTransfo();
		this.psiTransfo = node.getPsiTransfo();

		this.dimensionTransfo = node.getDimensionTransfo();
	}

	public NodeForm(String name, int xdim, int ydim, int zdim, int xori,
			int yori, int zori, int phi, int theta, int psi, int form) {
		super(name);
		this.name = name;
		int[] ori = { xori, yori, zori };
		this.origine = ori;

		this.isSelectedParam = new boolean[StaticField_Form.FORMDIMENSION_NAME[form].length];
		this.dimensionTransfo = new int[StaticField_Form.FORMDIMENSION_NAME[form].length][3];

		if (StaticField_Form.CODE_ADDITIONAL_FORM[form] != null) {
			this.dimension = new int[3 + StaticField_Form.CODE_ADDITIONAL_FORM[form].length];

			this.dimension[0] = xdim;
			this.dimension[1] = ydim;
			this.dimension[2] = zdim;
		} else {
			// this.dimensionTransfo = new int[3][3];
			int[] dim = { xdim, ydim, zdim };
			this.dimension = dim;
		}
		int[] angle_i = { phi, theta, psi };
		this.angle = angle_i;
		this.form = form;

		for (int i = 0; i < StaticField_Form.FORMDIMENSION_NAME[form].length; i++) {
			this.isSelectedParam[i] = false;
		}
	}

	public NodeForm(String name, int[] dim, int xori, int yori, int zori,
			int phi, int theta, int psi, int form) {
		super(name);
		this.name = name;
		int[] ori = { xori, yori, zori };
		this.origine = ori;

		this.isSelectedParam = new boolean[StaticField_Form.FORMDIMENSION_NAME[form].length];
		this.dimensionTransfo = new int[StaticField_Form.FORMDIMENSION_NAME[form].length][3];

		this.dimension = new int[dim.length];
		System.arraycopy(dim, 0, this.dimension, 0, this.dimension.length);

		int[] angle_i = { phi, theta, psi };
		this.angle = angle_i;
		this.form = form;

		for (int i = 0; i < StaticField_Form.FORMDIMENSION_NAME[form].length; i++) {
			this.isSelectedParam[i] = false;
		}
	}

	// ############################### METHOD
	// #################################################

	/**
	 * get the CreateForm object constituing of the parameters of this NodeForm.
	 * 
	 * @return obj, CreateForm an AbstractForm who could be draw
	 **/
	public CreateForm getCreatForm(float pixelValue, int layer) {
		CreateForm obj = StaticField_Form.getCreatForm(this.form, pixelValue,
				layer, this.getCenterRotation(), this.dimension, this.origine,
				this.angle);
		return obj;
	}

	public void copy(NodeForm node) {
		this.dimension = new int[node.getDimension().length];
		this.origine = new int[3];
		this.angle = new int[3];

		System.arraycopy(node.getOrigine(), 0, this.origine, 0, 3);
		System.arraycopy(node.getDimension(), 0, this.dimension, 0,
				node.getDimension().length);
		// this.pixelValue = node.getPixelValue();
		this.form = node.getForm();
		System.arraycopy(node.getAngle(), 0, this.angle, 0, 3);
		this.name = node.getName();
		this.angleFlex = node.getAngleFlex();
	}

	public int[] getOrigine() {
		return this.origine;
	}

	public void setOriX(int newOri) {
		this.origine[0] = newOri;
	}

	public void setOriY(int newOri) {
		this.origine[1] = newOri;
	}

	public void setOriZ(int newOri) {
		this.origine[2] = newOri;
	}

	public void setPhi(int newPhi) {
		this.angle[0] = newPhi;
	}

	public void setTheta(int newTheta) {
		this.angle[1] = newTheta;
	}

	public void setPsi(int newPsi) {
		this.angle[2] = newPsi;
	}

	public void setDim(int val, int ID) {
		this.dimension[ID] = val;
	}

	public int[] getDimension() {
		return this.dimension;
	}

	public boolean[] isSelectedParam() {
		return this.isSelectedParam;
	}

	public void setIsSelectedParam(boolean[] bol) {
		this.isSelectedParam = new boolean[bol.length];
		System.arraycopy(bol, 0, this.isSelectedParam, 0,
				this.isSelectedParam.length);
	}

	public void setOri(int val, int ID) {
		this.origine[ID] = val;
	}

	public void setAngle(int val, int ID) {
		this.angle[ID] = val;
	}

	public void setisDimSelected(boolean bol, int ID) {
		this.isSelectedParam[ID] = bol;
	}

	public boolean getisDimSelected(int ID) {
		return this.isSelectedParam[ID];
	}

	public int getForm() {
		return this.form;
	}

	public void setForm(int nf) {
		this.form = nf;
		this.name = StaticField_Form.FORM_NAME[this.form];

		// initialization
		this.isSelectedParam = new boolean[StaticField_Form.FORMDIMENSION_NAME[form].length];
		this.dimensionTransfo = new int[StaticField_Form.FORMDIMENSION_NAME[form].length][3];

		// IF the dimension parameters is greater than 3
		if (StaticField_Form.CODE_ADDITIONAL_FORM[nf] != null) {
			int[] oldDimension = new int[3];
			System.arraycopy(this.dimension, 0, oldDimension, 0, 3);
			this.dimension = new int[3 + StaticField_Form.CODE_ADDITIONAL_FORM[nf].length];
			System.arraycopy(oldDimension, 0, this.dimension, 0, 3);
		} else {
			int[] oldDimension = new int[3];
			System.arraycopy(this.dimension, 0, oldDimension, 0, 3);
			this.dimension = new int[3];
			System.arraycopy(oldDimension, 0, this.dimension, 0, 3);
		}

		for (int i = 0; i < StaticField_Form.FORMDIMENSION_NAME[form].length; i++) {
			this.isSelectedParam[i] = false;
		}
	}

	public int[] getAngle() {
		return this.angle;
	}

	public String getName() {
		return this.name;
	}

	public void setAngleFlex(float angle) {
		this.angleFlex = angle;
	}

	public float getAngleFlex() {
		return this.angleFlex;
	}

	public int getSelectedCenter() {
		return this.selectedCenter;
	}

	public int[] getCenterRotation() {
		System.out
				.println("WARNING ::: TO Change ..... tree.NodeForm.getCenterRotation");
		int[] centerR = new int[3];
		switch (this.form) {
		case 0:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			case 1:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = dimension[2] / 2;
				break;
			case 2:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = dimension[2] / 2;
				break;
			case 3:
				centerR[0] = dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = dimension[2] / 2;
				break;
			case 4:
				centerR[0] = dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = dimension[2] / 2;
				break;
			case 5:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 6:
				centerR[0] = dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 7:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 8:
				centerR[0] = dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			}
			break;
		case 1:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			case 1:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = -dimension[2] / 2;
				break;
			case 2:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = dimension[2] / 2;
				break;
			}
			break;
		case 2:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			case 1:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = dimension[2] / 2;
				break;
			case 2:
				centerR[0] = dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 3:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 4:
				centerR[0] = dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			case 5:
				centerR[0] = -dimension[0] / 2;
				centerR[1] = dimension[1] / 2;
				centerR[2] = -dimension[2] / 2;
				break;
			}
			break;
		case 3:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			}
			break;
		case 4:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			case 1:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = dimension[2] / 2;
				break;
			case 2:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = +dimension[2] / 2;
				break;
			}
			break;
		case 5:
			switch (this.selectedCenter) {
			case 0:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = 0;
				break;
			case 1:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = dimension[2] / 2;
				break;
			case 2:
				centerR[0] = 0;
				centerR[1] = -dimension[1] / 2;
				centerR[2] = 0;
				break;
			case 3:
				centerR[0] = 0;
				centerR[1] = 0;
				centerR[2] = -dimension[2] / 2;
				break;
			case 4:
				centerR[0] = 0;
				centerR[1] = dimension[1] / 2;
				centerR[2] = 0;
				break;
			}
			break;
		}
		return centerR;
	}

	public void setSelectedCenter(int centerID) {
		this.selectedCenter = centerID;
	}

	public void setDimTransfo(int ID, int[] val) {
		this.dimensionTransfo[ID] = val;
	}

	public void setOrigineTransfo(int[] XoriTransfo, int[] YoriTransfo,
			int[] ZoriTransfo) {
		if (XoriTransfo != null) {
			this.XoriTransfo = XoriTransfo;
		}
		if (YoriTransfo != null) {
			this.YoriTransfo = YoriTransfo;
		}
		if (ZoriTransfo != null) {
			this.ZoriTransfo = ZoriTransfo;
		}
	}

	public void setAngleTransfo(int[] phiTransfo, int[] thetaTransfo,
			int[] psiTransfo) {
		if (phiTransfo != null) {
			this.phiTransfo = phiTransfo;
		}
		if (thetaTransfo != null) {
			this.thetaTransfo = thetaTransfo;
		}
		if (psiTransfo != null) {
			this.psiTransfo = psiTransfo;
		}
	}

	public void setisXoriSelected(boolean bol) {
		this.isXoriSelected = bol;
	}

	public void setisYoriSelected(boolean bol) {
		this.isYoriSelected = bol;
	}

	public void setisZoriSelected(boolean bol) {
		this.isZoriSelected = bol;
	}

	public void setisPhiTransfoSelected(boolean bol) {
		this.isPhiTransfoSelected = bol;
	}

	public void setisThetaTransfoSelected(boolean bol) {
		this.isThetaTransfoSelected = bol;
	}

	public void setisPsiTransfoSelected(boolean bol) {
		this.isPsiTransfoSelected = bol;
	}

	public int[][] getDimensionTransfo() {
		return this.dimensionTransfo;
	}

	public int[] getXoriTransfo() {
		return this.XoriTransfo;
	}

	public int[] getYoriTransfo() {
		return this.YoriTransfo;
	}

	public int[] getZoriTransfo() {
		return this.ZoriTransfo;
	}

	public int[] getPhiTransfo() {
		return this.phiTransfo;
	}

	public int[] getThetaTransfo() {
		return this.thetaTransfo;
	}

	public int[] getPsiTransfo() {
		return this.psiTransfo;
	}

	public boolean getisXoriSelected() {
		return this.isXoriSelected;
	}

	public boolean getisYoriSelected() {
		return this.isYoriSelected;
	}

	public boolean getisZoriSelected() {
		return this.isZoriSelected;
	}

	public boolean getisPhiTransfoSelected() {
		return this.isPhiTransfoSelected;
	}

	public boolean getisThetaTransfoSelected() {
		return this.isThetaTransfoSelected;
	}

	public boolean getisPsiTransfoSelected() {
		return this.isPsiTransfoSelected;
	}

	public ArrayList<Integer> getID_parameters() {
		ArrayList<Integer> id = new ArrayList<Integer>();
		for (int i = 0; i < this.isSelectedParam.length; i++) {
			if (isSelectedParam[i]) {
				id.add(NodeForm.DIM + i);
			}
		}
		if (this.isXoriSelected) {
			id.add(NodeForm.TRANSLATION_X);
		}
		if (this.isYoriSelected) {
			id.add(NodeForm.TRANSLATION_Y);
		}
		if (this.isZoriSelected) {
			id.add(NodeForm.TRANSLATION_Z);
		}
		if (this.isPhiTransfoSelected) {
			id.add(NodeForm.ROTATION_PHI);
		}
		if (this.isThetaTransfoSelected) {
			id.add(NodeForm.ROTATION_THETA);
		}
		if (this.isPsiTransfoSelected) {
			id.add(NodeForm.ROTATION_PSI);
		}
		return id;
	}

}
