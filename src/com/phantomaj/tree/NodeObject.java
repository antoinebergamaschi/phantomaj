
/**
* NodeObject.java
* 12/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.tree;

import java.util.ArrayList;

import javax.swing.tree.DefaultMutableTreeNode;


@SuppressWarnings("serial")
public class NodeObject extends DefaultMutableTreeNode{

	public final static int TRANSLATION_X = 0;
	public final static int TRANSLATION_Y = 1;
	public final static int TRANSLATION_Z = 2;
	public final static int ROTATION_PHI = 3;
	public final static int ROTATION_THETA = 4;
	public final static int ROTATION_PSI = 5;
	public final static int DIM = 6;


	private String name;
	private float pixelValue;
	private int layer;
	
	private int[] pixelTransfo = null;
	private int[] layerTransfo = null;
	
	private int [] phiTransfo = null;
	private int [] thetaTransfo = null;
	private int [] psiTransfo = null;

	private int [] XoriTransfo = null;
	private int [] YoriTransfo = null;
	private int [] ZoriTransfo = null;
	
	
	private int [] dimXTransfo = null;
	private int [] dimYTransfo = null;
	private int [] dimZTransfo = null;
	
	
	private boolean isXoriSelected = false ;
	private boolean isYoriSelected = false ;
	private boolean isZoriSelected = false ;
	private boolean isPhiTransfoSelected = false ;
	private boolean isThetaTransfoSelected = false ;
	private boolean isPsiTransfoSelected = false ;
	private boolean isDimZSelected = false ;
	private boolean isDimXSelected = false ;
	private boolean isDimYSelected = false ;
	
	
	// ################################################################################
//Construtor	
	
	public NodeObject(String name,float pixelValue,int layer){
		super(name);
		this.name = name;
		this.pixelValue = pixelValue;
		this.layer = layer;
	}

// ########################################################################################

//methode

	public String getName(){
		return this.name;
	}
	
	public float getPixelValue(){
		return this.pixelValue;
	}
	
	public int getLayer(){
		return this.layer;
	}
	
	public void setLayer(int layer){
		this.layer = layer;
	}
	
	public void setPixelValue(float pixelValue){
		this.pixelValue = pixelValue;
	}
	
	public void setDimTransfo(int [] dimXTransfo){
		if ( dimXTransfo != null ){
			this.dimXTransfo = new int[dimXTransfo.length];
			System.arraycopy(dimXTransfo,0,this.dimXTransfo,0,dimXTransfo.length);
		}
	}
	
	public void setOrigineTransfo(int [] XoriTransfo, int[] YoriTransfo , int [] ZoriTransfo){
		if ( XoriTransfo != null ){
			this.XoriTransfo = XoriTransfo;
		}
		if ( YoriTransfo != null ){
			this.YoriTransfo = YoriTransfo;
		}
		if ( ZoriTransfo != null ){
			this.ZoriTransfo = ZoriTransfo;
		}
	}
	
	public void setAngleTransfo(int [] phiTransfo , int [] thetaTransfo , int [] psiTransfo){
		if ( phiTransfo != null ){
			this.phiTransfo = phiTransfo;
		}
		if ( thetaTransfo != null ){
			this.thetaTransfo = thetaTransfo;
		}
		if ( psiTransfo != null ){
			this.psiTransfo = psiTransfo;
		}
	}
	
	public void setisXoriSelected(boolean bol){
		this.isXoriSelected = bol ;
	}
	
	public void setisYoriSelected(boolean bol){
		this.isYoriSelected = bol ;
	}

	public void setisZoriSelected(boolean bol){
		this.isZoriSelected = bol ;
	}
	

	public void setisDimXSelected(boolean bol){
		this.isDimXSelected = bol ;
	}	

	public void setisDimYSelected(boolean bol){
		this.isDimYSelected = bol ;
	}	
	
	public void setisDimZSelected(boolean bol){
		this.isDimZSelected = bol ;
	}	
	
	public void setisPhiTransfoSelected(boolean bol){
		this.isPhiTransfoSelected = bol ;
	}
	
	public void setisThetaTransfoSelected(boolean bol){
		this.isThetaTransfoSelected = bol ;
	}
	
	public void setisPsiTransfoSelected(boolean bol){
		this.isPsiTransfoSelected = bol ;
	}
	
	public int [] getDimXTransfo(){
		return this.dimXTransfo;
	}
	
	public int [] getDimYTransfo(){
		return this.dimYTransfo;
	}
	
	public int [] getDimZTransfo(){
		return this.dimZTransfo;
	}
	
	public int [] getXoriTransfo(){
		return this.XoriTransfo;
	}
	
	public int [] getYoriTransfo(){
		return this.YoriTransfo;
	}
	
	public int [] getZoriTransfo(){
		return this.ZoriTransfo;
	}
	
	public int [] getPhiTransfo(){
		return this.phiTransfo;
	}
	
	public int [] getThetaTransfo(){
		return this.thetaTransfo;
	}
	
	public int [] getPsiTransfo(){
		return this.psiTransfo;
	}
	
	public boolean getisXoriSelected(){
		return this.isXoriSelected;
	}
	
	public boolean getisYoriSelected(){
		return this.isYoriSelected;
	}

	public boolean getisZoriSelected(){
		return this.isZoriSelected;
	}
	

	public boolean getisDimXSelected(){
		return this.isDimXSelected;
	}	

	public boolean getisDimYSelected(){
		return this.isDimYSelected;
	}	
	
	public boolean getisDimZSelected(){
		return this.isDimZSelected;
	}	
	
	public boolean getisPhiTransfoSelected(){
		return this.isPhiTransfoSelected;
	}
	
	public boolean getisThetaTransfoSelected(){
		return this.isThetaTransfoSelected;
	}
	
	public boolean getisPsiTransfoSelected(){
		return this.isPsiTransfoSelected;
	}
	
	
	public ArrayList<Integer> getID_parameters(){
		ArrayList<Integer> id = new ArrayList<Integer>();
		if ( this.isDimXSelected ){
			id.add(NodeObject.DIM);
		}
		if( this.isXoriSelected ){
			id.add(NodeObject.TRANSLATION_X);
		}
		if ( this.isYoriSelected ){
			id.add(NodeObject.TRANSLATION_Y);
		}
		if ( this.isZoriSelected ){
			id.add(NodeObject.TRANSLATION_Z);
		}
		if ( this.isPhiTransfoSelected ){
			id.add(NodeObject.ROTATION_PHI);
		}
		if ( this.isThetaTransfoSelected ){
			id.add(NodeObject.ROTATION_THETA);
		}
		if ( this.isPsiTransfoSelected ){
			id.add(NodeObject.ROTATION_PSI);
		}
		return id;
	}

}