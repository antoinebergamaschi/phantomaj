//Code issu de http://www.coderanch.com/t/346509/GUI/java/JTree-drag-drop-inside-one
//Craig Wood 

package com.phantomaj.tree;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.form.CreateForm;
import com.phantomaj.obj3D.Parameters;
import com.phantomaj.utils.Chrono;

public class TreeDragAndDrop implements TreeSelectionListener, MouseListener,
		TreeModelListener {
	private boolean lock_setTree = true;

	private JTree tree;
	private DefaultMutableTreeNode root;
	private PhantoMaJ principal_window;
	private JScrollPane scroll;

	// debug time
	private long time = 0;

	public TreeDragAndDrop(PhantoMaJ principal_window) {
		this.principal_window = principal_window;
		root = new DefaultMutableTreeNode("Phantom");
		tree = new JTree(root);
		tree.addTreeSelectionListener(this);
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		treeModel.addTreeModelListener(this);
		tree.setUI(new CustomTreeUI());
		tree.setLargeModel(true);
		tree.addMouseListener(this);
	}

	public JScrollPane getContent() {
		tree.setDragEnabled(true);
		tree.setDropMode(DropMode.ON_OR_INSERT);
		tree.setTransferHandler(new TreeTransferHandler());
		tree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.CONTIGUOUS_TREE_SELECTION);
		expandTree(tree);
		this.scroll = new JScrollPane(tree,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.scroll.setMinimumSize(new Dimension(300, 500));
		return this.scroll;
		// return new JScrollPane(tree);
	}

	private void expandTree(JTree tree) {
		if (lock_setTree) {

			DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree
					.getModel().getRoot();
			Enumeration e = root.breadthFirstEnumeration();
			while (e.hasMoreElements()) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) e
						.nextElement();
				if (node.isLeaf())
					continue;
				int row = tree.getRowForPath(new TreePath(node.getPath()));
				tree.expandRow(row);
			}
		}
	}

	public void expandTree_() {
		expandTree(this.tree);
	}

	private boolean TreeAsLeaf(JTree tree) {
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel()
				.getRoot();
		Enumeration e = root.breadthFirstEnumeration();
		while (e.hasMoreElements()) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e
					.nextElement();
			if (node.isLeaf() && node instanceof NodeForm) {
				return true;
			}
		}
		return false;
	}

	public JTree getTree() {
		return tree;
	}

	/**
	 * Delete every node of this tree
	 **/
	public void clearTree() {
		this.root = new DefaultMutableTreeNode("Phantom");
		DefaultTreeModel treeModel = new DefaultTreeModel(this.root);
		tree.setModel(treeModel);
		treeModel.addTreeModelListener(this);
		// fire change to the view3D
		this.principal_window.update3DviewClear();
		// Change the model so no fire to change the tab propriety
		principal_window.setTabbedPaneVisible(false, 1);
		tree.setSelectionPath(new TreePath(this.root.getPath()));
	}

	public void resetSelectionPath() {
		tree.setSelectionPath(new TreePath(this.root.getPath()));
	}

	/**
	 * Call the loading of the 3DView
	 **/
	public void reload3DView() {
		for (int posObj = 0; posObj < this.root.getChildCount(); posObj++) {
			DefaultMutableTreeNode objNode = (DefaultMutableTreeNode) root
					.getChildAt(posObj);
			for (int posForm = 0; posForm < objNode.getChildCount(); posForm++) {
				NodeForm node = (NodeForm) objNode.getChildAt(posForm);
				this.principal_window.update3Dview(node.getAngle(),
						node.getOrigine(), node.getDimension(), node.getForm(),
						posObj, posForm);
			}
		}
	}

	/**
	 * Add an Object in this Jtree to the root.
	 **/
	public void add_Object(float pixelValue, int layer, boolean toSel) {
		Chrono c = new Chrono();
		c.start();

		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		NodeObject newNode = new NodeObject("Object "
				+ (root.getChildCount() + 1), pixelValue, layer);
		// DefaultMutableTreeNode newNode = new
		// DefaultMutableTreeNode("Object "+(root.getChildCount()+1));
		treeModel.insertNodeInto(newNode, root, root.getChildCount());

		if (toSel) {
			tree.setSelectionPath(new TreePath(newNode.getPath()));
		}
		// expandTree(tree);

		c.stop();
		time += c.delay();

		// Update 3Dview
		this.principal_window.update3DviewAddObject();
	}

	/**
	 * Change le nom des objects pour qu'il soit toujours dans le bonne ordre
	 **/
	public void renameAll() {
		ArrayList<String> array = new ArrayList<String>();
		for (int i = 0; i < this.root.getChildCount(); i++) {
			DefaultMutableTreeNode objNode = (DefaultMutableTreeNode) root
					.getChildAt(i);
			objNode.setUserObject("Object" + (i + 1));
			for (int z = 0; z < objNode.getChildCount(); z++) {
				NodeForm NodeForm = (NodeForm) objNode.getChildAt(z);
				NodeForm.setUserObject((String) (NodeForm.getName() + (z + 1)));
			}
		}
		if (root.getChildCount() == 0) {
			// principal_window.setButtonAddEnable(false);
			principal_window.setButtonDelEnable(false);
		}
		tree.repaint();
	}

	public ArrayList<String> getObject() {
		ArrayList<String> array = new ArrayList<String>();
		for (int i = 0; i < this.root.getChildCount(); i++) {
			String name = (String) ((DefaultMutableTreeNode) root.getChildAt(i))
					.getUserObject();
			array.add(name);
		}
		return array;
	}

	public ArrayList<NodeObject> getNodeObject() {
		ArrayList<NodeObject> array = new ArrayList<NodeObject>();
		for (int i = 0; i < this.root.getChildCount(); i++) {
			NodeObject obj = (NodeObject) root.getChildAt(i);
			array.add(obj);
		}
		return array;

	}

	/**
	 * Return the position in the tree of the selected node else return -1
	 **/
	public int getPosObj() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeObject) {
			return selectNode.getParent().getIndex(selectNode);
		}
		return -1;
	}

	/**
	 * Return the type ID of the currently Selected Node 0 == form, 1==object ,
	 * 2==root
	 **/
	public int getWhichType() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		int type = -1;
		if (selectNode instanceof NodeObject) {
			type = 1;
		} else if (selectNode instanceof NodeForm) {
			type = 0;
		} else if (selectNode == this.root) {
			type = 2;
		}
		return type;
	}

	/**
	 * Get the ArrayList of String containing the name of all Form at the object
	 * index obj
	 **/
	public ArrayList<String> getForm(int obj) {
		ArrayList<String> array = new ArrayList<String>();
		if (obj == -1) {
			DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();
			if (selectNode instanceof NodeObject) {
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					String name = (String) ((DefaultMutableTreeNode) selectNode
							.getChildAt(i)).getUserObject();
					array.add(name);
				}
			}
		} else {
			DefaultMutableTreeNode objNode = (DefaultMutableTreeNode) this.root
					.getChildAt(obj);
			for (int i = 0; i < objNode.getChildCount(); i++) {
				String name = (String) ((DefaultMutableTreeNode) objNode
						.getChildAt(i)).getUserObject();
				array.add(name);
			}
		}
		return array;
	}

	/**
	 * get the specified node in the tree, if there is not return null if obj ==
	 * -1 and form != -1 , get the NodeForm in the selected NodeObject else
	 * return null if obj == -1 and form ==-1 , get the selected NodeObject else
	 * return null if form == -1, get the selected NodeForm else return null
	 **/
	public DefaultMutableTreeNode getNode(int obj, int form) {
		if (obj == -1) {
			DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();
			if (form == -1) {
				if (selectNode instanceof NodeObject) {
					return selectNode;
				} else {
					return null;
				}
			} else {
				return (DefaultMutableTreeNode) selectNode.getChildAt(form);
			}
		} else if (form == -1) {
			DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();
			if (selectNode instanceof NodeForm) {
				return selectNode;
			} else {
				return null;
			}
		} else {
			return (DefaultMutableTreeNode) this.root.getChildAt(obj)
					.getChildAt(form);
		}
	}

	/**
	 * Delete the specified object
	 * 
	 * @param object
	 *            int the index of this object
	 **/
	public void del_Object(int object) {
		DefaultMutableTreeNode nodeDel = (DefaultMutableTreeNode) root
				.getChildAt(object);
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		treeModel.removeNodeFromParent(nodeDel);

		// Update the view3D
		this.principal_window.update3DviewRemoveObj(object);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		// System.out.println("Del obj :"+node);
		if (node == null) {
			// System.out.println("path is null");
			try {
				tree.setSelectionPath(new TreePath(
						((DefaultMutableTreeNode) (root.getChildAt(root
								.getChildCount() - 1))).getPath()));
			} catch (ArrayIndexOutOfBoundsException e) {
				tree.setSelectionPath(new TreePath(
						((DefaultMutableTreeNode) this.root).getPath()));
			}
		}
		renameAll();
	}

	/**
	 * Delete the specified form in the specified object
	 * 
	 * @param object
	 *            int the index of this object
	 * @param form
	 *            int the index of this form in this object
	 **/
	public void del_form(int object, int form) {
		DefaultMutableTreeNode nodeDel = (DefaultMutableTreeNode) this.root
				.getChildAt(object);
		nodeDel = (DefaultMutableTreeNode) nodeDel.getChildAt(form);
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		treeModel.removeNodeFromParent(nodeDel);

		// Update the view3D
		this.principal_window.update3DviewRemoveForm(object, form);

		renameAll();
	}

	public void del_node(DefaultMutableTreeNode node) {
		if (node instanceof NodeForm) {
			this.principal_window.update3DviewRemoveForm(this.root
					.getIndex(node.getParent()), node.getParent()
					.getIndex(node));
		} else if (node instanceof NodeObject) {
			this.principal_window.update3DviewRemoveObj(this.root
					.getIndex(node));
		}
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		treeModel.removeNodeFromParent(node);
	}

	public ArrayList<ArrayList<CreateForm>> getPhantom() {
		ArrayList<ArrayList<CreateForm>> phantom = new ArrayList<ArrayList<CreateForm>>();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) this.tree
				.getModel().getRoot();
		Enumeration e = root.depthFirstEnumeration();
		ArrayList<CreateForm> object = new ArrayList<CreateForm>();

		while (e.hasMoreElements()) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e
					.nextElement();
			// System.out.println("Node index :"+node.getParent());
			if (node.isLeaf() && node.getParent() instanceof NodeObject) {
				float pixelValue = ((NodeObject) node.getParent())
						.getPixelValue();
				int layer = ((NodeObject) node.getParent()).getLayer();
				if (object.size() == 0) {
					object = new ArrayList<CreateForm>();
					object.add(((NodeForm) node)
							.getCreatForm(pixelValue, layer));
				} else {
					object.add(((NodeForm) node)
							.getCreatForm(pixelValue, layer));
				}
			} else if (node.getParent() != null) {
				if (object.size() != 0) {
					phantom.add(object);
				}
				object = new ArrayList<CreateForm>();
			}
		}
		if (object.size() != 0) {
			phantom.add(object);
		}
		return phantom;
	}

	/**
	 * add a new node corresponding to a form under the selected Object.
	 * 
	 * @param node
	 *            NodeForm the node(leaf) to add in the selected object(node).
	 * @param obj
	 *            int the Index of the node where to add the new node.
	 **/
	public void add_form(NodeForm node, int obj) {
		// si -1
		Chrono c = new Chrono();
		c.start();

		if (obj < 0) {
			// if there is Object in the Tree
			if (this.root.getChildCount() > 0) {
				DefaultMutableTreeNode parent = (DefaultMutableTreeNode) tree
						.getLastSelectedPathComponent();
				// if the selected node is a NodeObject
				if (parent instanceof NodeObject) {
					DefaultTreeModel treeModel = (DefaultTreeModel) tree
							.getModel();
					treeModel.insertNodeInto(node, parent,
							parent.getChildCount());
					node.setUserObject((String) (node.getName() + parent
							.getChildCount()));
					// expandTree(tree);
				}
				// if the selected node is a NodeForm
				else if (parent instanceof NodeForm) {
					DefaultTreeModel treeModel = (DefaultTreeModel) tree
							.getModel();
					treeModel.insertNodeInto(node,
							(DefaultMutableTreeNode) parent.getParent(),
							((DefaultMutableTreeNode) parent.getParent())
									.getChildCount());
					node.setUserObject((String) (node.getName() + ((DefaultMutableTreeNode) parent
							.getParent()).getChildCount()));
					// expandTree(tree);
				}
				// If parent is null
				else {
					DefaultTreeModel treeModel = (DefaultTreeModel) tree
							.getModel();
					parent = (DefaultMutableTreeNode) this.root
							.getChildAt(this.root.getChildCount() - 1);
					treeModel.insertNodeInto(node, parent,
							parent.getChildCount());
					node.setUserObject((String) (node.getName() + parent
							.getChildCount()));
					// expandTree(tree);
				}

				c.stop();
				time += c.delay();

				// Update the 3Dview
				this.principal_window.update3DviewAddForm(
						((NodeForm) node).getAngle(),
						((NodeForm) node).getOrigine(),
						((NodeForm) node).getDimension(),
						((NodeForm) node).getForm(),
						this.root.getIndex(node.getParent()));

				// Select the last NodeObject
				if (lock_setTree) {
					tree.setSelectionPath(new TreePath(node.getPath()));
				}

			}
			// if the user try to creat a NodeForm before creat a new Object
			else {
				this.add_Object(100, 0, false);
				this.add_form(node, -1);
			}
		} else {
			DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
			DefaultMutableTreeNode nodeSel = (DefaultMutableTreeNode) root
					.getChildAt(obj);
			treeModel.insertNodeInto(node, nodeSel, nodeSel.getChildCount());
			node.setUserObject((String) (node.getName() + nodeSel
					.getChildCount()));
			// expandTree(tree);

			c.stop();
			time += c.delay();

			// Update the 3Dview
			this.principal_window.update3DviewAddForm(
					((NodeForm) node).getAngle(),
					((NodeForm) node).getOrigine(),
					((NodeForm) node).getDimension(),
					((NodeForm) node).getForm(),
					this.root.getIndex(node.getParent()));

			// Select the last NodeObject
			if (lock_setTree) {
				tree.setSelectionPath(new TreePath(node.getPath()));
			}
		}
	}

	// Debug Time
	public void printDelay() {
		System.out
				.println("Temps Pass� pour l'ajout de node Form/Objet L410 TreeDragAndDrop:: "
						+ time);
	}

	/**
	 * version of add_form, to use in the drop'n'drag capability of the tree
	 **/
	public void add_form_drop(NodeForm node, DefaultMutableTreeNode parent,
			int posForm) {
		DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
		// DefaultMutableTreeNode nodeSel =
		// (DefaultMutableTreeNode)root.getChildAt(posObj);
		treeModel.insertNodeInto(node, parent, posForm);
		node.setUserObject((String) (node.getName() + parent.getChildCount()));
		expandTree(tree);
		// Update the 3Dview
		this.principal_window.update3DviewAddForm(((NodeForm) node).getAngle(),
				((NodeForm) node).getOrigine(),
				((NodeForm) node).getDimension(), ((NodeForm) node).getForm(),
				this.root.getIndex(node.getParent()));

		// Select the last NodeObject
		tree.setSelectionPath(new TreePath(parent.getPath()));
	}

	public void _changeDimForm(int newVal, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode.isLeaf() && selectNode.getParent() != this.root
				&& selectNode != this.root) {
			((NodeForm) selectNode).setDim(newVal, ID);
			// Update the 3Dview
			this.principal_window.update3Dview(
					((NodeForm) selectNode).getAngle(),
					((NodeForm) selectNode).getOrigine(),
					((NodeForm) selectNode).getDimension(),
					((NodeForm) selectNode).getForm(),
					this.root.getIndex(selectNode.getParent()),
					(selectNode.getParent()).getIndex(selectNode));
		}
	}

	public void _changeRotForm(int newVal, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode.isLeaf() && selectNode.getParent() != this.root
				&& selectNode != this.root) {
			((NodeForm) selectNode).setAngle(newVal, ID);

			this.principal_window.update3Dview(
					((NodeForm) selectNode).getAngle(),
					((NodeForm) selectNode).getOrigine(),
					((NodeForm) selectNode).getDimension(),
					((NodeForm) selectNode).getForm(),
					this.root.getIndex(selectNode.getParent()),
					(selectNode.getParent()).getIndex(selectNode));

		}
	}

	public void _changePosForm(int newVal, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode.isLeaf() && selectNode.getParent() != this.root
				&& selectNode != this.root) {
			((NodeForm) selectNode).setOri(newVal, ID);

			this.principal_window.update3Dview(
					((NodeForm) selectNode).getAngle(),
					((NodeForm) selectNode).getOrigine(),
					((NodeForm) selectNode).getDimension(),
					((NodeForm) selectNode).getForm(),
					this.root.getIndex(selectNode.getParent()),
					(selectNode.getParent()).getIndex(selectNode));

		}
	}

	public void _changeForm(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode.isLeaf() && selectNode.getParent() != this.root
				&& selectNode != this.root) {
			((NodeForm) selectNode).setForm(ID);
			this.principal_window.update3Dview(
					((NodeForm) selectNode).getAngle(),
					((NodeForm) selectNode).getOrigine(),
					((NodeForm) selectNode).getDimension(),
					((NodeForm) selectNode).getForm(),
					this.root.getIndex(selectNode.getParent()),
					(selectNode.getParent()).getIndex(selectNode));
			// tree.setSelectionPath(new TreePath(selectNode.getPath()));
			this.principal_window.setInfoNode((NodeForm) selectNode,
					(NodeObject) selectNode.getParent());
			renameAll();
		}
	}

	/**
	 * set the new Center of rotation for this NodeForm.
	 **/
	public void setNewCenterOfRotation(int selectedCenter) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeForm) {
			((NodeForm) selectNode).setSelectedCenter(selectedCenter);
		}
	}

	/**
	 * Value update for NodeObject
	 **/
	public void applyO(float pixelValue, int layer) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeObject) {
			if (pixelValue > 0) {
				((NodeObject) selectNode).setPixelValue(pixelValue);
			}
			if (layer > 0) {
				((NodeObject) selectNode).setLayer(layer);
			}
		}
		// else{
		// System.out.println("ApplyO impossible !");
		// }
	}

	/**
	 * Update position value in all NodeForm child of the selected NodeObject
	 **/
	public void applyChangeObjPos(int identifier, int[][] tab, int newValue) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeObject) {
			switch (identifier) {
			case 0:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					NodeForm node = (NodeForm) selectNode.getChildAt(i);
					node.setOriX(tab[i][identifier] + newValue);
					// Update the 3Dview
					this.principal_window.update3Dview(node.getAngle(),
							node.getOrigine(), node.getDimension(),
							node.getForm(), this.root.getIndex(selectNode), i);
				}
				break;
			case 1:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					NodeForm node = (NodeForm) selectNode.getChildAt(i);
					node.setOriY(tab[i][identifier] + newValue);
					// Update the 3Dview
					this.principal_window.update3Dview(node.getAngle(),
							node.getOrigine(), node.getDimension(),
							node.getForm(), this.root.getIndex(selectNode), i);
				}
				break;
			case 2:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					NodeForm node = (NodeForm) selectNode.getChildAt(i);
					node.setOriZ(tab[i][identifier] + newValue);
					// Update the 3Dview
					this.principal_window.update3Dview(node.getAngle(),
							node.getOrigine(), node.getDimension(),
							node.getForm(), this.root.getIndex(selectNode), i);
				}
				break;
			}
			// Update the new Center in the rotation Slider
			this.principal_window.setNewCenterRotationObj();
		}
	}

	/**
	 * Link between TreeDragAndDrop computeObjectSize and PhantoMaJ
	 * applyChangeObjDim.
	 * 
	 * @param coef_size
	 *            the coefficient of dimension decrease or increase between ( 0
	 *            and +infinit ) [0;1] correspond to a shrink.
	 * @param origine
	 *            the origine array of each NodeForm ( reset every time the
	 *            NodeObject is disselected )
	 * @param dimension
	 *            the dimension array of each NodeForm (reset every time the
	 *            NodeObject is disselected )
	 **/
	public void applyChangeObjDim(int coef_size, int[][] origine,
			int[][] dimension) {
		computeObjectSize(((float) coef_size) / 100.0f, origine, dimension);
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoObjDim(int[] nv) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				((NodeObject) selectNode).setDimTransfo(nv);
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoObjInt(int[] nv) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				// Non implement�
				System.out
						.println("TODO :: Not implemented TreeDragAndDrop.TransfoObjInt");
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoObjAngle(int[] nv, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				switch (ID) {
				case 0:
					((NodeObject) selectNode).setAngleTransfo(nv, null, null);
					break;
				case 1:
					((NodeObject) selectNode).setAngleTransfo(null, nv, null);
					break;
				case 2:
					((NodeObject) selectNode).setAngleTransfo(null, null, nv);
					break;
				}
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoObjOri(int[] nv, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				switch (ID) {
				case 0:
					((NodeObject) selectNode).setOrigineTransfo(nv, null, null);
					break;
				case 1:
					((NodeObject) selectNode).setOrigineTransfo(null, nv, null);
					break;
				case 2:
					((NodeObject) selectNode).setOrigineTransfo(null, null, nv);
					break;
				}
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoFormAngle(int[] nv, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				switch (ID) {
				case 0:
					((NodeForm) selectNode).setAngleTransfo(nv, null, null);
					break;
				case 1:
					((NodeForm) selectNode).setAngleTransfo(null, nv, null);
					break;
				case 2:
					((NodeForm) selectNode).setAngleTransfo(null, null, nv);
					break;
				}
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoFormDim(int[] nv, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				((NodeForm) selectNode).setDimTransfo(ID, nv);
			}
		}
	}

	/**
	 * Update the transformation value
	 **/
	public void TransfoFormOri(int[] nv, int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				switch (ID) {
				case 0:
					((NodeForm) selectNode).setOrigineTransfo(nv, null, null);
					break;
				case 1:
					((NodeForm) selectNode).setOrigineTransfo(null, nv, null);
					break;
				case 2:
					((NodeForm) selectNode).setOrigineTransfo(null, null, nv);
					break;
				}
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoFormAngleSel(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				switch (ID) {
				case 0:
					((NodeForm) selectNode)
							.setisPhiTransfoSelected(!((NodeForm) selectNode)
									.getisPhiTransfoSelected());
					break;
				case 1:
					((NodeForm) selectNode)
							.setisThetaTransfoSelected(!((NodeForm) selectNode)
									.getisThetaTransfoSelected());
					break;
				case 2:
					((NodeForm) selectNode)
							.setisPsiTransfoSelected(!((NodeForm) selectNode)
									.getisPsiTransfoSelected());
					break;
				}
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoFormDimSel(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				((NodeForm) selectNode).setisDimSelected(
						!((NodeForm) selectNode).getisDimSelected(ID), ID);
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoFormOriSel(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode.isLeaf() && selectNode instanceof NodeForm) {
				switch (ID) {
				case 0:
					((NodeForm) selectNode)
							.setisXoriSelected(!((NodeForm) selectNode)
									.getisXoriSelected());
					break;
				case 1:
					((NodeForm) selectNode)
							.setisYoriSelected(!((NodeForm) selectNode)
									.getisYoriSelected());
					break;
				case 2:
					((NodeForm) selectNode)
							.setisZoriSelected(!((NodeForm) selectNode)
									.getisZoriSelected());
					break;
				}
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoObjAngleSel(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				switch (ID) {
				case 0:
					((NodeObject) selectNode)
							.setisPhiTransfoSelected(!((NodeObject) selectNode)
									.getisPhiTransfoSelected());
					break;
				case 1:
					((NodeObject) selectNode)
							.setisThetaTransfoSelected(!((NodeObject) selectNode)
									.getisThetaTransfoSelected());
					break;
				case 2:
					((NodeObject) selectNode)
							.setisPsiTransfoSelected(!((NodeObject) selectNode)
									.getisPsiTransfoSelected());
					break;
				}
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoObjDimSel() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				((NodeObject) selectNode)
						.setisDimXSelected(!((NodeObject) selectNode)
								.getisDimXSelected());
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoObjIntSel() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				System.out
						.println("WARNING ::: Intensity Rand is not implemented");
				// update le champs dans NodeObject
			}
		}
	}

	/**
	 * switch true or false on the selected parameter
	 **/
	public void TransfoObjOriSel(int ID) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			if (selectNode instanceof NodeObject) {
				switch (ID) {
				case 0:
					((NodeObject) selectNode)
							.setisXoriSelected(!((NodeObject) selectNode)
									.getisXoriSelected());
					break;
				case 1:
					((NodeObject) selectNode)
							.setisYoriSelected(!((NodeObject) selectNode)
									.getisYoriSelected());
					break;
				case 2:
					((NodeObject) selectNode)
							.setisZoriSelected(!((NodeObject) selectNode)
									.getisZoriSelected());
					break;
				}
			}
		}
	}

	/**
	 * Copy the selected Node, NodeForm and add it a the end of the NodeObject
	 * Parent or NodeObject and add it at the end of the Tree
	 **/
	public void copy() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeObject) {
			this.add_Object(((NodeObject) selectNode).getPixelValue(),
					((NodeObject) selectNode).getLayer(), false);
			for (int i = 0; i < selectNode.getChildCount(); i++) {
				this.add_form(
						new NodeForm(((NodeForm) selectNode.getChildAt(i))
								.getName(), ((NodeForm) selectNode
								.getChildAt(i)).getDimension()[0],
								((NodeForm) selectNode.getChildAt(i))
										.getDimension()[1],
								((NodeForm) selectNode.getChildAt(i))
										.getDimension()[2],
								((NodeForm) selectNode.getChildAt(i))
										.getOrigine()[0],
								((NodeForm) selectNode.getChildAt(i))
										.getOrigine()[1],
								((NodeForm) selectNode.getChildAt(i))
										.getOrigine()[2],
								((NodeForm) selectNode.getChildAt(i))
										.getAngle()[0], ((NodeForm) selectNode
										.getChildAt(i)).getAngle()[1],
								((NodeForm) selectNode.getChildAt(i))
										.getAngle()[2], ((NodeForm) selectNode
										.getChildAt(i)).getForm()), this.root
								.getChildCount() - 1);
			}
		} else if (selectNode instanceof NodeForm) {
			this.add_form(new NodeForm(((NodeForm) selectNode).getName(),
					((NodeForm) selectNode).getDimension()[0],
					((NodeForm) selectNode).getDimension()[1],
					((NodeForm) selectNode).getDimension()[2],
					((NodeForm) selectNode).getOrigine()[0],
					((NodeForm) selectNode).getOrigine()[1],
					((NodeForm) selectNode).getOrigine()[2],
					((NodeForm) selectNode).getAngle()[0],
					((NodeForm) selectNode).getAngle()[1],
					((NodeForm) selectNode).getAngle()[2],
					((NodeForm) selectNode).getForm()), this.root
					.getChildCount() - 1);
		}
	}

	/**
	 * Delete the current Selected Node.
	 **/
	public void del() {
		DefaultTreeModel treeModel = (DefaultTreeModel) this.tree.getModel();
		TreePath currentSelection = tree.getSelectionPath();
		if (currentSelection != null) {
			DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection
					.getLastPathComponent());
			MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
			if (parent != null) {
				// Update view3D
				if (currentNode instanceof NodeForm) {
					int posForm = parent.getIndex(currentNode);
					int posObj = this.root.getIndex(parent);
					this.principal_window.update3DviewRemoveForm(posObj,
							posForm);
				} else {
					int posObj = parent.getIndex((TreeNode) currentNode);
					this.principal_window.update3DviewRemoveObj(posObj);
				}

				treeModel.removeNodeFromParent(currentNode);
			}
		}
		renameAll();
	}

	/**
	 * get the maximum position for one parameter
	 **/
	public int getMaximumPosObj(int identifier, int[] stackDimension) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		int max = -1;
		int dim = 200;
		if (selectNode instanceof NodeObject && selectNode.getChildCount() > 0) {
			switch (identifier) {
			case 0:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					// System.out.println(((NodeForm)selectNode.getChildAt(i)).getOrigine()[0]
					// );
					if (max < ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[0]) {
						max = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[0];
					}
				}
				dim = stackDimension[0];
				break;
			case 1:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					if (max < ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[1]) {
						max = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[1];
					}
				}
				dim = stackDimension[1];
				break;
			case 2:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					if (max < ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[2]) {
						max = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[2];
					}
				}
				dim = stackDimension[2];
				break;

			}
		}
		if (max == -1) {
			max = 200;
		}
		return (int) (dim - max);
	}

	/**
	 * get the minimum position for one parameter
	 **/
	public int getMinimumPosObj(int identifier, int[] stackDimension) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		int min = 10000000;
		int dim = 200;
		if (selectNode instanceof NodeObject && selectNode.getChildCount() > 0) {
			switch (identifier) {
			case 0:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					if (min > ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[0]) {
						min = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[0];
					}
					dim = stackDimension[0];
				}
				break;
			case 1:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					if (min > ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[1]) {
						min = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[1];
					}
				}
				dim = stackDimension[1];
				break;
			case 2:
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					if (min > ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[2]) {
						min = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[2];
					}
				}
				dim = stackDimension[2];
				break;

			}
		}
		if (min == 10000000) {
			min = 200;
		}
		return (int) -min;
	}

	/**
	 * Save for Each NodeForm in a NodeObject the origine position in an array
	 * Tab
	 **/
	public int[] getOriObj(int identifier) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			int[] tab = new int[selectNode.getChildCount()];
			if (selectNode instanceof NodeObject) {
				switch (identifier) {
				case 0:
					for (int i = 0; i < selectNode.getChildCount(); i++) {
						tab[i] = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[0];
					}
					break;
				case 1:
					for (int i = 0; i < selectNode.getChildCount(); i++) {
						tab[i] = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[1];
					}
					break;
				case 2:
					for (int i = 0; i < selectNode.getChildCount(); i++) {
						tab[i] = ((NodeForm) selectNode.getChildAt(i))
								.getOrigine()[2];
					}
					break;
				}
			}

			return tab;
		}
		return null;
	}

	/**
	 * Save for each NodeForm in the currently selected NodeObject the origine
	 * array
	 **/
	public int[][] getOrigineObj() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			int[][] tab = new int[selectNode.getChildCount()][3];
			if (selectNode instanceof NodeObject) {
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					tab[i][0] = ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[0];

					tab[i][1] = ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[1];

					tab[i][2] = ((NodeForm) selectNode.getChildAt(i))
							.getOrigine()[2];
				}
			}
			return tab;
		}
		return null;
	}

	/**
	 * Save the angle for each form of the currently Selected Object
	 **/
	public int[][] getAngleObj() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			int[][] tab = new int[selectNode.getChildCount()][3];
			if (selectNode instanceof NodeObject) {
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					tab[i][0] = ((NodeForm) selectNode.getChildAt(i))
							.getAngle()[0];

					tab[i][1] = ((NodeForm) selectNode.getChildAt(i))
							.getAngle()[1];

					tab[i][2] = ((NodeForm) selectNode.getChildAt(i))
							.getAngle()[2];
				}
			}
			return tab;
		}
		return null;
	}

	/**
	 * Save the dimension for each form of the currently selected Object
	 **/
	public int[][] getDimObj() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode != null) {
			int[][] tab = new int[selectNode.getChildCount()][3];
			if (selectNode instanceof NodeObject) {
				for (int i = 0; i < selectNode.getChildCount(); i++) {
					// switch(id){
					// case 0:
					tab[i][0] = ((NodeForm) selectNode.getChildAt(i))
							.getDimension()[0];
					// break;
					// case 1:
					tab[i][1] = ((NodeForm) selectNode.getChildAt(i))
							.getDimension()[1];
					// break;
					// case 2:
					tab[i][2] = ((NodeForm) selectNode.getChildAt(i))
							.getDimension()[2];
					// break;
					// }
				}
			}
			return tab;
		}
		return null;
	}

	/**
	 * Compute the rotation of an entire currently selected object, and change
	 * the position of each form constituing this object. The Center of rotation
	 * is the Gravity center where each form have a weight of 1.
	 * 
	 * @param angle
	 *            int[] phi,theta,psi the rotation
	 * @param ori
	 *            int[][] origine x,y,z of every Form in an NodeObject
	 * @param centerObj
	 *            int[] Gravity center of every NodeObject
	 **/
	public void computeObjectRotation(int[] angle, int[][] ori, int[] centerObj) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		int[] newOrigine = new int[3];
		if (selectNode instanceof NodeObject) {
			double[][] matrice_rotation_euler = new double[3][3];

			double phi = Math.toRadians(-angle[0]);
			double theta = Math.toRadians(-angle[1]);
			double psi = Math.toRadians(-angle[2]);

			double cphi = Math.cos(phi);
			double sphi = Math.sin(phi);
			double ctheta = Math.cos(theta);
			double stheta = Math.sin(theta);
			double cpsi = Math.cos(psi);
			double spsi = Math.sin(psi);

			matrice_rotation_euler[0][0] = (cpsi * ctheta * cphi) - spsi * sphi;
			matrice_rotation_euler[0][1] = (cpsi * ctheta * sphi)
					+ (spsi * cphi);
			matrice_rotation_euler[0][2] = -cpsi * stheta;
			matrice_rotation_euler[1][0] = (-spsi * ctheta * cphi) - cpsi
					* sphi;
			matrice_rotation_euler[1][1] = (-spsi * ctheta * sphi)
					+ (cpsi * cphi);
			matrice_rotation_euler[1][2] = (spsi * stheta);
			matrice_rotation_euler[2][0] = stheta * cphi;
			matrice_rotation_euler[2][1] = stheta * sphi;
			matrice_rotation_euler[2][2] = ctheta;

			for (int i = 0; i < selectNode.getChildCount(); i++) {

				// int[] ori =
				// ((NodeForm)selectNode.getChildAt(i)).getOrigine();
				for (int coor = 0; coor < 3; coor++) {
					newOrigine[coor] = (int) ((ori[i][0] - centerObj[0])
							* matrice_rotation_euler[coor][0]
							+ (ori[i][1] - centerObj[1])
							* matrice_rotation_euler[coor][1]
							+ (ori[i][2] - centerObj[2])
							* matrice_rotation_euler[coor][2] + centerObj[coor] + 0.5);
				}
				// System.out.println("New Origine"+newOrigine[0]+","+newOrigine[1]+","+newOrigine[2]);
				((NodeForm) selectNode.getChildAt(i)).setOriX(newOrigine[0]);
				((NodeForm) selectNode.getChildAt(i)).setOriY(newOrigine[1]);
				((NodeForm) selectNode.getChildAt(i)).setOriZ(newOrigine[2]);
				newOrigine = new int[3];
				// Update the 3Dview
				this.principal_window.update3Dview(
						((NodeForm) selectNode.getChildAt(i)).getAngle(),
						((NodeForm) selectNode.getChildAt(i)).getOrigine(),
						((NodeForm) selectNode.getChildAt(i)).getDimension(),
						((NodeForm) selectNode.getChildAt(i)).getForm(),
						this.root.getIndex(selectNode), i);
			}
			// Update slider position Object
			this.principal_window.setNewCenterPositionObj();
		}
	}

	/**
	 * Compute the new Euler angle value of each form after the object
	 * transformation
	 **/
	public void computeObjectRotation_typeT(int[] angle, int[][] OldAngle) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		if (selectNode instanceof NodeObject) {
			// for Each Form
			for (int i = 0; i < selectNode.getChildCount(); i++) {
				// //Mutiply the old rotation matrix with the new transformation
				double phi = 0;
				double theta = 0;
				double psi = 0;

				// new transformation
				double[][] matrice_rotation_euler = new double[3][3];

				phi = Math.toRadians(OldAngle[i][0]);
				theta = Math.toRadians(OldAngle[i][1]);
				psi = Math.toRadians(OldAngle[i][2]);

				double cphi = Math.cos(phi);
				double sphi = Math.sin(phi);
				double ctheta = Math.cos(theta);
				double stheta = Math.sin(theta);
				double cpsi = Math.cos(psi);
				double spsi = Math.sin(psi);

				matrice_rotation_euler[0][0] = (cpsi * ctheta * cphi) - spsi
						* sphi;
				matrice_rotation_euler[0][1] = (cpsi * ctheta * sphi)
						+ (spsi * cphi);
				matrice_rotation_euler[0][2] = -cpsi * stheta;
				matrice_rotation_euler[1][0] = (-spsi * ctheta * cphi) - cpsi
						* sphi;
				matrice_rotation_euler[1][1] = (-spsi * ctheta * sphi)
						+ (cpsi * cphi);
				matrice_rotation_euler[1][2] = (spsi * stheta);
				matrice_rotation_euler[2][0] = stheta * cphi;
				matrice_rotation_euler[2][1] = stheta * sphi;
				matrice_rotation_euler[2][2] = ctheta;

				// transformation already here
				double[][] matrice_rotation_euler_second = new double[3][3];

				phi = Math.toRadians(-angle[0]);
				theta = Math.toRadians(-angle[1]);
				psi = Math.toRadians(-angle[2]);

				// System.out.println("Obj\n Phi : "+phi+"\nTheta : "+theta+"\nPsi : "+psi);

				cphi = Math.cos(phi);
				sphi = Math.sin(phi);
				ctheta = Math.cos(theta);
				stheta = Math.sin(theta);
				cpsi = Math.cos(psi);
				spsi = Math.sin(psi);

				matrice_rotation_euler_second[0][0] = (cpsi * ctheta * cphi)
						- spsi * sphi;
				matrice_rotation_euler_second[0][1] = (cpsi * ctheta * sphi)
						+ (spsi * cphi);
				matrice_rotation_euler_second[0][2] = -cpsi * stheta;
				matrice_rotation_euler_second[1][0] = (-spsi * ctheta * cphi)
						- cpsi * sphi;
				matrice_rotation_euler_second[1][1] = (-spsi * ctheta * sphi)
						+ (cpsi * cphi);
				matrice_rotation_euler_second[1][2] = (spsi * stheta);
				matrice_rotation_euler_second[2][0] = stheta * cphi;
				matrice_rotation_euler_second[2][1] = stheta * sphi;
				matrice_rotation_euler_second[2][2] = ctheta;

				double[][] matrice_rotation_euler_f = new double[3][3];

				// oldAngle * angle
				matrice_rotation_euler_f[0][0] = matrice_rotation_euler[0][0]
						* matrice_rotation_euler_second[0][0]
						+ matrice_rotation_euler[0][1]
						* matrice_rotation_euler_second[0][1]
						+ matrice_rotation_euler[0][2]
						* matrice_rotation_euler_second[0][2];
				matrice_rotation_euler_f[0][1] = matrice_rotation_euler[0][0]
						* matrice_rotation_euler_second[1][0]
						+ matrice_rotation_euler[0][1]
						* matrice_rotation_euler_second[1][1]
						+ matrice_rotation_euler[0][2]
						* matrice_rotation_euler_second[1][2];
				matrice_rotation_euler_f[0][2] = matrice_rotation_euler[0][0]
						* matrice_rotation_euler_second[2][0]
						+ matrice_rotation_euler[0][1]
						* matrice_rotation_euler_second[2][1]
						+ matrice_rotation_euler[0][2]
						* matrice_rotation_euler_second[2][2];

				matrice_rotation_euler_f[1][0] = matrice_rotation_euler[1][0]
						* matrice_rotation_euler_second[0][0]
						+ matrice_rotation_euler[1][1]
						* matrice_rotation_euler_second[0][1]
						+ matrice_rotation_euler[1][2]
						* matrice_rotation_euler_second[0][2];
				matrice_rotation_euler_f[1][1] = matrice_rotation_euler[1][0]
						* matrice_rotation_euler_second[1][0]
						+ matrice_rotation_euler[1][1]
						* matrice_rotation_euler_second[1][1]
						+ matrice_rotation_euler[1][2]
						* matrice_rotation_euler_second[1][2];
				matrice_rotation_euler_f[1][2] = matrice_rotation_euler[1][0]
						* matrice_rotation_euler_second[2][0]
						+ matrice_rotation_euler[1][1]
						* matrice_rotation_euler_second[2][1]
						+ matrice_rotation_euler[1][2]
						* matrice_rotation_euler_second[2][2];

				matrice_rotation_euler_f[2][0] = matrice_rotation_euler[2][0]
						* matrice_rotation_euler_second[0][0]
						+ matrice_rotation_euler[2][1]
						* matrice_rotation_euler_second[0][1]
						+ matrice_rotation_euler[2][2]
						* matrice_rotation_euler_second[0][2];
				matrice_rotation_euler_f[2][1] = matrice_rotation_euler[2][0]
						* matrice_rotation_euler_second[1][0]
						+ matrice_rotation_euler[2][1]
						* matrice_rotation_euler_second[1][1]
						+ matrice_rotation_euler[2][2]
						* matrice_rotation_euler_second[1][2];
				matrice_rotation_euler_f[2][2] = matrice_rotation_euler[2][0]
						* matrice_rotation_euler_second[2][0]
						+ matrice_rotation_euler[2][1]
						* matrice_rotation_euler_second[2][1]
						+ matrice_rotation_euler[2][2]
						* matrice_rotation_euler_second[2][2];

				// [m00 m01 m02]
				// [m10 m11 m12]
				// [m20 m21 m22]
				// Assuming the angles are in radians.
				theta = Math.toDegrees(Math
						.acos(matrice_rotation_euler_f[2][2]));
				// System.out.println("Index : "+i+"############################");
				// if cos(Theta) == 1 or -1, theta = 0 or 180 , we can assume
				// that phi = (oldphi + oldpsi )
				if (matrice_rotation_euler_f[2][2] > 0.999
						|| matrice_rotation_euler_f[2][2] < -0.999) { // singularity
																		// at
																		// north
																		// pole
					phi = Math.toDegrees(Math
							.signum(matrice_rotation_euler_f[0][1])
							* Math.acos(matrice_rotation_euler_f[0][0]));
					psi = 0;
				} else {
					// System.out.println("OTHER");
					phi = Math.toDegrees(Math.atan2(
							matrice_rotation_euler_f[2][1],
							matrice_rotation_euler_f[2][0]));
					psi = Math.toDegrees(Math.atan2(
							matrice_rotation_euler_f[1][2],
							-matrice_rotation_euler_f[0][2]));
				}

				((NodeForm) selectNode.getChildAt(i)).setPhi((int) phi);
				((NodeForm) selectNode.getChildAt(i)).setTheta((int) theta);
				((NodeForm) selectNode.getChildAt(i)).setPsi((int) psi);

				// Update the 3Dview
				this.principal_window.update3Dview(
						((NodeForm) selectNode.getChildAt(i)).getAngle(),
						((NodeForm) selectNode.getChildAt(i)).getOrigine(),
						((NodeForm) selectNode.getChildAt(i)).getDimension(),
						((NodeForm) selectNode.getChildAt(i)).getForm(),
						this.root.getIndex(selectNode), i);
			}
			// Update slider position Object
			this.principal_window.setNewCenterPositionObj();
		}
	}

	/***************************************************************************************************************************************************************************/
	/***************************************************************************************************************************************************************************/
	// Fonction for Object3D.java to draw object with angle Parameters
	/**
	 * Same as computeObjectRotation_typeT but return the new angle as an array,
	 * and for only one NodeForm
	 * 
	 * @param angle
	 *            int[] phi/theta/psi angle of transformation
	 * @param posObj
	 *            int
	 * @param posForm
	 *            int
	 **/
	public int[] computeObjectRotation_typeT_vObject3D(int angleS, int idAngle,
			int posObj, int posForm) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) this.root
				.getChildAt(posObj);

		int[] angle = { 0, 0, 0 };
		angle[idAngle] = angleS;

		if (selectNode instanceof NodeObject) {
			NodeForm form = (NodeForm) selectNode.getChildAt(posForm);
			int[] OldAngle = form.getAngle();

			// //Mutiply the old rotation matrix with the new transformation
			double phi = 0;
			double theta = 0;
			double psi = 0;

			// new transformation
			double[][] matrice_rotation_euler = new double[3][3];

			phi = Math.toRadians(OldAngle[0]);
			theta = Math.toRadians(OldAngle[1]);
			psi = Math.toRadians(OldAngle[2]);

			double cphi = Math.cos(phi);
			double sphi = Math.sin(phi);
			double ctheta = Math.cos(theta);
			double stheta = Math.sin(theta);
			double cpsi = Math.cos(psi);
			double spsi = Math.sin(psi);

			matrice_rotation_euler[0][0] = (cpsi * ctheta * cphi) - spsi * sphi;
			matrice_rotation_euler[0][1] = (cpsi * ctheta * sphi)
					+ (spsi * cphi);
			matrice_rotation_euler[0][2] = -cpsi * stheta;
			matrice_rotation_euler[1][0] = (-spsi * ctheta * cphi) - cpsi
					* sphi;
			matrice_rotation_euler[1][1] = (-spsi * ctheta * sphi)
					+ (cpsi * cphi);
			matrice_rotation_euler[1][2] = (spsi * stheta);
			matrice_rotation_euler[2][0] = stheta * cphi;
			matrice_rotation_euler[2][1] = stheta * sphi;
			matrice_rotation_euler[2][2] = ctheta;

			// transformation already here
			double[][] matrice_rotation_euler_second = new double[3][3];

			phi = Math.toRadians(-angle[0]);
			theta = Math.toRadians(-angle[1]);
			psi = Math.toRadians(-angle[2]);

			// System.out.println("Obj\n Phi : "+phi+"\nTheta : "+theta+"\nPsi : "+psi);

			cphi = Math.cos(phi);
			sphi = Math.sin(phi);
			ctheta = Math.cos(theta);
			stheta = Math.sin(theta);
			cpsi = Math.cos(psi);
			spsi = Math.sin(psi);

			matrice_rotation_euler_second[0][0] = (cpsi * ctheta * cphi) - spsi
					* sphi;
			matrice_rotation_euler_second[0][1] = (cpsi * ctheta * sphi)
					+ (spsi * cphi);
			matrice_rotation_euler_second[0][2] = -cpsi * stheta;
			matrice_rotation_euler_second[1][0] = (-spsi * ctheta * cphi)
					- cpsi * sphi;
			matrice_rotation_euler_second[1][1] = (-spsi * ctheta * sphi)
					+ (cpsi * cphi);
			matrice_rotation_euler_second[1][2] = (spsi * stheta);
			matrice_rotation_euler_second[2][0] = stheta * cphi;
			matrice_rotation_euler_second[2][1] = stheta * sphi;
			matrice_rotation_euler_second[2][2] = ctheta;

			double[][] matrice_rotation_euler_f = new double[3][3];

			// oldAngle * angle
			matrice_rotation_euler_f[0][0] = matrice_rotation_euler[0][0]
					* matrice_rotation_euler_second[0][0]
					+ matrice_rotation_euler[0][1]
					* matrice_rotation_euler_second[0][1]
					+ matrice_rotation_euler[0][2]
					* matrice_rotation_euler_second[0][2];
			matrice_rotation_euler_f[0][1] = matrice_rotation_euler[0][0]
					* matrice_rotation_euler_second[1][0]
					+ matrice_rotation_euler[0][1]
					* matrice_rotation_euler_second[1][1]
					+ matrice_rotation_euler[0][2]
					* matrice_rotation_euler_second[1][2];
			matrice_rotation_euler_f[0][2] = matrice_rotation_euler[0][0]
					* matrice_rotation_euler_second[2][0]
					+ matrice_rotation_euler[0][1]
					* matrice_rotation_euler_second[2][1]
					+ matrice_rotation_euler[0][2]
					* matrice_rotation_euler_second[2][2];

			matrice_rotation_euler_f[1][0] = matrice_rotation_euler[1][0]
					* matrice_rotation_euler_second[0][0]
					+ matrice_rotation_euler[1][1]
					* matrice_rotation_euler_second[0][1]
					+ matrice_rotation_euler[1][2]
					* matrice_rotation_euler_second[0][2];
			matrice_rotation_euler_f[1][1] = matrice_rotation_euler[1][0]
					* matrice_rotation_euler_second[1][0]
					+ matrice_rotation_euler[1][1]
					* matrice_rotation_euler_second[1][1]
					+ matrice_rotation_euler[1][2]
					* matrice_rotation_euler_second[1][2];
			matrice_rotation_euler_f[1][2] = matrice_rotation_euler[1][0]
					* matrice_rotation_euler_second[2][0]
					+ matrice_rotation_euler[1][1]
					* matrice_rotation_euler_second[2][1]
					+ matrice_rotation_euler[1][2]
					* matrice_rotation_euler_second[2][2];

			matrice_rotation_euler_f[2][0] = matrice_rotation_euler[2][0]
					* matrice_rotation_euler_second[0][0]
					+ matrice_rotation_euler[2][1]
					* matrice_rotation_euler_second[0][1]
					+ matrice_rotation_euler[2][2]
					* matrice_rotation_euler_second[0][2];
			matrice_rotation_euler_f[2][1] = matrice_rotation_euler[2][0]
					* matrice_rotation_euler_second[1][0]
					+ matrice_rotation_euler[2][1]
					* matrice_rotation_euler_second[1][1]
					+ matrice_rotation_euler[2][2]
					* matrice_rotation_euler_second[1][2];
			matrice_rotation_euler_f[2][2] = matrice_rotation_euler[2][0]
					* matrice_rotation_euler_second[2][0]
					+ matrice_rotation_euler[2][1]
					* matrice_rotation_euler_second[2][1]
					+ matrice_rotation_euler[2][2]
					* matrice_rotation_euler_second[2][2];

			// [m00 m01 m02]
			// [m10 m11 m12]
			// [m20 m21 m22]
			// Assuming the angles are in radians.
			theta = Math.toDegrees(Math.acos(matrice_rotation_euler_f[2][2]));
			// System.out.println("Index : "+i+"############################");
			// if cos(Theta) == 1 or -1, theta = 0 or 180 , we can assume that
			// phi = (oldphi + oldpsi )
			if (matrice_rotation_euler_f[2][2] > 0.999
					|| matrice_rotation_euler_f[2][2] < -0.999) { // singularity
																	// at north
																	// pole
				phi = Math.toDegrees(Math
						.signum(matrice_rotation_euler_f[0][1])
						* Math.acos(matrice_rotation_euler_f[0][0]));
				psi = 0;
			} else {
				// System.out.println("OTHER");
				phi = Math.toDegrees(Math.atan2(matrice_rotation_euler_f[2][1],
						matrice_rotation_euler_f[2][0]));
				psi = Math.toDegrees(Math.atan2(matrice_rotation_euler_f[1][2],
						-matrice_rotation_euler_f[0][2]));
			}

			int[] newAngle = { (int) phi, (int) theta, (int) psi };
			return newAngle;
		}
		return null;
	}

	/**
	 * Same as computeObjectRotation, but for only one form, used in Object3D
	 * 
	 * @param angle
	 *            int[] phi,theta,psi the rotation
	 * @param posObj
	 *            int
	 * @param posForm
	 *            int
	 **/
	public int[] computeObjectRotation_vObject(int angleS, int idAngle,
			int posObj, int posForm) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) this.root
				.getChildAt(posObj);
		int[] newOrigine = new int[3];

		int[] angle = { 0, 0, 0 };
		angle[idAngle] = angleS;

		if (selectNode instanceof NodeObject) {
			NodeForm form = (NodeForm) selectNode.getChildAt(posForm);

			int[] ori = form.getOrigine();
			int[] centerObj = this.getCenterObject_i(posObj);

			double[][] matrice_rotation_euler = new double[3][3];

			double phi = Math.toRadians(-angle[0]);
			double theta = Math.toRadians(-angle[1]);
			double psi = Math.toRadians(-angle[2]);

			double cphi = Math.cos(phi);
			double sphi = Math.sin(phi);
			double ctheta = Math.cos(theta);
			double stheta = Math.sin(theta);
			double cpsi = Math.cos(psi);
			double spsi = Math.sin(psi);

			matrice_rotation_euler[0][0] = (cpsi * ctheta * cphi) - spsi * sphi;
			matrice_rotation_euler[0][1] = (cpsi * ctheta * sphi)
					+ (spsi * cphi);
			matrice_rotation_euler[0][2] = -cpsi * stheta;
			matrice_rotation_euler[1][0] = (-spsi * ctheta * cphi) - cpsi
					* sphi;
			matrice_rotation_euler[1][1] = (-spsi * ctheta * sphi)
					+ (cpsi * cphi);
			matrice_rotation_euler[1][2] = (spsi * stheta);
			matrice_rotation_euler[2][0] = stheta * cphi;
			matrice_rotation_euler[2][1] = stheta * sphi;
			matrice_rotation_euler[2][2] = ctheta;

			// int[] ori = ((NodeForm)selectNode.getChildAt(i)).getOrigine();
			for (int coor = 0; coor < 3; coor++) {
				newOrigine[coor] = (int) ((ori[0] - centerObj[0])
						* matrice_rotation_euler[coor][0]
						+ (ori[1] - centerObj[1])
						* matrice_rotation_euler[coor][1]
						+ (ori[2] - centerObj[2])
						* matrice_rotation_euler[coor][2] + centerObj[coor] + 0.5);
			}
			return newOrigine;
		}
		return null;
	}

	/***************************************************************************************************************************************************************************/
	/***************************************************************************************************************************************************************************/

	/**
	 * Return the gravity center of the selected object with a weight of 1 for
	 * each form
	 **/
	public int[] getCenterObject() {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();

		int[] center = new int[3];

		// initialize
		int[][] extremum = new int[3][2];

		for (int coor = 0; coor < 3; coor++) {
			extremum[coor][0] = 10000;
			extremum[coor][1] = -10000;
		}

		if (selectNode instanceof NodeObject) {
			for (int i = 0; i < selectNode.getChildCount(); i++) {
				int[] ori = ((NodeForm) selectNode.getChildAt(i)).getOrigine();
				// for coor = x,y,z
				for (int coor = 0; coor < 3; coor++) {
					if (ori[coor] < extremum[coor][0]) {
						extremum[coor][0] = ori[coor];
					}
					if (ori[coor] > extremum[coor][1]) {
						extremum[coor][1] = ori[coor];
					}
				}
			}
		}

		for (int coor = 0; coor < 3; coor++) {
			center[coor] = (extremum[coor][1] + extremum[coor][0]) / 2;
		}
		// System.out.println("Center : "+center[0]+","+center[1]+","+center[2]);
		return center;
	}

	/**
	 * Return the gravity center of an object with a weight of 1 for each form
	 * 
	 * @param objPos
	 *            the position of the object.
	 **/
	public int[] getCenterObject_i(int objPos) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) this.root
				.getChildAt(objPos);

		int[] center = new int[3];

		// initialize
		int[][] extremum = new int[3][2];

		for (int coor = 0; coor < 3; coor++) {
			extremum[coor][0] = 10000;
			extremum[coor][1] = -10000;
		}

		if (selectNode instanceof NodeObject) {
			for (int i = 0; i < selectNode.getChildCount(); i++) {
				int[] ori = ((NodeForm) selectNode.getChildAt(i)).getOrigine();
				// for coor = x,y,z
				for (int coor = 0; coor < 3; coor++) {
					if (ori[coor] < extremum[coor][0]) {
						extremum[coor][0] = ori[coor];
					}
					if (ori[coor] > extremum[coor][1]) {
						extremum[coor][1] = ori[coor];
					}
				}
			}
		}

		for (int coor = 0; coor < 3; coor++) {
			center[coor] = (extremum[coor][1] + extremum[coor][0]) / 2;
		}
		// System.out.println(center[0]+","+center[1]+","+center[2]);
		return center;
	}

	/**
	 * Compute the resizing of every form in the currently selected object
	 * (resizing is done for every dimension at the same time).
	 * 
	 * @param coef_size
	 *            the coefficient of dimension decrease or increase between ( 0
	 *            and +infinit ) [0;1] correspond to a shrink.
	 * @param ori_old
	 *            the origine array of each NodeForm ( reset every time the
	 *            NodeObject is disselected )
	 * @param dim_old
	 *            the dimension array of each NodeForm (reset every time the
	 *            NodeObject is disselected )
	 **/
	public void computeObjectSize(float coef_size, int[][] ori_old,
			int[][] dim_old) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) tree
				.getLastSelectedPathComponent();
		int[] centerObj = this.getCenterObject();

		if (selectNode instanceof NodeObject) {
			for (int i = 0; i < selectNode.getChildCount(); i++) {

				NodeForm nodeForme = (NodeForm) selectNode.getChildAt(i);

				nodeForme
						.setOriX((int) (((float) (ori_old[i][0] - centerObj[0])) * coef_size)
								+ centerObj[0]);
				nodeForme
						.setDim((int) (((float) dim_old[i][0]) * coef_size), 0);

				nodeForme
						.setOriY((int) (((float) (ori_old[i][1] - centerObj[1])) * coef_size)
								+ centerObj[1]);
				nodeForme
						.setDim((int) (((float) dim_old[i][1]) * coef_size), 1);

				nodeForme
						.setOriZ((int) (((float) (ori_old[i][2] - centerObj[2])) * coef_size)
								+ centerObj[2]);
				nodeForme
						.setDim((int) (((float) dim_old[i][2]) * coef_size), 2);

				this.principal_window.update3Dview(nodeForme.getAngle(),
						nodeForme.getOrigine(), nodeForme.getDimension(),
						nodeForme.getForm(), this.root.getIndex(selectNode), i);

			}
		}
	}

	/**
	 * Version of computeObjectSize for Object3D.java
	 **/
	public int[][] computeObjectSize_typeT(float coef_size, int posObj,
			int posForm) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) this.root
				.getChildAt(posObj);
		// int[] new_ori = new int[3];
		int[] centerObj = this.getCenterObject();
		// int[] new_dim = new int[3];
		int[][] output = new int[2][3];

		if (selectNode instanceof NodeObject) {
			NodeForm form = (NodeForm) selectNode.getChildAt(posForm);
			int[] ori_old = form.getOrigine();
			int[] dim_old = form.getDimension();

			for (int i = 0; i < 3; i++) {
				output[0][i] = (int) (((float) (ori_old[i] - centerObj[i])) * coef_size)
						+ centerObj[i];
				output[1][i] = (int) (((float) dim_old[i]) * coef_size);
			}
		}

		return output;
	}

	/**
	 * Return the distance from the center of each NodeForm of a NodeObject
	 **/
	public int[][] getDistanceFromCenter(int objPos) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) this.root
				.getChildAt(objPos);

		int[][] dist = new int[selectNode.getChildCount()][3];
		int[] center = this.getCenterObject_i(objPos);

		if (selectNode instanceof NodeObject) {
			for (int i = 0; i < selectNode.getChildCount(); i++) {
				int[] ori = ((NodeForm) selectNode.getChildAt(i)).getOrigine();
				// for coor = x,y,z
				for (int coor = 0; coor < 3; coor++) {
					dist[i][coor] = ori[coor] - center[coor];
				}
			}
		}
		return dist;
	}

	/**
	 * Creat an ArrayList of Parameters Object. An object Parameters is creat
	 * each time a Node as a transformation parameter
	 * selected."Used to creat the MultiPhantomOptionFrame"
	 * 
	 * @return ArrayList<Parameters>
	 **/
	public ArrayList<Parameters> getParameters() {

		ArrayList<Parameters> array = new ArrayList<Parameters>();
		// for every ObjectNode
		for (int i = 0; i < this.root.getChildCount(); i++) {
			NodeObject obj = (NodeObject) this.root.getChildAt(i);
			ArrayList<Integer> id_paramObj = obj.getID_parameters();
			// for every parameter selected in this Object
			for (int param = 0; param < id_paramObj.size(); param++) {
				// parameters(posForm,posObj,node,type,idParam,distance)
				array.add(new Parameters(-1, i, (DefaultMutableTreeNode) obj,
						1, id_paramObj.get(param), this
								.getDistanceFromCenter(this.root.getIndex(obj))));
			}
			// for every NodeForm of this Object
			for (int j = 0; j < obj.getChildCount(); j++) {
				NodeForm form = (NodeForm) obj.getChildAt(j);
				// System.out.println(form.getisDimXSelected()+" ............. L 1246 TreeDragAndDrop");
				ArrayList<Integer> id_form = form.getID_parameters();
				// for every parameter selected in this form
				for (int paramf = 0; paramf < id_form.size(); paramf++) {
					array.add(new Parameters(j, i,
							(DefaultMutableTreeNode) form, 0, id_form
									.get(paramf)));
				}
			}
		}
		return array;
	}

	public void setLock_setTree(boolean bol) {
		this.lock_setTree = bol;
	}

	/**
	 * @Override set the info display on the tabbedPane with the currently
	 *           selected Node Update to the view3D
	 **/
	public void valueChanged(TreeSelectionEvent e) {
		if (this.lock_setTree) {

			// System.out.println("Enter Value Changed ");

			TreePath path = e.getNewLeadSelectionPath();
			if (path == null) {
				// System.out.println("path is null");
				path = e.getPath().getParentPath();
			}

			try {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) path
						.getLastPathComponent();

				// System.out.println("sel"+node.getClass().getName());

				if (node instanceof NodeForm) {
					this.principal_window.setInfoNode((NodeForm) node,
							(NodeObject) node.getParent());
					this.principal_window.setButtonDelEnable(true);
					// Update view3D
					int posObj = this.root.getIndex(node.getParent());
					int posForm = node.getParent().getIndex(node);
					this.principal_window.update3DviewSelectedForm(posObj,
							posForm);
					// System.out.println("sel"+node.getClass().getName());
					// expandTree(tree);
				} else {
					if (node instanceof NodeObject) {

						this.principal_window.setInfoObject((NodeObject) node);
						this.principal_window.setButtonDelEnable(true);
						// Update _ObjectPanel (information)
						if (node.getChildCount() > 0) {
							this.principal_window.setNewCenterRotationObj();
							// this.principal_window.setNewCenterPositionObj();
						}
						// Update view3D
						int posObj = this.root.getIndex(node);
						this.principal_window.update3DviewSelectedObj(posObj);
					} else {
						this.principal_window.setInfoRoot();
						this.principal_window.setButtonDelEnable(false);
						// Update view3D
						this.principal_window.update3DviewSelectedObj(-1);

					}
					// System.out.println("value changed: node is not NodeForm");
				}
			} catch (NullPointerException exp) {
				// exp.printStackTrace();
				System.out
						.println("NullPointerException .... TreeDragAndDrop.ValueChanged");
			}
		}
	}

	/**
	 * Override
	 **/
	public void mouseClicked(MouseEvent e) {
	}

	/**
	 * Override
	 **/
	public void mousePressed(MouseEvent e) {
	}

	/**
	 * Override if the right Mouse button is released : add the PopUpMenuTree to
	 * add, delete or copy NodeObject and NodeForm
	 **/
	public void mouseReleased(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
			if (selPath != null) {
				// A partir du chemin, on r�cup�re le noeud
				tree.setSelectionPath(selPath);
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath
						.getLastPathComponent();
				// System.out.println("Y :"+e.getY()+" ,Scroll :"+this.scroll.getVerticalScrollBar().getValue());
				if (node instanceof NodeForm) {
					PopUpMenuTree pop = new PopUpMenuTree(2, this);
					pop.show(principal_window, e.getX(),
							(e.getY() + 60 - (this.scroll
									.getVerticalScrollBar().getValue())));
					// System.out.println("je clique sur le bouton droit "+node.toString());
				} else if (node instanceof NodeObject) {
					PopUpMenuTree pop = new PopUpMenuTree(1, this);
					pop.show(principal_window, e.getX(),
							(e.getY() + 60 - (this.scroll
									.getVerticalScrollBar().getValue())));
				} else if (node == this.root) {
					PopUpMenuTree pop = new PopUpMenuTree(0, this);
					pop.show(principal_window.getLeftPanel(), e.getX(), (e
							.getY() - (this.scroll.getVerticalScrollBar()
							.getValue())));
				} else {
					System.out.println("Node inconnu");
				}
			}
		}
	}

	/**
	 * Override
	 **/
	public void mouseEntered(MouseEvent e) {

	}

	/**
	 * Override
	 **/
	public void mouseExited(MouseEvent e) {

	}

	/**
	 * Override javax.swing.event.TreeModelListener.treeNodesChanged
	 **/
	public void treeNodesChanged(TreeModelEvent e) {
	}

	/**
	 * TabbedPane 1 => Modify phantom ( multi creat ) Override
	 * javax.swing.event.TreeModelListener.treeNodesInserted
	 **/
	public void treeNodesInserted(TreeModelEvent e) {
		// if ( this.root.getChildCount()>0 &&
		// this.root.getChildAt(0).getChildCount()>0){
		// principal_window.setComboObj();
		// }
		if (TreeAsLeaf(tree)) {
			principal_window.setTabbedPaneVisible(true, 1);
			// principal_window.setComboObj();
		} else {
			principal_window.setTabbedPaneVisible(false, 1);
		}
	}

	/**
	 * Override javax.swing.event.TreeModelListener.treeNodesRemoved
	 **/
	public void treeNodesRemoved(TreeModelEvent e) {
		if (TreeAsLeaf(tree)) {
			principal_window.setTabbedPaneVisible(true, 1);
		} else {
			principal_window.setTabbedPaneVisible(false, 1);
		}
	}

	/**
	 * Override javax.swing.event.TreeModelListener.treeStructureChanged
	 **/
	public void treeStructureChanged(TreeModelEvent e) {
		if (TreeAsLeaf(tree)) {
			principal_window.setTabbedPaneVisible(true, 1);
		} else {
			principal_window.setTabbedPaneVisible(false, 1);
		}
	}

	// ################################################################################################################################
	// INNER Class

	class TreeTransferHandler extends TransferHandler {
		DataFlavor nodesFlavor;
		DataFlavor[] flavors = new DataFlavor[1];
		DefaultMutableTreeNode[] nodesToRemove;

		public TreeTransferHandler() {
			try {
				String mimeType = DataFlavor.javaJVMLocalObjectMimeType
						+ ";class=\""
						+ javax.swing.tree.DefaultMutableTreeNode[].class
								.getName() + "\"";
				nodesFlavor = new DataFlavor(mimeType);
				flavors[0] = nodesFlavor;
			} catch (ClassNotFoundException e) {
				System.out.println("ClassNotFound: " + e.getMessage());
			}
		}

		public boolean canImport(TransferHandler.TransferSupport support) {
			if (!support.isDrop()) {
				return false;
			}
			support.setShowDropLocation(true);
			if (!support.isDataFlavorSupported(nodesFlavor)) {
				return false;
			}
			// Do not allow a drop on the drag source selections.
			JTree.DropLocation dl = (JTree.DropLocation) support
					.getDropLocation();
			JTree tree = (JTree) support.getComponent();
			int dropRow = tree.getRowForPath(dl.getPath());

			DefaultTreeModel treeModel = (DefaultTreeModel) tree.getModel();
			DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
					.getRoot();

			if (dl.getPath().getPathCount() < 2) {
				return false;
			}

			// Si la droplocation n'est pas un noeud ( ou ne deviendra pas un
			// noeud )
			if (dl.getChildIndex() == -1) {
				return false;
			}

			int[] selRows = tree.getSelectionRows();
			for (int i = 0; i < selRows.length; i++) {
				if (selRows[i] == dropRow) {
					return false;
				}
			}
			// Do not allow MOVE-action drops if a non-leaf node is
			// selected unless all of its children are also selected.
			int action = support.getDropAction();
			if (action == MOVE) {
				return haveCompleteNode(tree);
			}
			// Do not allow a non-leaf node to be copied to a level
			// which is less than its source level.
			TreePath dest = dl.getPath();
			DefaultMutableTreeNode target = (DefaultMutableTreeNode) dest
					.getLastPathComponent();
			TreePath path = tree.getPathForRow(selRows[0]);
			DefaultMutableTreeNode firstNode = (DefaultMutableTreeNode) path
					.getLastPathComponent();
			if (firstNode.getChildCount() > 0
					&& target.getLevel() < firstNode.getLevel()) {
				return false;
			}
			return true;
		}

		private boolean haveCompleteNode(JTree tree) {
			int[] selRows = tree.getSelectionRows();
			TreePath path = tree.getPathForRow(selRows[0]);
			DefaultMutableTreeNode first = (DefaultMutableTreeNode) path
					.getLastPathComponent();
			int childCount = first.getChildCount();
			// first has children and no children are selected.
			if (childCount > 0 && selRows.length == 1)
				return false;
			// first may have children.
			for (int i = 1; i < selRows.length; i++) {
				path = tree.getPathForRow(selRows[i]);
				DefaultMutableTreeNode next = (DefaultMutableTreeNode) path
						.getLastPathComponent();
				if (first.isNodeChild(next)) {
					// Found a child of first.
					if (childCount > selRows.length - 1) {
						// Not all children of first are selected.
						return false;
					}
				}
			}
			return true;
		}

		protected Transferable createTransferable(JComponent c) {
			JTree tree = (JTree) c;
			TreePath[] paths = tree.getSelectionPaths();
			if (paths != null) {
				// Make up a node array of copies for transfer and
				// another for/of the nodes that will be removed in
				// exportDone after a successful drop.
				List<NodeForm> copies = new ArrayList<NodeForm>();
				List<NodeForm> toRemove = new ArrayList<NodeForm>();
				NodeForm node = (NodeForm) paths[0].getLastPathComponent();
				NodeForm copy = (NodeForm) copy(node);
				copies.add(copy);
				toRemove.add(node);
				for (int i = 1; i < paths.length; i++) {
					NodeForm next = (NodeForm) paths[i].getLastPathComponent();
					// Do not allow higher level nodes to be added to list.
					if (next.getLevel() < node.getLevel()) {
						break;
					} else if (next.getLevel() > node.getLevel()) { // child
																	// node
						copy.add((NodeForm) copy(next));
						// node already contains child
					} else { // sibling
						copies.add((NodeForm) copy(next));
						toRemove.add(next);
					}
				}
				NodeForm[] nodes = copies.toArray(new NodeForm[copies.size()]);
				nodesToRemove = toRemove.toArray(new NodeForm[toRemove.size()]);
				return new NodesTransferable((NodeForm[]) nodes);
			}
			return null;
		}

		/** Defensive copy used in createTransferable. */
		private NodeForm copy(NodeForm node) {
			return new NodeForm(node);
		}

		protected void exportDone(JComponent source, Transferable data,
				int action) {
			// System.out.println("ExportDone");
			if ((action & MOVE) == MOVE) {
				JTree tree = (JTree) source;
				DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
				// save the parent node
				NodeForm childNode = (NodeForm) nodesToRemove[0];
				DefaultMutableTreeNode ParentNode = (DefaultMutableTreeNode) (childNode
						.getParent());

				// Remove nodes saved in nodesToRemove in createTransferable.
				for (int i = 0; i < nodesToRemove.length; i++) {
					del_node(nodesToRemove[i]);
					// model.removeNodeFromParent(nodesToRemove[i]);
				}
				// added
				if (ParentNode.getChildCount() == 0) {
					del_node(ParentNode);
					// model.removeNodeFromParent(ParentNode);
				}
			}
		}

		public int getSourceActions(JComponent c) {
			return COPY_OR_MOVE;
		}

		public boolean importData(TransferHandler.TransferSupport support) {
			if (!canImport(support)) {
				return false;
			}
			// Extract transfer data.
			NodeForm[] nodes = null;
			try {
				Transferable t = support.getTransferable();
				nodes = (NodeForm[]) t.getTransferData(nodesFlavor);
			} catch (UnsupportedFlavorException ufe) {
				System.out.println("UnsupportedFlavor: " + ufe.getMessage());
			} catch (java.io.IOException ioe) {
				System.out.println("I/O error: " + ioe.getMessage());
			}
			// Get drop location info.
			JTree.DropLocation dl = (JTree.DropLocation) support
					.getDropLocation();
			int childIndex = dl.getChildIndex();
			TreePath dest = dl.getPath();
			DefaultMutableTreeNode parent = (DefaultMutableTreeNode) dest
					.getLastPathComponent();
			JTree tree = (JTree) support.getComponent();
			DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
			// Configure for drop mode.
			int index = childIndex; // DropMode.INSERT
			if (childIndex == -1) { // DropMode.ON
				index = parent.getChildCount();
			}
			// Add data to model.
			for (int i = 0; i < nodes.length; i++) {
				// NodeForm nodef = (NodeForm)nodes[i].getPath()[0];
				// System.out.println(nodes[i].getPath()+","+nodes[i].getPath().length);
				add_form_drop((NodeForm) nodes[i], parent, index++);
				// model.insertNodeInto((NodeForm)nodes[i], parent, index++);
				// System.out.println(nodef.getClass().getName());
			}
			return true;
		}

		public String toString() {
			return getClass().getName();
		}

		public class NodesTransferable implements Transferable {
			NodeForm[] nodes;

			// DefaultMutableTreeNode[] nodes;

			public NodesTransferable(NodeForm[] nodes) {
				this.nodes = nodes;
			}

			public Object getTransferData(DataFlavor flavor)
					throws UnsupportedFlavorException {
				if (!isDataFlavorSupported(flavor))
					throw new UnsupportedFlavorException(flavor);
				return nodes;
			}

			public DataFlavor[] getTransferDataFlavors() {
				return flavors;
			}

			public boolean isDataFlavorSupported(DataFlavor flavor) {
				return nodesFlavor.equals(flavor);
			}
		}
	}

	class CustomTreeUI extends BasicTreeUI {

		@Override
		protected AbstractLayoutCache.NodeDimensions createNodeDimensions() {
			return new NodeDimensionsHandler() {
				@Override
				public Rectangle getNodeDimensions(Object value, int row,
						int depth, boolean expanded, Rectangle size) {
					Rectangle dimensions = super.getNodeDimensions(value, row,
							depth, expanded, size);
					dimensions.width = 150 - getRowX(row, depth);
					return dimensions;
				}
			};
		}

		@Override
		protected void paintHorizontalLine(Graphics g, JComponent c, int y,
				int left, int right) {
			// do nothing.
		}

		@Override
		protected void paintVerticalPartOfLeg(Graphics g, Rectangle clipBounds,
				Insets insets, TreePath path) {
			// do nothing.
		}
	}
}