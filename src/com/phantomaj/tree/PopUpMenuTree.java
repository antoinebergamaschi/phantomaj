package com.phantomaj.tree;
import java.awt.event.ActionEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.StaticField_Form;

public class PopUpMenuTree extends JPopupMenu{
	
	private TreeDragAndDrop tree;
	
	public PopUpMenuTree(int identifiers, TreeDragAndDrop tree){
		
		ItemAddO item_1 = new ItemAddO("New Object");
		JMenu item_2 = new JMenu("New Form");
		
		for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
			ItemAdd item = new ItemAdd("New "+StaticField_Form.FORM_NAME[i],i);
			item_2.add(item);
		}
		
		ItemCopy item_4 = new ItemCopy("Copy");
		ItemDelO item_3 = new ItemDelO("Delete");
		
		this.tree = tree;
		
		if ( identifiers == 0 ){
			item_1.setEnabled(true);
			item_4.setEnabled(false);
			item_2.setEnabled(false);
			item_3.setEnabled(false);
		}
		else if ( identifiers == 1 ){
			item_1.setEnabled(true);
			item_2.setEnabled(true);
			item_3.setEnabled(true);
			item_4.setEnabled(true);
		}
		else if ( identifiers == 2 ){
			item_3.setNode(true);
			item_1.setEnabled(true);
			item_2.setEnabled(false);
			item_3.setEnabled(true);
			item_4.setEnabled(true);
		}
		this.add(item_1);
		this.add(item_2);
		this.add(item_4);
		this.add(item_3);
		
	}


	
//********************** InnerClass *********************************
	class ItemAddO extends JMenuItem{

		public ItemAddO(String s){
			super(s);
		}

		public void fireActionPerformed(ActionEvent e){
			tree.add_Object(100,0,true);
		}
	}
	
	class ItemCopy extends JMenuItem{
	
		public ItemCopy(String s){
			super(s);
		}
		
		public void fireActionPerformed(ActionEvent e){
			tree.copy();
		}
	}
	
	class ItemAdd extends JMenuItem{
		private int identifier;

		public ItemAdd(String s,int identifier){
			super(s);
			this.identifier = identifier;
		}

		public void fireActionPerformed(ActionEvent e){
			tree.add_form(new NodeForm(StaticField_Form.FORM_NAME[this.identifier],StaticField_Form.BASICDIMENSION[this.identifier][0],StaticField_Form.BASICDIMENSION[this.identifier][1],StaticField_Form.BASICDIMENSION[this.identifier][2],StaticField_Form.BASICDIMENSION[this.identifier][3],
										StaticField_Form.BASICDIMENSION[this.identifier][4],StaticField_Form.BASICDIMENSION[this.identifier][5],StaticField_Form.BASICDIMENSION[this.identifier][6],StaticField_Form.BASICDIMENSION[this.identifier][7],StaticField_Form.BASICDIMENSION[this.identifier][8],this.identifier),-1);
		}
	}
	
	class ItemDelO extends JMenuItem{

		private boolean node = false;

		public ItemDelO(String s){
			super(s);
		}

		public void fireActionPerformed(ActionEvent e){
			tree.del();
		}
		
		public void setNode(boolean node){
			this.node = node;
		}
	}
	
}

