 
/**
* SliderAngle.java
* 16/10/2012
* @author Antoine Bergamaschi
**/


package com.phantomaj.select;


import com.phantomaj.component.BasicInterfaceComponent_FormPanel;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JComponent;

import java.util.ArrayList;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderAngle extends JSlider implements ChangeListener, FocusListener{

	private JLabel label;
	private BasicInterfaceComponent_FormPanel basicInterface;
	private int identifier;
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	public SliderAngle(int orientation, int min, int max, int value, JLabel label, BasicInterfaceComponent_FormPanel bi,int identifier){
		super(orientation,min,max,value);
		this.basicInterface = bi ;
		this.label = label;
		this.identifier = identifier;
		this.setMajorTickSpacing(90);
		this.setMinorTickSpacing(10);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}


	/**
	* Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		String test = Integer.toString(this.getValue());
		this.label.setText(test);

		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_FormPanel ){
				((BasicInterfaceComponent_FormPanel)component).setUpdateFormRot(this.getValue(),this.identifier);
			}
			else{
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize select.SliderAngle.stateChanged");
			}
		}
	
	}


	/**
	* Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.label.setForeground(Color.red);
	}

	/**
	* Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		this.label.setForeground(Color.black);
	}

	
	public void addListener(JComponent component){
		this.listener.add(component);
	}	
}
