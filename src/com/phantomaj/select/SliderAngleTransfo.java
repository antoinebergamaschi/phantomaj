/**
* SliderAngleTransfo.java
* 26/03/2012
* @author Antoine Bergamaschi
**/


package com.phantomaj.select;



import com.phantomaj.component.BasicInterfaceComponent_rotPanel;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JComponent;

import java.util.ArrayList;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderAngleTransfo extends JSlider implements ChangeListener, FocusListener{


	private JLabel label;
	private int identifier;
	private boolean bol=true;

	
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	public SliderAngleTransfo(int orientation, int min, int max, int value, JLabel label, int identifier){
		super(orientation,min,max,value);
		this.label = label;
		this.identifier = identifier;
		this.setMajorTickSpacing(90);
		this.setMinorTickSpacing(10);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}


	/**
	* @Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		String test = Integer.toString(this.getValue());
		this.label.setText(test);
	
		if ( bol ){
		
			for ( int i = 0 ;  i < this.listener.size() ; i++ ){
				JComponent component = this.listener.get(i);
				if ( component instanceof BasicInterfaceComponent_rotPanel ){
					((BasicInterfaceComponent_rotPanel)component).fireStatChanged(this.identifier,this.getValue());
				}
				else {
					System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize select.SliderAngleTransfo.stateChanged");
				}
			}
		}
    }

	public void setValue_(int nv){
		bol = false;
		this.setValue(nv);
		bol = true;
	}

	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.label.setForeground(Color.red);
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		this.label.setForeground(Color.black);
	}

	public void addListener(JComponent component){
		this.listener.add(component);
	}
}
