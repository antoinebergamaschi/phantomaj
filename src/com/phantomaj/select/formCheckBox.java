/**
* formCheckBox.java
* 16/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

import com.phantomaj.component.BasicInterfaceComponent_FormPanel;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class formCheckBox extends JCheckBox implements ActionListener{

	private BasicInterfaceComponent_FormPanel basicInterface;

	public formCheckBox(String name, BasicInterfaceComponent_FormPanel bi){
		super(name);
		this.basicInterface = bi;
		this.addActionListener(this);
	}


	public void actionPerformed(ActionEvent e) {
		this.basicInterface.FireFormChoosen();
	}
}
