/**
* TransfoCheckBox.java
* 15/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.filechooser.Utils;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.ImageIcon;
import java.awt.event.MouseListener;
import javax.swing.JLabel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;

/**
* Used in BasicComponent_DimensionPanelObj , RandGlobalParametersPosition , to delete.
**/
public class TransfoCheckBox extends JLabel implements MouseListener, MouseMotionListener{

	private JComponent[] component;
	private PhantoMaJ principal_window;
	private boolean isSelected;
	
	private ImageIcon greenButton;
	private ImageIcon redButton;
	private ImageIcon yellowButton;
	private ImageIcon purpleButton;
	
	private RandomGeneratorFrame _listener = null;
	
	
	public TransfoCheckBox(String name, JComponent[] component , PhantoMaJ principal_window){
		super(name);
		
//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("redButton"));
		redButton = Utils.getResourceIcon("redButton");
		
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("greenButton"));
		greenButton = Utils.getResourceIcon("greenButton");
		
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("purpleButton"));
		purpleButton = Utils.getResourceIcon("purpleButton");
		
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("yellowButton"));
		yellowButton = Utils.getResourceIcon("yellowButton");
		
		
		this.component = component;
		this.principal_window = principal_window;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setIcon(redButton);
		// this.setIcon(new ImageIcon("./PhantoMaJ/image/redButton.png" ));
	}


	public void mouseClicked(MouseEvent e){
		if ( e.getX() <= this.getIcon().getIconWidth() && e.getY() <= this.getIcon().getIconHeight()){
			if ( this.isSelected ){
				this.isSelected = false;
				this.setIcon(redButton);
				// this.setIcon(new ImageIcon("./PhantoMaJ/image/redButton.png"));
			}
			else{
				this.isSelected = true;
				this.setIcon(greenButton);
				// this.setIcon(new ImageIcon("./PhantoMaJ/image/greenButton.png"));
			}
			this.setEnableComponent();

			//null if TransfoCheckBox use in the PhantomGenerato Frame
			// if ( this.principal_window != null ){
				// this.principal_window.setInfoSelected(-1);
			// }
			if ( this._listener != null ){
				this._listener.updateCheck();
			}
		}
	}
	
	public void mouseEntered(MouseEvent e){
	}

	public void mouseExited(MouseEvent e){
		if ( this.isSelected ){
			this.setIcon(greenButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/greenButton.png"));
		}
		else{
			this.setIcon(redButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/redButton.png"));
		}
	}

	public void mousePressed(MouseEvent e){

	}
	
	public void mouseReleased(MouseEvent e){
	
	}
	
	public void mouseDragged(MouseEvent e){
	
	}
	public void mouseMoved(MouseEvent e){
		if ( e.getX() <= this.getIcon().getIconWidth() && e.getY() <= this.getIcon().getIconHeight()){
			if ( this.isSelected ){
				this.setIcon(this.yellowButton);
				// this.setIcon(new ImageIcon("./PhantoMaJ/image/yellowButton.png"));
			}
			else{
				this.setIcon(this.purpleButton);
				// this.setIcon(new ImageIcon("./PhantoMaJ/image/purpleButton.png"));
			}
		}
	}
	
	public void setSelected(boolean bol){
		this.isSelected = bol ;
		this.setEnableComponent();
		if( this.isSelected ){
			this.setIcon(greenButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/greenButton.png" ));
		}
		else{
			this.setIcon(redButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/redButton.png" ));
		}
	}
	
	public boolean getIsSelected(){
		return this.isSelected;
	}
	
	public void setComponentTab(JComponent[] component){
		this.component = component;
	}
	
	public void setEnableComponent(){
		for(int i = 0 ; i < this.component.length ; i++ ){
			this.component[i].setEnabled(this.isSelected);
		}
	}
	
	public void setListener(RandomGeneratorFrame frame){
		this._listener = frame;
	}
}