/**
* SliderDimObj_NEW.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;


import com.phantomaj.component.BasicInterfaceComponent_DimensionPanelObj_NEW;
import com.phantomaj.component.BasicInterfaceComponent_dimObjPanelRange;
import com.phantomaj.component.BasicInterfaceComponent_DimensionPanelObj_NEW;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JComponent;

import java.util.ArrayList;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderDimObj_NEW extends JSlider implements ChangeListener, FocusListener{

	// private BasicInterfaceComponent_DimensionPanelObj_NEW basicInterface;
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	private int[][] origine;
	private int[][] dimension;
	private final static int Xpos = 0;
	private final static int Ypos = 1;
	private final static int Zpos = 2;
	private JLabel label;
	private boolean check = true ;

	
	public SliderDimObj_NEW(int min,int max,JLabel label){
		super(JSlider.HORIZONTAL,min,max,100);
		this.label = label;
		this.setMajorTickSpacing((int)((max-min)/4));
		this.setMinorTickSpacing((int)(max-min)/20);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}
	

	public void setOrigine(int[][] tab){
		this.origine = tab;
	}
	
	public void setDimension(int[][] tab){
		this.dimension = tab;
	}
	
	/**
	* Special version of setMaximum
	**/
	public void setMaximum_(int max){
		this.check = false ;
		this.setMaximum(max);
		this.check = true ;
	}
	
	/**
	* Special version of setMinimum
	**/ 
	public void setMinimum_(int min){
		this.check = false ;
		this.setMinimum(min);
		this.check = true ;
	}
	
	/**
	* Special version of setValue
	**/
	public void setValue_(int val){
		this.check = false ;
		this.setValue(val);
		this.check = true ;
	}
	
	/**
	* Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		if ( label != null ){
			this.label.setText(""+this.getValue());
		}
		if ( this.check ){
			for ( int i = 0 ; i < listener.size() ; i++ ){
				JComponent component = listener.get(i);
				if ( component instanceof BasicInterfaceComponent_DimensionPanelObj_NEW){
					((BasicInterfaceComponent_DimensionPanelObj_NEW)component).applyChangeObjDim(this.dimension);
				}
				else if ( component instanceof BasicInterfaceComponent_dimObjPanelRange ){
					((BasicInterfaceComponent_dimObjPanelRange)component).stateChangedSliderObjDim();
				}
				else {
					System.out.println("WARNING :: the class ->"+component.getClass()+"is not recognized SliderDimObj_NEW.stateChanged");
				}
			}
		}
	}
	
	/**
	* Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		if ( label != null ){
			this.label.setForeground(Color.red);
		}
	}

	/**
	* Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		if ( label!=null ){ 
			this.label.setForeground(Color.black);
		}
	}
	
	public void addListener(JComponent component){
		listener.add(component);
	}
	
}