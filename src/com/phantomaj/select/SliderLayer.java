 
/**
* SliderLayer.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;

import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.randomGenerator.RandGlobalParametersPosition;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;


import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.KeyboardFocusManager;

public class SliderLayer extends JSlider implements ChangeListener, FocusListener{

	private JLabel label;
	
	private BasicInterfaceComponent_ObjectPanel basicInterface ;
	private RandGlobalParametersPosition basic_component2 = null;
	// identifier == 0,1 => tab 1, tab2
	// 2,3 => meanXdim, rangeX
	// 4,5 => meanYdim, RangeY
	// 6,7 => meanZdim, Range Z
	private int identifier;
	private boolean bol=true;
	
	/**
	* Default Constructor for PhantoMaJ Frame
	**/
	public SliderLayer(int orientation, int min, int max, int value, JLabel label, BasicInterfaceComponent_ObjectPanel bi){
		super(orientation,min,max,value);
		this.basicInterface = bi ;
		this.label = label;
		this.identifier = identifier ;
		this.setMajorTickSpacing(1); 
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}
	
	
	
	/**
	* Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		if ( bol ){
			//principal_window = null if use in the RandomGeneratorFrame
			if ( basicInterface != null ){
				label.setText(this.getValue()+"");
				basicInterface.setUpdateLayerO(this.getValue());
			}
		}
    }

	/**
	* Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.red);
		}
	}

	/**
	* Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.black);
		}
	}
	
	
	public void setValue_(int val){
		bol = false;
		this.setValue(val);
		bol = true;
	}
	
}