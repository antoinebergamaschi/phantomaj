/**
* SliderDimObj.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;


import com.phantomaj.component.BasicInterfaceComponent_DimensionPanelObj;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

/**
* Used in BasicInterfaceComponent_DimensionPanelObj, to delete.
**/
public class SliderDimObj extends JSlider implements ChangeListener, FocusListener{

	private BasicInterfaceComponent_DimensionPanelObj basicInterface;
	private int[][] origine;
	private int[][] dimension;
	private final static int Xpos = 0;
	private final static int Ypos = 1;
	private final static int Zpos = 2;
	private JLabel label;
	private boolean check = true ;

	private int identifier;
	
	private static final int Basic = 0;
	private static final int Transfo = 1;
	
	public SliderDimObj(int min,int max,JLabel label,BasicInterfaceComponent_DimensionPanelObj basicInterface,int identifier){
		super(JSlider.HORIZONTAL,min,max,0);
		this.label = label;
		this.identifier = identifier;
		this.basicInterface = basicInterface;
		this.setMajorTickSpacing((int)((max-min)/4));
		this.setMinorTickSpacing((int)(max-min)/20);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}
	

	public void setOrigine(int[][] tab){
		this.origine = tab;
	}
	
	public void setDimension(int[][] tab){
		this.dimension = tab;
	}
	
	/**
	* Special version of setMaximum
	**/
	public void setMaximum_(int max){
		this.check = false ;
		this.setMaximum(max);
		this.check = true ;
	}
	
	/**
	* Special version of setMinimum
	**/ 
	public void setMinimum_(int min){
		this.check = false ;
		this.setMinimum(min);
		this.check = true ;
	}
	
	/**
	* Special version of setValue
	**/
	public void setValue_(int val){
		this.check = false ;
		this.setValue(val);
		this.check = true ;
	}
	
	/**
	* @Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		if ( label != null ){
			this.label.setText(""+this.getValue());
		}
		if ( this.check ){
			if ( this.identifier == this.Basic ){
				this.basicInterface.applyChangeObjDim(this.origine,this.dimension);
			}
			else if ( this.identifier == this.Transfo ){
				this.basicInterface.stateChangedSliderDimension();
			}
		}
	}
	
	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		if ( label != null ){
			this.label.setForeground(Color.red);
		}
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		if ( label!=null ){ 
			this.label.setForeground(Color.black);
		}
	}
	
	
}