 
/**
* SliderDim_NEW.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;


import com.phantomaj.component.*;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;
import javax.swing.JComponent;

import java.util.ArrayList;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.KeyboardFocusManager;

public class SliderDim_NEW extends JSlider implements ChangeListener, FocusListener{

	private JLabel label;
	
	// identifier == 0,1 => tab 1, tab2
	// 2,3 => meanXdim, rangeX
	// 4,5 => meanYdim, RangeY
	// 6,7 => meanZdim, Range Z
	private int identifier;
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	private int secondID = 0;
	private boolean bol = true;
	
	/**
	* Default Constructor for PhantoMaJ Frame
	**/
	public SliderDim_NEW(int orientation, int min, int max, int value, JLabel label,int identifier){
		super(orientation,min,max,value);
		this.identifier = identifier;
		this.label = label;
		// this.identifier = identifier ;
		if ( (max-min) < 400 ){
			this.setMajorTickSpacing((int)((max-min)/4));
			this.setMinorTickSpacing((int)(max-min)/20);
		}
		else{
			this.setMajorTickSpacing((int)((max-min)/4));
			// this.setMinorTickSpacing((int)(max-min)/20);
		}
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}
	
	
	/**
	* @Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		if ( label != null ){
			String test = Integer.toString(this.getValue());
			this.label.setText(test);
		}
		if ( bol){
			for ( int i = 0 ; i < listener.size() ; i++ ){
				JComponent component = listener.get(i);
				if ( component instanceof BasicInterfaceComponent_dimPanel){
					((BasicInterfaceComponent_dimPanel)component).setUpdateSlider(this.getValue(),this.identifier);
				}
				else if ( component instanceof BasicInterfaceComponent_FormPanel){
					((BasicInterfaceComponent_FormPanel)component).setUpdateSlider(this.getValue(),this.identifier);
				}
				else if ( component instanceof BasicInterfaceComponent_posPanel){
					//case Object
					if ( this.identifier == BasicInterface.SLIDER_POSX_R || this.identifier == BasicInterface.SLIDER_POSY_R || this.identifier == BasicInterface.SLIDER_POSZ_R ){
						((BasicInterfaceComponent_posPanel)component).fireStatChanged(this.identifier);
					}
					//case Form
					else if ( this.identifier == BasicInterface.SLIDER_POSX_N || this.identifier == BasicInterface.SLIDER_POSY_N || this.identifier == BasicInterface.SLIDER_POSZ_N ){
						((BasicInterfaceComponent_posPanel)component).fireStatChanged(this.identifier);
					}
					else {
						System.out.println("WARNING ::: identifier ("+this.identifier+") is not recognize select.SliderDim_NEW.stateChanged");
					}
				}
				else if ( component instanceof BasicInterfaceComponent_dimFormPanelRange){
					((BasicInterfaceComponent_dimFormPanelRange)component).fireStatChanged(this.identifier,this.secondID);
				}
				else {
					System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize select.SliderDim_NEW.stateChanged");
				}	
			}
		}
    }

	public void setSecondID(int ne){
		this.secondID = ne;
	}
	
	/**
	* Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.red);
		}
	}

	/**
	* Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.black);
		}
	}

	
	public void setMaximum_(int newMax){
		this.setPaintLabels(false);
		if ( newMax < 400 ){
			this.setMajorTickSpacing((int)((newMax)/4));
			this.setMinorTickSpacing((int)(newMax)/20);
		}
		else{
			this.setMajorTickSpacing((int)((newMax)/2));
			this.setMinorTickSpacing((int)(newMax)/10);
		}
		super.setMaximum(newMax);
		this.updateUI();
		this.repaint();
	}
	
	public void setValue_(int nv){
		bol = false;
		this.setValue(nv);
		bol = true;
	}
	
	public void addListener(JComponent component){
		this.listener.add(component);
	}
	
}
