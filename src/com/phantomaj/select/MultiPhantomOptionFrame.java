/**
* MultiPhantomOptionFrame.java
* 02/04/2012
* @author Antoine Bergamaschi
**/
package com.phantomaj.select;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.obj3D.Parameters;
import com.phantomaj.tree.NodeForm;
import com.phantomaj.tree.NodeObject;
	
import com.phantomaj.button.ButtonMultiCreat;
import com.phantomaj.button.ButtonUpnDown;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;

import java.util.ArrayList;
import java.util.Iterator;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;

public class MultiPhantomOptionFrame extends JFrame{

	private JPanel parameter;
	private ArrayList<ObjectParameterPanel> paramObjPanel;
	private ArrayList<ParameterPanel> parameterPanels;
	private JLabel actualPhantomNumber;

	private ArrayList<Parameters> array_parameters;
	private ATextField numbOfPhantom;
	private PhantoMaJ principal_window;
	private JLabel stackSizeAppro;
	private JCheckBox isPaired;
	
	//Constructor
	public MultiPhantomOptionFrame(ArrayList<Parameters> parameter, PhantoMaJ principal_window){
		super("Phantom option");
		this.principal_window = principal_window ;
		this.parameterPanels = new ArrayList<ParameterPanel>();
		this.paramObjPanel = new ArrayList<ObjectParameterPanel>();
		this.array_parameters = parameter ;
		if( this.array_parameters.size() == 0 ){
			// Error no param selected
			System.out.println("Error No param Selected or Detected .............. MultiPhantomOptionFrame L.78");
		}
		else{
			this.build();
		}
	}
	

//#######################################################################
//Private Methode
	
	private void build(){ 
		System.out.println("begin of MultiPhantomOptionFrame building .............. build L.78");
		this.setResizable(false); 
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(buildContentPane());	
		// this.setMinimumSize(new Dimension(200,200));
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		computeStackSize();

		
		this.pack();
		System.out.println("end of MultiPhantomOptionFrame building .............. build L.95");
		this.setVisible(true);
	}

	private JPanel buildContentPane(){
		//*********************************
		//Variable
		JLabel label = new JLabel();
		JPanel panel = new JPanel();
		int width = 0;
		int position = 0;
		int height = 0;
		//*********************************
		
		JPanel principal = new JPanel(new GridBagLayout()); 
		
		label = new JLabel("<HTML><h1>Phantom Distribution Option</h1></HTML>");
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,200,0,200,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,principal,label);
		height += label.getPreferredSize().height;
		// System.out.println("Height :"+height);
		
		panel = new JPanel(new GridBagLayout());
		label = new JLabel("<html><h3>Phantom asked Number :</h3></html>");
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,label);
		
		width += label.getPreferredSize().width;
		// System.out.println("Width :"+width);
		height += label.getPreferredSize().height;
		// System.out.println("Height :"+height);

		
		numbOfPhantom = new ATextField("8",8,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,0,panel,numbOfPhantom);	
		width += label.getPreferredSize().width;
		// System.out.println("Width :"+width);
		
		addGridBag(GridBagConstraints.WEST,10,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,principal,panel);
	
	
		panel = new JPanel(new GridBagLayout());
		label = new JLabel("<html><h3>Actual number of phantom</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,label);
		
		actualPhantomNumber = new JLabel("00000000000");
		actualPhantomNumber.setPreferredSize(actualPhantomNumber.getPreferredSize());
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,panel,actualPhantomNumber);	
		actualPhantomNumber.setText("0");
		
		stackSizeAppro = new JLabel("0000 MB");
		stackSizeAppro.setPreferredSize(stackSizeAppro.getPreferredSize());
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,panel,stackSizeAppro);	
		
		addGridBag(GridBagConstraints.WEST,10,20,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,1,1,principal,panel);
		
		isPaired = new JCheckBox("Paired Parameters");
		addGridBag(GridBagConstraints.WEST,5,20,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,principal,isPaired);
		
		//Panel to change with the parameter
		parameter = new JPanel(new GridBagLayout());
		
		Iterator <Parameters> parameter_iterator = this.array_parameters.iterator();
		// for each Parameters
		while ( parameter_iterator.hasNext()){
			Parameters current_parameter = parameter_iterator.next();
			//if the parameter is apply to a Form
			if ( current_parameter.getType() == 0 ){
				// -1  if there is no object, else return the object position in the arrayList of PanelObject
				int posObj = this.getPanelObject(current_parameter.getPosObj());
				// if the object containing this form already exist
				if ( posObj >= 0 ){
					//-1 if there is no form, else return the object position in the arrayList
					int posForm = this.getPanelForm(posObj,current_parameter.getPosForm());
					if  ( posForm >= 0 ){
						//add the parameter at the specified position
						this.addParameter(current_parameter,this.paramObjPanel.get(posObj).getForm().get(posForm));
					}
					else{
						// creat a new form 
						this.addPanelForm(current_parameter,this.paramObjPanel.get(posObj));
						
						// the form created
						FormParameterPanel _form_ = this.paramObjPanel.get(posObj).getForm().get(this.paramObjPanel.get(posObj).getForm().size()-1);
						this.addParameter(current_parameter,_form_);
					}
				}
				// else creat the object
				else{
					//add the new PanelObject and then add a form to it, last 0 to specified the Parameters Object is not for the Object itself
					this.addPanelObject(current_parameter,parameter,0);
					//add the form in the Object just created
					this.addPanelForm(current_parameter,this.paramObjPanel.get(paramObjPanel.size()-1));
					// the form created
					FormParameterPanel _form_ = this.paramObjPanel.get(this.paramObjPanel.size()-1).getForm().get(this.paramObjPanel.get(this.paramObjPanel.size()-1).getForm().size() -1 );
					this.addParameter(current_parameter,_form_);
				}
			}
			//if the parameter apply to an Object
			else if ( current_parameter.getType() == 1 ){
				int posObj = this.getPanelObject(current_parameter.getPosObj());
				// if the object  already exist
				if ( posObj >= 0 ){
					this.addParameterObj(current_parameter,this.paramObjPanel.get(posObj));
				}
				// else creat the object
				else{
					//add the new PanelObject
					this.addPanelObject(current_parameter,parameter,1);
					this.addParameterObj(current_parameter,this.paramObjPanel.get(this.paramObjPanel.size()-1));
				}
			}
		}
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,20,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,3,principal,parameter);
	
		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,20,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,4,principal,label);
	
		ButtonMultiCreat b_go = new ButtonMultiCreat("Run",this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,60,20,60,GridBagConstraints.BOTH,1,0,0,0,2,1,0,5,principal,b_go);
	
		// this.setSize(principal.getPreferredSize());
		this.setMinimumSize(new Dimension(width,height));
		return principal;
	}

	/**
	* Test if the Object corresponding to the objectID already existe in the ObjectParameterPanel ArrayList
	* @return int, -1 if this object don't exist, else return the position in the arrayList of this Object
	**/
	private int getPanelObject(int objectID){
		int pos = 0 ;
		Iterator <ObjectParameterPanel> objPanel = this.paramObjPanel.iterator();
		while ( objPanel.hasNext()){
			if ( objPanel.next().getObjID() == objectID ){
				return pos;
			}
			pos++;
		}
		return -1;
	}
	
	private int getPanelForm(int posObj, int formID){
		int pos = 0 ;
		Iterator <FormParameterPanel> formPanel = this.paramObjPanel.get(posObj).getForm().iterator();
		while ( formPanel.hasNext()){
			if ( formPanel.next().getFormID() == formID ){
				return pos;
			}
			pos++;
		}
		return -1;
	}
	
	/**
	* Test if this ArrayList<ArrayList<Integer>> has element in it
	* @return true is this array has element, false else.
	**/
	private boolean isNotEmpty(ArrayList<ArrayList<Integer>> array){
		Iterator <ArrayList<Integer>> array_it = array.iterator();
		while ( array_it.hasNext() ){
			if ( !array_it.next().isEmpty() ){
				return true;
			}
		}
		return false;
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	public void setActualPhantomNumber(int numb){
		this.actualPhantomNumber.setText(""+numb);
	}
	
	/**
	* Compute the minimal commum value
	**/
	public int computeMinimumNumberOfDif(){
		double tmp = 1;
		int z = 0;
		
		if ( this.isPaired.isSelected() ){
			while ( Integer.parseInt(this.numbOfPhantom.getText()) >= tmp ){
				tmp = 1 ;
				z += 1 ;
				for ( int i = 2 ; i < ( parameterPanels.size() ) ; i++ ){
					int sum = 0 ;
					for ( int j = i ; j < ( parameterPanels.size() ) ; j++ ){
						sum += z ;
					}
					tmp += z*sum ;
				}
				// System.out.println(tmp);
			}
		}
		else {
			while ( Integer.parseInt(this.numbOfPhantom.getText()) >= tmp ){
				z += 1 ;
				tmp = 1 ;
				for ( int i=0 ; i < parameterPanels.size() ; i++ ){
					tmp *= z ;
					// System.out.println(tmp);
				}
			}
		}
		
		return (z-1);
	}
	
	/**
	* Update every JLabel with the minimal commun value under the asked Phantom value
	**/
	public void setNumberOfDif(){
		int numbDif = computeMinimumNumberOfDif();
		Iterator<ParameterPanel> param = this.parameterPanels.iterator();
		while(param.hasNext()){
			param.next().setNumberOfRepetition(numbDif);
		}
		computeNumberOfPhantom();
	}
	
	
	/**
	* Add a new ObjectParameterPanel, in a JComponent container at the specified position in the GridBagLayout 
	**/
	public void addPanelObject(Parameters param,JComponent container,int nullParameters){
		int position = this.paramObjPanel.size()+1;
		//If the new Object don't have this Parameters but is new containing form
		if ( nullParameters == 0 ){
			ObjectParameterPanel obj = new ObjectParameterPanel();
			//set the Identity Obj from the Information of the Form we will add later
			obj.setIdentity(param,nullParameters);
			//adding obj in the ArrayList object
			this.paramObjPanel.add(obj);
			this.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,1,1,0,position,container,obj);
		}
		//If the new Object has this Parameters Object
		else if ( nullParameters == 1 ){
			ObjectParameterPanel obj = new ObjectParameterPanel();
			//set the Identity Obj from the Information of the Form we will add later
			obj.setIdentity(param,nullParameters);
			//adding obj in the ArrayList object
			this.paramObjPanel.add(obj);
			this.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,1,1,0,position,container,obj);
		}
	}
	
	/**
	* Add a new FormParameterPanel
	**/
	public void addPanelForm(Parameters param, JComponent container){
		FormParameterPanel form = new FormParameterPanel();	
		//set he Identity of this form
		form.setIdentity(param);
		//get the last form position
		int position = ((ObjectParameterPanel)container).getParameter().size() + ((ObjectParameterPanel)container).getForm().size()+1;	
		//adding this Form in the ArrayList obj
		((ObjectParameterPanel)container).addFormPanel(form);
		this.addGridBag(GridBagConstraints.CENTER,10,5,10,5,GridBagConstraints.BOTH,1,1,0,0,2,1,0,position,container,form);
	}
	
	/**
	* Add a new panelParam in the JPanel and in a ArrayList
	**/
	public void addParameter(Parameters param , JComponent container){
		ParameterPanel panelParam = new ParameterPanel(param);
		
		int position = ((FormParameterPanel)container).getParameter().size() + 1;
		
		//adding in the ArrayList of the container
		((FormParameterPanel)container).addParameter(panelParam);
		//adding as a new component for the button up'n'down
		((FormParameterPanel)container).addComponentButton(panelParam);
		//adding in the container (DEPRECATE )
		this.parameterPanels.add(panelParam);
		this.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,2,1,0,position,container,panelParam);
	}
	
	
	/**
	* Add a new panelParam in the JPanel and in a ArrayList
	**/
	public void addParameterObj(Parameters param , JComponent container){
		ParameterPanel panelParam = new ParameterPanel(param);
		
		int position = ((ObjectParameterPanel)container).getParameter().size() + ((ObjectParameterPanel)container).getForm().size()+1;
		
		//adding in the ArrayList of the container
		((ObjectParameterPanel)container).addParameterObj(panelParam);
		//adding in the container (DEPRECATE )
		this.parameterPanels.add(panelParam);
		this.addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,2,1,0,position,container,panelParam);
	}
	
	
	/**
	* Compute the number of phantom to creat to have the exact number asked by the User
	* Update Every FormParameterPanel in the Frame.
	**/
	public void computeNumberOfPhantom(){
		double numbOfPhan = 0;
		// Iterator<ObjectParameterPanel> param = this.paramObjPanel.iterator();
		// while(param.hasNext()){
			// //update the numberOfPhatom value
			// ObjectParameterPanel current = param.next();
			// current.computeNumberOfPhantom();
			
			// double tmp = current.getNumberOfRepetition();
			// numbOfPhan += tmp;
		// }
		// this.actualPhantomNumber.setText(""+(int)numbOfPhan);
		// computeStackSize();
		for ( int i = 0 ; i < parameterPanels.size()-1 ; i++ ){
			for ( int j = i + 1 ; j < parameterPanels.size() ; j++ ){
				numbOfPhan += this.parameterPanels.get(i).getNumberOfRepetition() * this.parameterPanels.get(j).getNumberOfRepetition();
				System.out.println("Numb Of Phan L431 :: "+ numbOfPhan ); 
			}
		}
		this.actualPhantomNumber.setText(""+(int)numbOfPhan);
		computeStackSize();
	}

	
	public double getSumRepetition(int debut, int fin){
		double tmp = 0 ;
		for ( int i=debut ; i < fin ; i++ ){
			tmp += this.parameterPanels.get(i).getNumberOfRepetition();
		}
		return tmp;
	}
	
	public void computeStackSize(){
		int multiply = Integer.parseInt(this.actualPhantomNumber.getText());
		int[] dimStack = this.principal_window.getDimension();
		double tmp = dimStack[0]*dimStack[1]*dimStack[2]*4;
		tmp *= (multiply);
		this.stackSizeAppro.setText(""+(int)(tmp/1000000)+" MB");
	}
	
	
	/**
	* Initialize the creation of the Phantom Image
	**/
	public void creatMulti_form(){
		Iterator<Parameters> param = this.array_parameters.iterator();
		while ( param.hasNext() ){
			Parameters test = param.next();
			System.out.println(test.toString());
		}
		// ArrayList<ArrayList<Integer>> ID = this.getTableIdCount();
		this.principal_window.creatMulti_form(this.array_parameters,this.getActualNumber(),this.isPaired.isSelected());
		this.dispose();
	}
	

	
	public int getNumberOfAskedPhantom(){
		return Integer.parseInt(numbOfPhantom.getText());
	}
	
	public int getActualNumber(){
		return Integer.parseInt(actualPhantomNumber.getText());
	}
	
	
//##############################################################################
	// INNER CLASS #############################################################
	
	/** 
	* ObjectParameterPanel merge all the FormParameterPanel from this ObjectParameterPanel.
	* this class is use to compute and show the number of phantom who will be draw for this Object.
	**/
	class ObjectParameterPanel extends JPanel{
		
		private int posObj;
		private int objID;
		private JLabel numbOfRepetition;
		private JLabel name;
		private ArrayList<ParameterPanel> parameters;
		private ArrayList<FormParameterPanel> formPanel;
		
		/**
		* Constructor Default
		**/
		public ObjectParameterPanel(){
			super(new GridBagLayout());
			buildPane();
			this.parameters = new ArrayList<ParameterPanel>();
			this.formPanel = new ArrayList<FormParameterPanel>();
			TitledBorder title = BorderFactory.createTitledBorder("");
			this.setBorder(title);
		}
		
		
		
		private void buildPane(){
			name = new JLabel("Object : ");
			this.addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,this,name);
			
			numbOfRepetition = new JLabel("000");
			numbOfRepetition.setPreferredSize(numbOfRepetition.getPreferredSize());
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			this.addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,0,this,numbOfRepetition);
			numbOfRepetition.setText("0");
		}
		
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
		
		public int getNumberOfRepetition(){
			return Integer.parseInt(this.numbOfRepetition.getText());
		}
		
		public void setNumberOfRepetition(int numb){
			this.numbOfRepetition.setText(""+numb);
		}
		
		public void addFormPanel(FormParameterPanel form){
			this.formPanel.add(form);
		}
		
		public void addParameterObj(ParameterPanel pane){
			this.parameters.add(pane);
		}
		
		/**
		* Update this ObjectParameterPanel for showing the right numbOfPhantom that will be draw
		**/
		public void computeNumberOfPhantom(){
			double numbOfPhan = 0;
			Iterator<FormParameterPanel> param = this.formPanel.iterator();
			while(param.hasNext()){
				FormParameterPanel current = param.next();
				
				//update FormParameterPanel
				current.computeNumberOfPhantom();
				//get the result
				double tmp = current.getNumberOfRepetition();
				// sum whitout combining 
				numbOfPhan += tmp;
			}
			this.setNumberOfRepetition((int)numbOfPhan);
		}
		
		public ArrayList<FormParameterPanel> getForm(){
			return this.formPanel;
		}
		
		public ArrayList<ParameterPanel> getParameter(){
			return this.parameters;
		}
		
		public int getObjID(){
			return this.objID;
		}
	
		/**
		* Set the panel identity
		**/ 
		public void setIdentity(Parameters param,int ident){
			this.objID = param.getPosObj();
			if ( ident == 0 ){
				this.name.setText(((NodeObject)param.getNode().getParent()).getName()+" : ");
			}
			else if ( ident == 1 ){
				this.name.setText(((NodeObject)param.getNode()).getName()+" : ");
			}
		}
	
	}
	
//#########################################################################################################################################

	/**
	* FormParameterPanel merge all the parameter information, use to compute and show the number of phantom to draw
	**/
	class FormParameterPanel extends JPanel{
	
		private JLabel numbOfRepetition;
		private JLabel name;
		private ButtonUpnDown bouttonUpDown;
		private int formID;
		private ArrayList<ParameterPanel> parameterPanels;
		
		/**
		* Constructor
		**/
		public FormParameterPanel(){
			super(new GridBagLayout());
			buildPane();
			this.parameterPanels = new ArrayList<ParameterPanel>();
			TitledBorder title = BorderFactory.createTitledBorder("Form Modification");
			this.setBorder(title);
		}
		
		
		
		private void buildPane(){
			bouttonUpDown = new ButtonUpnDown("",null);
			JComponent[] component = new JComponent[2];
		
			name = new JLabel("Form : ");
			this.addGridBag(GridBagConstraints.WEST,0,5,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,this,name);
			
			component[0] = name ;
			
			numbOfRepetition = new JLabel("000");
			numbOfRepetition.setPreferredSize(numbOfRepetition.getPreferredSize());
			this.addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,this,numbOfRepetition);
			numbOfRepetition.setText("0");
			
			component[1] = numbOfRepetition;
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.NORTHEAST,0,0,5,5,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,this,bouttonUpDown); 
			
			bouttonUpDown.setComponent(component);
			bouttonUpDown.setSelected(true);
		}
		
		
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
		
		
		
		public void addComponentButton(JComponent component){
			this.bouttonUpDown.addComponent(component);
			this.bouttonUpDown.setSelected(true);
		}
		
		
		
		public int getNumberOfRepetition(){
			return Integer.parseInt(this.numbOfRepetition.getText());
		}
		
		
		public void setNumberOfRepetition(int numb){
			this.numbOfRepetition.setText(""+numb);
		}
		
		
		
		/**
		* Compute the number of phantom to creat to have same order number asked by the User
		* for this Form
		**/
		public void computeNumberOfPhantom(){
			double numbOfPhan = 0;
			if ( isPaired.isSelected() ){
				// compute by paire
				for ( int i=0 ; i <  parameterPanels.size()  ; i++ ){
					numbOfPhan += parameterPanels.get(i).getNumberOfRepetition() * this.getSumRepetition(i,parameterPanels.size());
				}
				System.out.println("Numb of phantom : "+numbOfPhan);
				// numbOfPhan = Math.pow(numbOfPhan,2);
				// numbOfPhan = 2*numbOfPhan;
			}
			else{
				// megaMix
				Iterator<ParameterPanel> param = this.parameterPanels.iterator();
				while(param.hasNext()){
					double tmp = param.next().getNumberOfRepetition();
					if ( numbOfPhan == 0 ){
						numbOfPhan += tmp;
					}
					else{
						numbOfPhan *= tmp ;
					}
				}
			}
			this.setNumberOfRepetition((int)numbOfPhan);
		}
		
		
		/**
		* Get the sum between two position in a table
		**/
		public double getSumRepetition(int debut, int fin){
			double tmp = 0 ;
			if ( debut == fin ){
				tmp = this.parameterPanels.get(debut).getNumberOfRepetition();
			}
			else{
				for ( int i=debut ; i < fin ; i++ ){
					tmp += this.parameterPanels.get(i).getNumberOfRepetition();
				}
			}
			return tmp;
		}
		
		public void addParameter(ParameterPanel pane){
			this.parameterPanels.add(pane);
		}
		
		public ArrayList<ParameterPanel> getParameter(){
			return this.parameterPanels;
		}
		
		public int getFormID(){
			return this.formID;
		}
		
		/**
		* Set the panel identity
		**/ 
		public void setIdentity(Parameters param){
			this.formID = param.getPosForm();
			this.name.setText(((NodeForm)param.getNode()).getName()+" : ");
		}
		
	}

//#########################################################################################################################################
	
	/**
	* ParameterPanel, basic Panel where user can choose to update the number of phantom to draw
	**/
	class ParameterPanel extends JPanel{
		// ID of this parameter
		private int ID;
		private Parameters parameter;
		private JLabel numbOfRepetition;
	
		public ParameterPanel(Parameters param){
			super(new GridBagLayout());
			this.ID = param.getIdParam();
			this.parameter = param;
			buildPane();
			TitledBorder title = BorderFactory.createTitledBorder("");
			this.setBorder(title);
		}
		
		private String getNameFromID(){
			String name ="";
			switch(this.ID){
				case 0:
					name = "<html><ul><li>X origine Transformation</li></ul></html>";
					break;
				case 1:
					name = "<html><ul><li>Y origine Transformation</li></ul></html>";
					break;
				case 2:
					name = "<html><ul><li>Z origine Transformation</li></ul></html>";
					break;
				case 3:
					name = "<html><ul><li>Phi Transformation</li></ul></html>";
					break;
				case 4:
					name = "<html><ul><li>Theta Transformation</li></ul></html>";
					break;
				case 5:
					name = "<html><ul><li>Psi Transformation</li></ul></html>";
					break;
				case 6:
					name = "<html><ul><li>Dimension X transformation</li></ul></html>";
					break;
				case 7:
					name = "<html><ul><li>Dimension Y transformation</li></ul></html>";
					break;
				case 8:
					name = "<html><ul><li>Dimension Z transformation</li></ul></html>";
					break;
				case 10:
					name = "<html><ul><li>X origine Transformation</li></ul></html>";
					break;
				case 11:
					name = "<html><ul><li>Y origine Transformation</li></ul></html>";
					break;
				case 12:
					name = "<html><ul><li>Z origine Transformation</li></ul></html>";
					break;
				case 13:
					name = "<html><ul><li>Phi Transformation</li></ul></html>";
					break;
				case 14:
					name = "<html><ul><li>Theta Transformation</li></ul></html>";
					break;
				case 15:
					name = "<html><ul><li>Psi Transformation</li></ul></html>";
					break;
				case 16:
					name = "<html><ul><li>Dimension X transformation</li></ul></html>";
					break;
				case 17:
					name = "<html><ul><li>Dimension Y transformation</li></ul></html>";
					break;
				case 18:
					name = "<html><ul><li>Dimension Z transformation</li></ul></html>";
					break;
				default:
					name = "UnIdentified";
					break;
			}
			return name;
		}
		
		private void buildPane(){
			JLabel label = new JLabel(this.getNameFromID());
			this.addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,this,label);
		
			label = new JLabel("Number of different value : ");
			this.addGridBag(GridBagConstraints.CENTER,0,60,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);
			
			numbOfRepetition = new JLabel("000");
			numbOfRepetition.setPreferredSize(numbOfRepetition.getPreferredSize());
			this.addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,1,this,numbOfRepetition);
			numbOfRepetition.setText("0");
			
			JPanel pane = new JPanel(new GridBagLayout());
			BasicArrow bNorth = new BasicArrow(BasicArrowButton.NORTH,numbOfRepetition);
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,pane,bNorth);
			BasicArrow bSouth = new BasicArrow(BasicArrowButton.SOUTH,numbOfRepetition);
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,pane,bSouth);

			this.addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,2,2,0,this,pane);
		}
		
		public int getNumberOfRepetition(){
			return Integer.parseInt(this.numbOfRepetition.getText());
		}
		
		public void setNumberOfRepetition(int numb){
			this.numbOfRepetition.setText(""+numb);
			//change the corresponding parameter to
			this.parameter.setNumbofTransformation(numb);
		}
		
	
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
	}
	
	
	//#########################################################################################################################################
	
	
	
	/**
	* Basic Arrow display, fire the updating of the number of phantom to draw
	**/
	class BasicArrow extends BasicArrowButton{
		private JLabel label;
	
		public BasicArrow(int direction,JLabel label){
			super(direction);
			this.label = label;
		}
		
		public void fireActionPerformed(ActionEvent e) {
			if ( direction == BasicArrowButton.NORTH ){
				int pos = Integer.parseInt(label.getText());
				pos+=1;
				label.setText(""+pos);
				if ( getActualNumber() > getNumberOfAskedPhantom() ){
					pos-=1;
				}
				if ( pos < 1 ){
					pos +=1;
				}
				label.setText(""+pos);
			}
			else if ( direction == BasicArrowButton.SOUTH ){
				int pos = Integer.parseInt(label.getText());
				pos-=1;
				if ( pos < 1 ){
					pos+=1;
				}
				label.setText(""+pos);
			}
			computeNumberOfPhantom();
		}
	}
 	
}