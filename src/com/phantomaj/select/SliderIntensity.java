/**
* SliderIntensity.java
* 12/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;


import com.phantomaj.PhantoMaJ;
import com.phantomaj.component.BasicInterfaceComponent_IntensityPanel;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;



public class SliderIntensity extends JSlider implements ChangeListener{

	private JLabel label;
	
	private PhantoMaJ principal_window ;
	private BasicInterfaceComponent_IntensityPanel basic_component = null;
	
	// identifier == 0,1 => tab 1, tab2
	// 2,3 => meanXdim, rangeX
	// 4,5 => meanYdim, RangeY
	// 6,7 => meanZdim, Range Z
	private int identifier;
	private boolean bol = true;

	/**
	* Default Constructor for RandomGeneratorFrame
	**/
	public SliderIntensity(int orientation, int min, int max, int value, JLabel label, BasicInterfaceComponent_IntensityPanel basic_component, int identifier){
		super(orientation,min,max,value);
		this.basic_component = basic_component ;
		this.label = label;
		this.identifier = identifier ;
		this.setMajorTickSpacing((int)((max-min)/4));
		this.setMinorTickSpacing((int)(max-min)/20);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
	}
	
	/**
	* Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		if ( bol ){
			basic_component.stateChangedSliderIntensity(this.identifier);
		}
    }

	public void setValue_(int nv){
		this.bol = false;
		this.setValue(nv);
		this.bol = true;
	}
	
}