package com.phantomaj.select;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;

import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CenterRotationCheckBox extends JCheckBox implements ActionListener{

	private BasicInterfaceComponent_ObjectPanel basicInterface;
	private BasicInterfaceComponent_FormPanel basicInterface2;
	private int identifier;
	
	public CenterRotationCheckBox(String name,int identifier, BasicInterfaceComponent_ObjectPanel bi){
		super(name);
		this.identifier = identifier;
		this.basicInterface = bi;
		this.setSelected(false);
		this.addActionListener(this);
	}
	
	public CenterRotationCheckBox(String name, BasicInterfaceComponent_FormPanel bi, int identifier){
		super(name);
		this.identifier = identifier;
		this.basicInterface2 = bi;
		this.basicInterface = null;
		this.setSelected(false);
		this.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if ( basicInterface != null ){
			basicInterface.setCenterRotationChangeVisible(this.isSelected(),this.identifier);	
		}
		else {
			basicInterface2.setCenterRotationChangeVisible(this.isSelected(),this.identifier);
		}
	}
}