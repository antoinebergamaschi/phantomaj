
/**
* VTextField.java
* 08/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;
import com.phantomaj.PhantoMaJ;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.JTextField;
import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.KeyboardFocusManager;
import java.util.regex.Pattern;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* used in RandGlobalParameters, top delete.
**/
public class VTextField extends JTextField implements KeyListener , FocusListener{

	private PhantoMaJ principal_window=null;
	private RandomGeneratorFrame rand_window = null ;
	private int identifier;
	private Color BgColor = Color.yellow;

//######################################################################################
//Constructor	
	
	/**
	* TextField specified in number. No character alpha authorized 
	* @param String, Text to display
	* @param int, number of colonne to display the text
	* @param int, identifier tell where is this VTextField in the phanto_maJ Frame
	* @param Phanto_maJ, the Frame that display this VTextField
	**/
	public VTextField(String str,int col,int identifier, PhantoMaJ principal_window){
		super(str,col);
		this.identifier = identifier;
		this.principal_window = principal_window;
		// this.addActionListener(this);
		this.addKeyListener(this);
		this.addFocusListener(this);
	}

	
	public VTextField(String str,int col, RandomGeneratorFrame rand_window){
		super(str,col);
		this.rand_window = rand_window;
		// this.addActionListener(this);
		this.addKeyListener(this);
		this.addFocusListener(this);
	}

//###########################################################################################
//Methode
	
	/**
	* @Override KeyListener.keyPressed()
	**/	
	public void keyPressed(KeyEvent e){
	}

	/**
	* @Override KeyListener.keyReleased()
	**/	
	public void keyReleased(KeyEvent e){
		if ( this.rand_window == null ){
			// // if Enter is typed
			// if ( e.getKeyCode() == KeyEvent.VK_ENTER ){
				// KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
				// focusManager.clearGlobalFocusOwner();	
			// }
			// // if Identifier == -1 this is a Object value
			// else if ( this.identifier < 0 ){
					// if ( this.getText().length()  >= 1 && !Pattern.matches(".*[^0-9].*", this.getText()) ){
						// principal_window.setNewMax(Integer.parseInt(this.getText()),this.identifier);
						// principal_window.applyO();
					// }
				// // }
			// }
		}
		else{
			if ( e.getKeyCode() == KeyEvent.VK_ENTER ){
				KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
				focusManager.clearGlobalFocusOwner();	
			}
		}
	}
	
	/**
	* @Override KeyListener.keyTyped()
	**/	
	public void keyTyped(KeyEvent e){
	} 

	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.setBackground(BgColor);
		this.selectAll();
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		if ( this.rand_window == null ){
			// If the Text is empty or if the is something other than a Number in the Text
			if(this.getText().length()  < 1 || Pattern.matches(".*[^0-9].*", this.getText())){
				this.BgColor = Color.red;
				KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
				focusManager.clearGlobalFocusOwner();
				this.requestFocusInWindow();
			}
			else{
				this.BgColor = Color.yellow;
				// principal_window.setNewMax(Integer.parseInt(this.getText()),this.identifier);
				this.setBackground(Color.white);
			}
		}
		// case its in the randomGeneratorFrame
		else{
			if(this.getText().length()  < 1 || Pattern.matches(".*[^0-9].*", this.getText())){
				this.BgColor = Color.red;
				KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
				focusManager.clearGlobalFocusOwner();
				this.requestFocusInWindow();
			}
			else{
				this.BgColor = Color.yellow;
				this.setBackground(Color.white);
			}
		}
	}

}
