
/**
* ATextField.java
* 20/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;
import com.phantomaj.PhantoMaJ;


import javax.swing.JTextField;
import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.KeyboardFocusManager;
import java.util.regex.Pattern;

import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* identifier = {0,1,2} for {x,y,z} and -1 for (pixel,layer), and -2 background value
**/
public class ATextField extends JTextField implements KeyListener, FocusListener{

    private Color BgColor = Color.yellow;
	private MultiPhantomOptionFrame _MpOF;

	public ATextField(String str,int col, PhantoMaJ principal_window){
		super(str,col);
        PhantoMaJ principal_window1 = principal_window;
		// this.addActionListener(this);
		this.addKeyListener(this);
		this.addFocusListener(this);
	}

	public ATextField(String str,int col, MultiPhantomOptionFrame _MpOF){
		super(str,col);
		this._MpOF = _MpOF;
		// this.addActionListener(this);
		this.addKeyListener(this);
		this.addFocusListener(this);
	}

	
	/**
	* @Override KeyListener.keyPressed()
	**/	
	public void keyPressed(KeyEvent e){
	}

	/**
	* @Override KeyListener.keyReleased()
	**/	
	public void keyReleased(KeyEvent e){
		if ( e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_CANCEL || e.getKeyCode() == KeyEvent.VK_TAB){
			KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			focusManager.clearGlobalFocusOwner();
		}
		// System.out.println(this.getText());
		if ( this.getText().length() > 0){
			_MpOF.setNumberOfDif();
		}
	}
	
	/**
	* @Override KeyListener.keyTyped()
	**/	
	public void keyTyped(KeyEvent e){
	} 
	


	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.setBackground(BgColor);
		_MpOF.setNumberOfDif();		
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		if(Pattern.matches(".*[^0-9].*", this.getText()) || this.getText().length() < 1 ){
			this.BgColor = Color.red;
			KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			focusManager.clearGlobalFocusOwner();
			this.requestFocusInWindow();
		}
		else{
			this.BgColor = Color.yellow;
			this.setBackground(Color.white);
		}
	}

}
