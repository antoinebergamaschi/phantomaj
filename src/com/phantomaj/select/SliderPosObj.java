/**
* SliderPosObj.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;

// import PhantoMaJ.PhantoMaJ;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;

import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderPosObj extends JSlider implements ChangeListener, FocusListener{

	private BasicInterfaceComponent_ObjectPanel basicInterface;
	private int identifier;
	private int [] tab;
	private final static int Xpos = 0;
	private final static int Ypos = 1;
	private final static int Zpos = 2;
	private JLabel label;
	private boolean check = true ;

	public SliderPosObj(int min,int max,int identifier,JLabel label,BasicInterfaceComponent_ObjectPanel bi){
		super(JSlider.HORIZONTAL,min,max,0);
		this.identifier = identifier;
		this.label = label;
		this.basicInterface = bi ;
		this.setMajorTickSpacing((int)((max-min)/4));
		this.setMinorTickSpacing((int)(max-min)/20);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}

	public void setTab(int[] tab){
		this.tab = tab;
	}
	
	/**
	* Special version of setMaximum
	**/
	public void setMaximum_(int max){
		this.check = false ;
		this.setMaximum(max);
		this.check = true ;
	}
	
	/**
	* Special version of setMinimum
	**/ 
	public void setMinimum_(int min){
		this.check = false ;
		this.setMinimum(min);
		this.check = true ;
	}
	
	/**
	* Special version of setValue
	**/
	public void setValue_(int val){
		this.check = false ;
		this.setValue(val);
		this.check = true ;
	}
	
	/**
	* @Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		this.label.setText(""+this.getValue());
		if ( this.check ){
			this.basicInterface.setUpdatePositionO(this.identifier,this.getValue());
		}
	}
	
	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.label.setForeground(Color.red);
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		this.label.setForeground(Color.black);
	}
	
	
}