 
/**
* SliderDim.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;


import com.phantomaj.PhantoMaJ;
import com.phantomaj.randomGenerator.RandGlobalParametersPosition;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;


import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderDim extends JSlider implements ChangeListener, FocusListener{

	private JLabel label;
	
	private PhantoMaJ principal_window ;

	private RandGlobalParametersPosition basic_component2 = null;
	// identifier == 0,1 => tab 1, tab2
	// 2,3 => meanXdim, rangeX
	// 4,5 => meanYdim, RangeY
	// 6,7 => meanZdim, Range Z
	private int identifier;
	
	/**
	* Default Constructor for PhantoMaJ Frame
	**/
	public SliderDim(int orientation, int min, int max, int value, JLabel label, PhantoMaJ principal_window, int identifier){
		super(orientation,min,max,value);
		this.principal_window = principal_window ;
		this.label = label;
		this.identifier = identifier ;
		if ( (max-min) < 400 ){
			this.setMajorTickSpacing((int)((max-min)/4));
			this.setMinorTickSpacing((int)(max-min)/20);
		}
		else{
			this.setMajorTickSpacing((int)((max-min)/4));
			// this.setMinorTickSpacing((int)(max-min)/20);
		}
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}

	
	/**
	* Default Constructor for RandGlobalParametersPosition
	**/
	public SliderDim(int orientation, int min, int max, int value, JLabel label, RandGlobalParametersPosition basic_component, int identifier,int z){
		super(orientation,min,max,value);
		this.basic_component2 = basic_component ;
		this.label = label;
		this.identifier = identifier ;
		if ( (max-min) < 400 ){
			this.setMajorTickSpacing((int)((max-min)/4));
			this.setMinorTickSpacing((int)(max-min)/20);
			this.setPaintTicks(true);
		}
		else{
			this.setMajorTickSpacing((int)((max-min)/4));
			this.setMinorTickSpacing((int)(max-min)/10);
			// this.setPaintTicks(false);
		}
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}
	
	
	/**
	* Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {

		if ( this.label != null ){
			String test = Integer.toString(this.getValue());
			this.label.setText(test);
		}

		if ( basic_component2 !=null){
			basic_component2.stateChangedSliderDim(this.identifier);	
		}
    }

	/**
	* Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.red);
		}
	}

	/**
	* Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		//case in the RandomGeneratorFrame use in the BasicInterface_dimPanelRange
		if ( this.label != null ){
			this.label.setForeground(Color.black);
		}
	}

	
	public void setMaximum_(int newMax){
		this.setPaintLabels(false);
		if ( newMax < 400 ){
			this.setMajorTickSpacing((int)((newMax)/4));
			this.setMinorTickSpacing((int)(newMax)/20);
		}
		else{
			this.setMajorTickSpacing((int)((newMax)/2));
			this.setMinorTickSpacing((int)(newMax)/10);
		}
		super.setMaximum(newMax);
		this.updateUI();
		this.repaint();
	}
	
	
}
