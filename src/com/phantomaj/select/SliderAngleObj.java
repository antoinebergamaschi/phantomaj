/**
* SliderAngleObj.java
* 18/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.JLabel;


import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;

public class SliderAngleObj extends JSlider implements ChangeListener, FocusListener{

	private JLabel label;
	private BasicInterfaceComponent_ObjectPanel basicInterface;
	// private int[][] ori;
	// private int[] center;
	// private int[][] angle;
	private boolean check = true ;
	private int identifier;
	
	public SliderAngleObj(int orientation, int min, int max, int value, JLabel label,int identifier, BasicInterfaceComponent_ObjectPanel bi){
		super(orientation,min,max,value);
		this.basicInterface = bi ;
		this.label = label;
		this.identifier = identifier;
		this.setMajorTickSpacing(90);
		this.setMinorTickSpacing(10);
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.addChangeListener(this);
		this.addFocusListener(this);
	}

	// public void setOri(int[][] ori){
		// this.ori = ori ;
	// }
	
	// public int[][] getOri(){
		// return this.ori;
	// }
	
	// public void setCenter(int[] center){
		// this.center = center ;
	// }
	
	// public int[] getCenter(){
		// return this.center;
	// }
	
	// public void setAngle(int[][] angle){
		// this.angle = angle ;
	// }
	
	// public int[][] getAngle(){
		// return this.angle;
	// }
	
	/**
	* @Override ChangeListener.stateChanged()
	**/
	public void stateChanged(ChangeEvent e) {
		String test = Integer.toString(this.getValue());
		this.label.setText(test);
		if ( check ){
			this.basicInterface.setUpdateRotationO();
		}
    }

	
	/**
	* Special version of setValue
	**/
	public void setValue_(int val){
		this.check = false ;
		this.setValue(val);
		this.check = true ;
	}
	
	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.label.setForeground(Color.red);
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		this.label.setForeground(Color.black);
	}

}
