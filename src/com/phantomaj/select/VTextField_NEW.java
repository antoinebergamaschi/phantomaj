
/**
* VTextField_NEW.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;
import com.phantomaj.component.BasicInterfaceComponent_rootPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_rootPanel;
// import PhantoMaJ.PhantoMaJ;
// import PhantoMaJ.randomGenerator.RandomGeneratorFrame;

import javax.swing.JTextField;
import javax.swing.JComponent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusListener;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.KeyboardFocusManager;
import java.util.regex.Pattern;
import java.awt.Event;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

/**
* identifier = {0,1,2} for {x,y,z} -2 pixelValue Object, 3 background value , 4 noiseValue 
**/
public class VTextField_NEW extends JTextField implements KeyListener , FocusListener{


	private JComponent parent = null ;
	private int identifier;
	private Color BgColor = Color.yellow;

//######################################################################################
//Constructor	
	
	/**
	* TextField specified in number. No character alpha authorized 
	* @param String, Text to display
	* @param int, number of colonne to display the text
	* @param int, identifier tell where is this VTextField in the phanto_maJ Frame
	* @param Phanto_maJ, the Frame that display this VTextField
	**/
	public VTextField_NEW(String str,int col,int identifier, JComponent parent){
		super(str,col);
		this.identifier = identifier;
		this.parent = parent;
		this.addKeyListener(this);
		this.addFocusListener(this);
	}


//###########################################################################################
//Methode
	
	/**
	* @Override KeyListener.keyPressed()
	**/	
	public void keyPressed(KeyEvent e){
	}

	/**
	* @Override KeyListener.keyReleased()
	**/	
	public void keyReleased(KeyEvent e){
			// if Enter is typed
			if ( e.getKeyCode() == KeyEvent.VK_ENTER ){
				KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
				focusManager.clearGlobalFocusOwner();
			}
			// if Identifier == -1 this is a Object value
			// else if ( this.identifier < 0 ){
				// if ( this.getText().length()  >= 1 && !Pattern.matches(".*[^0-9].*", this.getText()) ){
					// // principal_window.setNewMax(Integer.parseInt(this.getText()),this.identifier);
					// // this.callUpdate();
					// // principal_window.applyO();
				// }
			// }
	}
	
	/**
	* @Override KeyListener.keyTyped()
	**/	
	public void keyTyped(KeyEvent e){
	} 

	/**
	* @Override FocusListener.focusGained
	**/
	public void focusGained(FocusEvent e){
		this.setBackground(BgColor);
		this.selectAll();
	}

	/**
	* @Override FocusListener.focusLost
	**/
	public void focusLost(FocusEvent e){
		// If the Text is empty or if the is something other than a Number in the Text
		if(this.getText().length()  < 1 || Pattern.matches(".*[^0-9].*", this.getText())){
			this.BgColor = Color.red;
			KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
			focusManager.clearGlobalFocusOwner();
			this.requestFocusInWindow();
		}
		else{
			this.BgColor = Color.yellow;
			//case of the StackDimension
			// if ( this.identifier >= 0 ){
				this.callUpdate();
			// }
			this.setBackground(Color.white);
		}
	}
	
	
	private void callUpdate(){
		switch(this.identifier){
			case -1:
				System.out.println("Case -1 VTextField L123");
				break;
			//Case for the PixelValue in the BasicInterfaceComponent_ObjectPanel
			case -2:
				if ( parent != null && parent instanceof BasicInterfaceComponent_ObjectPanel){
					((BasicInterfaceComponent_ObjectPanel)parent).setUpdatePixelValueO(Integer.parseInt(this.getText()));
				}
				else {
					System.out.println("WARNING ::: instaceof "+parent.getClass() +" is not recognize select.VTextField_NEW.callUpdate");
				}
				break;
			//Case for background Value
			case 3:
				((BasicInterfaceComponent_rootPanel)parent).setUpdateBackgroundValue(Integer.parseInt(this.getText()));
				break;
			//Case for Noise Value
			case 4:
				((BasicInterfaceComponent_rootPanel)parent).setUpdateNoiseValue(Integer.parseInt(this.getText()));
				break;
			default:
				//Case StackDimension in the rootPanel
				if ( parent != null && parent instanceof BasicInterfaceComponent_rootPanel ){
					((BasicInterfaceComponent_rootPanel)parent).setUpdateDim(this.identifier,Integer.parseInt(this.getText()));
				}
				else {
					System.out.println("WARNING ::: instaceof "+parent.getClass() +" is not recognize select.VTextField_NEW.callUpdate");
				}
				break;
		}
	}

}