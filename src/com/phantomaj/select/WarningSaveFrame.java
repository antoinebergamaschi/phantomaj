/**
* WarningSaveFrame.java
* 06/04/2012
* @author Antoine Bergamaschi
**/
package com.phantomaj.select;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.button.ButtonFindDest;
import com.phantomaj.filechooser.Utils;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JTextField;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.io.File;

public class WarningSaveFrame extends JFrame{
	
	private JTextField dest_Text;
	private PhantoMaJ principal_window;
	private RandomGeneratorFrame rand_window;
	private Button_Ok ok_button;
	
	//Constructor
	
	public WarningSaveFrame(String title,PhantoMaJ principal_window){
		super(title);
		this.principal_window = principal_window;
		this.rand_window = null;
		this.build();
	}	
	
	public WarningSaveFrame(String title, RandomGeneratorFrame rand_window){
		super(title);
		this.principal_window = null;
		this.rand_window = rand_window;
		this.build();
	}
	
	//Private Methode
	
	private void build(){ 
		this.setResizable(false); 
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(buildContentPane());	
		// this.setMinimumSize(new Dimension(200,200));
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		this.pack();
		this.setVisible(true);
	}

	private JPanel buildContentPane(){
		//*********************************
		//Variable
		JLabel label = new JLabel();
		JPanel panel = new JPanel();
		int width = 0;
		int position = 0;
		int height = 0;
		
		dest_Text = new JTextField("Enter where to save your data", 30);
		ok_button = new Button_Ok("OK",this);
		ButtonFindDest find_button = new ButtonFindDest("",this);
		
//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("folder"));
//		Image image = Toolkit.getDefaultToolkit().getImage(url);
		
		find_button.setIcon(Utils.getResourceIcon("folder"));
		//*********************************
		
		JPanel principal = new JPanel(new GridBagLayout()); 
		
		label = new JLabel("<HTML><h1>WARNING</h1></HTML>");
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,200,0,200,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,principal,label);
		height += label.getPreferredSize().height;

		
		label = new JLabel("<html><ul><li style=\"color:red\"><p style=\"color:red\">Warning 0 : </p></li></ul><html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,principal,label);
		height += label.getPreferredSize().height;

		label = new JLabel("<html><p style=\"color:red\">In order to save the phantom, a Destination File should be notified.</p></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,60,0,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,2,principal,label);
		width += label.getPreferredSize().width;
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.EAST,10,60,10,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,principal,dest_Text);	
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.WEST,10,5,10,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,3,principal,find_button);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,200,0,200,GridBagConstraints.BOTH,0,0,0,0,2,1,0,4,principal,ok_button);
		
		this.setMinimumSize(new Dimension(width,height));
		return principal;
	}

	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	//Public Methode
	
	public void setDestText(String str){
		this.dest_Text.setText(str);
		File f = new File(str);
		if ( f.exists() && f.isDirectory() ){
			this.ok_button.setEnabled(true);
			if ( this.rand_window == null ){
				this.principal_window.setDestText(str);
			}
			else{
				this.rand_window.setPathFolder(str);
			}
		}
	}
	
	public void openWarningFrame(){
		if ( this.rand_window == null ){
			this.principal_window.openWarningFrame();
		}
		else{
			this.rand_window.creatStack();
		}
	}
	
	// INNER CLASS
	
	class Button_Ok extends JButton{
		private WarningSaveFrame _WsF;
		
		private final int _NoDest = 0 ;
		private final int _NoParam = 1 ;
		private final int _NoSaveParam = 2 ;
		private final int _NoName = 3 ;
	
		public Button_Ok(String name, WarningSaveFrame _WsF){
			super(name);
			this._WsF = _WsF ;
			this.setEnabled(false);
		}
		
		/**
		* @override AbstractButton.fireActionPerformed
		**/
		public void fireActionPerformed(ActionEvent e) {
			// always true
			if ( 0 == this._NoDest ){
				this._WsF.dispose();
				this._WsF.openWarningFrame();
				// this._WsF.dispose();
			}
			else{
				this._WsF.dispose();
			}
		}
	}
}