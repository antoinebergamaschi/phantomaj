/**
* TransfoCheckBox_NEW.java
* 15/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.select;

// import PhantoMaJ.PhantoMaJ;
// import PhantoMaJ.randomGenerator.RandomGeneratorFrame;
import com.phantomaj.component.BasicInterface;
import com.phantomaj.component.BasicInterfaceComponent_Second_rootPanel;
import com.phantomaj.component.BasicInterfaceComponent_rotPanel;
import com.phantomaj.component.BasicInterfaceComponent_dimObjPanelRange;
import com.phantomaj.component.BasicInterfaceComponent_IntensityPanel;
import com.phantomaj.component.BasicInterfaceComponent_posPanel;
import com.phantomaj.component.BasicInterfaceComponent_dimFormPanelRange;
import com.phantomaj.filechooser.Utils;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import java.util.ArrayList;

public class TransfoCheckBox_NEW extends JLabel implements MouseListener, MouseMotionListener{

	private JComponent[] component;
	private boolean isSelected;
	private int identifier;

	
	private ImageIcon greenButton;
	private ImageIcon redButton;
	private ImageIcon yellowButton;
	private ImageIcon purpleButton;

	private ArrayList<JComponent> listeners = new ArrayList<JComponent>();
	
	private int secondID = 0;
	
	public TransfoCheckBox_NEW(String name, JComponent[] component,int identifier){
		super(name);

//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("redButton"));
        redButton = Utils.getResourceIcon("redButton");

//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("greenButton"));
        greenButton = Utils.getResourceIcon("greenButton");

//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("purpleButton"));
        purpleButton = Utils.getResourceIcon("purpleButton");

//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("yellowButton"));
        yellowButton = Utils.getResourceIcon("yellowButton");
		
		this.identifier = identifier;
		this.component = component;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setIcon(redButton);
	}


	public void mouseClicked(MouseEvent e){
		if ( e.getX() <= this.getIcon().getIconWidth() && e.getY() <= this.getIcon().getIconHeight()){
			if ( this.isSelected ){
				this.isSelected = false;
				this.setIcon(redButton);
			}
			else{
				this.isSelected = true;
				this.setIcon(greenButton);
			}
			this.setEnableComponent();

			//Update every listeners
			for(int i = 0 ; i < this.listeners.size() ; i++ ){
				JComponent comp = listeners.get(i);
				if ( comp instanceof BasicInterfaceComponent_Second_rootPanel){
					//Fire event with ID BACKGROUND_ROOT or NOISE_ROOT
					if ( this.identifier == BasicInterface.BACKGROUND_ROOT || this.identifier == BasicInterface.NOISE_ROOT ){
						// ((BasicInterfaceComponent_Second_rootPanel)comp).setInfoSelected(this.identifier);
						((BasicInterfaceComponent_Second_rootPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				else if ( comp instanceof BasicInterfaceComponent_rotPanel){
					if ( this.identifier == BasicInterface.SLIDER_PHI_R || this.identifier == BasicInterface.SLIDER_THETA_R || this.identifier == BasicInterface.SLIDER_PSI_R){
						// ((BasicInterfaceComponent_Second_rootPanel)comp).setInfoSelected(this.identifier);
						((BasicInterfaceComponent_rotPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else if (this.identifier == BasicInterface.SLIDER_PHI_N || this.identifier == BasicInterface.SLIDER_THETA_N || this.identifier == BasicInterface.SLIDER_PSI_N ){
						((BasicInterfaceComponent_rotPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				else if ( comp instanceof BasicInterfaceComponent_dimObjPanelRange){
					if ( this.identifier == BasicInterface.SLIDER_SIZE_R ){
						((BasicInterfaceComponent_dimObjPanelRange)comp).fireInfoSelected(this.identifier,secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				else if ( comp instanceof BasicInterfaceComponent_IntensityPanel){
					if ( this.identifier == BasicInterface.SLIDER_OBJ_INT || this.identifier == BasicInterface.SLIDER_RAND_INT ){
						((BasicInterfaceComponent_IntensityPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				else if ( comp instanceof BasicInterfaceComponent_posPanel ){
					if ( this.identifier == BasicInterface.SLIDER_POSX_R || this.identifier == BasicInterface.SLIDER_POSY_R || this.identifier == BasicInterface.SLIDER_POSZ_R ){
						((BasicInterfaceComponent_posPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else if ( this.identifier == BasicInterface.SLIDER_POSX_N || this.identifier == BasicInterface.SLIDER_POSY_N || this.identifier == BasicInterface.SLIDER_POSZ_N ){
						((BasicInterfaceComponent_posPanel)comp).fireInfoSelected(this.identifier,secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				else if ( comp instanceof BasicInterfaceComponent_dimFormPanelRange ){
					if ( this.identifier == BasicInterface.SLIDER_DIM_N ){
						((BasicInterfaceComponent_dimFormPanelRange)comp).fireInfoSelected(this.identifier,this.secondID);
					}
					else {
						System.out.println("WARNING ::: Wrong Identifier ("+this.identifier+") TransfoCheckBox_NEW.mouseClicked");
					}
				}
				// else if ( comp instanceof PhantoMaJ.randomGenerator.RandomGeneratorFrame ){
					// ((RandomGeneratorFrame)comp).updateCheck();	
				// }
				else{
					System.out.println("WARNING ::: instanceof "+comp.getClass() +" is not recognize select.TransfoCheckBox_NEW.mouseClicked");
				}
			}
		}
	}
	
	public void setSecondeID(int ne){
		this.secondID = ne;
	}
	
	public void mouseEntered(MouseEvent e){
	}

	public void mouseExited(MouseEvent e){
		if ( this.isSelected ){
			this.setIcon(greenButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/greenButton.png"));
		}
		else{
			this.setIcon(redButton);
			// this.setIcon(new ImageIcon("./PhantoMaJ/image/redButton.png"));
		}
	}

	public void mousePressed(MouseEvent e){

	}
	
	public void mouseReleased(MouseEvent e){
	
	}
	
	public void mouseDragged(MouseEvent e){
	
	}
	
	public void mouseMoved(MouseEvent e){
		if ( e.getX() <= this.getIcon().getIconWidth() && e.getY() <= this.getIcon().getIconHeight()){
			if ( this.isSelected ){
				this.setIcon(this.yellowButton);
			}
			else{
				this.setIcon(this.purpleButton);
			}
		}
	}
	
	public void setSelected(boolean bol){
		this.isSelected = bol ;
		this.setEnableComponent();
		if( this.isSelected ){
			this.setIcon(greenButton);
		}
		else{
			this.setIcon(redButton);
		}
	}
	
	public boolean getIsSelected(){
		return this.isSelected;
	}
	
	public void setComponentTab(JComponent[] component){
		this.component = component;
	}
	
	public void setEnableComponent(){
		for(int i = 0 ; i < this.component.length ; i++ ){
			this.component[i].setEnabled(this.isSelected);
		}
	}
	
	public void addListener(JComponent component){
		this.listeners.add(component);
	}
}