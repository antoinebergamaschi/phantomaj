/**
* PhantoMaJ
* 27/02/2012
* @author Antoine Bergamaschi
* @version 1.0
**/

package com.phantomaj;

//Local Imports

import com.phantomaj.button.*;
import com.phantomaj.component.BasicInterface;
import com.phantomaj.filechooser.Utils;
import com.phantomaj.form.CreateForm;
import com.phantomaj.menu.MenuAbout;
import com.phantomaj.menu.MenuOption;
import com.phantomaj.menu.MenuOptionDraw;
import com.phantomaj.menu.MenuRandom;
import com.phantomaj.obj3D.Object3D;
import com.phantomaj.obj3D.Parameters;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;
import com.phantomaj.select.MultiPhantomOptionFrame;
import com.phantomaj.select.WarningSaveFrame;
import com.phantomaj.tree.NodeForm;
import com.phantomaj.tree.NodeObject;
import com.phantomaj.tree.TreeDragAndDrop;
import com.phantomaj.utils.*;
import com.phantomaj.view3D.Panel3D;
import ij.ImageJ;
import ij.plugin.PlugIn;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
//Java imports
//ImageJ import 

/**
* Principal controler Frame, PhantoMaJ is the class implementing ImageJ PlugIn interface. This Class act like a controleur for the TreeDragAndDrop class that
* store data (NodeForm and NodeObject), Object3D class that is the algorithmic part of the programme (creation of banks), the Panel3D class that store the Form3D
* responsible for the visualisation and finally the BasicInterface class which is basically the interface.
**/
public class PhantoMaJ extends JFrame implements PlugIn{

//    public static ResourceBundle resourceBundle = ResourceBundle.getBundle("PhantomaJ");

	private boolean lock_setTree = true;
	private boolean lock_apply = true ;
	private boolean _stateChangedSilder = true ;
	
	private BasicInterface basicInterface=new BasicInterface();
	
	private Thread_option Toption = new Thread_option();
	private Compute_Intensity Cint = new Compute_Intensity();
	
	private Panel3D pan3D;
	
	//Use when creating a new NodeObject
	private static final float basicPixelValue = 100.0f;
	private static final int basicLayerValue = 0;
	
	
	private ButtonCreat draw_button;
	private ButtonAddMenu addMenu_button;
	private ButtonDel del_button;
	private ButtonDelMenu delMenu_button;
	private ButtonMultiCreat multiDraw_button;


	private TreeDragAndDrop tree;
	private JScrollPane leftPanel;
	private JTabbedPane tabbedPane;
	

	
	private JPanel _DefaultPanel2;
	private JPanel _inDefaultPanel2;
	private JPanel _DefaultPanel;	
	
	//Used to save Path
	private JLabel dest_Text = new JLabel("");
	
	
	private ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	
	//Used in the LoadingBar
	private String loadingT = "";
	private int loadingX = 0;

	public PhantoMaJ(){
        run("");
    }

	public PhantoMaJ(boolean isStandAlone){
//		run("");
		if ( isStandAlone ){
			build();
			this.setTitle("PhantoMaJ");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.err.println("Test");
		}else{
			run("");
		}
	}

	/**
	* @overide PlugIn.run
	**/
	public void run(String arg) {
		build();
		this.setTitle("PhantoMaJ");
	}

	public static void main(String[] agrs){
		new ImageJ();

		new PhantoMaJ(true);
	}
	
	
	/**
	* Private Methode, continue the building process of the Frame
	*/
	private void build(){ 
		this.basicInterface.addListener(this);
	
		setResizable(true); 
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		setContentPane(buildContentPane());
	
		this.setVisible(true);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(new Dimension(1300,800));

		Dimension windowSize = this.getSize();

		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY); 
		this.repaint();
	}


	/**
	* Final methode use to build up this frame.Add all the elements constituing the 
	* default Frame.
	*@return JPanel the default contentPane to be display
	*/
	private JPanel buildContentPane(){
	//CREAT NEW BORDERFACTORY

		Border blackline;
		blackline = BorderFactory.createLineBorder(Color.black);
	
	//Variable 
	
		JLabel label = new JLabel();
		
	//CONSTRUCTION JMENU
		JMenuBar menuBar = new JMenuBar();

		MenuOption option = new MenuOption(this);
		MenuOptionDraw optionDraw = new MenuOptionDraw(this);
		MenuAbout about = new MenuAbout(this);
		MenuRandom random = new MenuRandom(this);
		
		menuBar.add(option);
		menuBar.add(optionDraw);
		menuBar.add(random);
		menuBar.add(about);
		setJMenuBar(menuBar);
	
	
	//CONSTRUCTION PRINCIPAL
		JPanel principal = new JPanel(new GridBagLayout());
	
	//CONSTRUCTION TABBEDPANE
		tabbedPane = new JTabbedPane();		
	
	//########################################################################################################
	//Boutton add_form

		draw_button = new ButtonCreat("Draw",this);
		addMenu_button = new ButtonAddMenu("New",this);
		
//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("blackArrow"));
//		Image image = Toolkit.getDefaultToolkit().getImage(url);
//        URL url = this.getClass().getResource(PhantoMaJ.resourceBundle.getString("blackArrow"));
//        return new ImageIcon(url);
		
		addMenu_button.setIcon(Utils.getResourceIcon("blackArrow"));
		addMenu_button.setVerticalTextPosition(SwingConstants.CENTER);
		addMenu_button.setHorizontalTextPosition(SwingConstants.LEFT);
		del_button = new ButtonDel("DELETE",this);
		delMenu_button = new ButtonDelMenu("",this);
		delMenu_button.setIcon(Utils.getResourceIcon("blackArrow"));
	
		

		JPanel creat_add = new JPanel(new GridBagLayout());

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,creat_add,addMenu_button);


		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,20,0,1,1,0,1,creat_add,del_button);

		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,1,creat_add,delMenu_button);


		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,2,creat_add,draw_button);



	//########################################################################################################
	// DefaultPanel
		//the content of the first Tab
		_DefaultPanel = new JPanel(new GridBagLayout());

//#####################################################################
// TREE
		tree = new TreeDragAndDrop(this);
		leftPanel = tree.getContent();
		leftPanel.setMaximumSize(new Dimension((int)leftPanel.getPreferredSize().getWidth()-30,500));
		leftPanel.setMinimumSize(new Dimension((int)leftPanel.getPreferredSize().getWidth()-30,500));
		leftPanel.setPreferredSize(new Dimension((int)leftPanel.getPreferredSize().getWidth()-30,500));
		//System.out.println("Preferred size Jscroll :"+leftPanel.getPreferredSize().getWidth());
		
//####################################################################
//BUTTON SECOND PANEL
		
		multiDraw_button = new ButtonMultiCreat("Creat Multi Phantom",this);

//#####################################################################
// SECOND TAB

		_DefaultPanel2 = new JPanel(new GridBagLayout());
		_inDefaultPanel2 = new JPanel(new GridBagLayout());
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_DefaultPanel2,_inDefaultPanel2);	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,10,0,GridBagConstraints.VERTICAL,1,0,60,30,1,1,0,1,_DefaultPanel2,multiDraw_button);	



//#####################################################################
//FINAL BUILD TABBEDPANE
		//SCROLL MODIF
		JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// scroll.setMinimumSize(new Dimension(300,500));
		scroll.setViewportView(_DefaultPanel2);
		
		tabbedPane.addTab(" Phantom Creation ",_DefaultPanel);
		// tabbedPane.addTab("default2",_DefaultPanel2);
		tabbedPane.addTab(" Phantom Modification ",scroll);
		tabbedPane.setEnabledAt(1,false);

//######################################################################
//Build Option View3D from Pane3D
		JPanel paneOption3DView = new JPanel(new GridBagLayout());

		JPanel pane = new JPanel(new GridBagLayout());
		
		ButtonAxeView3D axeButton = new ButtonAxeView3D("X/Y",0,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,0,1,0,0,2,1,0,0,pane,axeButton);
		
		axeButton = new ButtonAxeView3D("X/Z",1,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,5,5,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,1,pane,axeButton);
		
		axeButton = new ButtonAxeView3D("Z/Y",2,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,5,5,0,GridBagConstraints.BOTH,0,1,0,0,1,1,1,1,pane,axeButton);
		
		//Empty
		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,2,2,0,pane,label);
		
		pane.setBorder(blackline);
		
		//*****
		//Fin construction de pane
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.NORTH,5,5,5,5,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,paneOption3DView,pane);
		
		ButtonPmage bPmage = new ButtonPmage(this);
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("phantomFinal_Icone60"));
//		Image pmage = Toolkit.getDefaultToolkit().getImage(url);
		bPmage.setIcon(Utils.getResourceIcon("phantomFinal_Icone60"));
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.EAST,5,0,5,5,GridBagConstraints.NONE,0,1,0,0,1,1,0,1,paneOption3DView,bPmage);
		
//######################################################################
// Final Build

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,0,principal,leftPanel);


		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,2,1,0,principal,tabbedPane);

		// New JPanel canvas 3D
		this.pan3D = new Panel3D();
		
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,2,0,principal,pan3D);
		
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,2,1,principal,paneOption3DView);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,1,principal,creat_add);

		

		// this.setMinimumSize(new Dimension(1300,800));
		this.setMinimumSize(new Dimension(900,600));
		this.setPreferredSize(this.getPreferredSize());
	
		return principal;
	
	}


	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update a dimension
	* @param v the new value
	* @param id the identifier of the dimension to update
	**/
	public void updateDim(int v, int id){
		if ( this.lock_apply ){
			this.tree._changeDimForm(v,id);
		}
	}

	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update a position
	* @param v the new value
	* @param id the identifier of the position to update
	**/	
	public void updatePos(int v, int id){
		if ( this.lock_apply ){
			this.tree._changePosForm(v,id);
		}
	}
	
	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update a rotation
	* @param v the new value
	* @param id the identifier of the rotation to update
	**/
	public void updateRot(int v, int id){
		if ( this.lock_apply ){
			this.tree._changeRotForm(v,id);
		}
	}

	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update the form ID
	* @param id the new identifier
	**/	
	public void updateForm(int id){
		if ( this.lock_apply ){
			this.tree._changeForm(id);
		}
	}

	/**
	* Linker between BasicInterface and Compute_Intensity ( Event Fired in BasicInterface )
	* Update a dimension
	* @param bv the new background Value
	**/	
	public void updateBackgroundVal(float bv){
		this.Cint.setBackgroundValue(bv);
	}

	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update a the layer value
	* @param new_layer the new layer value
	**/	
	public void applyLayerO(int new_layer){
		this.tree.applyO(-1.0f,new_layer);
	}

	/**
	* Linker between BasicInterface and TreeDragAndDrop ( Event Fired in BasicInterface )
	* Update a the Object pixel value
	* @param new_pixelValue the new pixel value
	**/		
	public void applyPixelValueO(int new_pixelValue){
		this.tree.applyO((float)(new_pixelValue),-1);
	}
	
	/**
	* Change every position in form from the currently selected object
	* @param identifier
	* @param newValue the position modifier for every object contained in the NodeObject
	* @param oldValue the old position parameters of every objects
	**/
	public void applyChangeObjPos(int identifier,int newValue, int[][] oldValue){
		this.tree.applyChangeObjPos(identifier,oldValue,newValue);
	}

	/**
	* Change every dimension in form from the currently selected object
	* @param value the modifier.
	* @param origine the origine position of every form contained in the NodeObject.
	* @param dimension the dimension of every form contained in the NodeObject.
	**/
	public void applyChangeObjDim(int value, int[][] origine,int[][] dimension){
		this.tree.applyChangeObjDim(value,origine,dimension);
	}
	
	/**
	* Object Rotation Updating.
	* @param newAngle the rotation angle phi,theta,psi.
	* @param oldAngle the old angle value of every form contained in the NodeObject.
	* @param ori the origine of every form contained in the NodeObject.
	* @param center the Object center, computed with a weight of 1 for every form.
	**/
	public void UpdateRotationObject(int[] newAngle, int[][] oldAngle, int[][] ori, int[] center){
		//Change Form position in object
		this.tree.computeObjectRotation(newAngle,ori,center);
		//Change angle position in object
		this.tree.computeObjectRotation_typeT(newAngle,oldAngle);
	}
	
	/**
	* Linker between Tree and view
	* Add a new Object in the Panel3D ( a new addBranchGraph )
	**/
	public void update3DviewAddObject(){
		this.pan3D.addObject();
	}
	
	/**
	* Linker between Tree and view
	* Add a new Form3D in the 3Dview
	* @param angle the angle value of the form to add.
	* @param position the origine value of the form to add.
	* @param dimension this dimension value of the form to add.
	* @param identifier the type of the Form3D to add.
	* @param posObj the Object identifier where to add this Form3D
	**/
	public void update3DviewAddForm(int[] angle, int[] position,int[] dimension,int identifier, int posObj){
		int[] dim = this.getDimension();
		float[] positionFloat = {((float)position[0])/((float)dim[0]/2.0f), ((float)position[1])/((float)dim[1]/2.0f), ((float)position[2])/((float)dim[2]/2.0f)};
		float[] dimensionFloat = {((float)dimension[0])/((float)dim[0]), ((float)dimension[1])/((float)dim[1]), ((float)dimension[2])/((float)dim[2])};
		this.pan3D.addForm(angle,positionFloat,dimensionFloat,identifier,posObj);
	}
	
	/**
	* Linker between Tree and view.
	* Update a new Form3D in the 3Dview.
	* @param angle the angle value of the form to update.
	* @param position the origine value of the form to update.
	* @param dimension this dimension value of the form to update.
	* @param formID the tyope of the Form3D to update.
	* @param posForm the Form identifier of the Form3D to update.
	* @param posObj the Object identifier where to update this Form3D.
	**/
	public void update3Dview(int[] angle, int[] position, int[] dimension,int formID, int posObj, int posForm){
		
		int[] dim = BasicInterface.getStackDimension();
		int d = 3;
		
		if ( StaticField_Form.CODE_ADDITIONAL_FORM[formID] != null ){
			d += StaticField_Form.CODE_ADDITIONAL_FORM[formID].length;
		}
		
		float[] dimensionFloat = new float[d];
		float[] positionFloat = {((float)position[0])/((float)dim[0]/2.0f), ((float)position[1])/((float)dim[1]/2.0f), ((float)position[2])/((float)dim[2]/2.0f)};
		
		for ( int i = 0 ; i < dimension.length ; i++ ){
			if ( i < 3 ){
				dimensionFloat[i] = ((float)dimension[i])/((float)dim[i]);
			}
			else {
				dimensionFloat[i] = ((float)dimension[i]);
			}
		}
		
		this.pan3D.update(angle,positionFloat,dimensionFloat,formID,posObj,posForm);
	}
	
	
	/**
	* Linker between Tree and view.
	* Remove the specified Form3D.
	* @param posObj the identifier of the object containing the form to delete.
	* @param posForm the identifier of the form to delete;
	**/
	public void update3DviewRemoveForm(int posObj, int posForm ){
		this.pan3D.deleteForm(posObj,posForm);
	}
	
	/**
	* Linker between Tree and view
	* delete the specified object in the 3DView
	* @param posObj the Object identifier to delete.
	**/
	public void update3DviewRemoveObj(int posObj){
		this.pan3D.deleteObj(posObj);
	}
	
	/**
	* Linker between Tree and view
	* Clear the panel3D.
	**/
	public void update3DviewClear(){
		this.pan3D.clearView();
	}
	
	/**
	* Linker between Tree and view
	* set the Selected form in Red in the Panel3D
	**/
	public void update3DviewSelectedForm(int posObj, int posForm){
		this.pan3D.updateColorForm(posObj, posForm,0);
	}
	
	/**
	* Linker between Tree and view
	* Update the color of the selected Object
	**/
	public void update3DviewSelectedObj(int posObj){
		this.pan3D.updateColorObj(posObj);
	}
	
	/**
	* Linker between Tree and view
	* Set the Axe of view in the 3DView
	**/
	public void update3DViewAxe(int axe){
		this.pan3D.reset(axe);
	}
	
	/**
	* Call for updating the 3D View by reloading every information in the TreeDragAndDrop
	**/
	public void reload3DView(){
		tree.reload3DView();
	}
	
	/**
	* Delete the selected NodeForm or Object in the TreeDragAndDrop.
	* @param identO the index of this object in the TreeDragAndDrop.
	* @param identF the index of this form in this object of the TreeDragAndDrop.
	**/
	public void Del(int identO, int identF){
		if( identO < 0 ){
			tree.del();
		}
		else if(identF <0){
			tree.del_Object(identO);
		}
		else{
			tree.del_form(identO,identF);
		}
	}

	
	/**
	* Add an Object in the TreeDragAndDrop, ident = -1, correspond to add this object at the end of the Tree.
	* @param ident the index where to add this object in the TreeDragAndDrop
	**/
	public void add_Object(int ident){
		if( ident < 0){
			tree.add_Object(basicPixelValue,basicLayerValue,true);
		}
		else{
			System.out.println("ATTENTION PAS DE FONCTION ICI");
		}
	}

	
	/**
	* Draw the current phantom represented by the Tree, in a new stack window.
	**/
	public void creat_form(){
		ArrayList<ArrayList<CreateForm>>  phantom = tree.getPhantom();
		
		Object3D object3D = new Object3D(phantom,this.tree);
		Object3D.resetIdObj();
		
		object3D.setNoiseSD(0);
		int [] dimStack = {BasicInterface.getStackDimension()[0], BasicInterface.getStackDimension()[1], BasicInterface.getStackDimension()[2]};		
		object3D.setDimStack(dimStack);
		object3D.setComputeIntensity(this.Cint);
		object3D.setThreadOption(this.Toption);
		object3D.draw();
	}

	/**
	* Open the Warning interface Frame, If there is no Selected File where to save Data
	**/
	public void openWarningFrame(){
		File f = new File(this.dest_Text.getText());
		//TODO ajout si il n'y a pas de parametre selectionner
		if ( f.exists() && f.isDirectory() ){
			this.openPhantomOptionFrame();
		}
		else{
			WarningSaveFrame w = new WarningSaveFrame("Warning",this);
		}
	}
	
	/**
	* Open the Phantom Distribution interface frame
	**/
	public void openPhantomOptionFrame(){
		final ArrayList<Parameters> parameters = this.tree.getParameters();
		final PhantoMaJ f = this;
		Thread t = new Thread(new Runnable() {
				public void run() {
					SwingUtilities.invokeLater(new Runnable() {
					  public void run() {
						MultiPhantomOptionFrame optionPhan = new MultiPhantomOptionFrame(parameters,f);
					  }
					});
				}
		});
		t.start();
	}
	
	
	/**
	* Draw the current phantom represented by the Tree, and by the specified variable condition.
	**/
	public void creatMulti_form(ArrayList<Parameters> param, int numbOfPhantom, boolean isPaired){
		final ArrayList<ArrayList<CreateForm>>  phantom = tree.getPhantom();
		final int numbOfPhantomF = numbOfPhantom;
		final ArrayList<Parameters> paramF = param;
		final boolean isPairedF = isPaired;
		final TreeDragAndDrop treeF = this.tree;
		final Compute_Intensity CintF = this.Cint;
		
		final int [] dimStack = {BasicInterface.getStackDimension()[0], BasicInterface.getStackDimension()[1], BasicInterface.getStackDimension()[2]};		
		
		//name of the output file
		final String path = this.dest_Text.getText() + File.separator ;
		
		final int save_format = 0;
		// final double noiseSD = 	Double.parseDouble(_noiseSD.getText());
		final double noiseSD = 0;
		
		Thread t = new Thread(new Runnable() {
				public void run() {

						Object3D object3D = new Object3D(phantom,treeF);
						// Object3D.resetIdObj();
						object3D.setDimStack(dimStack);
						object3D.setNumberOfPhantom(numbOfPhantomF);
						object3D.setComputeIntensity(CintF);
						object3D.setNoiseSD(noiseSD);
						object3D.multiDraw(paramF,isPairedF,path,save_format);

				}
		});
		t.start();
	}
	
	public int[] getDimension(){
		int [] dimStack = {BasicInterface.getStackDimension()[0], BasicInterface.getStackDimension()[1], BasicInterface.getStackDimension()[2]};
		return dimStack;
	}
	
	
	/**
	* Update the interface with the specified panel
	* @param idPanel (0,1,2)<->(_RootPanel,_ObjectPanel,_NodeFormPanel)
	**/
	public void updatePanel(int idPanel){
		switch(idPanel){
			case 0:
				//Update TabbedPane 1
				_DefaultPanel.removeAll();
				
				//Set the new Panel
				basicInterface.setPhantomPanel();
				
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_DefaultPanel,basicInterface.getCurrentPanel());			
				
				//Update TabbedPane 2
				_inDefaultPanel2.removeAll();
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_inDefaultPanel2,basicInterface.getSecondPanel());			
				break;
			case 1:
				_DefaultPanel.removeAll();
				
				//Set the new Panel
				basicInterface.setObjectPanel();
				
				JScrollPane scroll = new JScrollPane(basicInterface.getCurrentPanel(),JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				
				scroll.setMinimumSize(new Dimension(0,0));
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_DefaultPanel,scroll);
				
				//Update TabbedPane 2
				_inDefaultPanel2.removeAll();
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_inDefaultPanel2,basicInterface.getSecondPanel());		
				
				break;
			case 2:
				_DefaultPanel.removeAll();
				
				//Set the new Panel
				basicInterface.setFormPanel();
				
				scroll = new JScrollPane(basicInterface.getCurrentPanel(),JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
				scroll.setMinimumSize(new Dimension(0,0));
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_DefaultPanel,scroll);		

				//Update TabbedPane 2
				_inDefaultPanel2.removeAll();
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_inDefaultPanel2,basicInterface.getSecondPanel());		
				
				break;
			default:
				_DefaultPanel.removeAll();	
	
				//Set the new Panel
				basicInterface.setPhantomPanel();
				
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
				addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_inDefaultPanel2,basicInterface.getCurrentPanel());					
				break;
		}

		_DefaultPanel.validate();
		this.validate();
		this.repaint();
	}
	
	/**
	* Update information relative to the root node
	**/
	public void setInfoRoot(){
		//update the view to remove the unused center
		this.pan3D.removeCenter();
		
		updatePanel(0);
		
	}
	
	/**
	* Update information relative to the selected NodeObject
	**/
	public void setInfoObject(NodeObject node){
		if ( lock_setTree ){
			//update the view to remove the unused center
			this.pan3D.removeCenter();
			
			this.updatePanel(1);
			this.basicInterface.setObjectInfo(this.tree.getDimObj(),this.tree.getOrigineObj(),tree.getForm(-1),node.getPixelValue(),node.getLayer(),node.getName());
			//Update transformation Tab
			this.setTabObj(node);
		}
	}
	
	/**
	* Update Slider value of the center of rotation and the angle parameters of the NodeObject currently selected
	* (Reset the slider value to 0)
	**/
	public void setNewCenterRotationObj(){
		int[][] angle = tree.getAngleObj();
		int[] center = tree.getCenterObject();
		this.basicInterface.setObjectInfoSliderRot(angle,center);
	}
	
	
	/**
	* Update the slider value of the position center of the NodeObject currently selected.
	* (Reset the slider to 0)
	**/
	public void setNewCenterPositionObj(){
		int[] min = {tree.getMinimumPosObj(0,BasicInterface.getStackDimension()),tree.getMinimumPosObj(1,BasicInterface.getStackDimension()),tree.getMinimumPosObj(2,BasicInterface.getStackDimension())};
		int[] max = {tree.getMaximumPosObj(0,BasicInterface.getStackDimension()),tree.getMaximumPosObj(1,BasicInterface.getStackDimension()),tree.getMaximumPosObj(2,BasicInterface.getStackDimension())};
		this.basicInterface.setObjectInfoSliderPos(min,max);	
	}
	

	/**
	* Update the interface with information contained in the nodeForm object
	**/
	public void setInfoNode(NodeForm node,NodeObject nodeParent){
		if ( lock_setTree ){
			// lock is here to say that the data are updating and dont need a apply when changed
			this.lock_apply = false ;
			
			//update the view to remove the unused center
			this.pan3D.removeCenter();
			
			updatePanel(2);
			
			basicInterface.setFormInfo(node.getName(),nodeParent.getName(),node.getAngle(),node.getDimension(),node.getForm(),node.getOrigine());

			setTabForm(node);
			
			this.lock_apply = true;
		}
	}
	
	
	/**
	* Update the interface with information contained in the NodeObject object
	**/
	public void setTabObj(NodeObject node){
		
		boolean[] selected = new boolean[7];
		
		selected[0] = node.getisDimXSelected();
		
		selected[1] = node.getisPhiTransfoSelected();
		selected[2] = node.getisThetaTransfoSelected();
		selected[3] = node.getisPsiTransfoSelected();

		selected[4] = node.getisXoriSelected();
		selected[5] = node.getisYoriSelected();
		selected[6] = node.getisZoriSelected();
		
		System.out.println("WARNING ::: Intensity not implemented yet (dim either but work on the interface)");

		int[] r_d;
		// int[][] range = new int[7][];
		if ( node.getDimXTransfo() != null ){
			r_d = node.getDimXTransfo();
		}
		else {
			r_d = new int[3];
			r_d[0] = 100;
			r_d[1] = 100;
			r_d[2] = 100;
		}
		
		int[] r_p = new int[2];
		if( node.getPhiTransfo() != null ){
			r_p = node.getPhiTransfo();
		}


		int[] r_t = new int[2];
		if( node.getThetaTransfo() != null ){
			r_t = node.getThetaTransfo();
		}

		int[] r_ps = new int[2];
		if( node.getPsiTransfo() != null ){
			r_ps = node.getPsiTransfo();				
		}

		
		//if already initialize 
		int[] r_tx = new int[2];
		if( node.getXoriTransfo() != null ){
			r_tx = node.getXoriTransfo();
		}

		int[] r_ty = new int[2];
		if( node.getYoriTransfo() != null ){
			r_ty = node.getYoriTransfo();
		}

		int[] r_tz = new int[2];
		if( node.getZoriTransfo() != null ){
			r_tz = node.getZoriTransfo();
		}

		int[][] range = {r_d,r_p,r_t,r_ps,r_tx,r_ty,r_tz};
		basicInterface.setObjectInfo_Seconde(selected,range);
	}

	
	public void setTabForm(NodeForm node){

		boolean[] select_a = node.isSelectedParam();
		boolean[] select_z = {node.getisXoriSelected(),node.getisYoriSelected(),node.getisZoriSelected()};
		boolean[] select_e = {node.getisPhiTransfoSelected(),node.getisThetaTransfoSelected(),node.getisPsiTransfoSelected()};
		boolean[][] selected = {select_z,select_e,select_a};
		

		int[][] val_a = node.getDimensionTransfo();
		
		int[] val_b = new int[2];
		if( node.getXoriTransfo() != null ){
			val_b = node.getXoriTransfo();
		}
		
		int[] val_c = new int[2];
		if( node.getYoriTransfo() != null ){
			val_c = node.getYoriTransfo();	
		}

		
		int[] val_d = new int[2];
		if( node.getZoriTransfo() != null ){
			val_d = node.getZoriTransfo();
		}

		
		int[] val_e = new int[2];
		if( node.getPhiTransfo() != null ){	
			val_e = node.getPhiTransfo();
		}
		
		int[] val_f = new int[2];
		if( node.getThetaTransfo() != null ){
			val_f = node.getThetaTransfo();
		}
	
		int[] val_g = new int[2];
		if( node.getPsiTransfo() != null ){
			val_g = node.getPsiTransfo();				
		}
		
		int[][] valTransfo = {val_b,val_c,val_d,val_e,val_f,val_g};
		
		basicInterface.setFormInfo_Second(selected,valTransfo,val_a);
	}

	
	/**
	* Update the Rotation Center for an Object
	* Update the view by adding a point in place of the center of Rotation( object only)
	**/
	public void updateSliderRotationObjCenterofRotation(int formID){
	//id formID == -1 , center by default
		int[] ori = new int[3];
		if ( formID == -1){
			ori = tree.getCenterObject();
			int[][] angle = tree.getAngleObj();		
			this.basicInterface.setObjectInfoSliderRot(angle,ori);
		}
	//else formID == x, center of the form
		else{
			ori = ((NodeForm)tree.getNode(-1,formID)).getOrigine();
			int[][] angle = tree.getAngleObj();
			this.basicInterface.setObjectInfoSliderRot(angle,ori);
		}
		
		//Update view3D add a point representing the Center of Rotation
		int[] dim = this.getDimension();
		float[] positionFloat = {((float)ori[0])/((float)dim[0]/2.0f), ((float)ori[1])/((float)dim[1]/2.0f), ((float)ori[2])/((float)dim[2]/2.0f)};
		//reset center
		this.pan3D.removeCenter();
		//add center
		this.pan3D.addCenter(positionFloat,formID,this.tree.getPosObj());
	}
	

	public void setButtonDelEnable(boolean bol){
			del_button.setEnabled(bol);
			delMenu_button.setEnabled(bol);
	}

	
	public void setTabbedPaneVisible(boolean bol, int tab){
		tabbedPane.setEnabledAt(tab,bol);
	}
	
	
	public void setSelectedCenter(int selectedCenter){
		tree.setNewCenterOfRotation(selectedCenter);
	}
	
	
	public ArrayList<String> getArrayObject(){
		return this.tree.getObject();
	}

	public ArrayList<String> getArrayForm(int obj){
		return this.tree.getForm(obj);
	}	


	/**
	* Save this phantom in a obj file named filename.
	* @param filename the name of the file to be save
	**/
	public void save(String filename){
		try{
			ArrayList<ArrayList<CreateForm>>  phantom = tree.getPhantom();

			Object3D object3D = new Object3D(phantom,this.tree);
			Object3D.resetIdObj();

			int [] dimStack = {BasicInterface.getStackDimension()[0], BasicInterface.getStackDimension()[1], BasicInterface.getStackDimension()[2]};		
			object3D.setDimStack(dimStack);
			object3D.saveFileObj(filename,tree.getNodeObject());
		}
		catch(NullPointerException e){
			System.out.println("Error :::: PhantoMaJ.save");
			e.printStackTrace();
		}
	}


	//TODO activer quand on click sur ouvrir du JFileTree ( a voir )
	/**
	* Open an obj file, where a phantom description can be found.
	* @param filename the name of the file to open
	**/
	public boolean open(String filename){
		final boolean[] test = {true};
		final Chrono c = new Chrono();
		c.start();
	
		Object3D object3D = new Object3D(null,null);
		Object3D.resetIdObj();
		this.clearTree();
		
		//Lock Thread
		this.pan3D.setIsInBuild(true);	
		this.lock_setTree = false;	
		this.tree.setLock_setTree(false);
		
		final Object3D object3DF = object3D;
		final String filenameF = filename;
		
		final LoadingBar l = new LoadingBar();
		l.setInfo(1,this);
		l.start();
		
		final Thread g = new Thread(){
			public void run(){
				//catch IOexeption if the filenameF is not a valide file
				try{
					setTree(object3DF.openFileObj(filenameF),object3DF.openFile(filenameF),this);
				}
				catch(Exception e){
					test[0]=false;
					l.setLoadTitle("Error while drawing");
					loadingT = "";
					loadingX = -1;
					try{
						this.join(1);
					}
					catch(InterruptedException f){
						f.printStackTrace();
					}
				}
				finally{
					//Lock Thread end Lock
					tree.setLock_setTree(true);
					pan3D.setIsInBuild(false);	
					lock_setTree = true;
					
					tree.resetSelectionPath();
					
					// l.dispose();
					pan3D.printDelay();
					tree.printDelay();
					
					c.stop();
					System.out.println("Time Passez Total ajout :: "+c.delayString());
				}
			}
		};
		g.start();

		while ( g.isAlive() ){

		}
		l.dispose();
		
		return test[0];
	}

	/**
	* Clear this tree.
	**/
	public void clearTree(){
		tree.clearTree();
		System.out.println("GarbageCollector");
		Runtime.getRuntime().gc(); 
	}

	/**
	* add this phantom ArrayList<ArrayList<NodeForm>> in this tree.
	* @param phantom every form and Object Node.
	* @param phantom_info the Object and phantom information.
	* @param g
	**/
	public void setTree(ArrayList<ArrayList<NodeForm>> phantom, ArrayList<String> phantom_info,Thread g)throws Exception{
		
		if ( phantom == null ||phantom_info == null || phantom.size() == 0){
			throw(new Exception("Phantom Vide, nom du fichier incorrect"));
		}
		
		int count = 0;
		int maxCount = 0;
		for ( int k=0;k<phantom.size();k++){
			maxCount += phantom.get(k).size();
		}
		//init
		@SuppressWarnings("rawtypes")
		Future[] future=new Future[maxCount];
		
		System.out.println("Loading "+maxCount+" Object");
		
		int i = 0;
		
		this.pan3D.initializeArrayListObject(phantom.size());
		
		Iterator<ArrayList<NodeForm>> objects = phantom.iterator();


		String[] _dim = phantom_info.get(1).split("\t");
		
		this.basicInterface.setStackDimension(Integer.parseInt(_dim[0]),0);
		this.basicInterface.setStackDimension(Integer.parseInt(_dim[1]),1);
		this.basicInterface.setStackDimension(Integer.parseInt(_dim[2]),2);

		//for every objects in the phantom
		while (objects.hasNext()) {
			//set new information in the ObjectNode

			tree.add_Object(new Float(phantom_info.get(2*i+2)),new Integer(phantom_info.get(2*i+3)),true);

			// add_Object(-1);
			ArrayList<NodeForm> object = objects.next();
			
			Iterator<NodeForm> forms = object.iterator();
			//for every form constituing this object
			while(forms.hasNext()){
				NodeForm obj = forms.next();

				future[count] = exec.submit(new ThreadSetTree(this.tree,obj,i));
				this.loadingX = (int)(((((float)count)/((float)maxCount))*2.0f)*100.0f);
				this.loadingT = "Remaing object :: "+count+" / "+maxCount;
				count++;
			}
			i += 1;
		}
		
		this.loadingX = -1;
		this.loadingT = "Finalization";
	
		//Fin des threads
		for(int z=0;z<future.length;z++){
			try{
				future[z].get();
			}
			catch(ExecutionException ex){
				System.out.println("ExecutionException ............ PhantoMaJ.setTree");
			}
			catch(InterruptedException e){
				System.out.println("InterruptedException ............ PhantoMaJ.setTree");
			}
		}
	}

	public JScrollPane getLeftPanel(){
		return this.leftPanel;
	}
	
	public void setDestText(String path){
		this.dest_Text.setText(path);
	}
	
	
	/**
	* Notify the nodeform that transformation value has changed
	* ID = (0,1,2) (form,obj,phantom);
	**/
	public void setInfoTabInNode(int[] nv, int ID_select, int secondID){
		int ID = this.tree.getWhichType();
		if ( lock_apply ){
			if ( ID == 0 ){
				switch(ID_select){
					case BasicInterface.SLIDER_PHI_N:
							tree.TransfoFormAngle(nv,0);
						break;
					case BasicInterface.SLIDER_THETA_N:
							tree.TransfoFormAngle(nv,1);
						break;
					case BasicInterface.SLIDER_PSI_N:
							tree.TransfoFormAngle(nv,2);
						break;
					case BasicInterface.SLIDER_DIM_N:
							tree.TransfoFormDim(nv,secondID);
						break;
					case BasicInterface.SLIDER_POSX_N:
							tree.TransfoFormOri(nv,0);
						break;
					case BasicInterface.SLIDER_POSY_N:
							tree.TransfoFormOri(nv,1);
						break;
					case BasicInterface.SLIDER_POSZ_N:
							tree.TransfoFormOri(nv,2);
						break;
					default :
						System.out.println("WARNING ::: unrecognized Identifier ("+ID_select+") PhantoMaJ.setInfoSelected");
				}
			}
			//if NodeObject
			else if ( ID == 1 ){
			
				switch(ID_select){
					case BasicInterface.SLIDER_PHI_R:
							tree.TransfoObjAngle(nv,0);
						break;
					case BasicInterface.SLIDER_THETA_R:
							tree.TransfoObjAngle(nv,1);
						break;
					case BasicInterface.SLIDER_PSI_R:
							tree.TransfoObjAngle(nv,2);
						break;
					case BasicInterface.SLIDER_SIZE_R:
							tree.TransfoObjDim(nv);
						break;
					case BasicInterface.SLIDER_OBJ_INT:
							tree.TransfoObjInt(nv);
						break;
					case BasicInterface.SLIDER_POSX_R:
							tree.TransfoObjOri(nv,0);
						break;
					case BasicInterface.SLIDER_POSY_R:
							tree.TransfoObjOri(nv,1);
						break;
					case BasicInterface.SLIDER_POSZ_R:
							tree.TransfoObjOri(nv,2);
						break;
					default :
						System.out.println("WARNING ::: unrecognized Identifier ("+ID_select+") PhantoMaJ.setInfoSelected");
				}
			}
			else if ( ID == 2 ){
				System.out.println("FireInfoSelected in Root do not work properly yet");
				// switch(ID_select){
					// case BasicInterface.BACKGROUND_ROOT:
						// break;
					// case BasicInterface.NOISE_ROOT:
						// break;
				// }
			}
		
		}
	}
	
	/**
	* Notify the nodeform or NodeObject that transformation value has been selected
	**/
	public void setInfoSelected(int ID_select,int secondID){
		int ID = this.tree.getWhichType();
		if ( lock_apply ){
			//If NodeForm
			if ( ID == 0 ){
				switch(ID_select){
					case BasicInterface.SLIDER_PHI_N:
							tree.TransfoFormAngleSel(0);
						break;
					case BasicInterface.SLIDER_THETA_N:
							tree.TransfoFormAngleSel(1);
						break;
					case BasicInterface.SLIDER_PSI_N:
							tree.TransfoFormAngleSel(2);
						break;
					case BasicInterface.SLIDER_DIM_N:
							tree.TransfoFormDimSel(secondID);
						break;
					case BasicInterface.SLIDER_POSX_N:
							tree.TransfoFormOriSel(0);
						break;
					case BasicInterface.SLIDER_POSY_N:
							tree.TransfoFormOriSel(1);
						break;
					case BasicInterface.SLIDER_POSZ_N:
							tree.TransfoFormOriSel(2);
						break;
					default :
						System.out.println("WARNING ::: unrecognized Identifier ("+ID_select+") PhantoMaJ.setInfoSelected");
				}
			}
			//if NodeObject
			else if ( ID == 1 ){
			
				switch(ID_select){
					case BasicInterface.SLIDER_PHI_R:
							tree.TransfoObjAngleSel(0);
						break;
					case BasicInterface.SLIDER_THETA_R:
							tree.TransfoObjAngleSel(1);
						break;
					case BasicInterface.SLIDER_PSI_R:
							tree.TransfoObjAngleSel(2);
						break;
					case BasicInterface.SLIDER_SIZE_R:
							tree.TransfoObjDimSel();
						break;
					case BasicInterface.SLIDER_OBJ_INT:
							tree.TransfoObjIntSel();
						break;
					case BasicInterface.SLIDER_POSX_R:
							tree.TransfoObjOriSel(0);
						break;
					case BasicInterface.SLIDER_POSY_R:
							tree.TransfoObjOriSel(1);
						break;
					case BasicInterface.SLIDER_POSZ_R:
							tree.TransfoObjOriSel(2);
						break;
					default :
						System.out.println("WARNING ::: unrecognized Identifier ("+ID_select+") PhantoMaJ.setInfoSelected");
				}
			}
			else if ( ID == 2 ){
				System.out.println("FireInfoSelected in Root do not work properly yet");
				// switch(ID_select){
					// case BasicInterface.BACKGROUND_ROOT:
						// break;
					// case BasicInterface.NOISE_ROOT:
						// break;
				// }
			}
		}
	}
	
	public void addNewForm(NodeForm node, int index){
		tree.add_form(node,index);
	}
	
	/**
	* Open a new Frame containing the randomGenerator Interface
	**/
	public void randomizeFrame(){
		RandomGeneratorFrame frame_rand = new RandomGeneratorFrame();
	}
	
	/**
	* Lock on the Slider update
	**/
	public boolean getIsChangedEnable(){
		return this._stateChangedSilder;
	}
	
	public Compute_Intensity getCompute_Intensity(){
		return this.Cint;
	}
	
	public void setComputeIntensity(Compute_Intensity Cint){
		this.Cint = Cint;
	}
	
	public void setThreadNumber(int numb){
		this.Toption.setNumberOfProcessor(numb);
	}
	
	public Thread_option getThreadOption(){
		return Toption;
	}
	
	
	public void ask_update_loading(LoadingBar bar){
		bar.updateThread(this.loadingX,this.loadingT);
	}
	
	
	//###########################################################################################################################
	//private methode
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
}
