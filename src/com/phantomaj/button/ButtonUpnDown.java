/**
* ButtonUpnDown.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.filechooser.Utils;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import javax.swing.JComponent;
import java.awt.Container;

import java.awt.Insets;
import java.awt.GridBagConstraints;

/**
* Used to show and hide JComponent[].
**/
public class ButtonUpnDown extends JButton{


	private boolean isSelected = true;
	private ImageIcon down;
	private ImageIcon up;
	private JComponent[] component;
	
//Constructor
	public ButtonUpnDown(String titre, JComponent[] component){
		super(titre);
		
//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("blackArrow2"));
		down = Utils.getResourceIcon("blackArrow2");
		
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("blackArrow3"));
		up = Utils.getResourceIcon("blackArrow3");

		
		this.component = component;
		this.setPreferredSize(new Dimension(20,20));
		this.setIcon(down);
	}

	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {

		Container parent = this.getParent();
		parent.remove(this);
		if ( isSelected ){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(0,0,10,10);
			c.fill = GridBagConstraints.REMAINDER;
			c.anchor = GridBagConstraints.NORTHEAST;
			c.weightx = 1;
			c.weighty = 1;
			c.ipadx = 0;
			c.ipady = 0;
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth = 1;
			c.gridheight = 1;
			parent.add(this,c);
			
			this.isSelected = false ;
			this.setIcon(down);
		}
		else{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(0,0,10,10);
			c.fill = GridBagConstraints.REMAINDER;
			c.anchor = GridBagConstraints.NORTHEAST;
			c.weightx = 0;
			c.weighty = 0;
			c.ipadx = 0;
			c.ipady = 0;
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth = 1;
			c.gridheight = 1;
			parent.add(this,c);
			this.isSelected = true ;
			this.setIcon(up);
		}
		setVisibleComponent();
		parent.validate();
	}
	
	private void setVisibleComponent(){
		for(int i = 0 ; i < this.component.length ; i++ ){
			this.component[i].setVisible(this.isSelected);
		}
	}

	public void setComponent(JComponent[] component){
		this.component = component;
	}
	
	public void addComponent(JComponent new_component){
		JComponent[] newComponent = new JComponent[this.component.length + 1 ];
		for ( int i=0; i < this.component.length ; i++ ){
			newComponent[i] = this.component[i];
		}
		newComponent[this.component.length] = new_component;
		this.component = newComponent;
	}
	
	public void setSelected(boolean bol){
		this.isSelected = bol ;
		setVisibleComponent();
		Container parent = this.getParent();
		parent.remove(this);
		if(isSelected){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(0,0,10,10);
			c.fill = GridBagConstraints.REMAINDER;
			c.anchor = GridBagConstraints.NORTHEAST;
			c.weightx = 0;
			c.weighty = 0;
			c.ipadx = 0;
			c.ipady = 0;
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth = 1;
			c.gridheight = 1;
			parent.add(this,c);
			this.setIcon(up);
		}
		else{
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(0,0,10,10);
			c.fill = GridBagConstraints.REMAINDER;
			c.anchor = GridBagConstraints.NORTHEAST;
			c.weightx = 1;
			c.weighty = 1;
			c.ipadx = 0;
			c.ipady = 0;
			c.gridx = 1;
			c.gridy = 0;
			c.gridwidth = 1;
			c.gridheight = 1;
			parent.add(this,c);
			this.setIcon(down);
		}
	}
	
} 