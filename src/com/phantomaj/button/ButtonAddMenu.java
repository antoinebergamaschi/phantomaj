/**
* ButtonDelMenu.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;

//Local import 
import com.phantomaj.PhantoMaJ;
import com.phantomaj.tree.NodeForm;
import com.phantomaj.utils.StaticField_Form;

import javax.swing.*;
import java.awt.event.ActionEvent;

//Java import

/**
* JButton use to Check if all the information about the structur to be add to the 
* J3D environment have been added, then add this structur in the list draw in the J3D environment
**/
public class ButtonAddMenu extends JButton{

	private PhantoMaJ principal_window;
	private JPopupMenu menu;

//Constructor
	public ButtonAddMenu(String titre, PhantoMaJ principal_window){
		super(titre);
		this.principal_window = principal_window;
		buildMenuPopUp();
	}


	public void buildMenuPopUp(){
		menu = new JPopupMenu();
			
		ItemAddO item_1 = new ItemAddO("New Object");
		JMenu item_2 = new JMenu("New Form");
		
		for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
			ItemAdd item = new ItemAdd("New "+StaticField_Form.FORM_NAME[i],i);
			item_2.add(item);
		}
	
		menu.add(item_1);
		menu.add(item_2);	
	}

	/**
	*  
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		buildMenuPopUp();
		menu.show(this,(int)this.getSize().getWidth(),-1);
	}
	
//********************** InnerClass *********************************
	class ItemAddO extends JMenuItem{

		public ItemAddO(String s){
			super(s);
		}

		public void fireActionPerformed(ActionEvent e){
			principal_window.add_Object(-1);
		}
	}
	
	class ItemAdd extends JMenuItem{
		private int identifier;

		public ItemAdd(String s,int identifier){
			super(s);
			this.identifier = identifier;
		}

		public void fireActionPerformed(ActionEvent e){
			principal_window.addNewForm(new NodeForm(StaticField_Form.FORM_NAME[this.identifier],StaticField_Form.BASICDIMENSION[this.identifier][0],StaticField_Form.BASICDIMENSION[this.identifier][1],StaticField_Form.BASICDIMENSION[this.identifier][2],StaticField_Form.BASICDIMENSION[this.identifier][3],
										StaticField_Form.BASICDIMENSION[this.identifier][4],StaticField_Form.BASICDIMENSION[this.identifier][5],StaticField_Form.BASICDIMENSION[this.identifier][6],StaticField_Form.BASICDIMENSION[this.identifier][7],StaticField_Form.BASICDIMENSION[this.identifier][8],this.identifier),-1);
		}
	}	

} 
