package com.phantomaj.button;
import com.phantomaj.PhantoMaJ;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

import java.lang.Thread;

/**
* JButton use to Check if all the information about the structur to be add to the 
* J3D environment have been added, then add this structur in the list draw in the J3D environment
**/
public class ButtonCreat extends JButton{

	private PhantoMaJ principal_window;

//Constructor
	public ButtonCreat(String titre, PhantoMaJ principal_window){
		super(titre);
		this.principal_window = principal_window;
	}

	/**
	* Lauch the PhantoMaJ.check_all_input function, if true TODO
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		new Thread(){
			public void run(){
				principal_window.creat_form();
		}}.start();
	}


} 
