


/**
* ButtonFindDest.java
* 14/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.PhantoMaJ;
import com.phantomaj.filechooser.FileChooserFrame;
import com.phantomaj.select.WarningSaveFrame;

import java.lang.NullPointerException;
import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class ButtonFindDest extends JButton{

	private PhantoMaJ principal_window;
	private WarningSaveFrame _WsF = null;
	
	//first Constructor
	public ButtonFindDest(String name, PhantoMaJ principal_window){
		super(name);
		this.principal_window = principal_window;
	}
	
	//seconde Constructor
	public ButtonFindDest(String name, WarningSaveFrame _WsF){
		super(name);
		this.principal_window = null;
		this._WsF = _WsF;
	}

	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		FileChooserFrame savefc = new FileChooserFrame(2);
		if ( _WsF != null ){
			try{
				_WsF.setDestText(savefc.getFile().getPath());
			}
			catch(NullPointerException ev){
			}
			finally{
				savefc.dispose();
			}
		}
		else{
			try{
				principal_window.setDestText(savefc.getFile().getPath());
			}
			catch(NullPointerException ev){
			}
			finally{
				savefc.dispose();
			}
		}
	}
}