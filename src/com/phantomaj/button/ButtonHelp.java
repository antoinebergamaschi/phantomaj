/**
* ButtonHelp.java
* 14/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;

import com.phantomaj.randomGenerator.RandGlobalParameters;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class ButtonHelp extends JButton{

	private RandGlobalParameters principal_window;
	private String text_help ="";
	
	//Constructor 1
	public ButtonHelp(String text_help,RandGlobalParameters principal_window){
		super("?");
		this.text_help = text_help;
		this.principal_window = principal_window;
        this.setToolTipText(text_help);
	}

	
	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		System.out.println("TODO ::: Creat Fenetre help");
		System.out.println(this.text_help);
	}
}