/**
* RadioCenterButton.java
* 31/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;

import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;

public class RadioCenterButton extends JRadioButton{

	private BasicInterfaceComponent_FormPanel basicInterface;
	private int id;

	public RadioCenterButton(int ID,BasicInterfaceComponent_FormPanel bi){
		super();
		this.id = ID;
		this.basicInterface = bi;
	}

	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		System.out.println("WARNING :::::  3DVieW will not Update ....... button.RadioCenterButton.fireActionPerformed");
		basicInterface.setSelectedCenter(id);
	}

}