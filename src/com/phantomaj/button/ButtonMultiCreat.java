
/**
* ButtonMultiCreat.java
* 14/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.select.MultiPhantomOptionFrame;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class ButtonMultiCreat extends JButton{

	private MultiPhantomOptionFrame _MpOF;
	private PhantoMaJ principal_window;

	//Constructor 1
	public ButtonMultiCreat(String name, PhantoMaJ principal_window){
		super(name);
		this.principal_window = principal_window;
	}

	//Constructor 2
	public ButtonMultiCreat(String name, MultiPhantomOptionFrame _MpOF){
		super(name);
		this._MpOF = _MpOF;
	}
	
	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		// this.principal_window.creatMulti_form();
		//System.out.println(this.getText());
		if ( this.getText() == "Run" ){
			this._MpOF.creatMulti_form();
		}
		else {
			// this.principal_window.openPhantomOptionFrame();
			this.principal_window.openWarningFrame();
		}
	}
}