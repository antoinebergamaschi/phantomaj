/**
* ButtonRotationCenterObj.java
* 19/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;

import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;
import com.phantomaj.component.BasicInterfaceComponent_ObjectPanel;

import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JComponent;

import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;

public class ButtonRotationCenterObj extends JPanel {
	
	private BasicInterfaceComponent_ObjectPanel basicInterface;

	public ButtonRotationCenterObj(BasicInterfaceComponent_ObjectPanel bi){
		super(new GridBagLayout());
		this.basicInterface = bi;
	}

	public void build(ArrayList<String> formName){
		this.removeAll();
		RadioCenter button = null;
		ButtonGroup Bgrp = new ButtonGroup();
		int i = -1 ;
		JLabel label = new JLabel();
		
		label = new JLabel("<html><h3>Select this Object Rotation Center</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,this,label);
		
		
		button = new RadioCenter(i,"Default (Center)",this.basicInterface);
		button.setSelected(true);
		Bgrp.add(button);
		
		i++;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,this,button);
		
		Iterator<String> name = formName.iterator();
		while(name.hasNext()){
			button = new RadioCenter(i,name.next(),this.basicInterface);
			Bgrp.add(button);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,i+2,this,button);
			
			i++;
		}
		
		
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	
	
	/******************************************************************************************************************************************************/
	//Private Class
	class RadioCenter extends JRadioButton{

		private BasicInterfaceComponent_ObjectPanel basicInterface;
		private int id;

		public RadioCenter(int ID,String title ,BasicInterfaceComponent_ObjectPanel bi){
			super(title);
			this.id = ID;
			this.basicInterface = bi;
		}

		/**
		* @override AbstractButton.fireActionPerformed
		**/
		public void fireActionPerformed(ActionEvent e) {
			// System.out.println("radio fire :"+this.id);
			this.basicInterface.updateSliderRotationObjCenterofRotation(id);
		}
	}

}