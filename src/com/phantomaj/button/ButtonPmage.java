package com.phantomaj.button;
import com.phantomaj.PhantoMaJ;

import javax.swing.JLabel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.Thread;


/**
* JButton use to Check if all the information about the structur to be add to the 
* J3D environment have been added, then add this structur in the list draw in the J3D environment
**/
public class ButtonPmage extends JLabel implements MouseListener{

	private boolean isIn = false;
	private PhantoMaJ principal_window;

//Constructor
	public ButtonPmage(PhantoMaJ principal_window){
		super();
		this.principal_window = principal_window;
		this.addMouseListener(this);
	}


	public void mouseClicked(MouseEvent arg0) {
		String t = "T";
		String i = "I";
		String m = "M";
		String y = "Y";
		for (int z = 0 ; z<10;  z++){
			System.out.println(t+i+m+y+"!");
			t+="T";
			m+="M";
			i+="I";
			y+="Y";
		}
		isIn = false;
	}

	public void mouseEntered(MouseEvent arg0) {
		this.isIn = true;
		new Thread(){
			public void run(){
				String b = "B";
				String o = "O";
				String h = "H";
				while ( isIn ){
					System.out.println(b+o+o+h);
					o+="O";
					try{
						this.sleep(100L);
					}
					catch(Exception e){
					
					}
				}
			}
		}.start();
	}

	public void mouseExited(MouseEvent arg0) {
		this.isIn = false;
	}

	public void mousePressed(MouseEvent arg0) {
	}
	public void mouseReleased(MouseEvent arg0) {
	}
	
} 