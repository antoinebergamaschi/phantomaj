/**
* ButtonAxeView3D.java
* 16/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.PhantoMaJ;
import com.phantomaj.randomGenerator.Button_panel;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

/**
* Update the profile view in the 3DView
**/
public class ButtonAxeView3D extends JButton{

	private int identifier;
	private PhantoMaJ principal_window=null;
	private Button_panel rand_frame = null;

//#############################################################################
//Constructor	
	
	public ButtonAxeView3D(String title, int identifier, PhantoMaJ principal_window){
		super(title);
		this.identifier = identifier;
		this.principal_window = principal_window;
	}

	public ButtonAxeView3D(String title, int identifier, Button_panel rand_frame){
		super(title);
		this.identifier = identifier;
		this.rand_frame = rand_frame;
	}

//#############################################################################

	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		if ( this.principal_window != null ){
			this.principal_window.update3DViewAxe(this.identifier);
		}
		else if ( this.rand_frame != null ){
			this.rand_frame.update3DViewAxe(this.identifier);
		}
	}

}