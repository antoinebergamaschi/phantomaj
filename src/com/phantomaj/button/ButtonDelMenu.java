/**
* ButtonDelMenu.java
* 07/03/2012
* @author Antoine Bergamaschi
**/
package com.phantomaj.button;
import com.phantomaj.PhantoMaJ;

import javax.swing.JPopupMenu;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import java.util.ArrayList;
import java.util.Iterator;

/**
* JButton use to Check if all the information about the structur to be add to the 
* J3D environment have been added, then add this structur in the list draw in the J3D environment
**/
public class ButtonDelMenu extends JButton{

	private PhantoMaJ principal_window;
	private JPopupMenu menu;

//Constructor
	public ButtonDelMenu(String titre, PhantoMaJ principal_window){
		super(titre);
		this.principal_window = principal_window;
		this.setEnabled(false);
		buildMenuPopUp();
	}


	public void buildMenuPopUp(){
		menu = new JPopupMenu();
		//menu.setAutoscrolls(true);
		//menu.setPopupSize(100,200);
		JPopupMenu.Separator sep = new JPopupMenu.Separator();
		try{
			ArrayList<String> itemName = principal_window.getArrayObject();
			int i = 0;
			Iterator<String> titres = itemName.iterator();
			while ( titres.hasNext() ){
				String titre = titres.next();
				ItemAdd item = new ItemAdd(titre,i,-1);

				i++;
				
				menu.add(item);
				sep = new JPopupMenu.Separator();
				menu.add(sep);

				try{
					ArrayList<String> itemNameForm = principal_window.getArrayForm(i-1);
					int d = 0;
					Iterator<String> titresForm = itemNameForm.iterator();
					while ( titresForm.hasNext() ){
						String titreForm = titresForm.next();
						ItemAdd itemF = new ItemAdd(titreForm,i-1,d);
						d++;
						menu.add(itemF);
					}
				}
				catch (NullPointerException e){
					item = new ItemAdd("null",-1,-1);
					item.setEnabled(false);
					menu.add(item);			
				}
				sep = new JPopupMenu.Separator();
				menu.add(sep);

			}
			
		}
		catch (NullPointerException e){
			ItemAdd item = new ItemAdd("null",-1,-1);
			item.setEnabled(false);
			menu.add(item);			
		}
	}


	/**
	*  
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		buildMenuPopUp();
		menu.show(this,(int)this.getPreferredSize().getWidth(),-1);
	}


//********************** InnerClass *********************************
	class ItemAdd extends JMenuItem{

		private int identO;
		private int identF;

		public ItemAdd(String s,int identO, int identF){
			super(s);
			this.identO = identO;
			this.identF = identF;
		}

		public void fireActionPerformed(ActionEvent e){
			principal_window.Del(identO,identF);
		}
	}
} 
