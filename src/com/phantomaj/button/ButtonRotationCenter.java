/**
* ButtonRotationCenter.java
* 21/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.component.BasicInterfaceComponent_FormPanel;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Graphics2D;


import java.awt.Dimension;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.BasicStroke;
import javax.swing.ButtonGroup;
import java.awt.geom.Arc2D;
import java.util.Enumeration; 
import java.awt.geom.QuadCurve2D;

public class ButtonRotationCenter extends JPanel{

	private int formID=-1;
	private final static float dash1[] = {10.0f};
	private final static BasicStroke dashed = new BasicStroke(1.0f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f, dash1, 0.0f);
	private ButtonGroup bgrp;
	private int[][] positionButton;
	private BasicInterfaceComponent_FormPanel basicInterface;
	
	public ButtonRotationCenter( BasicInterfaceComponent_FormPanel bi){
		super();
		this.basicInterface = bi;
		this.setPreferredSize(new Dimension(100,100));
		this.setMinimumSize(new Dimension(100,100));
		this.setSize(new Dimension(100,100));
		this.setLayout(null);
	}

	private void build(Graphics2D g){
		if ( formID >= 0 ){
			double height = (double)this.getHeight()/2 - 40 ;
			double width = (double)this.getWidth()/2 - 80 ;
			
			//System.out.println("Height = "+height+"\twidht  = "+width);
			// System.out.println("Enter in Build");
			if ( g != null ){
				g.setColor(Color.blue);
				int[] x={60,60,55,60,65,60,60,130,130,140,130,130,60,111,101,111,111,60};
				int[] y={10,70,70,80,70,70,10,10,5,10,15,10,10,60,60,50,60,10};
				g.drawPolygon(x,y,x.length);
				g.fillPolygon(x,y,x.length);
				g.drawString("x", 115, 50 );
				g.drawString("y", 50, 85 );
				g.drawString("z", 145 , 10 );
				switch(formID){
					//Box
					case 0:
						// Draw a 2D representation of a BOX
						// System.out.println("Draw BOX");
						g.draw(new Line2D.Double(width,height,width+130,height));
						g.draw(new Line2D.Double(width,height,width,height+60));
						g.draw(new Line2D.Double(width,height+60,width+30,height+80));
						g.draw(new Line2D.Double(width+130,height,width+160,height+20));
						g.draw(new Line2D.Double(width+160,height+20,width+160,height+80));
						g.draw(new Line2D.Double(width+160,height+80,width+30,height+80));
						g.draw(new Line2D.Double(width+30,height+80,width+30,height+20));
						g.draw(new Line2D.Double(width+30,height+20,width+160,height+20));
						g.draw(new Line2D.Double(width+30,height+20,width,height));
						
						g.setStroke(dashed);
						g.draw(new Line2D.Double(width,height+60,width+130,height+60));
						g.draw(new Line2D.Double(width+130,height+60,width+160,height+80));
						g.draw(new Line2D.Double(width+130,height+60,width+130,height));
						break;
					//Cone
					case 1:
						g.draw(new Arc2D.Double(width,height,30,90,90,180,Arc2D.OPEN) );
						g.draw(new Line2D.Double(width+20,height,width+160,height+45) );
						g.draw(new Line2D.Double(width+20,height+90,width+160,height+45) );
						
						g.setStroke(dashed);
						g.draw(new Arc2D.Double(width,height,30,90,270,180,Arc2D.OPEN) );
						break;
					//Pyramid
					case 2:
						g.draw(new Line2D.Double(width,height+20,width,height+80));
						g.draw(new Line2D.Double(width+30,height,width,height+20));
						g.draw(new Line2D.Double(width,height+20,width+160,height+40));
						g.draw(new Line2D.Double(width,height+80,width+160,height+40));
						g.draw(new Line2D.Double(width+30,height,width+160,height+40));
						
						g.setStroke(dashed);
						g.draw(new Line2D.Double(width+30,height,width+30,height+60));
						g.draw(new Line2D.Double(width+30,height+60,width+160,height+40));
						g.draw(new Line2D.Double(width,height+80,width+30,height+60));
						break;
					//sphere
					case 3:
						g.draw(new Arc2D.Double(width,height,80,80,0,360,Arc2D.OPEN) );
						break;
					//tube
					case 4:
						g.draw(new Line2D.Double(width+15,height,width+160,height));
						g.draw(new Line2D.Double(width+15,height+80,width+160,height+80));
						g.draw(new Arc2D.Double(width,height,30,80,90,180,Arc2D.OPEN));
						g.draw(new Arc2D.Double(width+145,height,30,80,0,360,Arc2D.OPEN));
						
						g.setStroke(dashed);
						g.draw(new Arc2D.Double(width,height,30,80,270,180,Arc2D.OPEN));
						break;
					//ellipse
					case 5:
						g.draw(new Arc2D.Double(width,height,160,80,0,360,Arc2D.OPEN));
						g.draw(new Arc2D.Double(width,height+20,160,40,180,180,Arc2D.OPEN));
						
						g.setStroke(dashed);
						g.draw(new Arc2D.Double(width,height+20,160,40,0,180,Arc2D.OPEN));
						break;
				}
			}
		}
	}
	
	public void setForm(int formID){
		
		this.removeAll();
		
		RadioCenterButton _centerDefault;
		RadioCenterButton _East_1;
		RadioCenterButton _East_2;
		RadioCenterButton _East_3;
		RadioCenterButton _East_4;
		RadioCenterButton _West_1;
		RadioCenterButton _West_2;
		RadioCenterButton _West_3;
		RadioCenterButton _West_4;
		
		double height = (double)this.getHeight()/2 - 40 ;
		double width = (double)this.getWidth()/2 - 80 ;
		//System.out.println("Height = "+height+"\twidht  = "+width);
		this.formID = formID;
		switch(formID){
			//Box
			case 0:
				bgrp = new ButtonGroup();
				positionButton = new int[9][2];
			
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+80,(int)height+40,20,20);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 80 ;
				positionButton[0][1] = 40 ;
				
				//East
				
				_East_1 = new RadioCenterButton(1,this.basicInterface);
				this.add(_East_1);
				_East_1.setBounds((int)width+120,(int)height-10,20,20);
			
				positionButton[1][0] = 120 ;
				positionButton[1][1] = -10 ;
			
				_East_2 = new RadioCenterButton(2,this.basicInterface);
				this.add(_East_2);
				_East_2.setBounds((int)width+120,(int)height+50,20,20);
				
				positionButton[2][0] = 120 ;
				positionButton[2][1] = 50 ;
				
				_East_3 = new RadioCenterButton(3,this.basicInterface);
				this.add(_East_3);
				_East_3.setBounds((int)width+150,(int)height+70,20,20);
				
				positionButton[3][0] = 150 ;
				positionButton[3][1] = 70 ;
				
				_East_4 = new RadioCenterButton(4,this.basicInterface);
				this.add(_East_4);
				_East_4.setBounds((int)width+150,(int)height+10,20,20);
				
				positionButton[4][0] = 150 ;
				positionButton[4][1] = 10 ;
				
				// West
				
				_West_1 = new RadioCenterButton(5,this.basicInterface);
				this.add(_West_1);
				_West_1.setBounds((int)width-10,(int)height-10,20,20);
				
				positionButton[5][0] = -10 ;
				positionButton[5][1] = -10 ;
				
				_West_2 = new RadioCenterButton(6,this.basicInterface);
				this.add(_West_2);
				_West_2.setBounds((int)width+20,(int)height+10,20,20);
				
				positionButton[6][0] = 20 ;
				positionButton[6][1] = 10 ;
				
				_West_3 = new RadioCenterButton(7,this.basicInterface);
				this.add(_West_3);
				_West_3.setBounds((int)width-10,(int)height+50,20,20);
				
				positionButton[7][0] = -10 ;
				positionButton[7][1] = 50 ;
				
				_West_4 = new RadioCenterButton(8,this.basicInterface);
				this.add(_West_4);
				_West_4.setBounds((int)width+20,(int)height+70,20,20);
				
				positionButton[8][0] = 20 ;
				positionButton[8][1] = 70 ;
				
				bgrp.add(_centerDefault);
				bgrp.add(_East_1);
				bgrp.add(_East_2);
				bgrp.add(_East_3);
				bgrp.add(_East_4);
				bgrp.add(_West_1);
				bgrp.add(_West_2);
				bgrp.add(_West_3);
				bgrp.add(_West_4);
				break;
			//Cone
			case 1:
				bgrp = new ButtonGroup();
				positionButton = new int[3][2];
				
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+70,(int)height+35,15,15);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 70 ;
				positionButton[0][1] = 35 ;
				
				_East_1 = new RadioCenterButton(1,this.basicInterface);
				this.add(_East_1);
				_East_1.setBounds((int)width+150,(int)height+35,15,15);
				
				positionButton[1][0] = 150 ;
				positionButton[1][1] = 35 ;
				
				_West_1 = new RadioCenterButton(2,this.basicInterface);
				this.add(_West_1);
				_West_1.setBounds((int)width+5,(int)height+35,15,15);
				
				positionButton[2][0] = +5 ;
				positionButton[2][1] = 35 ;
				
				bgrp.add(_centerDefault);
				bgrp.add(_East_1);
				bgrp.add(_West_1);
				break;
			//Pyramid
			case 2:
				bgrp = new ButtonGroup();
				positionButton = new int[6][2];
				
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+70,(int)height+30,15,15);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 70 ;
				positionButton[0][1] = 35 ;
				
				//East
				
				_East_1 = new RadioCenterButton(1,this.basicInterface);
				this.add(_East_1);
				_East_1.setBounds((int)width+150,(int)height+30,15,15);
			
				positionButton[1][0] = 150 ;
				positionButton[1][1] = 30 ;
				
				// West
				
				_West_1 = new RadioCenterButton(2,this.basicInterface);
				this.add(_West_1);
				_West_1.setBounds((int)width-10,(int)height+10,20,20);
				
				positionButton[2][0] = -10 ;
				positionButton[2][1] = +10 ;
				
				_West_2 = new RadioCenterButton(3,this.basicInterface);
				this.add(_West_2);
				_West_2.setBounds((int)width+20,(int)height-10,20,20);
				
				positionButton[3][0] = 20 ;
				positionButton[3][1] = -10 ;
				
				_West_3 = new RadioCenterButton(4,this.basicInterface);
				this.add(_West_3);
				_West_3.setBounds((int)width-10,(int)height+70,20,20);
				
				positionButton[4][0] = -10 ;
				positionButton[4][1] = 70 ;
				
				_West_4 = new RadioCenterButton(5,this.basicInterface);
				this.add(_West_4);
				_West_4.setBounds((int)width+20,(int)height+50,20,20);
				
				positionButton[5][0] = 20 ;
				positionButton[5][1] = 50 ;
				
				bgrp.add(_centerDefault);
				bgrp.add(_East_1);
				bgrp.add(_West_1);
				bgrp.add(_West_2);
				bgrp.add(_West_3);
				bgrp.add(_West_4);
				
				break;
			//sphere
			case 3:
				bgrp = new ButtonGroup();
				positionButton = new int[1][2];
				
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+30,(int)height+30,15,15);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 30 ;
				positionButton[0][1] = 30 ;
				
				bgrp.add(_centerDefault);
				break;
			//tube
			case 4:
				bgrp = new ButtonGroup();
				positionButton = new int[3][2];
			
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+70,(int)height+30,15,15);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 70 ;
				positionButton[0][1] = 30 ;
				
				//East
				
				_East_1 = new RadioCenterButton(1,this.basicInterface);
				this.add(_East_1);
				_East_1.setBounds((int)width+150,(int)height+30,15,15);
			
				positionButton[1][0] = 150 ;
				positionButton[1][1] = 30 ;
				
				// West
				
				_West_1 = new RadioCenterButton(2,this.basicInterface);
				this.add(_West_1);
				_West_1.setBounds((int)width+5,(int)height+30,20,20);
				
				positionButton[2][0] = +5 ;
				positionButton[2][1] = +30 ;
				
				bgrp.add(_centerDefault);
				bgrp.add(_East_1);
				bgrp.add(_West_1);
				break;
			//ellipse
			case 5:
				bgrp = new ButtonGroup();
				positionButton = new int[5][2];
			
				_centerDefault = new RadioCenterButton(0,this.basicInterface);
				this.add(_centerDefault);
				_centerDefault.setBounds((int)width+70,(int)height+30,20,20);
				_centerDefault.setSelected(true);
				
				positionButton[0][0] = 70 ;
				positionButton[0][1] = 30 ;
				
				//East
				
				_East_1 = new RadioCenterButton(1,this.basicInterface);
				this.add(_East_1);
				_East_1.setBounds((int)width+150,(int)height+30,20,20);
			
				positionButton[1][0] = 150 ;
				positionButton[1][1] = 30 ;
				
				//North
				
				_East_2 = new RadioCenterButton(2,this.basicInterface);
				this.add(_East_2);
				_East_2.setBounds((int)width+70,(int)height-10,20,20);
			
				positionButton[2][0] = 70 ;
				positionButton[2][1] = -10 ;
				
				// West
				
				_West_1 = new RadioCenterButton(3,this.basicInterface);
				this.add(_West_1);
				_West_1.setBounds((int)width-10,(int)height+30,20,20);
				
				positionButton[3][0] = -10 ;
				positionButton[3][1] = +30 ;
				
				//South
				
				_West_2 = new RadioCenterButton(4,this.basicInterface);
				this.add(_West_2);
				_West_2.setBounds((int)width+70,(int)height+70,20,20);
				
				positionButton[4][0] = +70 ;
				positionButton[4][1] = +70 ;
				
				bgrp.add(_centerDefault);
				bgrp.add(_East_1);
				bgrp.add(_East_2);
				bgrp.add(_West_1);
				bgrp.add(_West_2);
				break;
		}
		this.repaint();
	}
	
	private void updateButtonPosition(){
		Enumeration buttons = bgrp.getElements();
		double height = (double)this.getHeight()/2 - 40 ;
		double width = (double)this.getWidth()/2 - 80 ;
		int i = 0;
		while ( buttons.hasMoreElements() ){
			RadioCenterButton button = (RadioCenterButton)buttons.nextElement();
			button.setBounds((int)width + positionButton[i][0],(int)height + positionButton[i][1],20,20);
			i += 1 ;
		}
	}
	
	/**
	*@Override 
	**/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		build((Graphics2D)g);
		updateButtonPosition();
    }
	
	
	
}
	



