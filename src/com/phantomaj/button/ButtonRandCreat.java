
/**
* ButtonRandCreat.java
* 11/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;

import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.JButton;
import java.awt.event.ActionEvent;

/**
* Button from the RandomGeneratorFrame
**/
public class ButtonRandCreat extends JButton{

	private RandomGeneratorFrame rand_window;

	//Constructor 1
	public ButtonRandCreat(String name, RandomGeneratorFrame rand_window){
		super(name);
		this.rand_window = rand_window;
	}

	
	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		this.rand_window.creatStack();
	}
}