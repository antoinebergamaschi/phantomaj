/**
* ButtonChangeView3D.java
* 20/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.button;
import com.phantomaj.randomGenerator.Button_panel;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;

public class ButtonChangeView3D extends JRadioButton{

	private Button_panel rand_frame;
	private int identifier;
	
//#############################################################################
//Constructor		
	//identifier 0,1 => form, loadedFantom
	public ButtonChangeView3D(String title, int identifier, Button_panel rand_frame){
		super(title);
		this.identifier = identifier;
		this.rand_frame = rand_frame;
	}
	
	/**
	* @override AbstractButton.fireActionPerformed
	**/
	public void fireActionPerformed(ActionEvent e) {
		this.rand_frame.setView3D(this.identifier);
	}



}