/**
*MenuRandom.java
*05/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.menu;

//perso
import com.phantomaj.PhantoMaJ;


import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;

public class MenuRandom extends JMenu{

	private PhantoMaJ principal_window;

	public MenuRandom(PhantoMaJ principal_window){
		super("Random Generator");
		this.principal_window = principal_window;
		this.build();
	}
	
	private void build(){
		ItemRandom choix1 = new ItemRandom("random generator F",0);

		this.add(choix1);
	}
//####################################################################
//InnerClass	
	class ItemRandom extends JMenuItem {

		public ItemRandom(String menuName,int id_rand){
			super(menuName);
		}
		
		public void fireActionPerformed(ActionEvent e){	
			System.out.println("Fire randomize MenuRandom.java l.43");
			principal_window.randomizeFrame();
		} 
	}	
	
}