/**
 * MenuAbout.java
 * 20/04/2012
 * @author Antoine Bergamaschi
 **/

package com.phantomaj.menu;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;

import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.filechooser.Utils;

@SuppressWarnings("serial")
public class MenuAbout extends JMenu {

	private PhantoMaJ principal_window;

	public MenuAbout(PhantoMaJ principal_window) {
		super("?");
		this.principal_window = principal_window;
		build();
	}

	private void build() {

		ItemAbout choix1 = new ItemAbout("About us");
		ItemHelp choix2 = new ItemHelp("Help");

		this.add(choix1);
		this.add(choix2);

	}

	// ####################################################################
	// InnerClass
	class ItemAbout extends JMenuItem {

		public ItemAbout(String menuName) {
			super(menuName);
		}

		public void fireActionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			FrameAbout fa = new FrameAbout("About us");
		}
	}

	class ItemHelp extends JMenuItem {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ItemHelp(String menuName) {
			super(menuName);
		}

		public void fireActionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			FrameHelp fh = new FrameHelp("Help");
		}
	}

	class FrameAbout extends JFrame {

		public FrameAbout(String title) {
			super(title);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			buildFrame();

			this.setVisible(true);
		}

		private void buildFrame() {
			this.setContentPane(buildContentPane());
			this.pack();
			setResizable(false);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();

			int windowX = Math
					.max(0, (screenSize.width - windowSize.width) / 2);
			int windowY = Math.max(0,
					(screenSize.height - windowSize.height) / 2);

			setLocation(windowX, windowY);
			this.repaint();
		}

		private JPanel buildContentPane() {
			JPanel pane = new JPanel(new GridBagLayout());

//			URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("phantomFinal_Icone"));
//			Image pmage = Toolkit.getDefaultToolkit().getImage(url);
//
//			url = getClass().getResource(PhantoMaJ.resourceBundle.getString("institut_curie_p"));
//			Image curie = Toolkit.getDefaultToolkit().getImage(url);

			JLabel label = new JLabel(
					"<html><h1> PhantoMaJ </h1><p>version : BETA 1.0</p><br><h3>Authors :</h3><<p>Bergamaschi Antoine</a></p><p>Cedric Messaoudi</p><p>Sergio Marco</p><h3>Contact us :</h3><p>antoine.bergamaschi@curie.fr</p><p>Cedric.Messaoudi@curie.fr</p><p>sergio.marco@curie.fr</p><br><p>Institut Curie / INSERM U759 </p><p>Campus Universitaire d'Orsay, Bat 112 </p><p>91405 Orsay cedex FRANCE</p></html>");
			label.setIcon(Utils.getResourceIcon("phantomFinal_Icone"));

			// anchor,insetsTop, insetLeft,insetBottom,insetRight,
			// fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent
			// container, JComponent 2
			addGridBag(GridBagConstraints.NORTH, 0, 0, 0, 0,
					GridBagConstraints.BOTH, 1, 1, 0, 0, 1, 1, 0, 0, pane,
					label);

			label = new JLabel();
			label.setIcon(Utils.getResourceIcon("institut_curie_p"));

			addGridBag(GridBagConstraints.EAST, 0, 0, 0, 0,
					GridBagConstraints.NONE, 0, 0, 0, 0, 1, 1, 0, 1, pane,
					label);

			return pane;
		}

		private void addGridBag(int anchor, int insetTop, int insetLeft,
				int insetBottom, int insetRight, int fill, double weightx,
				double weighty, int ipadx, int ipady, int width, int height,
				int gridx, int gridy, JComponent container, JComponent component) {
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop, insetLeft, insetBottom, insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
	}

	class FrameHelp extends JFrame {

		public FrameHelp(String title) {
			super(title);
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			buildFrame();
			this.setVisible(true);
		}

		private void buildFrame() {

			this.setContentPane(buildContentPane());

			this.setSize(new Dimension(900, 1000));
			setResizable(false);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();

			int windowX = Math
					.max(0, (screenSize.width - windowSize.width) / 2);
			int windowY = Math.max(0,
					(screenSize.height - windowSize.height) / 2);

			setLocation(windowX, windowY);
			this.repaint();
		}

		private JScrollPane buildContentPane() {

			JEditorPane editorPane = new JEditorPane();
			editorPane.setEditable(false);
			URL helpURL = getClass().getResource(Utils.getResourceValue("help_html"));
			if (helpURL != null) {
				try {
					editorPane.setPage(helpURL);
				} catch (IOException e) {
					System.err.println("Attempted to read a bad URL: "
							+ helpURL);
				}
			} else {
				System.err
						.println("Couldn't find file: TextSamplerDemoHelp.html");
			}

			// Put the editor pane in a scroll pane.
			JScrollPane editorScrollPane = new JScrollPane(editorPane);
			editorScrollPane
					.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			editorScrollPane
					.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			editorScrollPane.setPreferredSize(new Dimension(800, 1000));
			editorScrollPane.setMinimumSize(new Dimension(10, 10));

			return editorScrollPane;
		}

		// private void addGridBag(int anchor,int insetTop,int insetLeft,int
		// insetBottom, int insetRight,int fill,double weightx,double
		// weighty,int ipadx,int ipady,int width, int height, int gridx,int
		// gridy, JComponent container, JComponent component){
		// GridBagConstraints c = new GridBagConstraints();
		// c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		// c.fill = fill;
		// c.anchor = anchor;
		// c.weightx = weightx;
		// c.weighty = weighty;
		// c.ipadx = ipadx;
		// c.ipady = ipady;
		// c.gridx = gridx;
		// c.gridy = gridy;
		// c.gridwidth = width;
		// c.gridheight = height;
		// container.add(component, c);
		// }
	}

}
