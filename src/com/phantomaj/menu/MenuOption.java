package com.phantomaj.menu;
import com.phantomaj.PhantoMaJ;
import com.phantomaj.filechooser.FileChooserFrame;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;
import java.lang.Thread;


public class MenuOption extends JMenu{

	private PhantoMaJ principal_window;

	public MenuOption(PhantoMaJ principal_window){
		super("Option");
		this.principal_window = principal_window;
		build();
	}

	private void build(){

		ItemSave choix1 = new ItemSave("Save",this.principal_window);
		ItemLoad choix2 = new ItemLoad("Load",this.principal_window);
		ItemClear choix3 = new ItemClear("Clear Tree",this.principal_window);
		ItemRefresh choix4 = new ItemRefresh("Refresh 3D view",this.principal_window);

		this.add(choix1);
		this.addSeparator();
		this.add(choix2);
		this.addSeparator();
		this.add(choix3);
		this.add(choix4);
	}

}


//####################################################################
//InnerClass

class ItemSave extends JMenuItem {

	private PhantoMaJ principal_window;

	public ItemSave(String menuName,PhantoMaJ principal_window){
		super(menuName);
		this.principal_window = principal_window;
	}
	
	public void fireActionPerformed(ActionEvent e){	
		FileChooserFrame savefc = new FileChooserFrame(1);
		try{
			principal_window.save(savefc.getFile().getPath());
		}
		catch(NullPointerException ev){
		}
		finally{
			savefc.dispose();
		}
	} 
}


class ItemLoad extends JMenuItem {

	private PhantoMaJ principal_window;

	public ItemLoad(String menuName,PhantoMaJ principal_window){
		super(menuName);
		this.principal_window = principal_window;
	}
	
	public void fireActionPerformed(ActionEvent e){ 
		final FileChooserFrame df = new FileChooserFrame(0);
		try{
			new Thread(){
				public void run(){
					principal_window.open(df.getFile().getPath());
			}}.start();
		}
		catch(NullPointerException ev){
		}
		finally{
			df.dispose();
		}
	} 
}

class ItemClear extends JMenuItem{

	private PhantoMaJ principal_window;

	public ItemClear(String menuName,PhantoMaJ principal_window){
		super(menuName);
		this.principal_window = principal_window;
	}
	
	public void fireActionPerformed(ActionEvent e){ 		
		principal_window.clearTree();
	} 

}

class ItemRefresh extends JMenuItem{

	private PhantoMaJ principal_window;

	public ItemRefresh(String menuName,PhantoMaJ principal_window){
		super(menuName);
		this.principal_window = principal_window;
	}
	
	public void fireActionPerformed(ActionEvent e){ 		
		principal_window.reload3DView();
	} 

}

class ItemLoadO extends JMenuItem{

	private PhantoMaJ principal_window;

	public ItemLoadO(String menuName,PhantoMaJ principal_window){
		super(menuName);
		this.principal_window = principal_window;
	}
	
	public void fireActionPerformed(ActionEvent e){ 		
		//principal_window.loadO();
	} 

}


