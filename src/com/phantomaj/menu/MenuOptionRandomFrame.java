/**
*MenuOptionRandomFrame.java
*14/06/2012
*@author Antoine Bergamaschi
*/


package com.phantomaj.menu;

import com.phantomaj.filechooser.FileChooserFrame;
import com.phantomaj.randomGenerator.RandGlobalParameters;
import com.phantomaj.randomGenerator.RandGlobalParametersPosition;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionEvent;


public class MenuOptionRandomFrame extends JMenu{

	private RandomGeneratorFrame rand_window;

	public MenuOptionRandomFrame(RandomGeneratorFrame rand_window){
		super("Option");
		this.rand_window = rand_window ;
		build();
	}
	
	private void build(){
		ItemParameters choix1 = new ItemParameters("parameters",this.rand_window);
		ItemLoad choix2 = new ItemLoad("Load stack",this.rand_window);
		ItemParametersPosition choix3 = new ItemParametersPosition("Position parameters",this.rand_window);
		
		this.add(choix1);
		this.add(choix3);
		this.add(choix2);
	}
	
	//##############################################################################
	//InnerClass
	
	class ItemParameters extends JMenuItem{
			
		private RandomGeneratorFrame rand_window;
			
		public ItemParameters(String title,RandomGeneratorFrame rand_window){
			super(title);
			this.rand_window = rand_window;
		}
		
		public void fireActionPerformed(ActionEvent e){
			//Launch new Parameters frame
			RandGlobalParameters pf = new RandGlobalParameters(this.rand_window);
			pf.setFrame(this.rand_window.getStackDim(),this.rand_window.getNbform(),this.rand_window.getNbTry(),this.rand_window.getBasicDim());
		}
	}
	

	
	class ItemLoad extends JMenuItem{
			
		private RandomGeneratorFrame rand_window;
			
		public ItemLoad(String title,RandomGeneratorFrame rand_window){
			super(title);
			this.rand_window = rand_window;
		}
		
		public void fireActionPerformed(ActionEvent e){
			FileChooserFrame df = new FileChooserFrame(0);
			try{
				this.rand_window.open(df.getFile().getPath(),0);
			}
			catch(NullPointerException ev){
			}
			finally{
				df.dispose();
			}
		}
	}
	
	
	class ItemParametersPosition extends JMenuItem{
		
		private int type;
		private RandomGeneratorFrame rand_window;
			
		public ItemParametersPosition(String title,RandomGeneratorFrame rand_window){
			super(title);
			this.type = type;
			this.rand_window = rand_window;
		}
		
		public void fireActionPerformed(ActionEvent e){
			//Launch new Parameters frame
			RandGlobalParametersPosition pf = new RandGlobalParametersPosition(this.rand_window);
			pf.setFrame(this.rand_window.getStackDim(), this.rand_window.getDimRand(),this.rand_window.getRandType());
		}
	}

}