/**
* MenuOptionDraw.java
* 09/07/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.menu;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.component.InterfaceComponent_GraphicsIntensity;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;
import com.phantomaj.utils.Compute_Intensity;
import com.phantomaj.utils.Thread_option;

public class MenuOptionDraw extends JMenu{

	private PhantoMaJ principal_window;
	private RandomGeneratorFrame rand_window;
	
	/**
	* Constructor for PhantoMaJ
	**/
	public MenuOptionDraw(PhantoMaJ principal_window){
		super("Drawing Option");
		this.principal_window = principal_window;
		this.rand_window = null;
		build();
	}

	/**
	* Constructor for RandomGeneratorFrame
	**/
	public MenuOptionDraw(RandomGeneratorFrame rand_window){
		super("Drawing Option");
		this.rand_window = rand_window;
		this.principal_window = null;
		build();
	}
	
	private void build(){

		ItemIntensity choix1 = new ItemIntensity("Intensity decrease");
		ItemThread choix2 = new ItemThread("Multi-threading");
		ItemIntersection choix3 = new ItemIntersection("Form and Object intersection");

		this.add(choix1);
		this.add(choix2);
		this.add(choix3);
		
	}



	//####################################################################
	//InnerClass
	//MenuItem
	
	class ItemIntensity extends JMenuItem {

		public ItemIntensity(String menuName){
			super(menuName);
		}
		
		public void fireActionPerformed(ActionEvent e){	
			Compute_Intensity Cint = null;
			FrameIntensity fi = new FrameIntensity();
			if ( rand_window == null ){
				Cint = principal_window.getCompute_Intensity();
			}
			else {
				Cint = rand_window.getCompute_Intensity();
			}
			fi.setFrame(Cint.getFunctionId(),Cint.getCstValue(),Cint.getIsDouble(),Cint.getIsInverted(),Cint.getIsCstSel());
			System.out.println("Fire Intensity Frame");
		} 
	}


	class ItemThread extends JMenuItem {


		public ItemThread(String menuName){
			super(menuName);
		}
		
		public void fireActionPerformed(ActionEvent e){ 
			System.out.println("Fire Thread Frame");
			FrameThread ft = new FrameThread();
			Thread_option Toption = null ;
			if ( rand_window == null ){
				Toption = principal_window.getThreadOption();
			}
			else {
				Toption = rand_window.getThreadOption();
			}
			ft.setFrame(Toption.getNumberOfProcessor());
		} 
	}

	class ItemIntersection extends JMenuItem{

		public ItemIntersection(String menuName){
			super(menuName);
		}
		
		public void fireActionPerformed(ActionEvent e){	
			Compute_Intensity Cint = null;
			FrameIntersection fi = new FrameIntersection();
			if ( rand_window == null ){
				Cint = principal_window.getCompute_Intensity();
			}
			else {
				Cint = rand_window.getCompute_Intensity();
			}
			fi.setFrame(Cint);
			System.out.println("Fire Intensity Frame");
		}  

	}
	
	//####################################################################
	//InnerClass
	//Frame
	
	class FrameIntensity extends JFrame implements ActionListener,ChangeListener{
		
		private ButtonGroup Bgrp;
		
		private JCheckBox invertedBox;
		private JCheckBox doubleBox;
		private JCheckBox radius_;
		
		private InterfaceComponent_GraphicsIntensity graphics;
		private final Border blackline = BorderFactory.createLineBorder(Color.black);
		private JSlider slider;
		
		private final String[] tooltipFunction = {"f(x) = cos( pi / 2 ) * x",
		"f(x) = 1 - exp( -6 * (1-x) )",
		"f(x) = exp( -6 * x )",
		"f(x) = 1-x",
		"f(x) = 1 or f(x) = 0"
		};
		
		private String[] buttonName = { "Cosinus",
		"Logarithm",
		"Exponential",
		"Linear",
		"Binary",
		"Cst Radius (0%)"
		};
	
		public FrameIntensity(){
			super("Intensity Function");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			buildFrame();
			this.setVisible(true);
		}
		
		private void buildFrame(){
			
			this.setContentPane(buildContentPane());

			setResizable(false);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();

			int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
			int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);
			
			setLocation(windowX, windowY); 
			this.pack();
			this.repaint();
		}
		
		private JPanel buildContentPane(){
			JPanel pane = new JPanel(new GridBagLayout());	
			Bgrp = new ButtonGroup();
			JPanel infoPanel = new JPanel(new GridBagLayout());
			
			JLabel label = new JLabel("");
			label = new JLabel("<html><h3>Intensity Function</h3></html>");
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,70,0,70,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,pane,label);	
			
			//*****************************************************************
			//Construct infoPanel
			
			JRadioButton button = new JRadioButton(buttonName[0]);
			Bgrp.add(button);
			button.setToolTipText(this.tooltipFunction[0]);
			button.setSelected(true);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,infoPanel,button);	
			
			button = new JRadioButton(buttonName[1]);
			button.setToolTipText(this.tooltipFunction[1]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,infoPanel,button);	
			
			
			button = new JRadioButton(buttonName[2]);
			button.setToolTipText(this.tooltipFunction[2]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,infoPanel,button);	

			
			button = new JRadioButton(buttonName[3]);
			button.setToolTipText(this.tooltipFunction[3]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,infoPanel,button);	
			
			
			button = new JRadioButton(buttonName[4]);
			button.setToolTipText(this.tooltipFunction[4]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,4,infoPanel,button);	
			
			//*************************************************************************
			//Fin construct
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,pane,infoPanel);

			//set Border
			infoPanel.setBorder(blackline);
			
			//*************************************************************************
			//Construct infoPanel
			infoPanel = new JPanel(new GridBagLayout());
			
			invertedBox = new JCheckBox("Inverted function");
			invertedBox.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,infoPanel,invertedBox);	
			
			//*************************************************************************
			//Fin construct
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,pane,infoPanel);
			
			//set Border
			infoPanel.setBorder(blackline);
			
			
			//*************************************************************************
			//Construct infoPanel
			infoPanel = new JPanel(new GridBagLayout());
			
			radius_ = new JCheckBox(buttonName[5]+"00");
			radius_.setPreferredSize(radius_.getPreferredSize());
			radius_.setMinimumSize(radius_.getPreferredSize());
			radius_.setText(buttonName[5]);
			radius_.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,0,infoPanel,radius_);	
			
			this.slider = new JSlider(JSlider.HORIZONTAL,0,100,0);
			this.slider.addChangeListener(this);
			this.slider.setEnabled(false);
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,0,infoPanel,slider);
			
			//*************************************************************************
			//Fin construct
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,pane,infoPanel);
			
			//set Border
			infoPanel.setBorder(blackline);
			
			//*************************************************************************
			//Construct infoPanel
			infoPanel = new JPanel(new GridBagLayout());
			
			doubleBox = new JCheckBox("Middle function");
			doubleBox.addActionListener(this);

			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,infoPanel,doubleBox);	
			
			//*************************************************************************
			//Fin construct
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,4,pane,infoPanel);
			
			//set Border
			infoPanel.setBorder(blackline);
			
			graphics = new InterfaceComponent_GraphicsIntensity();
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,5,5,5,GridBagConstraints.BOTH,1,1,0,0,1,4,1,1,pane,graphics);	
			
			
			JButton _apply = new JButton("Apply");
			_apply.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,70,5,70,GridBagConstraints.BOTH,1,1,0,0,2,1,0,5,pane,_apply);	
			
			return pane;
		}
		
		/**
		* @Override ActionListener.actionPerformed(ActionEvent e)
		**/
		public void actionPerformed(ActionEvent e){
			if ( e.getSource() instanceof JRadioButton ){
				JRadioButton button = (JRadioButton)e.getSource();
				if ( button.getText() == buttonName[0] ){
					graphics.changeFunction(0);
				}
				else if ( button.getText() == buttonName[1] ){
					graphics.changeFunction(1);
				}
				else if ( button.getText() == buttonName[2] ){
					graphics.changeFunction(2);
				}
				else if ( button.getText() == buttonName[3] ){
					graphics.changeFunction(3);
				}
				else if ( button.getText() == buttonName[4]){
					graphics.changeFunction(4);
				}
			}
			else if ( e.getSource() instanceof JCheckBox ){ 
				JCheckBox button = (JCheckBox)e.getSource();
				if ( button.getText() == "Inverted function" ){
					graphics.changeInverted();
				}
				else if ( button.getText() == buttonName[5] ){
					if ( button.isSelected() ){
						this.slider.setEnabled(true);
						buttonName[5] = "Cst radius ("+this.slider.getValue()+"%)";
						button.setText(buttonName[5]);
						this.pack();
						
						graphics.setRadiusCst(true);
					}
					else{
						this.slider.setEnabled(false);
						graphics.setRadiusCst(false);
					}
				}
				else if ( button.getText() == "Middle function" ){
					graphics.changeDouble();
				}
			}
			else if ( e.getSource() instanceof JButton ){
				JButton button = (JButton)e.getSource();
				if ( button.getText() == "Apply" ){
					if ( rand_window == null ){
						System.out.println("Apply new Computed intensity");
						principal_window.setComputeIntensity(this.graphics.getCompute_Intensity());
					}
					else{
						rand_window.setComputeIntensity(this.graphics.getCompute_Intensity());
					}
					this.dispose();
				}
			}
		}
		
		/**
		* Override ChangeListener.stateChanged(ChangeEvent e)
		**/
		public void stateChanged(ChangeEvent e) {
			if ( radius_.isSelected() ){
				buttonName[5] = "Cst radius ("+this.slider.getValue()+"%)";
				radius_.setText(buttonName[5]);
				this.pack();
				
				graphics.updateCstValue(this.slider.getValue());
			}
		}
		
		
		public void setFrame(int functionID, float cstValue, boolean isDouble, boolean isInverted, boolean isCstSelected){
			// System.out.println("Test :"+functionID);
			int i = 0;
			Enumeration buttons = Bgrp.getElements();
			while ( buttons.hasMoreElements() ){
				JRadioButton button = (JRadioButton)(buttons.nextElement());
				if ( i == functionID ){
					button.doClick();
				}
				i++;
			}

			if ( isDouble ){
				doubleBox.doClick();
			}

			if ( isInverted ){ 
				invertedBox.doClick();
			}

			if( isCstSelected ){
				radius_.doClick();
				this.slider.setValue((int)(cstValue*100));
			}
	
		}
		
		
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
	}
	
	
	//******************************************************************************************************************************************************
	//Frame FrameIntersection
	
	class FrameIntersection extends JFrame implements ActionListener{
		
		private ButtonGroup Bgrp;
		
		private final Border blackline = BorderFactory.createLineBorder(Color.black);
		
		private Compute_Intensity Cint = null;
		
		private static final int MOD_MOY = 0;
		private static final int MOD_LASTIN = 1;
		private static final int MOD_SUM = 2;
		private static final int MOD_NEG = 3;
		
		private int selectedMode = 0;
		private String[] buttonName = { "Mean value",
		"Last draw value",
		"Sum value",
		"Negative value"};
	
		public FrameIntersection(){
			super("Intersection Mode");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			buildFrame();
			this.setVisible(true);
		}
		
		private void buildFrame(){
			
			this.setContentPane(buildContentPane());

			setResizable(false);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();

			int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
			int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);
			
			setLocation(windowX, windowY); 
			this.pack();
			this.repaint();
		}
		
		private JPanel buildContentPane(){
			JPanel pane = new JPanel(new GridBagLayout());	
			Bgrp = new ButtonGroup();
			JPanel infoPanel = new JPanel(new GridBagLayout());
			
			JLabel label = new JLabel("");
			label = new JLabel("<html><h3>Intersection Mode</h3></html>");
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,70,0,70,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,pane,label);	
			
			//*****************************************************************
			//Construct infoPanel
			
			JRadioButton button = new JRadioButton(buttonName[0]);
			Bgrp.add(button);
			// button.setToolTipText(this.tooltipFunction[0]);
			button.setSelected(true);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,infoPanel,button);	
			
			button = new JRadioButton(buttonName[1]);
			// button.setToolTipText(this.tooltipFunction[1]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,infoPanel,button);	
			
			
			button = new JRadioButton(buttonName[2]);
			// button.setToolTipText(this.tooltipFunction[2]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,infoPanel,button);	

			
			button = new JRadioButton(buttonName[3]);
			// button.setToolTipText(this.tooltipFunction[3]);
			Bgrp.add(button);
			button.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,infoPanel,button);	
			
			
			//*************************************************************************
			//Fin construct
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,pane,infoPanel);

			//set Border
			infoPanel.setBorder(blackline);
			
			//*************************************************************************
			//Fin construct
			
			JButton _apply = new JButton("Apply");
			_apply.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,70,5,70,GridBagConstraints.BOTH,1,1,0,0,2,1,0,2,pane,_apply);	
			
			return pane;
		}
		
		/**
		* Override ActionListener.actionPerformed(ActionEvent e)
		**/
		public void actionPerformed(ActionEvent e){
			if ( e.getSource() instanceof JRadioButton ){
				JRadioButton button = (JRadioButton)e.getSource();
				if ( button.getText() == buttonName[0] ){
					this.Cint.setModIntersection(FrameIntersection.MOD_MOY);
				}
				else if ( button.getText() == buttonName[1] ){
					this.Cint.setModIntersection(FrameIntersection.MOD_LASTIN);
				}
				else if ( button.getText() == buttonName[2] ){
					this.Cint.setModIntersection(FrameIntersection.MOD_SUM);
				}
				else if ( button.getText() == buttonName[3] ){
					this.Cint.setModIntersection(FrameIntersection.MOD_NEG);
				}
			}
			else if ( e.getSource() instanceof JButton ){
				JButton button = (JButton)e.getSource();
				if ( button.getText() == "Apply" ){
					if ( rand_window == null ){
						System.out.println("Apply new intersection Mode");
						principal_window.setComputeIntensity(this.Cint);
					}
					else{
						rand_window.setComputeIntensity(this.Cint);
					}
					this.dispose();
				}
			}
		}
		

		
		public void setFrame(Compute_Intensity Cint){
			// System.out.println("Test :"+functionID);
			this.Cint = Cint;
			int i = 0;
			Enumeration buttons = Bgrp.getElements();
			while ( buttons.hasMoreElements() ){
				JRadioButton button = (JRadioButton)(buttons.nextElement());
				if ( i == Cint.getModIntersection() ){
					button.doClick();
				}
				i++;
			}
	
		}
		
		
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
	}
	
	
	//******************************************************************************************************************************************************
	//Frame MultiThread
	
	class FrameThread extends JFrame implements ActionListener{
		
		private final Border blackline = BorderFactory.createLineBorder(Color.black);
		private JFormattedTextField textField;
	
		public FrameThread(){
			super("Multi-Threading");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			buildFrame();
			this.setVisible(true);
		}
		
		private void buildFrame(){

			
			this.setContentPane(buildContentPane());

			setResizable(false);
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();

			int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
			int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);
			
			setLocation(windowX, windowY); 
			this.pack();
			this.repaint();
		}
		
		private JPanel buildContentPane(){
			JPanel pane = new JPanel(new GridBagLayout());	
			
			JLabel label = new JLabel("");
			label = new JLabel("<html><h3>Multi-Threading</h3></html>");
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,1,0,0,2,1,0,0,pane,label);	
			
			//*****************************************************************
			//Construct infoPanel
			
			textField = new JFormattedTextField();
			textField.setValue(new Integer(0));
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,5,5,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,pane,textField);	
			
			label = new JLabel("Max processor available :: "+Runtime.getRuntime().availableProcessors());
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,0,5,5,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,1,pane,label);
			
			JButton _apply = new JButton("Apply");
			_apply.addActionListener(this);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2
			addGridBag(GridBagConstraints.CENTER,5,70,5,70,GridBagConstraints.BOTH,1,1,0,0,2,1,0,2,pane,_apply);	
			
			return pane;
		}
		
		/**
		* @Override ActionListener.actionPerformed(ActionEvent e)
		**/
		public void actionPerformed(ActionEvent e){
			JButton button = (JButton)e.getSource();
			if ( button.getText() == "Apply" ){
				if ( rand_window == null ){
					principal_window.setThreadNumber((Integer)(textField.getValue()));
					System.out.println("Thread Set to "+textField.getValue());
				}
				else{
					rand_window.setThreadNumber((Integer)(textField.getValue()));
				}
				this.dispose();
			}
		}
		
		public void setFrame(int numbProcess){
			textField.setValue(numbProcess);
		}
		
		private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
			GridBagConstraints c = new GridBagConstraints();
			c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
			c.fill = fill;
			c.anchor = anchor;
			c.weightx = weightx;
			c.weighty = weighty;
			c.ipadx = ipadx;
			c.ipady = ipady;
			c.gridx = gridx;
			c.gridy = gridy;
			c.gridwidth = width;
			c.gridheight = height;
			container.add(component, c);
		}
	}
	
	
}
