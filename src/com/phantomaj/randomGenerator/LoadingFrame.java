
/**
*LoadingFrame.java
*05/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.randomGenerator;

//import perso

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;


import java.awt.Dimension;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;


import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;

public class LoadingFrame extends JFrame implements ActionListener{

	private JLabel _todo;
	private int todo;
	private boolean already_end = false ;
	private JButton stop_end;
	
	public LoadingFrame(int todo){
		super("Loading...");
		this.setSize(new Dimension(250,150));
		this.todo = todo;
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 
		this.setContentPane(this.buildContentPane());
		// this.pack();
		this.setResizable(true); 
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		this.setVisible(true);
		// this.pack();
	}

	private JPanel buildContentPane(){
		JPanel pane = new JPanel(new GridBagLayout());
		_todo = new JLabel("Progress ........................... "+todo+"/"+todo);
		_todo.setPreferredSize(_todo.getPreferredSize());
		_todo.setText("Progress ........................... "+0+"/"+todo);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,10,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,pane,_todo);
		
		stop_end = new JButton("Stop");
		stop_end.addActionListener(this);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,5,10,5,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,pane,stop_end);
	
		
		return pane;
	}

	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	public void update(int remaining){
		remaining = todo - remaining; 
		// System.out.println(remaining);
		this._todo.setText("Progress ........... "+remaining+"/"+todo);
		this.validate();
		this.repaint();
		// this.pack();
	}
	
	public void end_load(int remaining){
		remaining = todo - remaining; 
		this._todo.setText("<html><h3>Thread successfully end</h3>Form Asked : "+todo+"<br>Form Created : "+remaining+"</html>");
		this.validate();
		this.repaint();
		// this.pack();
	}
	
	public boolean getStop(){
		return this.already_end;
	}
	
	public void setStop(boolean bol){
		if ( bol ){
			this.stop_end.setText("Close");
		}
		this.already_end = bol;
	}
	
	public void actionPerformed(ActionEvent e){
		if ( !already_end ){
			this.stop_end.setText("Close");
			already_end = true;
		}
		else{
			this.dispose();
		}
	}
}