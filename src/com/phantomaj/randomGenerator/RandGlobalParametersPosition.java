/**
*RandGlobalParametersPosition.java
*20/06/2012
*@author Antoine Bergamaschi
*/


package com.phantomaj.randomGenerator;

import com.phantomaj.button.ButtonHelp;
import com.phantomaj.select.SliderDim;
import com.phantomaj.select.TransfoCheckBox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;
import java.lang.ClassCastException;

public class RandGlobalParametersPosition extends JFrame implements ActionListener{

	private JLabel _dimXBegin=null;
	private JLabel _dimYBegin=null;
	private JLabel _dimZBegin=null;
	
	private JLabel _dimXEnd=null;
	private JLabel _dimYEnd=null;
	private JLabel _dimZEnd=null;

	private JLabel _meanLabelX=null;
	private JLabel _meanLabelY=null;
	private JLabel _meanLabelZ=null;
	

	
	
	private TransfoCheckBox _transfoChecking_dimX_form;
	private TransfoCheckBox _transfoChecking_dimY_form;
	private TransfoCheckBox _transfoChecking_dimZ_form;
	
	
	private SliderDim _uniformMinX;
	private SliderDim _uniformMinY;
	private SliderDim _uniformMinZ;
	private SliderDim _uniformMaxX;
	private SliderDim _uniformMaxY;
	private SliderDim _uniformMaxZ;
	
	private JPanel _uniform_distribParam;
	private JPanel _gaussian_distribParam;
	private JPanel _param;
	
	private RandomGeneratorFrame rand_window;
	
	private JRadioButton _uniformButton;
	private JRadioButton _gaussianButton;
	private JButton _apply;
	
	
	public RandGlobalParametersPosition(RandomGeneratorFrame rand_window){
		super("Parameters");
		this.rand_window = rand_window;
		this.build();
		this.setSelected(this.rand_window.getRand_type_pos());
		this.rand_window.ask_update(this);
	}

	
	private void build(){
		// this.setSize(new Dimension(200,400));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(this.buildContentPane());
		this.pack();
		this.setResizable(false); 
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		this.setVisible(true);
	}

	private final String[] text_help = { "Choose the center position for a Gaussian distribution of the position in 3D"};
	
	
	private JPanel buildContentPane(){
		//#######
		JLabel label = new JLabel();
		ButtonHelp help = new ButtonHelp("",null);
		//#######
		JPanel principal = new JPanel(new GridBagLayout());
	
		label = new JLabel("<html><h3>Position Random Generator Parameters</h3><html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,30,0,0,GridBagConstraints.BOTH,1,0,0,0,3,1,0,0,principal,label);		

	//##########################################################################
	//Choice panel
		JPanel _choose = new JPanel(new GridBagLayout());
		
		ButtonGroup Bgrp = new ButtonGroup();
		
		_uniformButton = new JRadioButton("Uniform");
		_gaussianButton = new JRadioButton("Gaussian");
		
		_uniformButton.addActionListener(this);
		_gaussianButton.addActionListener(this);
		
		Bgrp.add(_uniformButton);
		Bgrp.add(_gaussianButton);
		
		_uniformButton.setSelected(true);
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,_choose,_uniformButton);

		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,_choose,_gaussianButton);

		
	//##########################################################################
	//Param
		_param = new JPanel(new GridBagLayout());
		
	

	//##########################################################	
	// Uniform distribution
		_uniform_distribParam = new JPanel(new GridBagLayout());
		JComponent[] component = null;
		JPanel infoPane = null;
		label = new JLabel("") ;
	
	
	//X DIM	
		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_dimXBegin = new JLabel("00000");
		_dimXBegin.setPreferredSize(_dimXBegin.getPreferredSize());
		_dimXBegin.setMinimumSize(_dimXBegin.getPreferredSize());
		_dimXBegin.setText(0+"");
		
		_dimXEnd = new JLabel("00000");
		_dimXEnd.setPreferredSize(_dimXEnd.getPreferredSize());
		_dimXEnd.setMinimumSize(_dimXBegin.getPreferredSize());
		_dimXEnd.setText(200+"");
		
		_transfoChecking_dimX_form = new TransfoCheckBox("",null,null);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_dimX_form);

	
		label = new JLabel("X Dimension :");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_dimXBegin);
		//add component
		component[1] = _dimXBegin ;		
		
		
		_meanLabelX = new JLabel(" < Center (00000) < ");
		_meanLabelX.setPreferredSize(_meanLabelX.getPreferredSize());
		_meanLabelX.setMinimumSize(_meanLabelX.getPreferredSize());
		_meanLabelX.setText(" < Center (0) < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_meanLabelX);		
		//add component
		component[2] = _meanLabelX ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,4,0,infoPane,_dimXEnd);
		//add component
		component[3] = _dimXEnd ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.REMAINDER,1,0,0,0,4,1,0,0,_uniform_distribParam,infoPane);
		
		label = new JLabel("X range min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,_uniform_distribParam,label);				

		//add component
		component[4] = label ;
		
		_uniformMinX = new SliderDim(JSlider.HORIZONTAL,0,100,0,_dimXBegin,this,0,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,_uniform_distribParam,_uniformMinX);		
	
		//add component
		component[5] = _uniformMinX ;
	
		label = new JLabel("X range max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,_uniform_distribParam,label);

		//add component
		component[6] = label ;		
		
		_uniformMaxX = new SliderDim(JSlider.HORIZONTAL,0,200,0,_dimXEnd,this,1,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,_uniform_distribParam,_uniformMaxX);		
	
		//add component
		component[7] = _uniformMaxX ;
	
		//Set Component List
		_transfoChecking_dimX_form.setComponentTab(component);
		_transfoChecking_dimX_form.setSelected(false);

		
//Y DIM	
		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_dimYBegin = new JLabel("0000");
		_dimYBegin.setPreferredSize(_dimYBegin.getPreferredSize());
		_dimYBegin.setMinimumSize(_dimYBegin.getPreferredSize());
		_dimYBegin.setText(0+"");
		
		_dimYEnd = new JLabel("0000");
		_dimYEnd.setPreferredSize(_dimYEnd.getPreferredSize());
		_dimYEnd.setMinimumSize(_dimYEnd.getPreferredSize());
		_dimYEnd.setText(200+"");
		
		_transfoChecking_dimY_form = new TransfoCheckBox("",null,null);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_dimY_form);

	
		label = new JLabel(" Y Dimension :");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_dimYBegin);
		//add component
		component[1] = _dimYBegin ;		
		
		_meanLabelY = new JLabel(" < Center (00000) < ");
		_meanLabelY.setPreferredSize(_meanLabelY.getPreferredSize());
		_meanLabelY.setMinimumSize(_meanLabelY.getPreferredSize());
		_meanLabelY.setText(" < Center (0) < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_meanLabelY);		
		//add component
		component[2] = _meanLabelY ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_dimYEnd);
		//add component
		component[3] = _dimYEnd ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,2,_uniform_distribParam,infoPane);
		
		label = new JLabel("Y range min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,_uniform_distribParam,label);				

		//add component
		component[4] = label ;
		
		_uniformMinY = new SliderDim(JSlider.HORIZONTAL,0,100,0,_dimYBegin,this,2,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,1,3,_uniform_distribParam,_uniformMinY);		
	
		//add component
		component[5] = _uniformMinY ;
	
		label = new JLabel("Y range max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,3,_uniform_distribParam,label);

		//add component
		component[6] = label ;		
		
		_uniformMaxY = new SliderDim(JSlider.HORIZONTAL,0,200,0,_dimYEnd,this,3,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,3,3,_uniform_distribParam,_uniformMaxY);		
	
		//add component
		component[7] = _uniformMaxY ;
	
		//Set Component List
		_transfoChecking_dimY_form.setComponentTab(component);
		_transfoChecking_dimY_form.setSelected(false);
		
//Z DIM	
		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_dimZBegin = new JLabel("0000");
		_dimZBegin.setPreferredSize(_dimZBegin.getPreferredSize());
		_dimZBegin.setMinimumSize(_dimZBegin.getPreferredSize());
		_dimZBegin.setText(0+"");
		
		_dimZEnd = new JLabel("0000");
		_dimZEnd.setPreferredSize(_dimZEnd.getPreferredSize());
		_dimZEnd.setMinimumSize(_dimZEnd.getPreferredSize());
		_dimZEnd.setText(200+"");
		
		_transfoChecking_dimZ_form = new TransfoCheckBox("",null,null);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_dimZ_form);

	
		label = new JLabel(" Z Dimension :");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_dimZBegin);
		//add component
		component[1] = _dimZBegin ;		
		
		
		_meanLabelZ = new JLabel(" < Center (00000) < ");
		_meanLabelZ.setPreferredSize(_meanLabelZ.getPreferredSize());
		_meanLabelZ.setMinimumSize(_meanLabelZ.getPreferredSize());
		_meanLabelZ.setText(" < Center (0) < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_meanLabelZ);		
		//add component
		component[2] = _meanLabelZ ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_dimZEnd);
		//add component
		component[3] = _dimZEnd ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,4,_uniform_distribParam,infoPane);
		
		label = new JLabel("Z range min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,_uniform_distribParam,label);				

		//add component
		component[4] = label ;
		
		_uniformMinZ = new SliderDim(JSlider.HORIZONTAL,0,100,0,_meanLabelZ,this,4,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,1,5,_uniform_distribParam,_uniformMinZ);		
	
		//add component
		component[5] = _uniformMinZ ;
	
		label = new JLabel("Z range max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,5,_uniform_distribParam,label);

		//add component
		component[6] = label ;		
		
		_uniformMaxZ = new SliderDim(JSlider.HORIZONTAL,0,200,0,_dimZEnd,this,5,0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,3,5,_uniform_distribParam,_uniformMaxZ);		
	
		//add component
		component[7] = _uniformMaxZ ;
	
		//Set Component List
		_transfoChecking_dimZ_form.setComponentTab(component);
		_transfoChecking_dimZ_form.setSelected(false);
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,0,0,_param,_uniform_distribParam);	
		
	//##########################################################
		_apply = new JButton("Apply");
		_apply.addActionListener(this);
		
	//###########################################################
	//Final Build
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,20,0,20,GridBagConstraints.HORIZONTAL,1,0,0,0,3,1,0,1,principal,_choose);	
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,20,0,20,GridBagConstraints.HORIZONTAL,1,0,0,0,3,1,0,2,principal,_param);	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,20,0,20,GridBagConstraints.HORIZONTAL,1,0,0,0,3,1,0,3,principal,_apply);	
		
		return principal;
	}
	
	
	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	
	public void actionPerformed(ActionEvent e){
		// try{
			// JButton button = (JButton)e.getSource();
		if( e.getSource() instanceof JButton ){
			JButton button = (JButton)e.getSource();
			if ( button.getText() == _apply.getText()){
				System.out.println("Fire Apply ....................... RandGlobalParametersPosition L 508");
				
				int[][] dim_unif = { { Integer.parseInt(_dimXBegin.getText()),Integer.parseInt(_dimXEnd.getText()) },{ Integer.parseInt(_dimYBegin.getText()),Integer.parseInt(_dimYEnd.getText()) },{ Integer.parseInt(_dimZBegin.getText()),Integer.parseInt(_dimZEnd.getText()) } };

				//update the value in the RandomGeneratorFrame
				if ( witchSelected() == 1 ){
					// int[][] dim_unif = { {_uniformMinX.getValue(),_uniformMaxX.getValue()},{_uniformMinY.getValue(),_uniformMaxY.getValue()},{_uniformMinZ.getValue(),_uniformMaxZ.getValue()} };
					this.rand_window.updateRandom(1,dim_unif);
				}
				else if ( witchSelected() == 0 ){
					// int[][] dim_unif = { { Integer.parseInt(_gaussian_dimX.getText()), Integer.parseInt(_gaussian_dimX.getText()) }, {Integer.parseInt(_gaussian_dimY.getText()), Integer.parseInt(_gaussian_dimY.getText())}, {Integer.parseInt(_gaussian_dimZ.getText()), Integer.parseInt(_gaussian_dimZ.getText()) };
					this.rand_window.updateRandom(0,dim_unif);
				}
			}
			this.dispose();
		}
		else if( e.getSource() instanceof JRadioButton ){
			JRadioButton button = (JRadioButton)e.getSource();
			if ( button.getText() == _uniformButton.getText() ){
				_param.removeAll();
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,0,0,_param,_uniform_distribParam);	
			}
			else if ( button.getText() == _gaussianButton.getText() ){
				_param.removeAll();
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,0,0,_param,_uniform_distribParam);	
			
			}
		}
		
		this.validate();
		this.repaint();
		this.pack();
	}	
	


	public int witchSelected(){
		if ( _gaussianButton.isSelected() ){
			return 0;
		}
		else if ( _uniformButton.isSelected() ){
			return 1;
		}
		return -1;
	}

	public void setSelected(int type){
		if (type == 1 ){
			_uniformButton.setSelected(true);
		
		}
		else if ( type == 0 ){
			_gaussianButton.setSelected(true);
		}
	
	}
	
	
	public void stateChangedSliderDim(int ID_slider){
		int[] val = new int[2];
		// switch(ID_slider){
			// case 0:
				// val = computeVal(_uniformMinX.getValue(),_uniformMaxX.getValue(),_uniformMaxX.getMaximum() );
				// _dimXBegin.setText(val[0]+"");
				// _dimXEnd.setText(val[1]+"");
				// _meanLabelX.setText(" < Center ("+_uniformMinX.getValue()+") < ");
				// break;
			// case 1:
				// val = computeVal(_uniformMinX.getValue(),_uniformMaxX.getValue(),_uniformMaxX.getMaximum() );
				// _dimXBegin.setText(val[0]+"");
				// _dimXEnd.setText(val[1]+"");
				// _meanLabelX.setText(" < Center ("+_uniformMinX.getValue()+") < ");
				// break;
			// case 2:
				// val = computeVal(_uniformMinY.getValue(),_uniformMaxY.getValue(),_uniformMaxY.getMaximum());
				// _dimYBegin.setText(val[0]+"");
				// _dimYEnd.setText(val[1]+"");
				// _meanLabelY.setText(" < Center ("+_uniformMinY.getValue()+") < ");
				// break;
			// case 3:
				// val = computeVal(_uniformMinY.getValue(),_uniformMaxY.getValue(),_uniformMaxY.getMaximum());
				// _dimYBegin.setText(val[0]+"");
				// _dimYEnd.setText(val[1]+"");
				// _meanLabelY.setText(" < Center ("+_uniformMinY.getValue()+") < ");
				// break;
			// case 4:
				// val = computeVal(_uniformMinZ.getValue(),_uniformMaxZ.getValue(),_uniformMaxZ.getMaximum());
				// _dimZBegin.setText(val[0]+"");
				// _dimZEnd.setText(val[1]+"");
				// _meanLabelZ.setText(" < Center ("+_uniformMinZ.getValue()+") < ");
				// break;
			// case 5:
				// val = computeVal(_uniformMinZ.getValue(),_uniformMaxZ.getValue(),_uniformMaxZ.getMaximum());
				// _dimZBegin.setText(val[0]+"");
				// _dimZEnd.setText(val[1]+"");
				// _meanLabelZ.setText(" < Center ("+_uniformMinZ.getValue()+") < ");
				// break;
		// }
		switch(ID_slider){
			case 0:
				// val = computeVal(_uniformMinX.getValue(),_uniformMaxX.getValue(),_uniformMaxX.getMaximum() );
				_dimXBegin.setText(_uniformMinX.getValue()+"");
				_dimXEnd.setText(_uniformMaxX.getValue()+"");
				_meanLabelX.setText(" < Center ("+((_uniformMinX.getValue()+_uniformMaxX.getValue())/2)+") < ");
				break;
			case 1:
				// val = computeVal(_uniformMinX.getValue(),_uniformMaxX.getValue(),_uniformMaxX.getMaximum() );
				_dimXBegin.setText(_uniformMinX.getValue()+"");
				_dimXEnd.setText(_uniformMaxX.getValue()+"");
				_meanLabelX.setText(" < Center ("+((_uniformMinX.getValue()+_uniformMaxX.getValue())/2)+") < ");
				break;
			case 2:
				// val = computeVal(_uniformMinY.getValue(),_uniformMaxY.getValue(),_uniformMaxY.getMaximum());
				_dimYBegin.setText(_uniformMinY.getValue()+"");
				_dimYEnd.setText(_uniformMaxY.getValue()+"");
				_meanLabelY.setText(" < Center ("+((_uniformMinY.getValue()+_uniformMaxY.getValue())/2)+") < ");
				break;
			case 3:
				// val = computeVal(_uniformMinY.getValue(),_uniformMaxY.getValue(),_uniformMaxY.getMaximum());
				_dimYBegin.setText(_uniformMinY.getValue()+"");
				_dimYEnd.setText(_uniformMaxY.getValue()+"");
				_meanLabelY.setText(" < Center ("+((_uniformMinY.getValue()+_uniformMaxY.getValue())/2)+") < ");
				break;
			case 4:
				// val = computeVal(_uniformMinZ.getValue(),_uniformMaxZ.getValue(),_uniformMaxZ.getMaximum());
				_dimZBegin.setText(_uniformMinZ.getValue()+"");
				_dimZEnd.setText(_uniformMaxZ.getValue()+"");
				_meanLabelZ.setText(" < Center ("+((_uniformMinZ.getValue()+_uniformMaxZ.getValue())/2)+") < ");
				break;
			case 5:
				// val = computeVal(_uniformMinZ.getValue(),_uniformMaxZ.getValue(),_uniformMaxZ.getMaximum());
				_dimZBegin.setText(_uniformMinZ.getValue()+"");
				_dimZEnd.setText(_uniformMaxZ.getValue()+"");
				_meanLabelZ.setText(" < Center ("+((_uniformMinZ.getValue()+_uniformMaxZ.getValue())/2)+") < ");
				break;
		}
		this.validate();
		this.repaint();
	}
	
	private int[] computeVal(int mean, int range, int dim_universe){
		int[] val = new int[2];
		if ( (mean - range) < 0 ){
			val[0] = 0;
		}		
		else{
			val[0] = mean - range;
		}
		
		if ( (mean + range) > dim_universe ){
			val[1] = dim_universe;
		}
		else{
			val[1] = mean + range;
		}
		return val;
	}
	
	public void _update(int[] dimension){
		_dimXBegin.setText(0+"");
		_dimXEnd.setText(dimension[0]+"");
		int[] val = computeVal_inv(0,dimension[0]);
		_meanLabelX.setText(" < Center ("+val[0]+") < ");
		
		_uniformMaxX.setMaximum_(dimension[0]);
		_uniformMinX.setMaximum_(dimension[0]);
		
		_uniformMinX.setValue(val[0]);
		_uniformMaxX.setValue(val[1]);
		
		_dimYBegin.setText(0+"");
		_dimYEnd.setText(dimension[1]+"");
		val = computeVal_inv(0,dimension[1]);
		_meanLabelY.setText(" < Center ("+val[0]+") < ");
		
		_uniformMaxY.setMaximum_(dimension[1]);
		_uniformMinY.setMaximum_(dimension[1]);
		
		_uniformMinY.setValue(val[0]);
		_uniformMaxY.setValue(val[1]);
		
		_dimZBegin.setText(0+"");
		_dimZEnd.setText(dimension[2]+"");
		val = computeVal_inv(0,dimension[2]);
		_meanLabelZ.setText(" < Center ("+val[0]+") < ");
		
		_uniformMaxZ.setMaximum_(dimension[2]);
		_uniformMinZ.setMaximum_(dimension[2]);
		
		_uniformMinZ.setValue(val[0]);
		_uniformMaxZ.setValue(val[1]);
		
		this.repaint();
	}
	
	public int[] computeVal_inv(int min,int max){
		int[] val = new int[2];
		
		val[0] = (int)((max + min)/2) ;
		val[1] = (int)((max - min)/2) ;
		
		return val;
	}
	
	public void setFrame(int[] dimStack, int[][] dimRand, int TypeRand){
		_uniformMinX.setMaximum((int)(dimStack[0]/2));
		_uniformMaxX.setMaximum(dimStack[0]);
		_uniformMaxX.setMinimum((int)(dimStack[0]/2));
		
		_uniformMinY.setMaximum((int)(dimStack[1]/2));
		_uniformMaxY.setMaximum(dimStack[1]);
		_uniformMaxY.setMinimum((int)(dimStack[1]/2));
		
		_uniformMinZ.setMaximum((int)(dimStack[2]/2));
		_uniformMaxZ.setMaximum(dimStack[2]);
		_uniformMaxZ.setMinimum((int)(dimStack[2]/2));
		
		_uniformMinX.setValue(dimRand[0][0]);
		_uniformMinY.setValue(dimRand[1][0]);
		_uniformMinZ.setValue(dimRand[2][0]);
		
		_uniformMaxX.setValue(dimRand[0][1]);
		_uniformMaxY.setValue(dimRand[1][1]);
		_uniformMaxZ.setValue(dimRand[2][1]);
		
		if ( TypeRand == 1 ){
			_uniformButton.doClick();
		}
		else if ( TypeRand == 0 ){
			_gaussianButton.doClick();
		}
	}
	
}