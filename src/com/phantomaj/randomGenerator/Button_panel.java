/**
*Button_panel.java
*20/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.randomGenerator;

import com.phantomaj.button.ButtonAxeView3D;
import com.phantomaj.button.ButtonChangeView3D;

import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.ButtonGroup;



public class Button_panel extends JPanel{

	private RandomGeneratorFrame rand_frame;

	private final ButtonAxeView3D ba = new ButtonAxeView3D("Reset position",0,this);
	private final ButtonChangeView3D bc1 = new ButtonChangeView3D("Form",0,this);
	private final ButtonChangeView3D bc2 = new ButtonChangeView3D("Loaded Stack",1,this);

//#############################################################################
//Constructor	
	
	public Button_panel(RandomGeneratorFrame rand_frame){
		super(new GridBagLayout());
		this.build();
		this.rand_frame = rand_frame;
	}


//##############################################################################
//Private Method

	private void build(){
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,ba);
		
		ButtonGroup Bgrp = new ButtonGroup();
		Bgrp.add(bc1);
		Bgrp.add(bc2);
		
		bc1.setSelected(true);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,0,this,bc1);
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,0,this,bc2);

	}
	
	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	public void update3DViewAxe(int id){
		this.rand_frame.update3DViewAxe(id);
	}

	public void setView3D(int id){
		this.rand_frame.setView3D(id);
		
	}
	
	public void setSelected(int id,boolean bol){
		if ( id == 0){
			bc1.setSelected(bol);
		}
		else if ( id == 1 ){
			bc2.setSelected(bol);
		}
	}
	
}