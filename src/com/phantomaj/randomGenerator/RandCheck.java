/**
*RandCheck.java
*11/06/2012
*@author Antoine Bergamaschi
*/
package com.phantomaj.randomGenerator;

import com.phantomaj.filechooser.Utils;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class RandCheck extends JLabel{


	private ImageIcon greenButton;
	private ImageIcon redButton;

	public RandCheck(){
		super();
		
//		URL url = getClass().getResource(PhantoMaJ.resourceBundle.getString("redButton"));
		redButton = Utils.getResourceIcon("redButton");
		
//		url = getClass().getResource(PhantoMaJ.resourceBundle.getString("greenButton"));
		greenButton = Utils.getResourceIcon("greenButton");
		
		this.setIcon(redButton);
	}

	public void setNewToolTips(String text){
		this.setToolTipText(text);
	}
	
	public void setIsSelected(boolean bol){
		if ( bol ) {
			this.setIcon(greenButton);
		}
		else{
			this.setIcon(redButton);
		}
	}

}