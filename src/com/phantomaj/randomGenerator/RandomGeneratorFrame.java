
/**
*RandomGeneratorFrame.java
*05/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.randomGenerator;

//import perso
import com.phantomaj.button.ButtonRandCreat;
import com.phantomaj.component.BasicInterface;
import com.phantomaj.component.BasicInterfaceComponent_DimensionPanelObj;
import com.phantomaj.component.BasicInterfaceComponent_IntensityPanel;
import com.phantomaj.component.BasicInterfaceComponent_rotPanel;
import com.phantomaj.filechooser.FileChooserFrame;
import com.phantomaj.form.CreateForm;
import com.phantomaj.menu.MenuOptionDraw;
import com.phantomaj.menu.MenuOptionRandomFrame;
import com.phantomaj.obj3D.Object3D;
import com.phantomaj.select.WarningSaveFrame;
import com.phantomaj.tree.NodeForm;
import com.phantomaj.utils.Compute_Intensity;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.Thread_RandGenerator;
import com.phantomaj.utils.Thread_option;
import com.phantomaj.view3D.Panel3D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.JMenuBar;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.File;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;

import java.lang.Thread;
import javax.swing.SwingUtilities;
import java.util.Random;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Date;

public class RandomGeneratorFrame extends JFrame implements ActionListener{

	private Thread_option Toption = new Thread_option();
	private Compute_Intensity Cint = new Compute_Intensity();

	private JPanel form_selection;
	private Panel3D pan3D;
	private JPanel form_parameter;
	private long TrepouteNumber = 0;
	private ArrayList<ArrayList<CreateForm>> pre_phantom = null;
	private ArrayList<NodeForm> structure_phantom = null;
	
	private int[] basicDim = {10,10,10};
	private int[] basicAngle = {0,0,0};
	
	private int basic_phi=0;
	private int basic_theta=0;
	private int basic_psi=0;	

	private int basic_size = 100;
	
	private BasicInterface basicInterface = new BasicInterface();
	
	// private JPanel _dimPanel;
	private BasicInterfaceComponent_rotPanel _rotPanel;
	private BasicInterfaceComponent_DimensionPanelObj _objPanel;
	private SummaryRandPanel _summary; 
	private BasicInterfaceComponent_IntensityPanel _intensityPanel;
	
	private ButtonGroup Bgrp;
	
	private final ButtonRandCreat but_go = new ButtonRandCreat("Draw",this);
	private final Button_panel bp = new Button_panel(this);
	
	private int[] dimStack = null ;
	private int ID_form=0;
	private int nb_form = 7000;
	private int nb_try = 7000;
	private int[][] rand_distribution_param = {{0,200},{0,200},{0,200}};

//New Parameters 	
	int[][] dimensionRange;
	int[][] angleRange;
	int[] intensityRange;
	
	boolean[] dimensionSelected;
	boolean[] rotationSelected;
	boolean IntensitySelected;
	boolean isObject = false;
//Todo Update	
	
	private int rand_type_pos = 1;
	private int rand_type_ = 0;
	
	private String path ="";
	private String path_f ="";
	
//#########################################################################	
//Constructor
	public RandomGeneratorFrame(){
		super("Random Generator");
		this.dimStack = new int[3];
		this.dimStack[0] = 200;
		this.dimStack[1] = 200;
		this.dimStack[2] = 200;
		basicInterface.setRandomPanel();
		this.build();
		basicInterface.addListener(this);
	}
	

//#######################################################################
//Private Methode
	
	private void build(){ 
		this.setSize(new Dimension(200,400));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(this.buildContentPane());
		this.pack();
		this.setResizable(false); 
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		this.setVisible(true);
	}

	private JPanel buildContentPane(){
	
	//##############################################################
	// JMENU
		JMenuBar menuBar = new JMenuBar();
		
		MenuOptionRandomFrame option = new MenuOptionRandomFrame(this);
		MenuOptionDraw optionDraw = new MenuOptionDraw(this);
		
		menuBar.add(option);
		menuBar.add(optionDraw);
		setJMenuBar(menuBar);
	
	
	
		JPanel pane_principal = new JPanel(new GridBagLayout());
	//#######################################################
	//form_selection	
		
	
		form_selection = new JPanel(new GridBagLayout());
		Bgrp = new ButtonGroup();
		JRadioButton button = null;
		
		int k = 0;
		int j = 0;
		for ( int i=0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
			button = new JRadioButton(StaticField_Form.FORM_NAME[i]);
			Bgrp.add(button);
			button.addActionListener(this);
			if ( i == 0 ){
				button.setSelected(true);
			}
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,j,k,form_selection,button);
			j++;
			if ( j >= 8 ){
				k++;
				j=0;
			}
		}
		
		//Structure Button
		button = new JRadioButton("Structure");
		Bgrp.add(button);
		button.addActionListener(this);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,j,k,form_selection,button);
		
	//#######################################################
	//pan3D
		
		pan3D = new Panel3D();
		pan3D.addObject();
		int[] angle = {0,0,0};
		float[] positionFloat = {1f,1f,1f};
		float[] dimensionFloat = {0.5f,0.5f,0.5f};
		pan3D.addForm(angle,positionFloat,dimensionFloat,0,0);
	//########################################################
	//Summary Panel
		_summary = new SummaryRandPanel();
	//#######################################################
	//form_parameter
	
		form_parameter = new JPanel(new GridBagLayout());
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,form_parameter,this._getParametersPanel(0));	
		
	
	//########################################################
	//build _sum
		JPanel _sum = new JPanel(new GridBagLayout());
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,0,_sum,_summary);		
	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,_sum,but_go);		
		
	//########################################################
	//Final build
	//pane_principal		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,pane_principal,form_selection);		

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,pane_principal,pan3D);		
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,pane_principal,bp);	
	
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,pane_principal,form_parameter);		
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,20,10,0,5,GridBagConstraints.BOTH,0,0,0,0,1,4,1,0,pane_principal,_sum);		
	
	
		return pane_principal;
	}

	
	private JScrollPane _getParametersPanel(int id_form){
		JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JPanel panel = new JPanel(new GridBagLayout());
		
		//reset the variable
		dimensionRange = new int[StaticField_Form.FORMDIMENSION_NAME[id_form].length][2];
		angleRange = new int[3][2];
		intensityRange = new int[2];
		dimensionSelected = new boolean[StaticField_Form.FORMDIMENSION_NAME[id_form].length];
		rotationSelected = new boolean[3];
		IntensitySelected = false;
		
		for ( int i = 0 ; i < dimensionSelected.length ; i++ ){
			dimensionSelected[i] = false;
		}
		for ( int z = 0 ; z < 3 ; z++ ){
			rotationSelected[z] = false;
		}	

		//reset view3D
		pan3D.clearView();
		this.bp.setSelected(0,true);

		this.ID_form = id_form;

		basicInterface.setRandInfo(this.ID_form);
		JPanel _dimPanel = basicInterface.getRandPanel();

		
		_summary.setNewForm(this.ID_form);
	
		
		//add view3D
		pan3D.addObject();
		int[] angle = {0,0,0};
		float[] positionFloat = {1f,1f,1f};
		float[] dimensionFloat = new float[3];
		
		dimensionFloat[0] = ((float)StaticField_Form.BASICDIMENSION[this.ID_form][0]) / 200.0f;
		dimensionFloat[1] = ((float)StaticField_Form.BASICDIMENSION[this.ID_form][1]) / 200.0f;
		dimensionFloat[2] = ((float)StaticField_Form.BASICDIMENSION[this.ID_form][2]) / 200.0f;
		
		pan3D.addForm(angle,positionFloat,dimensionFloat,id_form,0);
	
	
		_dimPanel.setPreferredSize(new Dimension(480,900));
		scroll.setViewportView(_dimPanel);


		scroll.setPreferredSize(new Dimension(500,300));
		return scroll;
	}
	
	
	public void actionPerformed(ActionEvent e){
		this.isObject = false;
	
		JRadioButton button = (JRadioButton)e.getSource();
		for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
			if ( button.getText() == StaticField_Form.FORM_NAME[i] ){
				form_parameter.removeAll();
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,form_parameter,this._getParametersPanel(i));
			}
		}

		if ( button.getText() == "Structure"){
			
			this.isObject = true;
			
			//Choose the Structure to load
			FileChooserFrame df = new FileChooserFrame(0);
			try{
				this.open(df.getFile().getPath(),1);
			}
			catch(NullPointerException ev){
			}
			finally{
				df.dispose();
			}
			
			//update interface
		
			
			form_parameter.removeAll();
			
			
			JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			JPanel panel = new JPanel(new GridBagLayout());
			
			
			_objPanel = new BasicInterfaceComponent_DimensionPanelObj(null,1);
			
			
			TitledBorder title = BorderFactory.createTitledBorder("Dimension");
			_objPanel.setBorder(title);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,10,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,_objPanel); 
			
			
			_rotPanel = new BasicInterfaceComponent_rotPanel(0,0,0);
			
			title = BorderFactory.createTitledBorder("Rotation");
			_rotPanel.setBorder(title);
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,10,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,panel,_rotPanel); 
		
			
			_intensityPanel = new BasicInterfaceComponent_IntensityPanel(0);
			
			title = BorderFactory.createTitledBorder("Intensity");
			_intensityPanel.setBorder(title);
		
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,10,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,panel,_intensityPanel); 
			
		
		
			JLabel label = new JLabel();
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,10,10,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,panel,label); 
		
			//add listener
			_objPanel.addListenerCheck(this);
			_rotPanel.addListener(this);
			

			panel.setPreferredSize(new Dimension(480,900));
			scroll.setViewportView(panel);
			scroll.setPreferredSize(new Dimension(500,300));
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,form_parameter,scroll);
			
		}
		this.validate();
		this.repaint();
	}
	
	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	
	//type 0,1 => gaussian,uniform
	private int randomize(int rand_type, int min, int max, Random r){
		int rand_result = 0;
		float mean = ((float)(min + max))/2.0f ;
		
		if ( rand_type == 0 ){
			//Rand gaussian
			float sigma = ((float)max - mean)/3.0f;

			rand_result = (int)(r.nextGaussian()*sigma + mean);

		}
		else if ( rand_type == 1 ){
			float ecart = (float)(max - min);
			rand_result = (int)( ecart*(r.nextFloat()-0.5f) + mean);

		}
		return rand_result;
	}

	//###############################################################
	//public method
	
	
	/**
	* Create an Phantom array ( ArrayList<ArrayList<CreateForm>> ) of size nb_form. This methode is use to build for each step of the
	* process of building a new stack, in order to have random form to draw. Random Type :: 0,1 => Gaussian,Uniform
	* @param nb_form the number of form asked in the phantom.
	**/
	public ArrayList<ArrayList<CreateForm>> creatRandomPhantom(int nb_form){
		ArrayList<ArrayList<CreateForm>> phantom = new ArrayList<ArrayList<CreateForm>>();
		ArrayList<CreateForm> phantom_s = new ArrayList<CreateForm>();

		
		int[] _dim_ = null;
		int _dimf_;
		
		if ( _objPanel != null && _objPanel.getIsSelected() ){
			_dim_ = _objPanel.getDimension();
		}
		
		
		int[] dim = new int[StaticField_Form.FORMDIMENSION_NAME[this.ID_form].length + StaticField_Form.CODE_3D_FORM[this.ID_form]];
		int[] ori= {0,0,0};
		int[] angle = {0,0,0};
		int intensity = 0;

		//Initialize the random seed
		Random r = new Random();
		TrepouteNumber += nb_form;
		for ( int i = 0 ; i < nb_form ; i++ ){
			angle = new int[3];
	//Dimension
			int pos = 0;
			for ( int k = 0 ; k < StaticField_Form.FORMDIMENSION_NAME[this.ID_form].length ; k++ ){
				if ( dimensionSelected[k] ){
					if ( StaticField_Form.CODE_3D_FORM[this.ID_form] == StaticField_Form._2DFORM && k >= 1 ){
						dim[pos] = randomize(rand_type_,dimensionRange[k][0],dimensionRange[k][1],r);
						pos++;
					}
					else if ( StaticField_Form.CODE_3D_FORM[this.ID_form] == StaticField_Form._1DFORM && k >= 1 ){
						dim[pos] = randomize(rand_type_,dimensionRange[k][0],dimensionRange[k][1],r);
						pos++;
					}
					else if ( StaticField_Form.CODE_3D_FORM[this.ID_form] == StaticField_Form._3DFORM ){
						dim[pos] = randomize(rand_type_,dimensionRange[k][0],dimensionRange[k][1],r);
						pos++;
					}
					else{
						for ( int z = 0 ; z  <= StaticField_Form.CODE_3D_FORM[this.ID_form] ; z++ ){
							dim[pos] = randomize(rand_type_,dimensionRange[k][0],dimensionRange[k][1],r);
							pos++;
						}
					}
				}
				else{
					dim[pos] = basicDim[pos];
					pos++;
				}
			}
			
	//Center	
			//uniformly distribued
			ori[0] = randomize(rand_type_pos,rand_distribution_param[0][0],rand_distribution_param[0][1],r);
			while ( ori[0] < 0 || ori[0] > dimStack[0]){
				ori[0] = randomize(rand_type_pos,rand_distribution_param[0][0],rand_distribution_param[0][1],r);
				TrepouteNumber++;
			}
			ori[1] = randomize(rand_type_pos,rand_distribution_param[1][0],rand_distribution_param[1][1],r);
			while ( ori[1] < 0 || ori[1] > dimStack[1]){
				ori[1] = randomize(rand_type_pos,rand_distribution_param[1][0],rand_distribution_param[1][1],r);
				TrepouteNumber++;
			}
			ori[2] = randomize(rand_type_pos,rand_distribution_param[2][0],rand_distribution_param[2][1],r);
			while ( ori[2] < 0 || ori[2] > dimStack[2]){
				ori[2] = randomize(rand_type_pos,rand_distribution_param[2][0],rand_distribution_param[2][1],r);
				TrepouteNumber++;
			}
			
	//Rotation	

			for ( int z = 0 ; z < 3 ; z++ ){
				if ( rotationSelected[z] ){
					angle[z] = randomize(rand_type_,angleRange[z][0],angleRange[z][1],r);
				}
				else{
					angle[z] = basicAngle[z];
				}
			}
	//Intensity		
			if ( IntensitySelected ){
				intensity = randomize(rand_type_,intensityRange[0],intensityRange[1],r);
			}
			else{
				intensity = 100;
			}
			
		
	//Creation de la forme
			int[] centerRot = new int[3];
			if ( !isObject ){
				phantom_s.add(StaticField_Form.getCreatForm(this.ID_form,intensity,0,centerRot,dim,ori,angle));
			}
			else{

				ArrayList<NodeForm> structure_phantom_ = new ArrayList<NodeForm>();
				phantom_s = new ArrayList<CreateForm>();
			
				for ( int t = 0 ; t < structure_phantom.size() ; t++ ){					
					structure_phantom_.add(new NodeForm(structure_phantom.get(t)));
				}
				
			
				if ( _objPanel.getIsSelected() ){
					_dimf_ = randomize(0,_dim_[0],_dim_[1],r);
				}
				else {
					_dimf_ = basic_size;
				}
				
				_objPanel.computeObjectPosition(structure_phantom_,ori);
				_objPanel.computeSize(structure_phantom_,(((float)_dimf_)/((float)basic_size)));
				_objPanel.computeObjectRotation_typeT(structure_phantom_,angle);
				

				for ( int ik = 0 ; ik < structure_phantom_.size() ; ik++ ){
					phantom_s.add(structure_phantom_.get(ik).getCreatForm(intensity,0));
				}
				phantom.add(phantom_s);
			}
		}
		if ( !isObject ){
			phantom.add(phantom_s);
		}
	
		return phantom;
	}
	
	/**
	* Create a new Random stack with an Object3D. For each step create a phantom with random Form and random parameters.
	* Then try to draw the fantom in the stack in creation. If there isnt any form add from the phantom, stop_try grow up. Else if there is at least 1 form 
	* draw stop_try return to 1. The process stop when stop_try reach a specified value or if all the asked form have been draw.
	**/
	public void creatStack(){
		File f = new File(this.path);
		//Si il y a un dossier selectionner
		if ( f.exists() && f.isDirectory() ){
		
			//Creation du repertoire
			String savePath = this.path+File.separator;
			int num = 0;
			try{
				savePath += "RandomGenerator_"+num;
				f = new File(savePath);
				while ( f.exists() && f.isDirectory()){
					num++;
					savePath = this.path+File.separator+"RandomGenerator_"+num;
					f = new File(savePath);
				}
				// Create one directory
				f.mkdir();
				this.path_f = savePath + File.separator;
			}
			catch (Exception e){
				System.err.println("Error: " + e.getMessage());
			}
		
			//Lancement de la fonction
			Thread_RandGenerator t = new Thread_RandGenerator(this,this.nb_try,this.nb_form,this.dimStack,this.pre_phantom,this.Cint,this.Toption);
			t.start(); 
			
		}
		else{
			WarningSaveFrame w = new WarningSaveFrame("Warning",this);
		}
	}
	
	/**
	* Update the _summary panel
	*@deprecate
	**/
	public void updateCheck(){
		System.out.println("DEPRECATE ::: RandomGeneratorFrame.updateCheck");
		// boolean[] bol_dim = {false,false,false};
		// boolean[] bol_rot = _rotPanel.getIsSelected();
		// int i = 0;
		// for ( int pos=0; pos < bol_dim.length ; pos++){
			// _summary.setIsSelected(i,bol_dim[pos]);
			// i++;
		// }
		// for ( int pos_n=0; pos_n < bol_rot.length ; pos_n++){
			// _summary.setIsSelected(i,bol_rot[pos_n]);
			// i++;
		// }
	}
	
	
	/**
	* Update the _summary panel + param of this class
	**/
	public void updateParameters(int[] dimStack, int[] basicDim, int nb_form, int nb_try){
		this._summary.updateDimStack(dimStack);
		this._summary.updateBasicDim(basicDim);
		this.nb_form = nb_form;
		this.nb_try = nb_try;
		this.dimStack = dimStack;
		this.rand_distribution_param[0][1] = dimStack[0];
		this.rand_distribution_param[1][1] = dimStack[1];
		this.rand_distribution_param[2][1] = dimStack[2];
		this.basicDim = basicDim;
		this._summary.updateNbForm(nb_form);
		this._summary.updateNbTry(nb_try);
		compareDim();
		this.pack();
	}
	
	
	/**
	* Update the _summary panel + param of this class
	**/
	public void updateRandom(int type,int[][] dim_unif){
		this.rand_distribution_param = dim_unif;
		this.rand_type_pos = type;
		this._summary.updateRandomType(type);
		this._summary.updateRandDim(dim_unif);
	}
	
	
	public void compareDim(){
		for(int i=0;i<3;i++){
			if ( this.dimStack[i]<this.rand_distribution_param[i][1] ){
				this.rand_distribution_param[i][1] = this.dimStack[i];
			}
		}
		this._summary.updateRandDim(this.rand_distribution_param);
	}
	
	public void open(String filename,int type){
		// this.pan3D.printS();
		if ( type == 0 ){
			this.pan3D.clearView_rand();
			this.bp.setSelected(1,true);
			
			Object3D object3D = new Object3D(null,null);
		
			this.pan3D.setIsInBuild(true);
			
			this.getPhantom_base(object3D.openFileObj(filename),object3D.openFile(filename));
			
			this.pan3D.setIsInBuild(false);
			
			// this.bp.setSelected(1,true);
		}
		
		else if (type == 1){
			this.pan3D.clearView();
			this.bp.setSelected(0,true);
			
			Object3D object3D = new Object3D(null,null);
		
			this.pan3D.setIsInBuild(true);
			
			this.getPhantom_structure(object3D.openFileObj(filename),object3D.openFile(filename));
			
			this.pan3D.setIsInBuild(false);
			
			// this.bp.setSelected(0,true);
		}
		// this.pan3D.printS();
	}
	
	
	private void getPhantom_base(ArrayList<ArrayList<NodeForm>> phantom, ArrayList<String> phantom_info){
		
		this.pre_phantom = new ArrayList<ArrayList<CreateForm>>();
		// this.structure_phantom = new ArrayList<NodeForm>();
		
		// ArrayList<ArrayList<CreateForm>> phantom = new ArrayList<ArrayList<CreateForm>>();
		ArrayList<CreateForm> phantom_ = new ArrayList<CreateForm>();
		
		int i = 0;
		System.out.println("Phantom Size ::: "+phantom.size());
		
		// this.pan3D.initializeArrayListObject(phantom.size());
		
		Iterator<ArrayList<NodeForm>> objects = phantom.iterator();
		//System.out.println("Array setTree :"+phantom.size()+"\n");
		// _noiseSD.setText(phantom_info.get(0));
		String[] _dim = phantom_info.get(1).split("\t");
		
		int[] dim = {Integer.parseInt(_dim[0]),Integer.parseInt(_dim[1]),Integer.parseInt(_dim[2])};
		
		this._summary.updateDimStack(dim);
		this.dimStack = dim;
		rand_distribution_param[0][1] = dim[0];
		rand_distribution_param[1][1] = dim[1];
		rand_distribution_param[2][1] = dim[2];
		
		this._summary.updateRandDim(rand_distribution_param);
		
		pan3D.addObject_rand();
		//for every objects in the phantom
		while (objects.hasNext()) {
			//set new information in the ObjectNode
			float _pixelValue =  Float.parseFloat(phantom_info.get(2*i+2));
			int _layerValue = Integer.parseInt(phantom_info.get(2*i+3));

			ArrayList<NodeForm> object = objects.next();

			Iterator<NodeForm> forms = object.iterator();
			
			//for every form constituing this object
			while(forms.hasNext()){
				NodeForm obj = forms.next();
				
				// this.structure_phantom.add(obj);
				
				int[] position = obj.getOrigine();
				int[] dimension = obj.getDimension();
				
				float[] positionFloat = {((float)position[0])/((float)dim[0]/2.0f), ((float)position[1])/((float)dim[1]/2.0f), ((float)position[2])/((float)dim[2]/2.0f)};
				float[] dimensionFloat = {((float)dimension[0])/((float)dim[0]), ((float)dimension[1])/((float)dim[1]), ((float)dimension[2])/((float)dim[2])};
				
				phantom_.add(obj.getCreatForm(_pixelValue,_layerValue));
				pan3D.addForm_rand(obj.getAngle(),positionFloat,dimensionFloat,obj.getForm(),obj.getAngleFlex(),0);
				
			}
			i += 1;
		}
		
		this.pre_phantom.add(phantom_);
	}
	
	
	private void getPhantom_structure(ArrayList<ArrayList<NodeForm>> phantom, ArrayList<String> phantom_info){
		
		this.structure_phantom = new ArrayList<NodeForm>();
		
		int i = 0;
		System.out.println("Phantom Size ::: "+phantom.size());
		
		Iterator<ArrayList<NodeForm>> objects = phantom.iterator();

		String[] _dim = phantom_info.get(1).split("\t");
		
		int[] dim = {Integer.parseInt(_dim[0]),Integer.parseInt(_dim[1]),Integer.parseInt(_dim[2])};
		
		this._summary.updateDimStack(dim);
		this.dimStack = dim;
		rand_distribution_param[0][1] = dim[0];
		rand_distribution_param[1][1] = dim[1];
		rand_distribution_param[2][1] = dim[2];
		
		this._summary.updateRandDim(rand_distribution_param);
		
		pan3D.addObject();
		//for every objects in the phantom
		while (objects.hasNext()) {
			//set new information in the ObjectNode
			float _pixelValue =  Float.parseFloat(phantom_info.get(2*i+2));
			int _layerValue = Integer.parseInt(phantom_info.get(2*i+3));

			ArrayList<NodeForm> object = objects.next();

			Iterator<NodeForm> forms = object.iterator();
			
			//for every form constituing this object
			while(forms.hasNext()){
				NodeForm obj = forms.next();
				
				this.structure_phantom.add(obj);
				
				int[] position = obj.getOrigine();
				int[] dimension = obj.getDimension();
				
				float[] positionFloat = {((float)position[0])/((float)dim[0]/2.0f), ((float)position[1])/((float)dim[1]/2.0f), ((float)position[2])/((float)dim[2]/2.0f)};
				float[] dimensionFloat = {((float)dimension[0])/((float)dim[0]), ((float)dimension[1])/((float)dim[1]), ((float)dimension[2])/((float)dim[2])};

				pan3D.addForm_randS(obj.getAngle(),positionFloat,dimensionFloat,obj.getForm(),obj.getAngleFlex(),0);
				
			}
			i += 1;
		}
	}
	
	/**
	* Write the File containing the WorkFlow realized by the random Generator Algorithm 
	* @param created_form, int the number of form drawn.
	* @param time, String the time passed in the Algorithm.
	**/
	public void creatWorkFile(int created_form, String time){

		String toWrite = "";
		toWrite += "\n################################# ABBREVIATION #################################\n";
		toWrite += "DS\tDimension of the Stack\n";
		toWrite += "BD\tBasic Dimension used for the structure\n";
		toWrite += "NO\tNumber of Object ( structure ) asked\n";
		toWrite += "NT\tNumber of Try used by the algorithm\n";
		toWrite += "RD\tRandom Distribution type\n";
		toWrite += "RA\tRandom Area\n";
		//toWrite += "LS\tLoaded Stack path"++"\n";
		toWrite += "\n################################## PARAMETERS ##################################\n";
		toWrite += "DS\t"+this.dimStack[0]+"\t"+this.dimStack[1]+"\t"+this.dimStack[2]+"\n";
		toWrite += "BD\t"+this.basicDim[0]+"\t"+this.basicDim[1]+"\t"+this.basicDim[2]+"\n";
		toWrite += "NO\t"+this.nb_form+"\n";
		toWrite += "NT\t"+this.nb_try+"\n";
		toWrite += "\n############################## RANDOM PARAMETERS ###############################\n";
		toWrite += "RD\t"+randType_ToString(this.rand_type_pos)+"\n";
		toWrite += "RA\t["+this.rand_distribution_param[0][0]+","+this.rand_distribution_param[0][1]+"]\t["+this.rand_distribution_param[1][0]+","+this.rand_distribution_param[1][1]+"]\t["+this.rand_distribution_param[2][0]+","+this.rand_distribution_param[2][1]+"]\n";
		toWrite += "\n################################### RESULTS ####################################\n";
		toWrite += "Number of structures added :: "+created_form+"\n";
		toWrite += "Type of the structure :: "+id_form_ToString(this.ID_form)+"\n";
		toWrite += "Time :: "+time+"\n";
		
		try{
			FileWriter fw = new FileWriter(this.path+"WorkFlow.txt", false);
			BufferedWriter output = new BufferedWriter(fw);
			
			output.write(toWrite);

			output.flush();
			output.close();
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}
	}
	
	public String randType_ToString(int id){
		String str = "";
		switch(id){
			case 1:
				str += "Uniform";
				break;
			case 0:
				str += "Gaussian";
				break;
		}
		return str;
	}
		
	public String id_form_ToString(int id){
		String str ="";
		switch(id){
			case 0:
				str += "Box";
				break;
			case 1:
				str += "Cone";
				break;
			case 2:
				str += "Pyramid";
				break;
			case 3:
				str += "Sphere";
				break;
			case 4:
				str += "Tube";
				break;
			case 5:
				str += "Ellipsoid";
				break;
			case 6:
				str += "Fiber";
				break;
			case 7:
				str += "Structure";
				break;
		}
		return str;
	}
	
	public String getPath(){
		return this.path_f;
	}
	
	public void setPathFolder(String path){
		this.path = path;
	}
	
	/**
	* Update the Intensity object
	**/
	public void setComputeIntensity(Compute_Intensity Cint){
		this.Cint = Cint;
	}
	
	public Compute_Intensity getCompute_Intensity(){
		return this.Cint;
	}
	
	/**
	* Update the Thread Object
	**/
	public void setThreadNumber(int numb){
		this.Toption.setNumberOfProcessor(numb);
	}
	
	public Thread_option getThreadOption(){
		return Toption;
	}
	
	public void setEnable_butDraw(boolean bol){
		but_go.setEnabled(bol);
	}
	
	public void update3DViewAxe(int axe){
		this.pan3D.reset(axe);
	}
	
	public void setView3D(int id){
		this.pan3D.setView3D(id);
	}
	
	public int getRand_type_pos(){
		return this.rand_type_pos;
	}
	
	public long getTrepout(){
		return this.TrepouteNumber;
	}
	
	public void resetTrepout(){
		this.TrepouteNumber = 0;
	}
	
	public void ask_update(RandGlobalParametersPosition gp){		
		gp._update(this.dimStack);
	}

	public int[] getStackDim(){
		return this.dimStack;
	}
	
	public int getNbTry(){
		return this.nb_try;
	}
	
	public int getNbform(){
		return this.nb_form;
	}
	
	public int[] getBasicDim(){
		return this.basicDim;
	}
	
	public int[][] getDimRand(){
		return this.rand_distribution_param;
	}
	
	public int getRandType(){
		return this.rand_type_pos;
	}
	
	public boolean getIsStructureSelected(){
		return this.isObject;
	}
	

	//Update the Randomization parameters
	public void setDimensionRange(int ID, int[] nv){
		this.dimensionRange[ID] = nv;
	}
	
	public void setAngleRange(int ID, int[] nv){
		this.angleRange[ID] = nv;
	}
	
	public void setIntensityRange(int[] nv){
		this.intensityRange = nv;
	}
	
	public void setDimensionSelected(int ID, boolean bol){
		this.dimensionSelected[ID] = bol;
		this._summary.setIsDimSelected(this.dimensionSelected);
	}
	
	public void setRotationSelected(int ID, boolean bol){
		this.rotationSelected[ID] = bol;
		this._summary.setIsRotSelected(ID,bol);
	}
	
	public void setIntensitySelected(boolean bol){
		this.IntensitySelected = bol;
	}
	
}