/**
*SummaryRandPanel.java
*11/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.randomGenerator;

import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.StaticField_Form;

import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class SummaryRandPanel extends JPanel{

	private RandomGeneratorFrame _randomFrame;
	
	private JPanel info_CheckDim;
	
	private JLabel stackDimX;
	private JLabel stackDimY;
	private JLabel stackDimZ;
	
	private JLabel form_name;
	
	private JLabel basic_Xdim;
	private JLabel basic_Ydim;
	private JLabel basic_Zdim;
	
	private JLabel rand_Xdim;
	private JLabel rand_Ydim;
	private JLabel rand_Zdim;

	
	private JLabel nb_form;
	private JLabel nb_try;
	
	private RandCheck[] checkSelected_dim;

	
	private RandCheck checkSelected_Phi;
	private RandCheck checkSelected_Theta;
	private RandCheck checkSelected_Psi;
	
	private JLabel randType;
	
	private final String gaussian = "<html><ul><li> Gaussian </li></ul></html>";
	private final String uniform = "<html><ul><li> Uniform </li></ul></html>";
	
	
//#############################################################################
//Constructor	
	public SummaryRandPanel(){
		super(new GridBagLayout());
		this.build();
	}

//##############################################################################
//Private Method

	private void build(){
		form_name = new JLabel("<html><h1>"+ StaticField_Form.FORM_NAME[0]+"</h1></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,this,form_name);		
	
		JLabel label = new JLabel("<html><h3>Stack dimension : </h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);
		
		label = new JLabel("<html><ul><li> Width :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,2,this,label);

	//Stack Dimension X 
		stackDimX = new JLabel("200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,10,GridBagConstraints.REMAINDER,1,0,0,0,1,1,1,2,this,stackDimX);

		label = new JLabel("<html><ul><li> Height :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,3,this,label);

	//Stack Dimension Y 
		stackDimY = new JLabel("200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,10,GridBagConstraints.REMAINDER,1,0,0,0,1,1,1,3,this,stackDimY);
	
	
		label = new JLabel("<html><ul><li> Size :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,4,this,label);

	//Stack Dimension Z 
		stackDimZ = new JLabel("200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,10,GridBagConstraints.REMAINDER,1,0,0,0,1,1,1,4,this,stackDimZ);
	
	//Dimension object
		label = new JLabel("<html><h3>Object dimension : </h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,this,label);

		info_CheckDim = new JPanel(new GridBagLayout());
		checkSelected_dim = new RandCheck[StaticField_Form.FORMDIMENSION_NAME[0].length];
		
		int k = 0;
		int z = 0;
		for ( int i = 0 ; i < StaticField_Form.FORMDIMENSION_NAME[0].length ; i++ ){
			checkSelected_dim[i] = new RandCheck();
			checkSelected_dim[i].setNewToolTips(StaticField_Form.FORM_NAME[0]+" "+StaticField_Form.FORMDIMENSION_NAME[0][i]);
			if ( k < 3 ){
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,info_CheckDim,checkSelected_dim[i]);
				k++;
			}
			else{
				k = 0;
				z++;
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,info_CheckDim,checkSelected_dim[i]);
			}
		}
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,20,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,6,this,info_CheckDim);
	
	//Rotation object
		label = new JLabel("<html><h3>Object rotation : </h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,7,this,label);

		JPanel info = new JPanel(new GridBagLayout());
		
		checkSelected_Phi = new RandCheck();
		checkSelected_Phi.setNewToolTips("Phi");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,info,checkSelected_Phi);
	
		checkSelected_Theta = new RandCheck();
		checkSelected_Theta.setNewToolTips("Theta");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,1,0,info,checkSelected_Theta);
	
		checkSelected_Psi = new RandCheck();
		checkSelected_Psi.setNewToolTips("Psi");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,0,info,checkSelected_Psi);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,20,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,8,this,info);
		
	
	//Rand type
		label = new JLabel("<html><h3>Position Random type : </h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,9,this,label);

		randType = new JLabel(uniform);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,10,this,randType);	
	
		label = new JLabel("Random Position Limit X :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,11,this,label);
	
		rand_Xdim = new JLabel("0 - 200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,11,this,rand_Xdim);
	
		label = new JLabel("Random Position Limit Y :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,12,this,label);
	
		rand_Ydim = new JLabel("0 - 200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,12,this,rand_Ydim);
	
	
		label = new JLabel("Random Position Limit Z :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,13,this,label);
	
		rand_Zdim = new JLabel("0 - 200");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,13,this,rand_Zdim);
	
		
	//Global information
	
		label = new JLabel("<html><h3>Global information : </h3></html>");
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,14,this,label);

		label = new JLabel("Number of Form : ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,15,this,label);

		nb_form = new JLabel("7000");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,15,this,nb_form);

		label = new JLabel("Number of try : ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,16,this,label);
		
		nb_try = new JLabel("7000");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,16,this,nb_try);
		
		label = new JLabel("Basic Dimension X :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,17,this,label);
	
		basic_Xdim = new JLabel("10");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,17,this,basic_Xdim);
	
		label = new JLabel("Basic Dimension Y :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,18,this,label);
	
		basic_Ydim = new JLabel("10");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,18,this,basic_Ydim);
	
	
		label = new JLabel("Basic Dimension Z :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,15,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,19,this,label);
	
		basic_Zdim = new JLabel("10");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,5,5,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,19,this,basic_Zdim);
	
	
		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,20,this,label);
	
	//set Title
		TitledBorder title = BorderFactory.createTitledBorder("Summary");
		this.setBorder(title);
	
	}
	
	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	//###########################################################################
	//public method
	
	//0,1,2,3,4,5,6 ==> box,cone,pyramid,shpere,tube,ellipsoid,fiber
	public void setNewForm(int ID){
		form_name.setText("<html><h1>"+StaticField_Form.FORM_NAME[ID]+"</h1></html>");
		// form_name.setText(_name[ID]);
		info_CheckDim.removeAll();
		
		checkSelected_dim = new RandCheck[StaticField_Form.FORMDIMENSION_NAME[ID].length];
		
		int k = 0;
		int z = 0;
		for ( int i = 0 ; i < StaticField_Form.FORMDIMENSION_NAME[ID].length ; i++ ){
			checkSelected_dim[i] = new RandCheck();
			checkSelected_dim[i].setNewToolTips(StaticField_Form.FORM_NAME[ID]+" "+StaticField_Form.FORMDIMENSION_NAME[ID][i]);
			if ( k < 3 ){
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,info_CheckDim,checkSelected_dim[i]);
				k++;
			}
			else{
				k = 0;
				z++;
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,info_CheckDim,checkSelected_dim[i]);
			}
		}
	}
	
	//0,1,2 => checkSelected_Xdim,checkSelected_Ydim,checkSelected_Zdim
	//3,4,5 => checkSelected_Phi,checkSelected_Theta,checkSelected_Psi
	/**
	* @deprecate
	**/
	public void setIsSelected(int ID, boolean sel){
		System.out.println("DEPREACTE ::: SummaryRandPanel.setIsSelected");
		// switch(ID){	
			// case 3:
				// checkSelected_Phi.setIsSelected(sel);
				// break;		
			// case 4:
				// checkSelected_Theta.setIsSelected(sel);
				// break;
			// case 5:
				// checkSelected_Psi.setIsSelected(sel);
				// break;	
			// default:
				// if ( ID >= 10 ){
					// checkSelected_dim[ID-10].setIsSelected(sel);
				// }
				// else{
					// System.out.println("Error ID undefined.................L.220 SummaryRandPanel");
				// }
				// break;				
		// }
	}
	
	public void setIsDimSelected(boolean[] sel){
		for ( int i = 0 ; i < sel.length ; i++ ){
			checkSelected_dim[i].setIsSelected(sel[i]);
		}
	}
	
	public void setIsRotSelected(int ID, boolean bol){
		switch(ID){
			case 0:
				checkSelected_Phi.setIsSelected(bol);
				break;
			case 1:
				checkSelected_Theta.setIsSelected(bol);
				break;
			case 2:
				checkSelected_Psi.setIsSelected(bol);
				break;
			default:
				System.out.println("WARNING ::: ID ("+ID+") UNRECOGNIZE SummaryRandPanel.setIsRotSelected");
		}
	}

	
	public void updateDimStack(int[] dim){
		stackDimX.setText(dim[0]+"");
		stackDimY.setText(dim[1]+"");
		stackDimZ.setText(dim[2]+"");
	}
	
	
	//0,1 => Gaussian, Uniform
	public void updateRandomType(int type){
		switch(type){
			case 0: 
				randType.setText(gaussian);
				break;
			case 1:
				randType.setText(uniform);
				break;
			default:
				System.out.println("Error type undefined...................L.242 SummaryRandPanel");
				break;
		}
	}
	
	public void updateNbForm(int nb_form){
		this.nb_form.setText(nb_form+"");
	}
	
	public void updateNbTry(int nb_try){
		this.nb_try.setText(nb_try+"");
	}
	
	public void updateBasicDim(int[] basicDim){
		basic_Xdim.setText(basicDim[0]+"");
		basic_Ydim.setText(basicDim[1]+"");
		basic_Zdim.setText(basicDim[2]+"");
	}
	
	public void updateRandDim(int[][] lim){
		rand_Xdim.setText(lim[0][0]+" - "+lim[0][1]);
		rand_Ydim.setText(lim[1][0]+" - "+lim[1][1]);
		rand_Zdim.setText(lim[2][0]+" - "+lim[2][1]);
	}
	
}