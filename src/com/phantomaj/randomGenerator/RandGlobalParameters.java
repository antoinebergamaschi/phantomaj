/**
*RandomGlobalParameters.java
*14/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.randomGenerator;

import com.phantomaj.select.VTextField;
import com.phantomaj.button.ButtonHelp;
import com.phantomaj.button.ButtonHelp;
import com.phantomaj.select.VTextField;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JPanel;

import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.JComponent;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import java.awt.event.ActionListener; 
import java.awt.event.ActionEvent;

public class RandGlobalParameters extends JFrame implements ActionListener{

	private RandomGeneratorFrame rand_window;
	
	private VTextField _stack_dimX = null;
	private VTextField _stack_dimY = null;
	private VTextField _stack_dimZ = null;
	
	private VTextField _nb_form = null;
	private VTextField _nb_try = null;
	
	private VTextField _basic_DimX = null;
	private VTextField _basic_DimY = null;
	private VTextField _basic_DimZ = null;
	
	private final String[] text_help = { "<html>Dimension of the stack in wich random form will be draw.<html>",
	"Number of form ask by the users. The randomGenerator Algorithm will try to achieve this numbers.",
	"Maximum number of try that will be done by the randomGenerator algorithm to add a single form in the stack. If the algorithm exceed this number the algorithm will stop",
	"Dimension used if the corresponding parameters of the form is not randomly choosen.",
	"Defined the type of random use for positionning forms in the stack."
	};

	public RandGlobalParameters(RandomGeneratorFrame rand_window){
		super("Parameters");
		this.rand_window = rand_window;
		this.build();
	}
	
	private void build(){
		// this.setSize(new Dimension(200,400));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		this.setContentPane(this.buildContentPane());
		this.pack();
		this.setResizable(false); 
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();
		
		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY);
		this.setVisible(true);
	}
	
	
	private JPanel buildContentPane(){
		//#######
		JLabel label = new JLabel();
		ButtonHelp help = new ButtonHelp("",null);
		//#######
		JPanel principal = new JPanel(new GridBagLayout());
	
		label = new JLabel("<html><h3>General Random Generator Parameters</h3><html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,30,0,0,GridBagConstraints.BOTH,1,0,0,0,3,1,0,0,principal,label);		

		help = new ButtonHelp(text_help[0],this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,0,1,principal,help);		
	
	//##########################################################################
	//Stack Dimension
	
		label = new JLabel("Stack Dimension :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,2,1,1,1,principal,label);		

	//X	
		
		label = new JLabel("<html><ul><li> Width :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,2,principal,label);		

		
		_stack_dimX = new VTextField("200",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,2,principal,_stack_dimX);		

	//Y	
		
		label = new JLabel("<html><ul><li> Height :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,3,principal,label);		

		
		_stack_dimY = new VTextField("200",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,3,principal,_stack_dimY);
		
	//Z

		label = new JLabel("<html><ul><li> Size :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,4,principal,label);		

		
		_stack_dimZ = new VTextField("200",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,4,principal,_stack_dimZ);	
		
	//##########################################################################
	//Global information
	
	//nb_form
		help = new ButtonHelp(text_help[1],this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,principal,help);		
	
		
		label = new JLabel("Number of object :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,5,principal,label);		

		_nb_form = new VTextField("7000",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,5,principal,_nb_form);		

	//nb_try
		help = new ButtonHelp(text_help[2],this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,6,principal,help);		
	
		
		label = new JLabel("Number of try :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,1,6,principal,label);		

		_nb_try = new VTextField("7000",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,6,principal,_nb_try);		

	// Basic Form dimension
	
		help = new ButtonHelp(text_help[3],this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,7,principal,help);		
	

	
		label = new JLabel("Basic form dimension :");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,10,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,1,7,principal,label);		

	//X	
		
		label = new JLabel("<html><ul><li> Basic X Dimension :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,8,principal,label);		

		
		_basic_DimX = new VTextField("10",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,8,principal,_basic_DimX);		

	//Y	
		
		label = new JLabel("<html><ul><li> Basic Y Dimension :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,9,principal,label);		

		
		_basic_DimY = new VTextField("10",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,9,principal,_basic_DimY);
		
	//Z

		label = new JLabel("<html><ul><li> Basic Z Dimension :</ul></li></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,2,1,0,10,principal,label);		

		
		_basic_DimZ = new VTextField("10",8,this.rand_window);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,2,10,principal,_basic_DimZ);	
		
		
		JButton _apply = new JButton("Apply");
		_apply.addActionListener(this);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,20,0,20,GridBagConstraints.HORIZONTAL,1,0,0,0,3,1,0,11,principal,_apply);	
		
		return principal;
	}
	
	
	public void setFrame(int[] stack, int nb_form, int nb_try, int[] dim){
		_stack_dimX.setText(stack[0]+"");
		_stack_dimY.setText(stack[1]+"");
		_stack_dimZ.setText(stack[2]+"");
		
		_nb_form.setText(nb_form+"");
		_nb_try.setText(nb_try+"");
		
		_basic_DimX.setText(dim[0]+"");
		_basic_DimY.setText(dim[1]+"");
		_basic_DimZ.setText(dim[2]+"");
	}
	
	
	//add in the GridBagLayout
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	
	public void actionPerformed(ActionEvent e){
		int[] dimStack = {Integer.parseInt(_stack_dimX.getText()),Integer.parseInt(_stack_dimY.getText()),Integer.parseInt(_stack_dimZ.getText())};
		int[] basicDim = {Integer.parseInt(_basic_DimX.getText()),Integer.parseInt(_basic_DimY.getText()),Integer.parseInt(_basic_DimZ.getText())};
		this.rand_window.updateParameters(dimStack,basicDim,Integer.parseInt(_nb_form.getText()),Integer.parseInt(_nb_try.getText()));
		this.dispose();
	}	
	
}