/**
* BasicInterfaceComponent_rootPanel.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.phantomaj.select.VTextField_NEW;


public class BasicInterfaceComponent_rootPanel extends JPanel{
	private static final String[] label_text = {"<html><h3>Stack Dimension<h3></html>",
												"<html><ul><li>Width of the Stack : </li></ul></html>",
												"<html><ul><li>Height of the Stack : </li></ul></html>",
												"<html><ul><li>Size of the Stack : </li></ul></html>",
												"<html><h3>Stack Noise Value<h3></html>",
												"<html><ul><li>Gaussian Noise Standard Deviation : </li></ul></html>",
												"<html><h3>Background Pixel Value</h3></html>",
												"<html><ul><li>Value : </li></ul></html>"};
	
	
	private BasicInterface _basicInterface;

	public BasicInterfaceComponent_rootPanel(BasicInterface bi){
		super();
		this._basicInterface = bi;
		this.setLayout(new GridBagLayout());
		this.build();
	}
	
	private void build(){
	//Final Build
		JPanel dimPanel = this.getStackDimensionPanel();
		JPanel noisePanel = this.getPanelNoise();
		JPanel backgroundPanel = this.getPanelbackGroundVal();
		
		JPanel principal = new JPanel(new GridBagLayout());
		
		JLabel label = new JLabel("<html><h1>Phantom</h1></html>");
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,label);	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,principal,dimPanel);
		
		// //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		// addGridBag(GridBagConstraints.NORTH,10,5,0,5,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,principal,noisePanel);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,principal,backgroundPanel);
		
		TitledBorder title = BorderFactory.createTitledBorder("Phantom General Option");
		principal.setBorder(title);	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,principal);
		
		label = new JLabel("");
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,0,1,0,0,1,1,0,2,this,label);
		

	}
	
	private JPanel getStackDimensionPanel(){
		JLabel label;
		VTextField_NEW textFiel;
		JPanel principal = new JPanel(new GridBagLayout());
		
		label = new JLabel(label_text[0]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,3,1,0,0,principal,label);
		
	//dim X
	
		label = new JLabel(label_text[1]);	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,principal,label);
		

		textFiel = new VTextField_NEW(this._basicInterface.getStackDimension()[0]+"",3,0,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,10,0,1,1,1,1,principal,textFiel); 

		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,1,principal,label);
		
	//dim Y

		label = new JLabel(label_text[2]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,principal,label);
		
		textFiel = new VTextField_NEW(this._basicInterface.getStackDimension()[1]+"",3,1,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,2,principal,textFiel); 

		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,2,principal,label);
		
	//dim Z

		label = new JLabel(label_text[3]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,principal,label);

		textFiel = new VTextField_NEW(this._basicInterface.getStackDimension()[2]+"",3,2,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,3,principal,textFiel);    

		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,3,principal,label);
		
		return principal;
	}
	
	private JPanel getPanelNoise(){
		JLabel label;
		VTextField_NEW textFiel;
		JPanel pane = new JPanel(new GridBagLayout());
		
		label = new JLabel(label_text[4]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,3,1,0,0,pane,label);
	
		label = new JLabel(label_text[5]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,pane,label);

		textFiel = new VTextField_NEW("0",3,4,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,1,pane,textFiel); 

		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,25,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,1,pane,label);
		
		return pane;
	}
	
	private JPanel getPanelbackGroundVal(){
		JLabel label;
		VTextField_NEW textFiel;
		JPanel pane = new JPanel(new GridBagLayout());
		
		label = new JLabel(label_text[6]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,3,1,0,0,pane,label);
	
		label = new JLabel(label_text[7]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,pane,label);

		textFiel = new VTextField_NEW("0",3,3,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,1,pane,textFiel); 

		label = new JLabel("");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,25,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,1,pane,label);
		
		return pane;
	}
	
	/**
	* Update the BasicInterface Class
	**/
	public void setUpdateDim(int dim_type, int dim_val){
		this._basicInterface.setStackDimension(dim_val,dim_type);
	}
	
	public void setUpdateNoiseValue(int nv){
		this._basicInterface.setNoiseValue(nv);
	}

	public void setUpdateBackgroundValue(int nv){
		this._basicInterface.FireBackgroundValue((float)nv);
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
}