/**
* BasicInterfaceComponent_dimObjPanelRange.java
* 12/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.phantomaj.select.SliderDimObj_NEW;
import com.phantomaj.select.TransfoCheckBox_NEW;
import com.phantomaj.select.SliderDimObj_NEW;


public class BasicInterfaceComponent_dimObjPanelRange extends JPanel{
	
	private JLabel _dimXRangeStart=null;
	private JLabel _dimYRangeStart=null;
	private JLabel _dimZRangeStart=null;
	
	private JLabel _dimXRangeEnd=null;
	private JLabel _dimYRangeEnd=null;
	private JLabel _dimZRangeEnd=null;
	
	private JLabel _meanLabelX=null;
	private JLabel _meanLabelY=null;
	private JLabel _meanLabelZ=null;

	
	private TransfoCheckBox_NEW _transfoChecking_dimX_form;
	// private TransfoCheckBox_NEW _transfoChecking_dimY_form;
	// private TransfoCheckBox_NEW _transfoChecking_dimZ_form;

	private SliderDimObj_NEW transfoDimXMean_form;
	private SliderDimObj_NEW transfoDimXRange_form;
	
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	private int ori_z=0;
	private int ori_x=0;
	private int ori_y=0;

	private int universe_dim_x=0;
	private int universe_dim_y=0;
	private int universe_dim_z=0;
	

	private static final String[] label_basicText = {" Mean Value : "," Range : "," Size : "};
	
	
//##################################################################
// Constructor
	//type 0,1,2,3,4,5,6 => Box,Cone,Pyramid,Sphere,Tube,Ellipse,Fiber
	public BasicInterfaceComponent_dimObjPanelRange(){
		this.setLayout(new GridBagLayout());
		this.getVariable();
		this.build();
		_transfoChecking_dimX_form.addListener(this);
		transfoDimXMean_form.addListener(this);
		transfoDimXRange_form.addListener(this);
	}


//#################################################################
//Private method
	private void getVariable(){
		ori_x = 0;
		ori_y = 0;
		ori_z = 0;
		universe_dim_x = 200;
		universe_dim_y = 200;
		universe_dim_z = 200;
	}


	private void build(){
		
		JComponent[] component = null;
		JPanel infoPane = null;
		JLabel label = new JLabel("") ;

		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_dimXRangeStart = new JLabel("0000");
		_dimXRangeStart.setPreferredSize(_dimXRangeStart.getPreferredSize());
		_dimXRangeStart.setText("100");
		
		_dimXRangeEnd = new JLabel("0000");
		_dimXRangeEnd.setPreferredSize(_dimXRangeEnd.getPreferredSize());
		_dimXRangeEnd.setText("100");
		
		_transfoChecking_dimX_form = new TransfoCheckBox_NEW("",null,BasicInterface.SLIDER_SIZE_R);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_dimX_form);

	
		label = new JLabel(label_basicText[2]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_dimXRangeStart);
		//add component
		component[1] = _dimXRangeStart ;		
		
		
		_meanLabelX = new JLabel(" < 000 % < ");
		_meanLabelX.setPreferredSize(_meanLabelX.getPreferredSize());
		_meanLabelX.setText(" < 100 % < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_meanLabelX);		
		//add component
		component[2] = _meanLabelX ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_dimXRangeEnd);
		//add component
		component[3] = _dimXRangeEnd ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,this,infoPane);
		
		label = new JLabel(label_basicText[0]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);				

		//add component
		component[4] = label ;
		
		transfoDimXMean_form = new SliderDimObj_NEW(0,universe_dim_x,null);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,this,transfoDimXMean_form);		
	
		//add component
		component[5] = transfoDimXMean_form ;
	
		label = new JLabel(label_basicText[1]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,this,label);

		//add component
		component[6] = label ;		
		
		transfoDimXRange_form = new SliderDimObj_NEW(0,universe_dim_x,null);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,this,transfoDimXRange_form);		
	
		//add component
		component[7] = transfoDimXRange_form ;
	
		//Set Component List
		_transfoChecking_dimX_form.setComponentTab(component);
		_transfoChecking_dimX_form.setSelected(false);

	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method

	public boolean getIsSelected(){
		return _transfoChecking_dimX_form.getIsSelected();
	}
	
	
	public SliderDimObj_NEW getSlider(int ID){
		return this.transfoDimXRange_form;
	}
	
	public void setValueSlider(int[] value){
		if ( value.length == 3 ){
			_dimXRangeStart.setText(value[0]+"");
			_dimXRangeEnd.setText(value[1]+"");
			
			transfoDimXMean_form.setValue_(value[2]);
			_meanLabelX.setText(" < "+transfoDimXMean_form.getValue()+" % < ");

			transfoDimXRange_form.setValue_(Math.max(value[2]-value[0],value[1]-value[2]));
		}
		else {
			System.out.println("Error value.length == "+value.length+" != 3 BasicInterfaceComponent_dimObjPanelRange.setValueSlider");
		}
	}
	
	public void setSelected(boolean[] bol){
		_transfoChecking_dimX_form.setSelected(bol[0]);
	}
	

	
	private int[] computeVal(int mean, int range, int dim_universe){
		int[] val = new int[2];
		if ( (mean - range) < 0 ){
			val[0] = 0;
		}		
		else{
			val[0] = mean - range;
		}
		
		if ( (mean + range) > dim_universe ){
			val[1] = dim_universe;
		}
		else{
			val[1] = mean + range;
		}
		return val;
	}
	

	public void stateChangedSliderObjDim(){
		int[] val = new int[2];
		val = computeVal(transfoDimXMean_form.getValue(),transfoDimXRange_form.getValue(),universe_dim_x);
		_dimXRangeStart.setText(val[0]+"");
		_dimXRangeEnd.setText(val[1]+"");
		_meanLabelX.setText(" < "+transfoDimXMean_form.getValue()+" % < ");
		this.validate();
		this.repaint();
		
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				int[] dim = {Integer.parseInt(_dimXRangeStart.getText()),Integer.parseInt(_dimXRangeEnd.getText()),transfoDimXMean_form.getValue()};
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireUpdateSliderRange(dim,BasicInterface.SLIDER_SIZE_R);
			}
			else {
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_dimObjPanelRange.stateChangedSliderObjDim");
			}
		}
		
	}
	
	public void fireInfoSelected(int ID, int secondID){
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireInfoSelected(ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_dimObjPanelRange.fireInfoSelected");
			}
		}
	}
	
	public void addListener(JComponent component){
		this.listener.add(component);
	}
}