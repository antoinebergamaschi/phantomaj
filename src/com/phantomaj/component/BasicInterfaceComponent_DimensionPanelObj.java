/**
* BasicInterfaceComponent_DimensionPanelObj.java
* 27/09/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;
import com.phantomaj.select.SliderDimObj;
import com.phantomaj.select.TransfoCheckBox;
import com.phantomaj.tree.NodeForm;

/**
* Used in the RandomGeneratorFrame, to deleted.
**/
public class BasicInterfaceComponent_DimensionPanelObj extends JPanel{

	private PhantoMaJ principal_window;
	// private RandomGeneratorFrame rand_window;
	
	private JLabel _dimensionRangeStart=null;
	private JLabel _dimensionRangeEnd=null;
	private JLabel _dimensionMean=null;
	
	private int identifier;
	private TransfoCheckBox _transfoChecking_dimension;

	private SliderDimObj transfoDimensionMean;
	private SliderDimObj transfoDimensionRange;
	
	
	private static final String[][] label_text = {{"<html><h3>Object Dimension Change</h3></html>","<html><ul><li>X dimension Change :</li></ul></html>"},
													{"Dimension :", "Mean Dimension","Mean Value","Range"}};

	private static final String[] label_basicText = {};

	private static final int Basic_Interface = 0;
	private static final int Transfo_Interface = 1;
	
//##################################################################
// Constructor
	public BasicInterfaceComponent_DimensionPanelObj(PhantoMaJ principal_window,int identifier){
		this.principal_window = principal_window;
		this.identifier = identifier;
		// this.rand_window = null;
		this.setLayout(new GridBagLayout());
		this.build();
	}



//#################################################################
//Private method


	private void build(){
		
		JLabel label = null;
		JComponent[] component = null;
		
		if ( this.identifier == BasicInterfaceComponent_DimensionPanelObj.Basic_Interface ){
			ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
			component = new JComponent[4];
			
			label = new JLabel(label_text[0][0]);
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,this,label);
			
			component[0] = label ;
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,this,bouttonUpDown); 
			
			//Xpos
			label = new JLabel(label_text[0][1]);
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,this,label);
			
			component[1] = label ;
			
			_dimensionMean = new JLabel("0000");
			_dimensionMean.setPreferredSize(_dimensionMean.getPreferredSize());
			_dimensionMean.setText("0");
			
			transfoDimensionMean = new SliderDimObj(0,200,_dimensionMean,this,0);
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,transfoDimensionMean);
			
			component[2] = transfoDimensionMean ;
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,2,this,_dimensionMean);
			
			component[3] = _dimensionMean ;
			
			bouttonUpDown.setComponent(component);
			bouttonUpDown.setSelected(false);
		}
		else if ( this.identifier == BasicInterfaceComponent_DimensionPanelObj.Transfo_Interface ){
			JPanel infoPane = null;
		
		
			//initialize component
			component = new JComponent[8];	

			//Creat infoPane###################
			infoPane = new JPanel(new GridBagLayout());
			
			_dimensionRangeStart = new JLabel("0000000000");
			_dimensionRangeStart.setPreferredSize(_dimensionRangeStart.getPreferredSize());
			_dimensionRangeStart.setText("100");
			
			_dimensionRangeEnd = new JLabel("0000000000");
			_dimensionRangeEnd.setPreferredSize(_dimensionRangeEnd.getPreferredSize());
			_dimensionRangeEnd.setText("100");
			
			_transfoChecking_dimension = new TransfoCheckBox("",null,this.principal_window);		
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_dimension);

		
			label = new JLabel(label_text[1][0]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

			//add component
			component[0] = label ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_dimensionRangeStart);
			//add component
			component[1] = _dimensionRangeStart ;		
			
			
			_dimensionMean = new JLabel(" < "+label_text[1][1]+" (0000) < ");
			_dimensionMean.setPreferredSize(_dimensionMean.getPreferredSize());
			_dimensionMean.setText(" < "+label_text[1][1]+" (0) < ");
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_dimensionMean);		
			//add component
			component[2] = _dimensionMean ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_dimensionRangeEnd);
			//add component
			component[3] = _dimensionRangeEnd ;
			
			infoPane.setPreferredSize(infoPane.getPreferredSize());
			//fin creatPane######################
			
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,this,infoPane);
			
			label = new JLabel(label_text[1][2]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,50,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);				

			//add component
			component[4] = label ;
			
			transfoDimensionMean = new SliderDimObj(0,200,_dimensionMean,this,1);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,this,transfoDimensionMean);		
		
			//add component
			component[5] = transfoDimensionMean ;
		
			label = new JLabel(label_text[1][3]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,this,label);

			//add component
			component[6] = label ;		
			
			transfoDimensionRange = new SliderDimObj(0,200,null,this,1);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,this,transfoDimensionRange);		
		
			//add component
			component[7] = transfoDimensionRange ;
		
			//Set Component List
			_transfoChecking_dimension.setComponentTab(component);
			_transfoChecking_dimension.setSelected(false);
		
		}
	
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method

	public boolean getIsSelected(){
		boolean isSelected = _transfoChecking_dimension.getIsSelected();
		return isSelected;
	}
	
	public int[] getDimension(){
	//0,1,2,3,4,5 => Box,Cone,Pyramid,Sphere,Tube,Ellipse
		int[] dim = new int[2];
		if ( this.identifier == BasicInterfaceComponent_DimensionPanelObj.Transfo_Interface ){
			dim[0] = Integer.parseInt(_dimensionRangeStart.getText());
			dim[1] = Integer.parseInt(_dimensionRangeEnd.getText());
		}
		else if ( this.identifier == BasicInterfaceComponent_DimensionPanelObj.Basic_Interface ){
			dim[0] = Integer.parseInt(_dimensionMean.getText());
			dim[1] = 0;
		}
		return dim;
	}
	

	
	public void stateChangedSliderDimension(){
		int[] val = new int[2];
		val = computeVal(transfoDimensionMean.getValue(),transfoDimensionRange.getValue(),200);
		_dimensionRangeStart.setText(val[0]+"");
		_dimensionRangeEnd.setText(val[1]+"");
		_dimensionMean.setText(" < "+label_text[1][1]+"("+transfoDimensionMean.getValue()+") < ");
	}
	
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void applyChangeObjDim(int[][] origine, int[][] dimension){
		this.principal_window.applyChangeObjDim(this.transfoDimensionMean.getValue(),origine,dimension);
	}
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void setOrigine(int[][] tab){
		this.transfoDimensionMean.setOrigine(tab);
	}
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void setDimension(int[][] tab){
		this.transfoDimensionMean.setDimension(tab);
	}
	
	
	private int[] computeVal(int mean, int range, int dim_universe){
		int[] val = new int[2];
		if ( (mean - range) < 0 ){
			val[0] = 0;
		}		
		else{
			val[0] = mean - range;
		}
		
		if ( (mean + range) > dim_universe ){
			val[1] = dim_universe;
		}
		else{
			val[1] = mean + range;
		}
		return val;
	}

	
	public void addListenerCheck(RandomGeneratorFrame frame){
		_transfoChecking_dimension.setListener(frame);
	}	
	
	/********************************************************************************************************************************/
	//Methode de TreeDropAndDrag pour le calcule de rotation et de resizing
	public void computeSize(ArrayList<NodeForm> formis,float coef_size){
		
		Iterator<NodeForm> forms = formis.iterator();	
		int[] center = getCenterObject(formis);	
		
		//for every form constituing this object
		while(forms.hasNext()){
			NodeForm form = forms.next();
			
			int[] ori_old = form.getOrigine();
			int[] dim_old = form.getDimension();
			
			
			form.setOriX((int)(((float)(ori_old[0]-center[0]))*coef_size)+center[0]);
			form.setDim((int)(((float)dim_old[0])*coef_size),0);
			
			form.setOriY((int)(((float)(ori_old[1]-center[1]))*coef_size)+center[1]);
			form.setDim((int)(((float)dim_old[1])*coef_size),1);
			
			form.setOriZ((int)(((float)(ori_old[2]-center[2]))*coef_size)+center[2]);
			form.setDim((int)(((float)dim_old[2])*coef_size),2);
	
		}
	}
	
	
	/**
	* Return the gravity center of the selected object with a weight of 1 for each form
	**/
	public int[] getCenterObject(ArrayList<NodeForm> formis){
		int[] center = new int[3];
		
		//initialize 
		int[][] extremum = new int[3][2] ;
		
		for ( int coor = 0 ; coor < 3 ; coor ++ ){
			extremum[coor][0] = 10000 ;
			extremum[coor][1] = -10000 ;
		}
		
		Iterator<NodeForm> forms = formis.iterator();
			
		//for every form constituing this object
		while(forms.hasNext()){
			NodeForm form = forms.next();
			int[] ori = form.getOrigine();
			// for coor = x,y,z
			for ( int coor = 0 ; coor < 3 ; coor ++ ){
				if ( ori[coor] < extremum[coor][0] ){
					extremum[coor][0] = ori[coor] ;
				}
				if ( ori[coor] > extremum[coor][1] ){
					extremum[coor][1] = ori[coor] ;
				}
			}
			
		}
		
		for ( int coor = 0 ; coor < 3 ; coor ++ ){
			center[coor] = (int)(((float)(extremum[coor][1] + extremum[coor][0]))/2.0f) ;
		}
		// System.out.println("Center : "+center[0]+","+center[1]+","+center[2]);
		return center ;
	}
	
	
	
	public void computeObjectRotation_typeT(ArrayList<NodeForm> formis, int[] angle){
		Iterator<NodeForm> forms = formis.iterator();
			
		//for every form constituing this object
		while(forms.hasNext()){
			NodeForm form = forms.next();
		
			// //Mutiply the old rotation matrix with the new transformation
			double phi=0;
			double theta=0; 
			double psi=0;

			// new transformation
			double [][] matrice_rotation_euler = new double[3][3];

			phi = Math.toRadians(form.getAngle()[0]);
			theta = Math.toRadians(form.getAngle()[1]);
			psi = Math.toRadians(form.getAngle()[2]);

			
			double cphi = Math.cos(phi);
			double sphi = Math.sin(phi);
			double ctheta = Math.cos(theta);
			double stheta = Math.sin(theta);
			double cpsi = Math.cos(psi);
			double spsi = Math.sin(psi);

			matrice_rotation_euler[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
			matrice_rotation_euler[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
			matrice_rotation_euler[0][2] = -cpsi * stheta;
			matrice_rotation_euler[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
			matrice_rotation_euler[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
			matrice_rotation_euler[1][2] = ( spsi * stheta );
			matrice_rotation_euler[2][0] = stheta * cphi;
			matrice_rotation_euler[2][1] = stheta * sphi;
			matrice_rotation_euler[2][2] = ctheta;
			
			//transformation already here
			double [][] matrice_rotation_euler_second = new double[3][3];
		
			phi = Math.toRadians(-angle[0]);
			theta = Math.toRadians(-angle[1]);
			psi = Math.toRadians(-angle[2]);
			
			// System.out.println("Obj\n Phi : "+phi+"\nTheta : "+theta+"\nPsi : "+psi);
	
			cphi = Math.cos(phi);
			sphi = Math.sin(phi);
			ctheta = Math.cos(theta);
			stheta = Math.sin(theta);
			cpsi = Math.cos(psi);
			spsi = Math.sin(psi);

			matrice_rotation_euler_second[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
			matrice_rotation_euler_second[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
			matrice_rotation_euler_second[0][2] = -cpsi * stheta;
			matrice_rotation_euler_second[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
			matrice_rotation_euler_second[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
			matrice_rotation_euler_second[1][2] = ( spsi * stheta );
			matrice_rotation_euler_second[2][0] = stheta * cphi;
			matrice_rotation_euler_second[2][1] = stheta * sphi;
			matrice_rotation_euler_second[2][2] = ctheta;
			
			double [][] matrice_rotation_euler_f = new double[3][3];	
			
			//oldAngle * angle
			matrice_rotation_euler_f[0][0] = matrice_rotation_euler[0][0] * matrice_rotation_euler_second[0][0] + matrice_rotation_euler[0][1] * matrice_rotation_euler_second[0][1] + matrice_rotation_euler[0][2] * matrice_rotation_euler_second[0][2] ;
			matrice_rotation_euler_f[0][1] = matrice_rotation_euler[0][0] * matrice_rotation_euler_second[1][0] + matrice_rotation_euler[0][1] * matrice_rotation_euler_second[1][1] + matrice_rotation_euler[0][2] * matrice_rotation_euler_second[1][2] ;
			matrice_rotation_euler_f[0][2] = matrice_rotation_euler[0][0] * matrice_rotation_euler_second[2][0] + matrice_rotation_euler[0][1] * matrice_rotation_euler_second[2][1] + matrice_rotation_euler[0][2] * matrice_rotation_euler_second[2][2] ;
			
			matrice_rotation_euler_f[1][0] = matrice_rotation_euler[1][0] * matrice_rotation_euler_second[0][0] + matrice_rotation_euler[1][1] * matrice_rotation_euler_second[0][1] + matrice_rotation_euler[1][2] * matrice_rotation_euler_second[0][2] ;
			matrice_rotation_euler_f[1][1] = matrice_rotation_euler[1][0] * matrice_rotation_euler_second[1][0] + matrice_rotation_euler[1][1] * matrice_rotation_euler_second[1][1] + matrice_rotation_euler[1][2] * matrice_rotation_euler_second[1][2] ;
			matrice_rotation_euler_f[1][2] = matrice_rotation_euler[1][0] * matrice_rotation_euler_second[2][0] + matrice_rotation_euler[1][1] * matrice_rotation_euler_second[2][1] + matrice_rotation_euler[1][2] * matrice_rotation_euler_second[2][2] ;
			
			matrice_rotation_euler_f[2][0] = matrice_rotation_euler[2][0] * matrice_rotation_euler_second[0][0] + matrice_rotation_euler[2][1] * matrice_rotation_euler_second[0][1] + matrice_rotation_euler[2][2] * matrice_rotation_euler_second[0][2] ;
			matrice_rotation_euler_f[2][1] = matrice_rotation_euler[2][0] * matrice_rotation_euler_second[1][0] + matrice_rotation_euler[2][1] * matrice_rotation_euler_second[1][1] + matrice_rotation_euler[2][2] * matrice_rotation_euler_second[1][2] ;
			matrice_rotation_euler_f[2][2] = matrice_rotation_euler[2][0] * matrice_rotation_euler_second[2][0] + matrice_rotation_euler[2][1] * matrice_rotation_euler_second[2][1] + matrice_rotation_euler[2][2] * matrice_rotation_euler_second[2][2] ;
			
			// [m00 m01 m02]
			// [m10 m11 m12]
			// [m20 m21 m22]
			// Assuming the angles are in radians.
			theta = Math.toDegrees(Math.acos(matrice_rotation_euler_f[2][2]));
			// System.out.println("Index : "+i+"############################");
			// if cos(Theta) == 1 or -1, theta = 0 or 180 , we can assume that phi = (oldphi + oldpsi ) 
			if (matrice_rotation_euler_f[2][2] > 0.999 || matrice_rotation_euler_f[2][2] < -0.999  ) { // singularity at north pole
				phi  = 	Math.toDegrees(Math.signum(matrice_rotation_euler_f[0][1])*Math.acos(matrice_rotation_euler_f[0][0]));
				psi = 0;
			}
			else{
				// System.out.println("OTHER");
				phi = Math.toDegrees(Math.atan2(matrice_rotation_euler_f[2][1],matrice_rotation_euler_f[2][0]));
				psi = Math.toDegrees(Math.atan2(matrice_rotation_euler_f[1][2],-matrice_rotation_euler_f[0][2]));
			}
			
			form.setPhi((int)phi);
			form.setTheta((int)theta);
			form.setPsi((int)psi);
		}
		updatePos(formis,angle);
	}
	
	private void updatePos(ArrayList<NodeForm> formis, int[] angle){
		int[] centerObj = getCenterObject(formis);
		Iterator<NodeForm> forms = formis.iterator();
		while(forms.hasNext()){
			NodeForm form = forms.next();
			
			int[] newOrigine = new int[3];

			double [][] matrice_rotation_euler = new double[3][3];

			double phi = Math.toRadians(-angle[0]);
			double theta = Math.toRadians(-angle[1]);
			double psi = Math.toRadians(-angle[2]);
			
			
			double cphi = Math.cos(phi);
			double sphi = Math.sin(phi);
			double ctheta = Math.cos(theta);
			double stheta = Math.sin(theta);
			double cpsi = Math.cos(psi);
			double spsi = Math.sin(psi);

			matrice_rotation_euler[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
			matrice_rotation_euler[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
			matrice_rotation_euler[0][2] = -cpsi * stheta;
			matrice_rotation_euler[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
			matrice_rotation_euler[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
			matrice_rotation_euler[1][2] = ( spsi * stheta );
			matrice_rotation_euler[2][0] = stheta * cphi;
			matrice_rotation_euler[2][1] = stheta * sphi;
			matrice_rotation_euler[2][2] = ctheta;
		
		

		
			int[] ori = form.getOrigine();
			for ( int coor = 0 ; coor < 3 ; coor++ ){
				newOrigine[coor] = (int)((ori[0]-centerObj[0])*matrice_rotation_euler[coor][0] + (ori[1]-centerObj[1])*matrice_rotation_euler[coor][1] + (ori[2]-centerObj[2])*matrice_rotation_euler[coor][2] + centerObj[coor]+0.5);
			}

			form.setOriX(newOrigine[0]);
			form.setOriY(newOrigine[1]);
			form.setOriZ(newOrigine[2]);

		}
	}
	
	
	public void computeObjectPosition(ArrayList<NodeForm> formis, int[] addtopos){
		Iterator<NodeForm> forms = formis.iterator();
		int[] center = getCenterObject(formis);
		//for every form constituing this object
		while(forms.hasNext()){
			NodeForm form = forms.next();

			form.setOriX((form.getOrigine()[0]-center[0])+addtopos[0]);

			form.setOriY((form.getOrigine()[1]-center[1])+addtopos[1]);

			form.setOriZ((form.getOrigine()[2]-center[2])+addtopos[2]);
		}
	}
	
}