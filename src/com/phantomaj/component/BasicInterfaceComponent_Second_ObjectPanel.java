/**
* BasicInterfaceComponent_Second_ObjectPanel.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.phantomaj.button.ButtonUpnDown;

public class BasicInterfaceComponent_Second_ObjectPanel extends JPanel{

	private BasicInterface basicInterface;
	private JLabel nameObjLabelTab2;
	
	private BasicInterfaceComponent_rotPanel _angleTransfo;
	private BasicInterfaceComponent_dimObjPanelRange _dimensionTransfo_obj;
	private BasicInterfaceComponent_IntensityPanel _intensityPanel;
	private BasicInterfaceComponent_posPanel _posPanel;
	private ButtonUpnDown[] bouttonUpDown= new ButtonUpnDown[4];
	//Constructor
	
	public BasicInterfaceComponent_Second_ObjectPanel(BasicInterface basicInterface){
		super(new GridBagLayout());
		this.basicInterface = basicInterface;
		this.build();
	}

	//private building methode
	
	private void build(){
		JLabel label;
	
		nameObjLabelTab2 = new JLabel("<html><h1>Object : Object 1</h1></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,nameObjLabelTab2);	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,this.getDimPanel());	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,this.getPosPanel());	

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,this,this.getRotPanel());	

		// //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		// addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,this,_Transfo_obj_l);	

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,this,this.getIntPanel());			
		// _Transfo_obj.setVisible(false);
		
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,5,this,label);
		
	}
	
	

	private JPanel getRotPanel(){
		
		JLabel label;
		// ButtonUpnDown bouttonUpDown;
		JComponent[] component;
		TitledBorder title;
		
		_angleTransfo = new BasicInterfaceComponent_rotPanel(BasicInterface.SLIDER_PHI_R,BasicInterface.SLIDER_THETA_R,BasicInterface.SLIDER_PSI_R);
		_angleTransfo.addListener(this);
		JPanel _Transfo_obj_r = new JPanel(new GridBagLayout());

		bouttonUpDown[1] = new ButtonUpnDown("",null);
		component = new JComponent[2];
		
		label = new JLabel("<html><h3>Rotation Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_obj_r,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_obj_r,bouttonUpDown[1]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_obj_r,_angleTransfo);
		component[1] = _angleTransfo;
		
		title = BorderFactory.createTitledBorder("Modify Rotation Parameters");
		_Transfo_obj_r.setBorder(title);	
		
		bouttonUpDown[1].setComponent(component);
		bouttonUpDown[1].setSelected(false);	
		
		return _Transfo_obj_r;
	}	
	
	private JPanel getDimPanel(){
		
		JLabel label;
		// ButtonUpnDown bouttonUpDown;
		JComponent[] component;
		TitledBorder title;
		_dimensionTransfo_obj = new BasicInterfaceComponent_dimObjPanelRange();
		_dimensionTransfo_obj.addListener(this);
		JPanel _Transfo_obj_d = new JPanel(new GridBagLayout());

		bouttonUpDown[0] = new ButtonUpnDown("",null);
		component = new JComponent[2];
		
		label = new JLabel("<html><h3>Dimension Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_obj_d,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_obj_d,bouttonUpDown[0]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_obj_d,_dimensionTransfo_obj);
		component[1] = _dimensionTransfo_obj;
		
		title = BorderFactory.createTitledBorder("Modify Dimension Parameters");
		_Transfo_obj_d.setBorder(title);	
		
		bouttonUpDown[0].setComponent(component);
		bouttonUpDown[0].setSelected(false);
		
		return _Transfo_obj_d;
	}
	
	private JPanel getIntPanel(){
		JLabel label;
		JComponent[] component;
		TitledBorder title;
	
		JPanel _Transfo_obj_pi = new JPanel(new GridBagLayout());
		_intensityPanel = new BasicInterfaceComponent_IntensityPanel(BasicInterface.SLIDER_OBJ_INT);
		_intensityPanel.addListener(this);
		bouttonUpDown[3] = new ButtonUpnDown("",null);
		component = new JComponent[2];
		
		label = new JLabel("<html><h3>Pixel Value</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_obj_pi,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_obj_pi,bouttonUpDown[3]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_obj_pi,_intensityPanel);
		component[1] = _intensityPanel;
		
		title = BorderFactory.createTitledBorder("Modify Pixel Parameters");
		_Transfo_obj_pi.setBorder(title);	
		
		bouttonUpDown[3].setComponent(component);
		bouttonUpDown[3].setSelected(false);	
		
		return _Transfo_obj_pi;
	}

	private JPanel getPosPanel(){
		JLabel label;
		// ButtonUpnDown bouttonUpDown;
		JComponent[] component;
		TitledBorder title;
		
		_posPanel = new BasicInterfaceComponent_posPanel(BasicInterface.SLIDER_POSX_R,BasicInterface.SLIDER_POSY_R,BasicInterface.SLIDER_POSZ_R);
		_posPanel.addListener(this);
		
		JPanel _Transfo_obj_d = new JPanel(new GridBagLayout());

		bouttonUpDown[2] = new ButtonUpnDown("",null);
		component = new JComponent[2];
		
		label = new JLabel("<html><h3>Position Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_obj_d,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_obj_d,bouttonUpDown[2]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_obj_d,_posPanel);
		component[1] = _posPanel;
		
		title = BorderFactory.createTitledBorder("Modify Position Parameters");
		_Transfo_obj_d.setBorder(title);	
		
		bouttonUpDown[2].setComponent(component);
		bouttonUpDown[2].setSelected(false);
		
		return _Transfo_obj_d;
	}


	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}


	//public methode

	public void setInfo(boolean[] selected, int[][] range){
		boolean[] sel1 = new boolean[1];
		boolean[] sel2 = new boolean[3];
		boolean[] sel3 = new boolean[3];
		
		System.arraycopy(selected,0,sel1,0,1);
		System.arraycopy(selected,1,sel2,0,3);
		System.arraycopy(selected,4,sel3,0,3);
		
		int[][] r1 = new int[1][2];
		int[][] r2 = new int[3][2];
		int[][] r3 = new int[3][2];
		
		System.arraycopy(range,0,r1,0,1);
		System.arraycopy(range,1,r2,0,3);
		System.arraycopy(range,4,r3,0,3);
		
		
		this._angleTransfo.setSelected(sel2);
		this._dimensionTransfo_obj.setSelected(sel1);
		this._posPanel.setSelected(sel3);
		
		int k=0;
		int j=0;
		for ( int i = 0; i < selected.length ; i++ ){
			if ( i == 0 ){
				bouttonUpDown[0].setSelected(sel1[0]);
				this._dimensionTransfo_obj.setValueSlider(r1[0]);
			}
			else if ( i <= 3 ){
				if ( sel2[k]){
					bouttonUpDown[1].setSelected(sel2[k]);
				}
				this._angleTransfo.setValueSlider(k,r2[k]);
				k++;
			}
			else if ( i <= 6){
				if ( sel3[j] ){
					bouttonUpDown[2].setSelected(sel3[j]);
				}
				this._posPanel.setValueSlider(j,r3[j]);
				j++;
			}	
		}
		
	}
	
	public void fireInfoSelected(int ID,int secondID){
		basicInterface.FireInfoSelected(ID,secondID);
	}
	
	public void setInfoGeneral(String name){
		nameObjLabelTab2.setText("<html><h1>Object : "+name+"</h1></html>");
	}
	
	
	public void fireUpdateSliderRange(int[] dim, int ID){
		basicInterface.FireUpdateSliderObj_R(dim,ID);
	}
}