/**
* BasicInterfaceComponent_dimFormPanelRange.java
* 12/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.select.TransfoCheckBox_NEW;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.utils.StaticField_Form;


public class BasicInterfaceComponent_dimFormPanelRange extends JPanel{

	// private int ID;
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	private SliderDim_NEW[][] slider;
	private TransfoCheckBox_NEW[] _transfoCheck;
	private JLabel[][] dimLabel;
	

	private static final String[] label_basicText = {" Mean Value : "," Range : "};
	
	
//##################################################################
// Constructor
	//type 0,1,2,3,4,5,6 => Box,Cone,Pyramid,Sphere,Tube,Ellipse,Fiber
	public BasicInterfaceComponent_dimFormPanelRange(int type){
		super(new GridBagLayout());
		// this.ID = type;
		this.build(type);
		
		//add Listener
		for(int i=0 ; i < _transfoCheck.length ; i++ ){
			_transfoCheck[i].addListener(this);
			_transfoCheck[i].setSecondeID(i);
			slider[i][0].addListener(this);
			slider[i][1].addListener(this);
			slider[i][0].setSecondID(i);
			slider[i][1].setSecondID(i);
		}
	}


//#################################################################
//Private method



	private void build(int type){
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,this,this.getFormDim(type));
	}
	
	private JPanel getFormDim(int ID){
	
		slider = new SliderDim_NEW[StaticField_Form.FORMDIMENSION_NAME[ID].length][2];
		dimLabel = new JLabel[StaticField_Form.FORMDIMENSION_NAME[ID].length][3];
		_transfoCheck = new TransfoCheckBox_NEW[StaticField_Form.FORMDIMENSION_NAME[ID].length];

		JPanel infoPane;
		JLabel label;
		JPanel globalPanel = new JPanel(new GridBagLayout());
		JPanel panel;
		JComponent[] component_second;
		//Fill every Slider 3D dim
		for ( int i = 0 ; i < 3 - StaticField_Form.CODE_3D_FORM[ID] ; i++ ){
			
			component_second = new JComponent[8];	
			panel = new JPanel(new GridBagLayout());
	
			//Creat infoPane###################
			infoPane = new JPanel(new GridBagLayout());
			
			dimLabel[i][0] = new JLabel("00000");
			dimLabel[i][0].setPreferredSize(dimLabel[i][0].getPreferredSize());
			dimLabel[i][0].setText("0");
			
			dimLabel[i][1] = new JLabel("00000");
			dimLabel[i][1].setPreferredSize(dimLabel[i][1].getPreferredSize());
			dimLabel[i][1].setText("0");
			
			_transfoCheck[i] = new TransfoCheckBox_NEW("",null,BasicInterface.SLIDER_DIM_N);		
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoCheck[i]);

		
			label = new JLabel(StaticField_Form.FORM_NAME[ID]+" "+StaticField_Form.FORMDIMENSION_NAME[ID][i]+":");
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

			//add component
			component_second[0] = label ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,dimLabel[i][0]);
			//add component
			component_second[1] = dimLabel[i][0] ;		
			
			
			dimLabel[i][2] = new JLabel(" < 00000 < ");
			dimLabel[i][2].setPreferredSize(dimLabel[i][2].getPreferredSize());
			dimLabel[i][2].setText(" < 0 < ");
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,dimLabel[i][2]);		
			//add component
			component_second[2] = dimLabel[i][2] ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,dimLabel[i][1]);
			//add component
			component_second[3] = dimLabel[i][1] ;
			
			infoPane.setPreferredSize(infoPane.getPreferredSize());
			//fin creatPane######################
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,panel,infoPane);
			
			label = new JLabel(label_basicText[0]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,panel,label);				

			//add component
			component_second[4] = label ;
			
			slider[i][0] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[i],0,null,BasicInterface.SLIDER_DIM_N);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,panel,slider[i][0]);		
		
			//add component
			component_second[5] = slider[i][0] ;
		
			label = new JLabel(label_basicText[1]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,panel,label);

			//add component
			component_second[6] = label ;		
			
			slider[i][1] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[i],0,null,BasicInterface.SLIDER_DIM_N);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,panel,slider[i][1]);		
		
			//add component
			component_second[7] = slider[i][1] ;
			
			_transfoCheck[i].setComponentTab(component_second);
			_transfoCheck[i].setSelected(false);
			
			addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,i,globalPanel,panel);
		}
		
		int k = 0;
		//Fill the additional parameters Slider
		for ( int z = 3 - StaticField_Form.CODE_3D_FORM[ID] ; z < StaticField_Form.FORMDIMENSION_NAME[ID].length ; z++ ){
			panel = new JPanel(new GridBagLayout());
			infoPane = new JPanel(new GridBagLayout());
			// ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
			component_second = new JComponent[8];
		
			dimLabel[z][0] = new JLabel("00000");
			dimLabel[z][0].setPreferredSize(dimLabel[z][0].getPreferredSize());
			dimLabel[z][0].setText("0");
			
			dimLabel[z][1] = new JLabel("00000");
			dimLabel[z][1].setPreferredSize(dimLabel[z][1].getPreferredSize());
			dimLabel[z][1].setText("0");
			
			_transfoCheck[z] = new TransfoCheckBox_NEW("",null,BasicInterface.SLIDER_DIM_N);		
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoCheck[z]);

		
			label = new JLabel(StaticField_Form.FORM_NAME[ID]+" "+StaticField_Form.FORMDIMENSION_NAME[ID][z]+":");
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

			//add component
			component_second[0] = label ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,dimLabel[z][0]);
			//add component
			component_second[1] = dimLabel[z][0] ;		
			
			
			dimLabel[z][2] = new JLabel(" < 00000 < ");
			dimLabel[z][2].setPreferredSize(dimLabel[z][2].getPreferredSize());
			dimLabel[z][2].setText(" < 0 < ");
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,dimLabel[z][2]);		
			//add component
			component_second[2] = dimLabel[z][2] ;
			
			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,dimLabel[z][1]);
			//add component
			component_second[3] = dimLabel[z][1] ;
			
			infoPane.setPreferredSize(infoPane.getPreferredSize());
			//fin creatPane######################

			
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,panel,infoPane);
			
			label = new JLabel(label_basicText[0]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,panel,label);				

			//add component
			component_second[4] = label ;
			
			//Warning here the identifier is z+1 as this is the 4th dimension ect.. ( greater than 3 )
			if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 0 ){
				slider[z][0] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[0],0,null,BasicInterface.SLIDER_DIM_N);
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,panel,slider[z][0]);		
			}
			else if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 1 ){
				slider[z][0] = new SliderDim_NEW(JSlider.HORIZONTAL,0,180,0,null,BasicInterface.SLIDER_DIM_N);
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,panel,slider[z][0]);		
			}		
		
			//add component
			component_second[5] = slider[z][0] ;
		
			label = new JLabel(label_basicText[1]);
			// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,panel,label);

			//add component
			component_second[6] = label ;		
			
			//Warning here the identifier is z+1 as this is the 4th dimension ect.. ( greater than 3 )
			if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 0 ){
				slider[z][1] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[0],0,null,BasicInterface.SLIDER_DIM_N);
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,panel,slider[z][1]);		
			}
			else if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 1 ){
				slider[z][1] = new SliderDim_NEW(JSlider.HORIZONTAL,0,180,0,null,BasicInterface.SLIDER_DIM_N);
				// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,panel,slider[z][1]);		
			}	
	
			k++;
		
			//add component
			component_second[7] = slider[z][1] ;
			
			_transfoCheck[z].setComponentTab(component_second);
			_transfoCheck[z].setSelected(false);

			
			addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,z,globalPanel,panel);
		}
		
		
		return globalPanel;
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method

	public void fireInfoSelected(int ID, int secondID){
		for ( int i=0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				((BasicInterfaceComponent_Second_FormPanel)component).fireInfoSelected(ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				boolean bol = _transfoCheck[secondID].getIsSelected();
				((BasicInterfaceComponent_RandPanel)component).fireInfoSelected(ID,secondID,bol);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_dimFormPanelRange.fireInfoSelected");
			}
		}
	}

	public void fireStatChanged(int ID,int secondID){
		stateChangedSliderDim(secondID);
		
		for ( int i=0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				int[] nv = {Integer.parseInt(dimLabel[secondID][0].getText()),Integer.parseInt(dimLabel[secondID][1].getText()),slider[secondID][0].getValue()};
				((BasicInterfaceComponent_Second_FormPanel)component).fireStatChanged(nv,ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				int[] nv = {Integer.parseInt(dimLabel[secondID][0].getText()),Integer.parseInt(dimLabel[secondID][1].getText())};
				((BasicInterfaceComponent_RandPanel)component).fireStatChanged(nv,ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_dimFormPanelRange.fireStatChanged");
			}
		}
	}
	
	public boolean[] getIsSelected(){
		boolean[] isSelected = new boolean[_transfoCheck.length];
		for ( int i = 0 ;  i < isSelected.length ; i++ ){
			isSelected[i] = _transfoCheck[i].getIsSelected();
		}
		return isSelected;
	}
	
	public int[][] getDimension(){
		int[][] dim = new int[_transfoCheck.length][3];

		for ( int i = 0 ;  i < _transfoCheck.length ; i++ ){
			dim[i][0] = Integer.parseInt(dimLabel[i][0].getText());
			dim[i][1] = Integer.parseInt(dimLabel[i][1].getText());
			dim[i][2] = slider[i][0].getValue();
		}
		
		return dim;
	}
	
	
	public void setValueSlider(int ID, int[] value){
		if ( value.length == 3 ){
			dimLabel[ID][0].setText(value[0]+"");
			dimLabel[ID][1].setText(value[1]+"");
			
			slider[ID][0].setValue_(value[2]);
			dimLabel[ID][2].setText(" < "+slider[ID][0].getValue()+" < ");

			slider[ID][1].setValue_(Math.max(value[2]-value[0],value[1]-value[2]));
		}
		else {
			System.out.println("Error value.length == "+value.length+" != 3 BasicInterfaceComponent_dimFormPanelRange.setValueSlider");
		}
	}
	
	public void setSelected(boolean[] bol){
		for ( int i = 0 ;  i < _transfoCheck.length ; i++ ){
			_transfoCheck[i].setSelected(bol[i]);
		}
	}
	

	
	// //ID_Slider
	// // 2,3 => meanXdim, rangeX
	// // 4,5 => meanYdim, RangeY
	// // 6,7 => meanZdim, Range Z
	public void stateChangedSliderDim(int ID_slider){
		int[] val = new int[2];
		val = computeVal(slider[ID_slider][0].getValue(),slider[ID_slider][1].getValue(),BasicInterface.getStackDimension()[ID_slider]);
		dimLabel[ID_slider][0].setText(val[0]+"");
		dimLabel[ID_slider][1].setText(val[1]+"");
		dimLabel[ID_slider][2].setText(" < "+slider[ID_slider][0].getValue()+" < ");
		this.validate();
		this.repaint();
	}
	
	private int[] computeVal(int mean, int range, int dim_universe){
		int[] val = new int[2];
		if ( (mean - range) < 0 ){
			val[0] = 0;
		}		
		else{
			val[0] = mean - range;
		}
		
		if ( (mean + range) > dim_universe ){
			val[1] = dim_universe;
		}
		else{
			val[1] = mean + range;
		}
		return val;
	}
	
	public void addListener(JComponent component){
		this.listener.add(component);
	}
	
}