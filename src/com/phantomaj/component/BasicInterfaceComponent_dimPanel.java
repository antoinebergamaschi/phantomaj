/**
* BasicInterfaceComponent_dimPanel.java
* 07/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.select.SliderDim_NEW;


public class BasicInterfaceComponent_dimPanel extends JPanel{


	private int ID;
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	private SliderDim_NEW[] slider;
	
//##################################################################
// Constructor
	public BasicInterfaceComponent_dimPanel(int type){
		super(new GridBagLayout());
		this.ID = type;
		this.build();
	}


//#################################################################

	private void build(){
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,this,this.getFormDim());
	}
	
	
	private JPanel getFormDim(){
	
		JComponent[] component=new JComponent[(StaticField_Form.FORMDIMENSION_NAME[ID].length)*3];
		slider = new SliderDim_NEW[StaticField_Form.FORMDIMENSION_NAME[ID].length];
		JLabel label;
		JPanel globalPanel = new JPanel(new GridBagLayout());
		
		ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);

		//Fill every Slider 3D dim
		for ( int i = 0 ; i < 3 - StaticField_Form.CODE_3D_FORM[ID] ; i++ ){
			
			JPanel panel = new JPanel(new GridBagLayout());
		
			label = new JLabel("<html><ul><li>"+StaticField_Form.FORM_NAME[ID]+" "+StaticField_Form.FORMDIMENSION_NAME[ID][i]+":</li></ul></html>");
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,label);

			component[i*3] = label ;
			if(i==0){
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,panel,bouttonUpDown); 
			}
			label = new JLabel("1000");
			label.setPreferredSize(label.getPreferredSize());
			label.setText("100");
			
			slider[i] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[i],StaticField_Form.BASICDIMENSION[ID][i],label,i);
			slider[i].addListener(this);
			
			addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,panel,slider[i]);

			component[(i*3)+1] = slider[i] ;
			
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,1,panel,label);
		
			component[(i*3)+2] = label ;
			
			addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,i,globalPanel,panel);
		}
		
		int k = 0;
		//Fill the additional parameters Slider
		for ( int z = 3 - StaticField_Form.CODE_3D_FORM[ID] ; z < StaticField_Form.FORMDIMENSION_NAME[ID].length ; z++ ){
			JPanel panel = new JPanel(new GridBagLayout());
			// ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
		
			label = new JLabel("<html><ul><li>"+StaticField_Form.FORM_NAME[ID]+" "+StaticField_Form.FORMDIMENSION_NAME[ID][z]+":</li></ul></html>");
			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,label);

			component[z*3] = label ;

			label = new JLabel("1000");
			label.setPreferredSize(label.getPreferredSize());
			label.setText("0");
			
			
			//Warning here the identifier is z+1 as this is the 4th dimension ect.. ( greater than 3 )
			if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 0 ){
				slider[z] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[z],StaticField_Form.BASICDIMENSION[ID][z],label,z+1);
				addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,panel,slider[z]);
			}
			else if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID][k] == 1 ){
				slider[z] = new SliderDim_NEW(JSlider.HORIZONTAL,0,180,StaticField_Form.BASICDIMENSION[ID][z],label,z+1);
				addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,panel,slider[z]);
			}
			k++;
			component[(z*3)+1] = slider[z] ;
			slider[z].addListener(this);

			//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
			addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,1,panel,label);
		
			component[(z*3)+2] = label ;
			
			addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,z,globalPanel,panel);
		}
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(true);
		
		return globalPanel;
	}
	

	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method

	public void setUpdateSlider(int nv, int ID){
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_FormPanel ){
				((BasicInterfaceComponent_FormPanel)component).setUpdateSlider(nv,ID);
			}
			else {
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_dimPanel.setUpdateSlider");
			}
		}
	}

	public void setInfo(int ID,int dim[]){
		this.ID = ID;
		int k = 0;
		for ( int i=0 ; i < slider.length ; i++ ){
			switch(StaticField_Form.CODE_3D_FORM[ID]){
				case 0:
					slider[i].setValue(dim[i]);
					break;
				case 1:
					// System.out.println(dim[i]);
					if ( i == 0 || i == 2){
						slider[k].setValue(dim[i]);
						k++;
					}
					break;
				case 2:
					if ( i == 0 ){
						slider[i].setValue(dim[i]);
					}
					break;
				case -1:
					slider[i].setValue(dim[i]);
					break;
			}
			// slider[i].setValue(dim[i]);
		}
	}
	
	
	public void addListener(JComponent component){
		this.listener.add(component);
	}
}

