/**
* BasicInterface.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

import javax.swing.*;
import java.util.ArrayList;

/**
* As the name says BasicInterface contained the function for building and firing event relative to the user interaction with the interface.
**/
public class BasicInterface{

	public final static int BACKGROUND_ROOT = 0;
	public final static int NOISE_ROOT = 1;
	// public final static int ROTATION_OBJ = 2;
	
	public final static int SLIDER_PHI_N = 2;
	public final static int SLIDER_THETA_N = 3;
	public final static int SLIDER_PSI_N = 4;
	
	public final static int SLIDER_PHI_R=5;
	public final static int SLIDER_THETA_R=6;
	public final static int SLIDER_PSI_R=7;
	
	// public final static int SLIDER_OBJ_DIM = 8;
	public final static int SLIDER_OBJ_INT = 9;
	
	public static final int SLIDER_SIZE_R = 10;
	
	public static final int SLIDER_POSX_N = 11;
	public static final int SLIDER_POSY_N = 12;
	public static final int SLIDER_POSZ_N = 13;
	
	public static final int SLIDER_POSX_R = 14;
	public static final int SLIDER_POSY_R = 15;
	public static final int SLIDER_POSZ_R = 16;
	
	public static final int SLIDER_DIM_N = 17;

	public static final int SLIDER_RAND_INT = 18;
	
	private JPanel _currentPanel;
	private JPanel _secondPanel;
	private JPanel _randPanel;
	private ArrayList<JFrame> listener = new ArrayList<JFrame>();
	
	//Class Variable
	private static int[] StackDimension = {200,200,200};
	private int backgroundValue = 0;
	//deprecate
	private int noiseValue = 0;
	
	
	/**
	* Constructor
	**/
	public BasicInterface(){
	
	}

//*******************************************************************************************
//Basic Function	
	
	/**
	* Update the private JPanel _currentPanel and _secondPanel in the BasicInterface class. 
	**/
	public void setFormPanel(){
		this._currentPanel = new BasicInterfaceComponent_FormPanel(this,0);
		this._secondPanel = new BasicInterfaceComponent_Second_FormPanel(this,0);
	}
	
	/**
	* Update the private JPanel _currentPanel and _secondPanel in the BasicInterface class.
	**/
	public void setObjectPanel(){
		this._currentPanel  = new BasicInterfaceComponent_ObjectPanel(this);
		this._secondPanel = new BasicInterfaceComponent_Second_ObjectPanel(this);
	}
	
	/**
	* Update the private JPanel _currentPanel and _secondPanel in the BasicInterface class.
	**/
	public void setPhantomPanel(){
		this._currentPanel = new BasicInterfaceComponent_rootPanel(this);
		this._secondPanel = new BasicInterfaceComponent_Second_rootPanel(this);
	}
	
	/**
	* Update the private JPanel _randPanel in the BasicInterface class.
	**/
	public void setRandomPanel(){
		this._randPanel = new BasicInterfaceComponent_RandPanel(this);
	}
	
//********************************************************************************************
//Getter Setter


	public JPanel getCurrentPanel(){
		return this._currentPanel;
	}
	
	public JPanel getSecondPanel(){
		return this._secondPanel;
	}
	
	public JPanel getRandPanel(){
		return this._randPanel;
	}
	
	/**
	* Update the StackDimension
	**/
	public void setStackDimension(int dim_val, int dim_type){
		BasicInterface.StackDimension[dim_type] = dim_val;
		this.fireStackDimChange(dim_val,dim_type);
	}
	
	public static int[] getStackDimension(){
		return StackDimension;
	}	
	

	public void setNoiseValue(int nb){
		this.noiseValue = nb;
	}
	
	
	public int getNoiseValue(){
		return this.noiseValue;
	}
	
	public void addListener(JFrame new_listener){
		this.listener.add(new_listener);
	}
	
	/**
	* Set the basic information needed by the currently selected node Object.
	* @param  dim, the dimension of each form in the selected Object.
	* @param ori the origine of each form in the selected Object.
	* @param  nameForm the name of each form in the selected Object.
	* @param  pv the pixel value of the Object.
	* @param  layer the layer of the selected Object.
	* @param  name the name of the selected Object.
	**/
	public void setObjectInfo(int[][] dim, int[][] ori,ArrayList<String> nameForm,float pv,int layer,String name){
		((BasicInterfaceComponent_ObjectPanel)this._currentPanel).setObjectInfo(dim,ori,nameForm,pv,layer,name);
		((BasicInterfaceComponent_Second_ObjectPanel)this._secondPanel).setInfoGeneral(name);
	}
	
	public void setObjectInfoSliderPos(int[] min, int[] max){
		((BasicInterfaceComponent_ObjectPanel)this._currentPanel).initSlider(min, max);
	}
	
	public void setObjectInfoSliderRot(int[][] angle, int[] center){
		((BasicInterfaceComponent_ObjectPanel)this._currentPanel).setObjectInfoRot(angle, center);
	}
	
	public void setObjectInfo_Seconde(boolean[] selected, int[][] range){
		((BasicInterfaceComponent_Second_ObjectPanel)this._secondPanel).setInfo(selected, range);
	}
	
	/**
	* Set the basic information needed by the currently selected node Form.
	* @param nameForm the name of this form.
	* @param nameObj the name of this form parent ( node Object ).
	* @param  angle the  phi,theta,psi angle of this form.
	* @param  dim the dimension of this form ( including additional dimension ).
	* @param ori the center point of this form.
	**/
	public void setFormInfo(String nameForm, String nameObj, int[] angle, int[] dim, int ID, int[] ori){
		((BasicInterfaceComponent_Second_FormPanel)this._secondPanel).setFormInfo(ID,nameForm,nameObj);
		
		((BasicInterfaceComponent_FormPanel)this._currentPanel).setFormInfo(nameForm, nameObj, angle, dim, ID, ori);
	}
	
	public void setFormInfo_Second(boolean[][] selected,int[][] valTransfo, int[][] valTransfo_dim){
		((BasicInterfaceComponent_Second_FormPanel)this._secondPanel).setSelected(selected);
		((BasicInterfaceComponent_Second_FormPanel)this._secondPanel).setInfo(valTransfo,valTransfo_dim);
	}
	
	/**
	* Set the form ID in the randPanel 
	**/
	public void setRandInfo(int ID){
		((BasicInterfaceComponent_RandPanel)this._randPanel).setForm(ID);
	}
	
//********************************************************************************************
//Private Function Fire event

	/**
	* Fire stack dimension changed.
	* @param dim_val, the new dimension
	* @param dim_type, the dimension identifier
	**/
	private void fireStackDimChange(int dim_val, int dim_type){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).reload3DView();
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.fireStackDimChange");
			}
		}
	}

	/**
	* Fire background value changed.
	* @param bv, the new background value
	**/
	public void FireBackgroundValue(float bv){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updateBackgroundVal(bv);
				
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireBackgroundValue");
			}
		}
	}	

	//object
	/**
	* Fire Object dimension changed.
	* @param transfo, the transformation identifier.
	* @param origine, origine of every form contained in this object.
	* @param dimension, dimension of every form contained in this object.
	**/
	public void FireObjectDimChange(int transfo, int[][] origine, int[][] dimension){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).applyChangeObjDim(transfo,origine,dimension);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectDimChange");
			}
		}
	}
	
	/**
	* Fire Object dimension changed.
	* @param id, the transformation identifier.
	* @param ori, origine of every form contained in this object.
	* @param newValue, the position modifiers.
	**/
	public void FireObjectPosChange(int id,int newValue, int[][] ori){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).applyChangeObjPos(id,newValue,ori);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectPosChange");
			}
		}
	}
	
	public void FireObjectRotationChange(int[] newAngle, int[][] oldAngle,int[][] ori, int[] center ){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).UpdateRotationObject(newAngle,oldAngle,ori,center);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectRotationChange");
			}
		}
	}
	
	public void FireObjectRotationCenterChange(int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updateSliderRotationObjCenterofRotation(ID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectRotationCenterChange");
			}
		}
	}

	public void FireObjectPixelValueChange(int new_pv){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).applyPixelValueO(new_pv);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectPixelValueChange");
			}
		}
	}
	
	public void FireObjectLayerChange(int new_l){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).applyLayerO(new_l);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireObjectLayerChange");
			}
		}
	}
	
	
	//form
	public void FireUpdateSliderDim(int newVal, int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updateDim(newVal,ID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireUpdateSliderDim");
			}
		}
	}

	public void FireUpdateSliderPos(int newVal, int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updatePos(newVal,ID);
				// System.out.println("Faire Update la form");
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireUpdateSliderPos");
			}
		}
	}
	
	public void FireUpdateSliderRot(int newVal, int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updateRot(newVal,ID);
				// System.out.println("Faire Update la form");
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireUpdateSliderRot");
			}
		}
	}

	public void FireFormChoosen(int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).updateForm(ID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireFormChoosen");
			}
		}
	}

	
	//range
	public void FireInfoSelected(int ID, int secondID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).setInfoSelected(ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireInfoSelected");
			}
		}
	}
	
	
	public void FireUpdateSliderObj_R(int[] nv,int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).setInfoTabInNode(nv,ID,0);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireUpdateSliderObj_R");
			}
		}
	}
	
	public void FireUpdateSliderForm(int[] nv, int ID, int secondID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).setInfoTabInNode(nv,ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.FireUpdateSliderObj_R");
			}
		}
	}
	
	
	//randomGenerator
	public void fireInfoSelected_Rand(int ID, int secondID, boolean bol){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof RandomGeneratorFrame ){
				//if this is a dimension change
				if ( ID == BasicInterface.SLIDER_DIM_N ){
					((RandomGeneratorFrame)frame).setDimensionSelected(secondID,bol);
				}
				//if this is a rotation change
				else if ( ID == SLIDER_PHI_N || ID == SLIDER_PSI_N || ID == SLIDER_THETA_N ){
					((RandomGeneratorFrame)frame).setRotationSelected(secondID,bol);
				}
				//if this is a intensity change
				else if ( ID == SLIDER_RAND_INT ){
					((RandomGeneratorFrame)frame).setIntensitySelected(bol);
				}
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.fireInfoSelected_Rand");
			}
		}
	}

	public void fireStatChanged_Rand(int[] nv, int ID, int secondID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof RandomGeneratorFrame ){
				//if this is a dimension change
				if ( ID == BasicInterface.SLIDER_DIM_N ){
					((RandomGeneratorFrame)frame).setDimensionRange(secondID,nv);
				}
				//if this is a rotation change
				else if ( ID == SLIDER_PHI_N || ID == SLIDER_PSI_N || ID == SLIDER_THETA_N ){
					((RandomGeneratorFrame)frame).setAngleRange(secondID,nv);
				}
				//if this is a intensity change
				else if ( ID == SLIDER_RAND_INT ){
					((RandomGeneratorFrame)frame).setIntensityRange(nv);
				}
			}
			else {
				System.out.println("WARNING ::: instanceof "+frame.getClass() +" is not recognize component.BasicInterface.fireStatChanged_Rand");
			}
		}
	}
	
	public void setSelectedCenter(int ID){
		for(int i=0 ; i < this.listener.size() ; i++){
			JFrame frame = listener.get(i);
			if ( frame instanceof PhantoMaJ){
				((PhantoMaJ)frame).setSelectedCenter(ID);
			}
		}
	}
	
}