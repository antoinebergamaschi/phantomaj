/**
* BasicInterfaceComponent_DimensionPanelObj_NEW.java
* 27/09/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

//local imports
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.select.SliderDimObj_NEW;
//java imports


public class BasicInterfaceComponent_DimensionPanelObj_NEW extends JPanel{

	private BasicInterfaceComponent_ObjectPanel basicInterface;

	private JLabel _dimensionMean=null;
	

	private SliderDimObj_NEW transfoDimensionMean;
	
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	private static final String[] label_text = {"<html><h3>Object Dimension Change</h3></html>","<html><ul><li>X dimension Change :</li></ul></html>"};

	
//##################################################################
// Constructor
	public BasicInterfaceComponent_DimensionPanelObj_NEW(){
		super(new GridBagLayout());
		this.build();
		transfoDimensionMean.addListener(this);
	}



//#################################################################
//Private method


	private void build(){
		
		JLabel label = null;
		JComponent[] component = null;

		ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
		component = new JComponent[4];
		
		label = new JLabel(label_text[0]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,this,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,this,bouttonUpDown); 
		
		//Xpos
		label = new JLabel(label_text[1]);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,this,label);
		
		component[1] = label ;
		
		_dimensionMean = new JLabel("0000");
		_dimensionMean.setPreferredSize(_dimensionMean.getPreferredSize());
		_dimensionMean.setText("0");
		
		transfoDimensionMean = new SliderDimObj_NEW(0,200,_dimensionMean);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,transfoDimensionMean);
		
		component[2] = transfoDimensionMean ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,2,this,_dimensionMean);
		
		component[3] = _dimensionMean ;
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(false);
	
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method


	public int getDimension(){
		return transfoDimensionMean.getValue();
	}
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void applyChangeObjDim(int[][] dimension){
		for ( int i = 0 ; i < listener.size() ; i ++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_ObjectPanel ){
				((BasicInterfaceComponent_ObjectPanel)component).setChangeObjDim(this.transfoDimensionMean.getValue(),dimension);
			}
		}
	}
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void setOrigine(int[][] tab){
		this.transfoDimensionMean.setOrigine(tab);
	}
	
	/**
	* Linker Between SliderDimObj and PhantoMaJ
	**/
	public void setDimension(int[][] tab){
		this.transfoDimensionMean.setDimension(tab);
	}
	
	
	/**
	* Add a listener in this Panel
	* @param component the JComponent to add as listerner
	**/
	public void addListener(JComponent component){
		this.listener.add(component);
	}
	
}