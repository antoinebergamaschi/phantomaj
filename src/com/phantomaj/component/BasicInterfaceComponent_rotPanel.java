/**
* BasicInterfaceComponent_rotPanel.java
* 08/06/2012
* @author Antoine Bergamaschi
**/


package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.phantomaj.select.SliderAngleTransfo;
import com.phantomaj.select.TransfoCheckBox_NEW;
import com.phantomaj.select.SliderAngleTransfo;

public class BasicInterfaceComponent_rotPanel extends JPanel{


	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	private JLabel _beginPhi_form;
	private JLabel _endPhi_form;
	private JLabel _beginTheta_form;
	private JLabel _endTheta_form;
	private JLabel _beginPsi_form;
	private JLabel _endPsi_form;
	
	private TransfoCheckBox_NEW _transfoChecking_phi_form;
	private TransfoCheckBox_NEW _transfoChecking_theta_form;
	private TransfoCheckBox_NEW _transfoChecking_psi_form;
	
	private SliderAngleTransfo transfoPhiBegin_form ;
	private SliderAngleTransfo transfoThetaBegin_form ;
	private SliderAngleTransfo transfoPsiBegin_form ;
	private SliderAngleTransfo transfoPhiEnd_form ;
	private SliderAngleTransfo transfoThetaEnd_form ;
	private SliderAngleTransfo transfoPsiEnd_form ;
	
	
	private int phi = 0;
	private int theta = 0;
	private int psi = 0;
	
	private int ID_SLIDER_0 = 0;
	private int ID_SLIDER_1 = 1;
	private int ID_SLIDER_2 = 2;
	
//##################################################################
// Constructor


	public BasicInterfaceComponent_rotPanel(int ID_0, int ID_1, int ID_2){
		super(new GridBagLayout());
		
		this.ID_SLIDER_0 = ID_0;
		this.ID_SLIDER_1 = ID_1;
		this.ID_SLIDER_2 = ID_2;
		
		this.build();
		
		_transfoChecking_phi_form.addListener(this);
		_transfoChecking_phi_form.setSecondeID(0);
		_transfoChecking_psi_form.addListener(this);
		_transfoChecking_psi_form.setSecondeID(2);
		_transfoChecking_theta_form.addListener(this);
		_transfoChecking_theta_form.setSecondeID(1);
		
		transfoPhiBegin_form.addListener(this);
		transfoPhiEnd_form.addListener(this);
		
		transfoPsiBegin_form.addListener(this);
		transfoPsiEnd_form.addListener(this);
		
		transfoThetaBegin_form.addListener(this);
		transfoThetaEnd_form.addListener(this);
	}

	
	private void build(){
		//initialize component
		JComponent[] component = new JComponent[8];	
		JLabel label = new JLabel("");
		
		//Creat infoPane###################
		JPanel infoPane = new JPanel(new GridBagLayout());
		
		
		_beginPhi_form = new JLabel("0000");
		_beginPhi_form.setPreferredSize(_beginPhi_form.getPreferredSize());
		_beginPhi_form.setMinimumSize(_beginPhi_form.getPreferredSize());
		_beginPhi_form.setText("0");
		
		_endPhi_form = new JLabel("0000");
		_endPhi_form.setPreferredSize(_endPhi_form.getPreferredSize());
		_endPhi_form.setText("0");
		
		_transfoChecking_phi_form = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_0);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_phi_form);

	
		label = new JLabel("Phi : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginPhi_form);
		//add component
		component[1] = _beginPhi_form ;		
		
		
		label = new JLabel(" <  Phi  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_endPhi_form);
		//add component
		component[3] = _endPhi_form ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);				

		//add component
		component[4] = label ;
		
		transfoPhiBegin_form = new SliderAngleTransfo(JSlider.HORIZONTAL,-180,this.phi,this.phi,_beginPhi_form,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,this,transfoPhiBegin_form);		
	
		//add component
		component[5] = transfoPhiBegin_form ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,this,label);

		//add component
		component[6] = label ;		
		
		transfoPhiEnd_form = new SliderAngleTransfo(JSlider.HORIZONTAL,this.phi,180,this.phi,_endPhi_form,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,this,transfoPhiEnd_form);		
	
		//add component
		component[7] = transfoPhiEnd_form ;
	
		//Set Component List
		_transfoChecking_phi_form.setComponentTab(component);
		_transfoChecking_phi_form.setSelected(false);
	
//THETA

		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_beginTheta_form = new JLabel("0000");
		_beginTheta_form.setPreferredSize(_beginTheta_form.getPreferredSize());
		_beginTheta_form.setMinimumSize(_beginPhi_form.getPreferredSize());
		_beginTheta_form.setText("0");
		
		_endTheta_form = new JLabel("0000");
		_endTheta_form.setPreferredSize(_endTheta_form.getPreferredSize());
		_endTheta_form.setText("0");
		
		_transfoChecking_theta_form = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_1);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_theta_form);

	
		label = new JLabel("Theta : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginTheta_form);
		//add component
		component[1] = _beginTheta_form ;		
		
		
		label = new JLabel(" <  Theta  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,4,0,infoPane,_endTheta_form);
		//add component
		component[3] = _endTheta_form ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,2,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,this,label);				

		//add component
		component[4] = label ;
		
		transfoThetaBegin_form = new SliderAngleTransfo(JSlider.HORIZONTAL,-180,this.theta,this.theta,_beginTheta_form,this.ID_SLIDER_1);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,3,this,transfoThetaBegin_form);		
	
		//add component
		component[5] = transfoThetaBegin_form ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,3,this,label);

		//add component
		component[6] = label ;		
		
		transfoThetaEnd_form = new SliderAngleTransfo(JSlider.HORIZONTAL,this.theta,180,this.theta,_endTheta_form,this.ID_SLIDER_1);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,3,this,transfoThetaEnd_form);		
	
		//add component
		component[7] = transfoThetaEnd_form ;
	
		//Set Component List
		_transfoChecking_theta_form.setComponentTab(component);
		_transfoChecking_theta_form.setSelected(false);
	

	
//PSI

		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_beginPsi_form = new JLabel("0000");
		_beginPsi_form.setPreferredSize(_beginPsi_form.getPreferredSize());
		_beginPsi_form.setMinimumSize(_beginPsi_form.getPreferredSize());
		_beginPsi_form.setText("0");
		
		_endPsi_form = new JLabel("0000");
		_endPsi_form.setPreferredSize(_endPsi_form.getPreferredSize());
		_endPsi_form.setText("0");
		
		_transfoChecking_psi_form = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_2);	
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_psi_form);

	
		label = new JLabel("Psi : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginPsi_form);
		//add component
		component[1] = _beginPsi_form ;		
		
		label = new JLabel(" <  Psi  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,4,0,infoPane,_endPsi_form);
		//add component
		component[3] = _endPsi_form ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,4,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,this,label);				

		//add component
		component[4] = label ;
		
		transfoPsiBegin_form = new SliderAngleTransfo(JSlider.HORIZONTAL,-180,this.psi,this.psi,_beginPsi_form,this.ID_SLIDER_2);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,5,this,transfoPsiBegin_form);		
	
		//add component
		component[5] = transfoPsiBegin_form ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,5,this,label);

		//add component
		component[6] = label ;		
		
		transfoPsiEnd_form = new SliderAngleTransfo(JSlider.HORIZONTAL,this.psi,180,this.psi,_endPsi_form,this.ID_SLIDER_2);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,5,this,transfoPsiEnd_form);		
	
		//add component
		component[7] = transfoPsiEnd_form ;
	
		//Set Component List
		_transfoChecking_psi_form.setComponentTab(component);
		_transfoChecking_psi_form.setSelected(false);
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method
	
	public void fireStatChanged(int ID, int newValue){
		int[] dim = new int[2];
		int secondID = 0;
		if ( ID == ID_SLIDER_0 ){
			secondID = 0;
			dim[0] = Integer.parseInt(_beginPhi_form.getText());
			dim[1] = Integer.parseInt(_endPhi_form.getText());
		}
		else if ( ID == ID_SLIDER_1 ){
			secondID = 1;
			dim[0] = Integer.parseInt(_beginTheta_form.getText());
			dim[1] = Integer.parseInt(_endTheta_form.getText());
		}
		else if ( ID == ID_SLIDER_2){
			secondID = 2;
			dim[0] = Integer.parseInt(_beginPsi_form.getText());
			dim[1] = Integer.parseInt(_endPsi_form.getText());
		}
		else {
			System.out.println("WARNING ::: ID ("+ID+") is not recognize component.BasicInterfaceComponent_rotPanel.fireStatChanged");
		}
		
		
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireUpdateSliderRange(dim,ID);
			}
			else if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				((BasicInterfaceComponent_Second_FormPanel)component).fireUpdateSliderRange(dim,ID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				((BasicInterfaceComponent_RandPanel)component).fireStatChanged(dim,ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_rotPanel.fireStatChanged");
			}
		}
	}

	public void fireInfoSelected(int ID,int secondID){
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireInfoSelected(ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				((BasicInterfaceComponent_Second_FormPanel)component).fireInfoSelected(ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				boolean bol = getIsSelected()[secondID];
				((BasicInterfaceComponent_RandPanel)component).fireInfoSelected(ID,secondID,bol);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_rotPanel.fireInfoSelected");
			}
		}
	}
	
	public boolean[] getIsSelected(){
		boolean[] isSelected = new boolean[3];
		isSelected[0] = _transfoChecking_phi_form.getIsSelected();
		isSelected[1] = _transfoChecking_theta_form.getIsSelected();
		isSelected[2] = _transfoChecking_psi_form.getIsSelected();
		return isSelected;
	}

	//int{{begin,end},{...}}
	public int[][] getAngle(){
		int[][] angle = new int[3][2];
		angle[0][0] = transfoPhiBegin_form.getValue();
		angle[0][1] = transfoPhiEnd_form.getValue();
		
		angle[1][0] = transfoThetaBegin_form.getValue();
		angle[1][1] = transfoThetaEnd_form.getValue();
		
		angle[2][0] = transfoPsiBegin_form.getValue();
		angle[2][1] = transfoPsiEnd_form.getValue();
		
		return angle;
	}
	
	
	public void setSelected(boolean[] bol){
		_transfoChecking_phi_form.setSelected(bol[0]);
		_transfoChecking_theta_form.setSelected(bol[1]);
		_transfoChecking_psi_form.setSelected(bol[2]);
	}
	
	public void setValueSlider(int ID,int[] value){
		if ( ID == 0 ){
			_beginPhi_form.setText(value[0]+"");
			_endPhi_form.setText(value[1]+"");
		
			transfoPhiBegin_form.setValue_(value[0]);
			transfoPhiEnd_form.setValue_(value[1]);
		}
		else if ( ID == 1 ){
			_beginTheta_form.setText(value[0]+"");
			_endTheta_form.setText(value[1]+"");
			
			transfoThetaBegin_form.setValue_(value[0]);
			transfoThetaEnd_form.setValue_(value[1]);
		}
		else if ( ID == 2 ){
			_beginPsi_form.setText(value[0]+"");	
			_endPsi_form.setText(value[1]+"");	

			transfoPsiBegin_form.setValue_(value[0]);	
			transfoPsiEnd_form.setValue_(value[1]);	
		}
	}
	
	public void setNewLimits(int[] lim){
		transfoPhiBegin_form.setMaximum(lim[0]);
		transfoThetaBegin_form.setMaximum(lim[1]);
		transfoPsiBegin_form.setMaximum(lim[2]);
		
		transfoPhiEnd_form.setMinimum(lim[0]);
		transfoThetaEnd_form.setMinimum(lim[1]);
		transfoPsiEnd_form.setMinimum(lim[2]);
	}
	

	public void addListener(JComponent component){
		this.listener.add(component);
	}

	
	/**
	* @deprecate
	**/
	public void addListener(JFrame frame){
		System.out.println("DEPRECATE ::: BasicInterfaceComponent_rotPanel.addListener(JFrame");
	}
}
