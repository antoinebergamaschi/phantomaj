/**
* InterfaceComponent_GraphicsIntensity.java
* 09/07/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import com.phantomaj.utils.Compute_Intensity;
import com.phantomaj.utils.Compute_Intensity;

/**
* Interface class used to draw and represent the Intensity function selected by users.
**/
public class InterfaceComponent_GraphicsIntensity extends JPanel{

	private final int cosinus_f = 0;
	private final int logarithm_f = 1;
	private final int exponential_f = 2;
	private final int linear_f = 3;
	private final int binary_f = 4;
	
	private Compute_Intensity Cint = new Compute_Intensity();
	
	private boolean isDouble = false;
	private int cstValue = 0;
	private boolean isCstSelected = false;
	private float retard=0.0f;
	private int functionID=0;
	private final static float dash1[] = {10.0f};
	private final static BasicStroke dashed = new BasicStroke(1.0f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f, dash1, 0.0f);
	private boolean	inverted = false;
	
	public InterfaceComponent_GraphicsIntensity(){
		super();
		this.setPreferredSize(new Dimension(200,200));
		this.setMinimumSize(new Dimension(200,200));
		this.setSize(new Dimension(100,100));
		this.setLayout(null);
	}

	private void build(Graphics2D g){
		if ( g != null ){
			g.setColor(Color.blue);
			int[] x = {10,0,20,10,10,200,190,190,200,10};
			int[] y = {0,10,10,0,190,190,200,180,190,190};
			
			g.drawPolygon(x,y,x.length);
			g.fillPolygon(x,y,x.length);
			g.drawString("1",20,10);
			g.drawString("1",190,180);
			g.drawString("Relative distance",50,200);
			
			x = new int[180];
			y = new int[180];
			g.setColor(Color.red);
			
			
			
			if ( this.isDouble ){
				double cstValue_ = (double)this.cstValue/2;
				this.cstValue=0;
				computeTable(x,y,(int)(90 - cstValue_));
				if ( isCstSelected ){					
					for (int i = (int)(90 - cstValue_) ; i < 90 + cstValue_ ; i++ ){
						y[i] = (this.inverted) ? 10 : 190;
						x[i] = 10+i;
					}
				}
				if ( this.functionID == this.binary_f ){
					this.inverted = !this.inverted;
				}
				this.inverted = !this.inverted;
				this.cstValue = (int)(90  + cstValue_);
				computeTable(x,y,180);
				this.inverted = !this.inverted;
				this.cstValue = (int)(cstValue_*2);
				if(this.functionID==this.binary_f) this.inverted = !this.inverted;
			}
			
			else {
				if ( isCstSelected ){
					if ( this.functionID != this.binary_f ){
						for (int i = 0 ; i < this.cstValue ; i++ ){
							y[i] = this.inverted ? 190 : 10;
							x[i] = 10+i;
						}
						computeTable(x,y,180);
					}
					else {
						for (int i = 179  ; i >=180-this.cstValue ; i-- ){
							y[i] = this.inverted ? 10 : 190;
							x[i] = 10+i;
						}
						computeTable(x,y,180-this.cstValue );
					}
				}
				else{
					computeTable(x,y,180-this.cstValue );
				}
			}
			g.drawPolyline(x,y,x.length);
			g.setColor(Color.blue);
			g.translate(100,100); 
			g.rotate(-Math.PI/2);
			g.drawString("Relative Intensity",-50,-91);
		}
	}

	
	private void computeTable(int[] x, int[] y,int lim){
		double limite = (double)lim;
		switch(this.functionID){
				
			case cosinus_f:
				if ( this.inverted ){
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d*Math.cos((Math.PI/2)*(tmp-1)));
						x[i] = 10 + i;
					}
				}
				else {
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d*Math.cos((Math.PI/2)*tmp));
						x[i] = 10 + i;
					}
				}
				break;
			case logarithm_f:
				if ( this.inverted ){
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);

						y[i] = 10 + 180 - (int)(180.d*(1-Math.exp(6*(-tmp))));
						x[i] = 10 + i;
					}
				}
				else {
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d*(1-Math.exp(-6*(1-tmp))));
						
						x[i] = 10 + i;
					}
				}
				// g.drawPolyline(x,y,x.length);
				break;
			case exponential_f:
				if ( this.inverted ){
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);

						y[i] = 10 + 180 - (int)(180.0d * (Math.exp(6.0d*(tmp-1))));
						x[i] = 10 + i;
					}
				}
				else {
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d * Math.exp(-6.0d*tmp));
						x[i] = 10 + i;
					}
				}
				break;
			case linear_f:
				if ( this.inverted ){
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d * tmp);
						x[i] = 10 + i;
					}
				}
				else {
					for ( int i = this.cstValue ; i < lim ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = 10 + 180 - (int)(180.0d * (1-tmp));
						x[i] = 10 + i;
					}
				}
				break;
			case binary_f:
					for ( int i = this.cstValue ; i < lim  ; i++){
						double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
						y[i] = this.inverted ? 190 : 10 ;
						x[i] = 10 + i;
					}
					if ( !isDouble ){
						for ( int i = 0 ; i < lim  ; i++){
							double tmp = ((double)(i-this.cstValue))/(double)(lim-this.cstValue);
							y[i] = this.inverted ? 190 : 10 ;
							x[i] = 10 + i;
						}
					}
				break;
		}	
	}
	
	
	public void changeFunction(int idFunction){
		this.functionID = idFunction;
		Cint.changeFunction(idFunction);
		this.paintComponent(this.getGraphics());
	}
	
	public void setRadiusCst(boolean bol){
		this.isCstSelected = bol;
		Cint.setRadiusCst(bol);
		if ( !bol ){
			this.cstValue = 0;
		}
		this.paintComponent(this.getGraphics());
	}
	
	public void updateCstValue(int cst){
		this.cstValue = (int)(179.0f*(((float)cst)/100.0f));
		Cint.updateCstValue(((float)cst)/100.0f);
		this.paintComponent(this.getGraphics());
	}
	
	public void changeInverted(){
		if(this.inverted){
			Cint.changeInverted(false);
			this.inverted = false;
		}
		else {
			Cint.changeInverted(true);
			this.inverted = true;
		}
		this.paintComponent(this.getGraphics());
	}
	
	public void changeDouble(){
		if(this.isDouble){
			Cint.changeDouble(false);
			this.isDouble = false;
		}
		else {
			Cint.changeDouble(true);
			this.isDouble = true;
		}
		this.paintComponent(this.getGraphics());
	}
	
	public Compute_Intensity getCompute_Intensity(){
		return this.Cint;
	}
	
	
	/**
	*@Override 
	**/
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		build((Graphics2D)g);
    }

}