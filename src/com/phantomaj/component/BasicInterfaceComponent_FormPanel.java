/**
* BasicInterfaceComponent_FormPanel.java
* 16/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;

import com.phantomaj.button.ButtonRotationCenter;
import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.select.CenterRotationCheckBox;
import com.phantomaj.select.SliderAngle;
import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.select.formCheckBox;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.button.ButtonRotationCenter;
import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.select.SliderAngle;
import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.utils.StaticField_Form;


public class BasicInterfaceComponent_FormPanel extends JPanel{


	private String[] _importedForm;

	private SliderAngle[] sliderAngle = new SliderAngle[3];
	private SliderDim_NEW[] sliderOri = new SliderDim_NEW[3];
	
	private BasicInterface basicInterface;
	private JLabel nameForm = new JLabel("<html><h1>Form : sphere 1</h1></html>");
	private JLabel nameFormObj = new JLabel("<html><h3>object 1</h3></html>");

	ButtonRotationCenter _PanelChooseCenter;
	
	private int[] basicDimension;
	private ButtonGroup B_grp;
	private JPanel _formDim = new JPanel(new GridBagLayout());
	private int formID = -1;
	
//********************************************************************
//Constructor
	public BasicInterfaceComponent_FormPanel(BasicInterface bi,int ID){
		super(new GridBagLayout());
		this.formID = ID;
		this.basicInterface = bi;
		boolean bol = false;
		int l = 0;
		if ( StaticField_Form.CODE_ADDITIONAL_FORM[ID] != null ){
			l = StaticField_Form.CODE_ADDITIONAL_FORM[ID].length;
		}
		this.basicDimension = new int[l + 3];
		for ( int i = 0 ; i < this.basicDimension.length ; i++ ){
			if ( i < 3 ){
				this.basicDimension[i]  = StaticField_Form.BASICDIMENSION[ID][i];
			}
			else {
				this.basicDimension[i] = 0;
			}
		}
		
		for ( int z = 0 ; z < StaticField_Form.FORM_NAME.length ; z++ ){
			if ( ID == StaticField_Form.getIDfromName(StaticField_Form.FORM_NAME[z]) ){
				bol = true;
			}
		}

		
		if ( bol ){
			this.build();
			
			for ( int k=0; k < 3 ; k++ ){
				sliderAngle[k].addListener(this);
				sliderOri[k].addListener(this);
			}
			
		}
		else{
			System.out.println("WARNING ::: init BasicInterfaceComponent_FormPanel, UNRECOGNIZE ID ("+ID+")");
		}
		
	}


	
//*********************************************************************
//Public Methode
	
	/**
	* Update the Dimension Value of the Slider ID with the newVal
	**/
	public void setUpdateSlider(int newVal, int ID){
		if ( ID < 3 && ID >= 0){
			switch(StaticField_Form.CODE_3D_FORM[this.formID]){
				case 0:
					this.basicInterface.FireUpdateSliderDim(newVal,ID);
					break;
				case 1:
					//If the form lack one dimension, changing the first slider fire event for the two first dimension
					if ( ID == 0 ){
						this.basicInterface.FireUpdateSliderDim(newVal,ID);
						this.basicInterface.FireUpdateSliderDim(newVal,ID+1);
					}
					//the second fire event for the third dimension
					else{
						this.basicInterface.FireUpdateSliderDim(newVal,ID+1);
					}
					break;
				case 2:
					//If the form lack 2 dimension the only slider fire event for the three dimension
					this.basicInterface.FireUpdateSliderDim(newVal,ID);
					this.basicInterface.FireUpdateSliderDim(newVal,ID+1);
					this.basicInterface.FireUpdateSliderDim(newVal,ID+2);
					break;
				default:
					System.out.println("WARNING ::: Error CODE_3D_FORM ("+StaticField_Form.CODE_3D_FORM[this.formID]+") UNSUPPORTED");
					break;
			}
		}
		else if ( ID >= 0){
			this.basicInterface.FireUpdateSliderDim(newVal,ID);
		}
		else{
			this.basicInterface.FireUpdateSliderPos(newVal,(ID*-1)-1);
		}
	}

	public void setUpdateFormRot(int newVal, int ID){
		this.basicInterface.FireUpdateSliderRot(newVal,ID);
	}
	
	public void setCenterRotationChangeVisible(boolean bol, int id){
		_PanelChooseCenter.setVisible(bol);
		_PanelChooseCenter.getParent().validate();
		_PanelChooseCenter.setForm(whichCheckBox());
	}
	
	/**
	* Search for the JCheckBox selected and return an Integer corresponding to this JCheckBox;
	* @return int, ID of the JCheckBox selected
	**/
	public int whichCheckBox(){
		Enumeration<AbstractButton> listButton = B_grp.getElements();
		while ( listButton.hasMoreElements()){
			AbstractButton ce_boutton =  listButton.nextElement();
			if ( ce_boutton.isSelected()){
 
				for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i ++ ){
					if ( ce_boutton.getText() == StaticField_Form.FORM_NAME[i] ){
						this.formID = i;
						return i;
					}
				}
			}
		}
		return -1;
	}
	
	/**
	* Update the _formDim panel with the currently selected form
	**/
	public void FireFormChoosen(){
		
		this._formDim.removeAll();
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_formDim,this.getFormDim(whichCheckBox()));
		this.validate();
		// _formDim.validate();
		this.repaint();
		
		this.basicInterface.FireFormChoosen(whichCheckBox());
	}
	
	/**
	* Set the corresponding Node information in the Interface
	**/
	public void setFormInfo(String nameForm, String nameObj, int[] angle, int[] dim, int ID, int[] ori){
		this.nameForm.setText("<html><h1>Form : "+nameForm+"</h1></html>");
		this.nameFormObj.setText("<html><h3>"+nameObj+"</h3></html>");
		if ( ID != this.formID ){
			this.formID = ID;
			Enumeration<AbstractButton> listButton = B_grp.getElements();
			int t = 0;
			while ( listButton.hasMoreElements()){
				AbstractButton ce_boutton =  listButton.nextElement();
				if ( t == this.formID ){
					ce_boutton.setSelected(true);
				}
				else{
					ce_boutton.setSelected(false);
				}
				t++;	
			}
		}
		this._formDim.removeAll();
		BasicInterfaceComponent_dimPanel pane = (BasicInterfaceComponent_dimPanel)getFormDim(ID);
		pane.setInfo(ID,dim);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_formDim,pane);
		
		this.validate();
		this.repaint();
		
		sliderAngle[0].setValue(angle[0]);
		sliderAngle[1].setValue(angle[1]);
		sliderAngle[2].setValue(angle[2]);
		
		
		sliderOri[0].setValue(ori[0]);
		sliderOri[1].setValue(ori[1]);
		sliderOri[2].setValue(ori[2]);
	}
	
//**********************************************************************
//Private Methode

	private void build(){
		JLabel label;
		JPanel _formChoise = this.getFormChoice();
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_formDim,this.getFormDim(this.formID));
		
		JPanel _formRot = this.getFormRot();
		JPanel _formPos = this.getFormPos();
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,this.nameForm);
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,1,this,this.nameFormObj);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,2,10,2,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,_formChoise);
		
		TitledBorder title = BorderFactory.createTitledBorder("Form Selection");
		_formChoise.setBorder(title);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,2,10,2,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,this,_formDim);		
		title = BorderFactory.createTitledBorder("Form Dimension Option");
		_formDim.setBorder(title);
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,2,10,2,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,this,_formPos);	

		title = BorderFactory.createTitledBorder("Form Position Option");
		_formPos.setBorder(title);	
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,2,10,2,GridBagConstraints.BOTH,1,0,0,0,1,1,0,5,this,_formRot);		

		title = BorderFactory.createTitledBorder("Form Rotation Option");
		_formRot.setBorder(title);	
		
		label = new JLabel("");
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,6,this,label);
		
	}

	/**
	* @return JPanel, the dimension panel of this form ID
	**/
	private JPanel getFormDim(int ID){
		BasicInterfaceComponent_dimPanel pane = new BasicInterfaceComponent_dimPanel(ID);
		pane.addListener(this);
		return pane;
	}
	
	/**
	* @return JPanel, the form choice panel
	**/
	private	JPanel getFormChoice(){
		JPanel panel = new JPanel(new GridBagLayout());
		formCheckBox form_check;
		B_grp = new ButtonGroup();
		
		int z = 0;
		int k = 0;
		
		for ( int i = 0 ; i < StaticField_Form.FORM_NAME.length ; i++ ){
			form_check = new formCheckBox(StaticField_Form.FORM_NAME[i],this);
			if ( k < 8 ){
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,panel,form_check);
				k++;
			}
			else {
				k = 0;
				z++;
				//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
				addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,k,z,panel,form_check);
			}
			if ( i == this.formID ){
				form_check.setSelected(true);
			}
			else{
				form_check.setSelected(false);
			}
			B_grp.add(form_check);
		}

		return panel;
	}

	/**
	* @return JPanel, the form rotation panel
	**/
	private JPanel getFormRot(){
		JPanel panel = new JPanel(new GridBagLayout());
		JComponent[] component = new JComponent[10];
		ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
		JLabel label;
		
	//PHI
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");

		
		JLabel angle_name = new JLabel("<html><ul><li> Phi :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,angle_name);

		component[0] = angle_name ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,panel,bouttonUpDown); 
		sliderAngle[0] = new SliderAngle(JSlider.HORIZONTAL,-180,180,0,label,this,0);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,panel,sliderAngle[0]);
		component[1] = sliderAngle[0];
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,1,panel,label);
		
		component[2] = label ;


	//THETA
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
	
		angle_name = new JLabel("<html><ul><li> Theta :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,panel,angle_name);

		component[3] = angle_name ;
		
		sliderAngle[1] = new SliderAngle(JSlider.HORIZONTAL,-180,180,0,label,this,1);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,panel,sliderAngle[1]);
		component[4] = sliderAngle[1] ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,3,panel,label);
		
		component[5] = label ;

	//PSI
	  	label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
	
		angle_name = new JLabel("<html><ul><li> Psi :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,4,panel,angle_name);

		component[6] = angle_name ;
		
	  	sliderAngle[2] = new SliderAngle(JSlider.HORIZONTAL,-180,180,0,label,this,2);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,10,0,GridBagConstraints.BOTH,1,0,0,20,1,1,0,5,panel,sliderAngle[2]);
		
		component[7] = sliderAngle[2] ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,5,panel,label);
	
		component[8] = label ;

		JPanel _panelCenterRotGlobal = new JPanel(new GridBagLayout());
		
		CenterRotationCheckBox centerCheck = new CenterRotationCheckBox("Enter new Rotation Center",this,1);
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,0,_panelCenterRotGlobal,centerCheck);

		
		_PanelChooseCenter = new ButtonRotationCenter(this);
		_PanelChooseCenter.setVisible(false);
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,_panelCenterRotGlobal,_PanelChooseCenter);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,10,10,GridBagConstraints.BOTH,0,0,0,0,2,1,0,6,panel,_panelCenterRotGlobal);
		
		component[9] = _panelCenterRotGlobal ;
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(false);
		return panel;
	}
	
	/**
	* @return JPanel, the Form position panel
	**/
	private JPanel getFormPos(){
		JPanel panel = new JPanel(new GridBagLayout());
		JLabel label;
		

		JComponent[] component = new JComponent[9];
		ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
		
	//X
	
		label = new JLabel("<html><ul><li> X Origin :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,panel,label);	
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,panel,bouttonUpDown); 
		
		label = new JLabel("100");
		label.setPreferredSize(label.getPreferredSize());
		
		sliderOri[0] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[0],100,label,-1);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,panel,sliderOri[0]);	
		component[1] = sliderOri[0] ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,1,panel,label);	
		
		component[2] = label ;
	//Y
	
		label = new JLabel("<html><ul><li> Y Origin :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,panel,label);	

		component[3] = label ;
		
		label = new JLabel("100");
		label.setPreferredSize(label.getPreferredSize());
		
		sliderOri[1] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[1],100,label,-2);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,panel,sliderOri[1]);	
		
		component[4] = sliderOri[1] ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,3,panel,label);	
	
		component[5] = label ;
	
	//Z

	
		label = new JLabel("<html><ul><li> Z Origin :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,4,panel,label);	

		component[6] = label ;
		
		label = new JLabel("100");
		label.setPreferredSize(label.getPreferredSize());
		
		sliderOri[2] = new SliderDim_NEW(JSlider.HORIZONTAL,0,BasicInterface.getStackDimension()[2],100,label,-3);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,5,panel,sliderOri[2]);	
		
		component[7] = sliderOri[2] ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,5,panel,label);

		component[8] = label ;
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(false);
		
		return panel;
	}
	
	public void setSelectedCenter(int id){
		basicInterface.setSelectedCenter(id);
	}
	
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
}


