/**
* BasicInterfaceComponent_posPanel.java
* 24/10/2012
* @author Antoine Bergamaschi
**/


package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.phantomaj.select.SliderDim_NEW;
import com.phantomaj.select.TransfoCheckBox_NEW;
import com.phantomaj.select.SliderDim_NEW;

public class BasicInterfaceComponent_posPanel extends JPanel{

	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	
	private JLabel _beginX;
	private JLabel _endX;
	private JLabel _beginY;
	private JLabel _endY;
	private JLabel _beginZ;
	private JLabel _endZ;
	
	private TransfoCheckBox_NEW _transfoChecking_X;
	private TransfoCheckBox_NEW _transfoChecking_Y;
	private TransfoCheckBox_NEW _transfoChecking_Z;
	
	private SliderDim_NEW transfoXBegin ;
	private SliderDim_NEW transfoYBegin ;
	private SliderDim_NEW transfoZBegin ;
	private SliderDim_NEW transfoXEnd ;
	private SliderDim_NEW transfoYEnd ;
	private SliderDim_NEW transfoZEnd ;
	
	private int ID_SLIDER_0;
	private int ID_SLIDER_1;
	private int ID_SLIDER_2;
	
//##################################################################
// Constructor

	public BasicInterfaceComponent_posPanel(int ID_0, int ID_1, int ID_2){
		super(new GridBagLayout());
		this.ID_SLIDER_0 = ID_0;
		this.ID_SLIDER_1 = ID_1;
		this.ID_SLIDER_2 = ID_2;
		
		this.build();
		
		_transfoChecking_X.addListener(this);
		_transfoChecking_Y.addListener(this);
		_transfoChecking_Z.addListener(this);
		
		transfoXBegin.addListener(this);
		transfoXEnd.addListener(this);
		
		transfoYBegin.addListener(this);
		transfoYEnd.addListener(this);
		
		transfoZBegin.addListener(this);
		transfoZEnd.addListener(this);
	}

	
	private void build(){
		//initialize component
		JComponent[] component = new JComponent[8];	
		JLabel label = new JLabel("");
		
		//Creat infoPane###################
		JPanel infoPane = new JPanel(new GridBagLayout());
		
		// Xpos
		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_beginX = new JLabel("0000");
		_beginX.setPreferredSize(_beginX.getPreferredSize());
		_beginX.setText((BasicInterface.getStackDimension()[0]/2)+"");
		
		_endX = new JLabel("0000");
		_endX.setPreferredSize(_endX.getPreferredSize());
		_endX.setText((BasicInterface.getStackDimension()[0]/2)+"");
		
		_transfoChecking_X = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_0);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_X);

	
		label = new JLabel("X position : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginX);
		//add component
		component[1] = _beginX ;		
		
		
		label = new JLabel(" <  X  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_endX);
		//add component
		component[3] = _endX ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);				

		//add component
		component[4] = label ;
		
		transfoXBegin = new SliderDim_NEW(JSlider.HORIZONTAL,0,(BasicInterface.getStackDimension()[0]/2),(BasicInterface.getStackDimension()[0]/2),_beginX,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,this,transfoXBegin);		
	
		//add component
		component[5] = transfoXBegin ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,this,label);

		//add component
		component[6] = label ;		
		
		transfoXEnd = new SliderDim_NEW(JSlider.HORIZONTAL,(BasicInterface.getStackDimension()[0]/2),BasicInterface.getStackDimension()[0],(BasicInterface.getStackDimension()[0]/2),_endX,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,this,transfoXEnd);		
	
		//add component
		component[7] = transfoXEnd ;
	
		//Set Component List
		_transfoChecking_X.setComponentTab(component);
		_transfoChecking_X.setSelected(false);
		
		
// Ypos

		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_beginY = new JLabel("000");
		_beginY.setPreferredSize(_beginY.getPreferredSize());
		_beginY.setText((BasicInterface.getStackDimension()[1]/2)+"");
		
		_endY = new JLabel("000");
		_endY.setPreferredSize(_endY.getPreferredSize());
		_endY.setText((BasicInterface.getStackDimension()[1]/2)+"");
		
		_transfoChecking_Y = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_1);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_Y);

	
		label = new JLabel("Y position : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginY);
		//add component
		component[1] = _beginY ;		
		
		
		label = new JLabel(" <  Y  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_endY);
		//add component
		component[3] = _endY ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,2,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,this,label);				

		//add component
		component[4] = label ;
		
		transfoYBegin = new SliderDim_NEW(JSlider.HORIZONTAL,0,(BasicInterface.getStackDimension()[1]/2),(BasicInterface.getStackDimension()[1]/2),_beginY,this.ID_SLIDER_1);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,3,this,transfoYBegin);		
	
		//add component
		component[5] = transfoYBegin ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,15,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,3,this,label);

		//add component
		component[6] = label ;		
		
		transfoYEnd = new SliderDim_NEW(JSlider.HORIZONTAL,(BasicInterface.getStackDimension()[1]/2),BasicInterface.getStackDimension()[1],(BasicInterface.getStackDimension()[1]/2),_endY,this.ID_SLIDER_1);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,3,this,transfoYEnd);		
	
		//add component
		component[7] = transfoYEnd ;
	
		//Set Component List
		_transfoChecking_Y.setComponentTab(component);
		_transfoChecking_Y.setSelected(false);
		
// Zpos

		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_beginZ = new JLabel("000");
		_beginZ.setPreferredSize(_beginZ.getPreferredSize());
		_beginZ.setText((BasicInterface.getStackDimension()[2]/2)+"");
		
		_endZ = new JLabel("000");
		_endZ.setPreferredSize(_endZ.getPreferredSize());
		_endZ.setText((BasicInterface.getStackDimension()[2]/2)+"");
		
		_transfoChecking_Z = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_2);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_Z);

	
		label = new JLabel("Z position : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_beginZ);
		//add component
		component[1] = _beginZ ;		
		
		
		label = new JLabel(" <  Z  < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,label);		
		//add component
		component[2] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_endZ);
		//add component
		component[3] = _endZ ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,4,this,infoPane);
		
		label = new JLabel("Min : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,this,label);				

		//add component
		component[4] = label ;
		
		transfoZBegin = new SliderDim_NEW(JSlider.HORIZONTAL,0,(BasicInterface.getStackDimension()[2]/2),(BasicInterface.getStackDimension()[2]/2),_beginZ,this.ID_SLIDER_2);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,5,this,transfoZBegin);		
	
		//add component
		component[5] = transfoZBegin ;
	
		label = new JLabel("Max : ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,5,this,label);

		//add component
		component[6] = label ;		
		
		transfoZEnd = new SliderDim_NEW(JSlider.HORIZONTAL,(BasicInterface.getStackDimension()[2]/2),BasicInterface.getStackDimension()[2],(BasicInterface.getStackDimension()[2]/2),_endZ,this.ID_SLIDER_2);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,5,this,transfoZEnd);		
	
		//add component
		component[7] = transfoZEnd ;
	
		//Set Component List
		_transfoChecking_Z.setComponentTab(component);
		_transfoChecking_Z.setSelected(false);
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method
	
	public void fireStatChanged(int ID){
		int[] dim = new int[2];
		if ( ID == ID_SLIDER_0 ){
			dim[0] = transfoXBegin.getValue();
			dim[1] = transfoXEnd.getValue();
		}
		else if ( ID == ID_SLIDER_1 ){
			dim[0] = transfoYBegin.getValue();
			dim[1] = transfoYEnd.getValue();
		}
		else if ( ID == ID_SLIDER_2){
			dim[0] = transfoZBegin.getValue();
			dim[1] = transfoZEnd.getValue();
		}
		else{
			System.out.println("WARNING ::: ID ("+ID+") is not recognize component.BasicInterfaceComponent_posPanel.fireStatChanged");
		}
	
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireUpdateSliderRange(dim,ID);
			}
			else if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				((BasicInterfaceComponent_Second_FormPanel)component).fireUpdateSliderRange(dim,ID);
			}
			else {
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_posPanel.fireStatChanged");
			}
		}
	}

	public void fireInfoSelected(int ID,int secondID){
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireInfoSelected(ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_Second_FormPanel ){
				((BasicInterfaceComponent_Second_FormPanel)component).fireInfoSelected(ID,secondID);
			}
			else {
				System.out.println("WARNING ::: instaceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_posPanel.fireInfoSelected");
			}
		}
	}
	
	public boolean[] getIsSelected(){
		boolean[] isSelected = new boolean[3];
		isSelected[0] = _transfoChecking_X.getIsSelected();
		isSelected[1] = _transfoChecking_Y.getIsSelected();
		isSelected[2] = _transfoChecking_Z.getIsSelected();
		return isSelected;
	}

	//int{{begin,end},{...}}
	public int[][] getPosition(){
		int[][] angle = new int[3][2];
		angle[0][0] = transfoXBegin.getValue();
		angle[0][1] = transfoXEnd.getValue();
		
		angle[1][0] = transfoYBegin.getValue();
		angle[1][1] = transfoYEnd.getValue();
		
		angle[2][0] = transfoZBegin.getValue();
		angle[2][1] = transfoZEnd.getValue();
		
		return angle;
	}
	
	
	public void setSelected(boolean[] bol){
		_transfoChecking_X.setSelected(bol[0]);
		_transfoChecking_Y.setSelected(bol[1]);
		_transfoChecking_Z.setSelected(bol[2]);
	}
	
	public void setValueSlider(int ID,int[] value){
		if ( ID == 0 ){
			_beginX.setText(value[0]+"");
			_endX.setText(value[1]+"");
		
			transfoXBegin.setValue_(value[0]);
			transfoXEnd.setValue_(value[1]);
		}
		else if ( ID == 1 ){
			_beginY.setText(value[0]+"");
			_endY.setText(value[1]+"");
			
			transfoYBegin.setValue(value[0]);
			transfoYEnd.setValue(value[1]);
		}
		else if ( ID == 2 ){
			_beginZ.setText(value[0]+"");	
			_endZ.setText(value[1]+"");	

			transfoZBegin.setValue_(value[0]);	
			transfoZEnd.setValue_(value[1]);	
		}
	}
	
	public void setNewLimits(int[] lim){
		transfoXBegin.setMaximum(lim[0]);
		transfoYBegin.setMaximum(lim[1]);
		transfoZBegin.setMaximum(lim[2]);
		
		transfoXEnd.setMinimum(lim[0]);
		transfoYEnd.setMinimum(lim[1]);
		transfoZEnd.setMinimum(lim[2]);
	}
	

	public void addListener(JComponent component){
		this.listener.add(component);
	}
	
}
	