/**
* BasicInterfaceComponent_Second_rootPanel.java
* 19/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.phantomaj.select.TransfoCheckBox_NEW;

public class BasicInterfaceComponent_Second_rootPanel extends JPanel{


	private BasicInterface basicInterface;

//*************************************************************************************************************************************
//Constructor

	public BasicInterfaceComponent_Second_rootPanel(BasicInterface basicInterface){
		super();
		this.basicInterface = basicInterface;
		this.setLayout(new GridBagLayout());
		this.build();
	}

//************************************************************************************************************************************
//public methode

	public void fireInfoSelected(int ID,int secondID){
		// System.out.println("FireInfoSelected");
		this.basicInterface.FireInfoSelected(ID,secondID);
	}
	
/******************************************************************************************************************************************/
//Private Building methode	
	private void build(){	
	
	
		JPanel _noiseValTransfo_phan = this.getPanelNoiseModif();
		JPanel _backgroundValTransfo_phan = this.getPanelBackgroundModif();
		JPanel _Transfo_phan_n = new JPanel(new GridBagLayout());
		JLabel label;
		TitledBorder title;
		// ButtonUpnDown bouttonUpDown = new ButtonUpnDown("",null);
		// component = new JComponent[3];
		
		label = new JLabel("<html><h3>Background Value</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_phan_n,label);
		
		// component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		// addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_phan_n,bouttonUpDown); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		// addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_phan_n,_noiseValTransfo_phan);
		// component[1] = _noiseValTransfo_phan;
		
		// title = BorderFactory.createTitledBorder("Modify Noise Parameters");
		// _noiseValTransfo_phan.setBorder(title);	
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_phan_n,_backgroundValTransfo_phan);
		// component[2] = _backgroundValTransfo_phan;
		
		title = BorderFactory.createTitledBorder("Background value Parameters");
		_backgroundValTransfo_phan.setBorder(title);	
		
		
		// bouttonUpDown.setComponent(component);
		// bouttonUpDown.setSelected(true);
	
	
		label = new JLabel("<html><h1> Phantom </h1></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,1,0,5,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,label);	

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,_Transfo_phan_n);	
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,this,label);		
	}
	
	private JPanel getPanelNoiseModif(){
		JComponent[] component = new JComponent[4];
	
		JPanel _noiseValTransfo_phan = new JPanel(new GridBagLayout());

		JLabel label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_noiseValTransfo_phan,label);
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_noiseValTransfo_phan,label);
		
		
		// TransfoCheckBox_NEW _transfoChecking_noise = new TransfoCheckBox_NEW("",null,TransfoCheckBox_NEW.NOISE_ROOT);
		// //anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		// addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,1,_noiseValTransfo_phan,_transfoChecking_noise);

		
		label = new JLabel("Minimal noise:");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,2,1,_noiseValTransfo_phan,label);
		//Add in component List
		component[2] = label;
		
		
		JTextField _noiseBegin = new JTextField("0",3);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,3,1,_noiseValTransfo_phan,_noiseBegin);
		//Add in Component List
		component[0] = _noiseBegin ;
		
		label = new JLabel("     ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,4,1,_noiseValTransfo_phan,label);
		
		label = new JLabel("Maximum noise:");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,5,1,_noiseValTransfo_phan,label);
		//Add in Component List
		component[3] = label;
		
		JTextField _noiseEnd = new JTextField("0",3);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,6,1,_noiseValTransfo_phan,_noiseEnd);
		//Add in component list
		component[1] = _noiseEnd ;
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,7,1,_noiseValTransfo_phan,label);
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,_noiseValTransfo_phan,label);
		
		//Set Component List
		// _transfoChecking_noise.setComponentTab(component);
		// _transfoChecking_noise.setSelected(false);
		
		return _noiseValTransfo_phan;
	}
	
	private JPanel getPanelBackgroundModif(){
		//initialize component
		JComponent[] component = new JComponent[4];

		JPanel _backgroundValTransfo_phan = new JPanel(new GridBagLayout());
		// GridBagConstraints c_backgroundValTransfo_phan = new GridBagConstraints();
		
		// c_backgroundValTransfo_phan.fill = GridBagConstraints.BOTH;
		JLabel label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_backgroundValTransfo_phan,label);
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_backgroundValTransfo_phan,label);
		
		TransfoCheckBox_NEW _transfoChecking_background = new TransfoCheckBox_NEW("",null,BasicInterface.BACKGROUND_ROOT);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,1,1,_backgroundValTransfo_phan,_transfoChecking_background);

		
		label = new JLabel("Minimum Background Value:");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,2,1,_backgroundValTransfo_phan,label);

		//add component
		component[0] = label;
		
		JTextField _backgroundValBegin = new JTextField("0",3);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,3,1,_backgroundValTransfo_phan,_backgroundValBegin);

		//add component
		component[1] = _backgroundValBegin;
		
		label = new JLabel("     ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,4,1,_backgroundValTransfo_phan,label);

		
		label = new JLabel("Maximum background Value:");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,5,1,_backgroundValTransfo_phan,label);

		//add component
		component[2] = label;
		
		JTextField _backgroundValEnd = new JTextField("100",3);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0.1,1,0,0,1,1,6,1,_backgroundValTransfo_phan,_backgroundValEnd);

		//add component
		component[3] = _backgroundValEnd;
		
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,7,1,_backgroundValTransfo_phan,label);

		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,2,_backgroundValTransfo_phan,label);


		// //Set Component List
		_transfoChecking_background.addListener(this);
		_transfoChecking_background.setComponentTab(component);
		_transfoChecking_background.setSelected(false);
	
		return _backgroundValTransfo_phan;
	}
	
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
}