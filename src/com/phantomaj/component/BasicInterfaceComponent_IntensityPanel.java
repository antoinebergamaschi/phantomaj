/**
* BasicInterfaceComponent_IntensityPanel.java
* 12/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.phantomaj.select.SliderIntensity;
import com.phantomaj.select.TransfoCheckBox_NEW;

public class BasicInterfaceComponent_IntensityPanel extends JPanel{
	
	private JLabel _intensityRangeStart=null;
	private JLabel _intensityRangeEnd=null;
	private JLabel _intensityMean=null;
	
	private TransfoCheckBox_NEW _transfoChecking_intensity;

	private SliderIntensity transfoIntensityMean;
	private SliderIntensity transfoIntensityRange;
	
	private ArrayList<JComponent> listener = new ArrayList<JComponent>();
	private static final String[][] label_text = {{"Intensity :","Mean Intensity"}};

	private static final String[] label_basicText = {" Mean Value : "," Range : "};

	int ID_SLIDER_0 = 0;
//##################################################################
// Constructor
	//type 0,1,2,3,4,5 => Box,Cone,Pyramid,Sphere,Tube,Ellipse
	public BasicInterfaceComponent_IntensityPanel(int iD){
		this.setLayout(new GridBagLayout());
		this.ID_SLIDER_0 = iD;
		this.build();
		_transfoChecking_intensity.addListener(this);
	}


//#################################################################
//Private method


	private void build(){
		
		JComponent[] component = null;
		JPanel infoPane = null;
		JLabel label = new JLabel("") ;
	
		//initialize component
		component = new JComponent[8];	

		//Creat infoPane###################
		infoPane = new JPanel(new GridBagLayout());
		
		_intensityRangeStart = new JLabel("000");
		_intensityRangeStart.setPreferredSize(_intensityRangeStart.getPreferredSize());
		_intensityRangeStart.setText("100");
		
		_intensityRangeEnd = new JLabel("000");
		_intensityRangeEnd.setPreferredSize(_intensityRangeEnd.getPreferredSize());
		_intensityRangeEnd.setText("100");
		
		_transfoChecking_intensity = new TransfoCheckBox_NEW("",null,this.ID_SLIDER_0);		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,infoPane,_transfoChecking_intensity);

	
		label = new JLabel(label_text[0][0]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,1,0,infoPane,label);

		//add component
		component[0] = label ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,2,0,infoPane,_intensityRangeStart);
		//add component
		component[1] = _intensityRangeStart ;		
		
		
		_intensityMean = new JLabel(" < "+label_text[0][1]+" (000) < ");
		_intensityMean.setPreferredSize(_intensityMean.getPreferredSize());
		_intensityMean.setText(" < "+label_text[0][1]+" (0) < ");
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,0,0,0,0,1,1,3,0,infoPane,_intensityMean);		
		//add component
		component[2] = _intensityMean ;
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,0,0,GridBagConstraints.REMAINDER,0,0,0,0,1,1,4,0,infoPane,_intensityRangeEnd);
		//add component
		component[3] = _intensityRangeEnd ;
		
		infoPane.setPreferredSize(infoPane.getPreferredSize());
		//fin creatPane######################
		
		
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.NONE,1,0,0,0,4,1,0,0,this,infoPane);
		
		label = new JLabel(label_basicText[0]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,label);				

		//add component
		component[4] = label ;
		
		transfoIntensityMean = new SliderIntensity(JSlider.HORIZONTAL,0,255,0,null,this,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,1,1,this,transfoIntensityMean);		
	
		//add component
		component[5] = transfoIntensityMean ;
	
		label = new JLabel(label_basicText[1]);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,22,0,GridBagConstraints.BOTH,0,0,0,0,1,1,2,1,this,label);

		//add component
		component[6] = label ;		
		
		transfoIntensityRange = new SliderIntensity(JSlider.HORIZONTAL,0,255,0,null,this,this.ID_SLIDER_0);
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,15,0,10,10,GridBagConstraints.BOTH,1,0,0,0,1,1,3,1,this,transfoIntensityRange);		
	
		//add component
		component[7] = transfoIntensityRange ;
	
		//Set Component List
		_transfoChecking_intensity.setComponentTab(component);
		_transfoChecking_intensity.setSelected(false);
	
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
//#######################################################################
// Public method

	public void setSelected(boolean[] sel){
		_transfoChecking_intensity.setSelected(sel[0]);
	}

	public void setValueSlider(int[] value){
		if ( value.length == 3 ){
			_intensityRangeStart.setText(value[0]+"");
			_intensityRangeEnd.setText(value[1]+"");
			
			transfoIntensityMean.setValue_(value[2]);
			_intensityMean.setText(" < "+transfoIntensityMean.getValue()+" % < ");
			
			transfoIntensityRange.setValue_(Math.max(value[2]-value[0],value[1]-value[2]));
		}
		else {
			System.out.println("Error value.length == "+value.length+" != 3 BasicInterfaceComponent_IntensityPanel.setValueSlider");
		}
	}
	
	public boolean getIsSelected(){
		boolean isSelected = _transfoChecking_intensity.getIsSelected();
		return isSelected;
	}
	
	public int[] getIntensity(){
	//0,1,2,3,4,5 => Box,Cone,Pyramid,Sphere,Tube,Ellipse
		int[] intensity = new int[2];
		intensity[0] = Integer.parseInt(_intensityRangeStart.getText());
		intensity[1] = Integer.parseInt(_intensityRangeEnd.getText());
		return intensity;
	}
	
	public void fireInfoSelected(int ID,int secondID){
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireInfoSelected(ID,secondID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				boolean isSelected = _transfoChecking_intensity.getIsSelected();
				((BasicInterfaceComponent_RandPanel)component).fireInfoSelected(ID,secondID,isSelected);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_IntensityPanel.fireInfoSelected");
			}
		}
	}
	
//ID_Slider
	// 2,3 => meanXdim, rangeX
	// 4,5 => meanYdim, RangeY
	// 6,7 => meanZdim, Range Z
	public void stateChangedSliderIntensity(int ID){
		int[] val = new int[2];
		val = computeVal(transfoIntensityMean.getValue(),transfoIntensityRange.getValue(),255);
		_intensityRangeStart.setText(val[0]+"");
		_intensityRangeEnd.setText(val[1]+"");
		_intensityMean.setText(" < "+label_text[0][1]+"("+transfoIntensityMean.getValue()+") < ");
		
		for ( int i = 0 ; i < listener.size() ; i++ ){
			JComponent component = listener.get(i);
			if ( component instanceof BasicInterfaceComponent_Second_ObjectPanel ){
				int[] dim = {Integer.parseInt(_intensityRangeStart.getText()),Integer.parseInt(_intensityRangeEnd.getText()),transfoIntensityMean.getValue()};
				((BasicInterfaceComponent_Second_ObjectPanel)component).fireUpdateSliderRange(dim,ID);
			}
			else if ( component instanceof BasicInterfaceComponent_RandPanel ){
				int[] dim = {Integer.parseInt(_intensityRangeStart.getText()),Integer.parseInt(_intensityRangeEnd.getText())};
				((BasicInterfaceComponent_RandPanel)component).fireStatChanged(dim,ID,0);
			}
			else {
				System.out.println("WARNING ::: instanceof "+component.getClass() +" is not recognize component.BasicInterfaceComponent_IntensityPanel.stateChangedSliderIntensity");
			}
		}
		
	}
	
	private int[] computeVal(int mean, int range, int dim_universe){
		int[] val = new int[2];
		if ( (mean - range) < 0 ){
			val[0] = 0;
		}		
		else{
			val[0] = mean - range;
		}
		
		if ( (mean + range) > dim_universe ){
			val[1] = dim_universe;
		}
		else{
			val[1] = mean + range;
		}
		return val;
	}
	
	public void addListener(JComponent component){
		this.listener.add(component);
	}
	
}