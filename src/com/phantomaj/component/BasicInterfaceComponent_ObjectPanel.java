/**
* BasicInterfaceComponent_ObjectPanel.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;

import com.phantomaj.button.ButtonRotationCenterObj;
import com.phantomaj.button.ButtonUpnDown;
import com.phantomaj.select.CenterRotationCheckBox;
import com.phantomaj.select.SliderAngleObj;
import com.phantomaj.select.SliderLayer;
import com.phantomaj.select.SliderPosObj;
import com.phantomaj.select.VTextField_NEW;

public class BasicInterfaceComponent_ObjectPanel extends JPanel{

	private static final String[] label_text ={"<html><h1>Object : object 1</h1></html>",
											"Object General Option",
											"Object Dimension",
											"Object Position",
											"Object Rotation"};
	

	private BasicInterface _basicInterface;
	
	private JPanel _positionObject;
	private JPanel _dimensionObject;
	private JPanel _rotationObject;
	private JPanel _generalParametersObject;
	
	ButtonRotationCenterObj _PanelChooseCenterObj;
	
	private int[][] ori;
	private int[] center;
	private int[][] angle;
	
	private	SliderPosObj slider_change_posX;
	private SliderPosObj slider_change_posY;
	private SliderPosObj slider_change_posZ;
	
	private SliderAngleObj phi;
	private SliderAngleObj psi;
	private SliderAngleObj theta;
	
	private VTextField_NEW _pixelValue;
	private SliderLayer _layerValue;
	private JLabel _labelName;
	
	private ArrayList<String> nameForm = new ArrayList<String>();
	
	public BasicInterfaceComponent_ObjectPanel(BasicInterface bi){
		super(new GridBagLayout());
		this._basicInterface = bi;
		this.build();
	}

	private void build(){
		initPanel();
		
		JLabel label = new JLabel();
	
		_labelName = new JLabel(label_text[0]);

		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,_labelName);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,this,_generalParametersObject);
		
		
		TitledBorder title = BorderFactory.createTitledBorder(label_text[1]);
		_generalParametersObject.setBorder(title);
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,_dimensionObject);
		
		title = BorderFactory.createTitledBorder(label_text[2]);
		_dimensionObject.setBorder(title);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,this,_positionObject);
		
		title = BorderFactory.createTitledBorder(label_text[3]);
		_positionObject.setBorder(title);
		
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,5,0,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,this,_rotationObject);
		
		title = BorderFactory.createTitledBorder(label_text[4]);
		_rotationObject.setBorder(title);
		
		label = new JLabel("");
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,5,this,label);
	}
	
	
	private void setRotPanel(){
		JLabel label;
		ButtonUpnDown bouttonUpDown;
		JComponent[] component; 
	
	
		_rotationObject = new JPanel(new GridBagLayout());
		
		component = new JComponent[11];
		bouttonUpDown = new ButtonUpnDown("",null);
		
		label = new JLabel("<html><h3>Object Rotation Change</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_rotationObject,label);	
		
		component[10] = label ;
		
	//PHI
		
		label = new JLabel("<html><ul><li> Phi :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,_rotationObject,label);

		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,1,_rotationObject,bouttonUpDown); 
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
		phi = new SliderAngleObj(JSlider.HORIZONTAL,-180,180,0,label,0,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,_rotationObject,phi);
		
		component[1] = phi;
	
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,2,_rotationObject,label);
		
		component[2] = label ;


	//THETA
	
		label = new JLabel("<html><ul><li> Theta :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,_rotationObject,label);

		component[3] = label ;
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
		theta = new SliderAngleObj(JSlider.HORIZONTAL,-180,180,0,label,1,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,_rotationObject,theta);

		component[4] = theta ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,4,_rotationObject,label);
		
		component[5] = label ;

	//PSI
	
		label = new JLabel("<html><ul><li> Psi :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,10,GridBagConstraints.BOTH,0,0,0,0,1,1,0,5,_rotationObject,label);

		component[6] = label ;
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
	  	psi = new SliderAngleObj(JSlider.HORIZONTAL,-180,180,0,label,2,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,50,10,0,GridBagConstraints.BOTH,1,0,0,20,1,1,0,6,_rotationObject,psi);
		
		component[7] = psi ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,6,_rotationObject,label);
	
		component[8] = label ;
		
		
		JPanel _panelCenterRotGlobal = new JPanel(new GridBagLayout());
		
		CenterRotationCheckBox centerCheck = new CenterRotationCheckBox("Change Object Rotation Center",0,this);
		addGridBag(GridBagConstraints.CENTER,5,10,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,_panelCenterRotGlobal,centerCheck);
		
		_PanelChooseCenterObj = new ButtonRotationCenterObj(this);
		_PanelChooseCenterObj.setVisible(false);
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,_panelCenterRotGlobal,_PanelChooseCenterObj);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,10,10,GridBagConstraints.REMAINDER,0,0,0,0,2,1,0,7,_rotationObject,_panelCenterRotGlobal);
		
		component[9] = _panelCenterRotGlobal ;
		
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(false);
		
	}
	
	private void setPosPanel(){
		JLabel label;
		ButtonUpnDown bouttonUpDown;
		JComponent[] component; 
		
		_positionObject = new JPanel(new GridBagLayout());
		
		bouttonUpDown = new ButtonUpnDown("",null);
		component = new JComponent[10];

		
		
		label = new JLabel("<html><h3>Object Position Change</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_positionObject,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_positionObject,bouttonUpDown); 
		
		//Xpos
		label = new JLabel("<html><ul><li>X position Change :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,1,_positionObject,label);
		
		component[1] = label ;
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
		slider_change_posX = new SliderPosObj(0,BasicInterface.getStackDimension()[0],0,label,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,_positionObject,slider_change_posX);
		
		component[2] = slider_change_posX ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,2,_positionObject,label);
		
		component[3] = label ;
		
		//Ypos
		label = new JLabel("<html><ul><li>Y position Change :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,3,_positionObject,label);
		
		component[4] = label ;
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
		slider_change_posY = new SliderPosObj(0,BasicInterface.getStackDimension()[1],1,label,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,50,0,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,_positionObject,slider_change_posY);
		
		component[5] = slider_change_posY ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,25,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,4,_positionObject,label);

		component[6] = label ;
		
		//Zpos
		label = new JLabel("<html><ul><li>Z position Change :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,5,_positionObject,label);
		
		component[7] = label ;
		
		label = new JLabel("0000");
		label.setPreferredSize(label.getPreferredSize());
		label.setText("0");
		
		slider_change_posZ = new SliderPosObj(0,BasicInterface.getStackDimension()[2],2,label,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,50,10,0,GridBagConstraints.BOTH,1,0,0,0,1,1,0,6,_positionObject,slider_change_posZ);
		
		component[8] = slider_change_posZ ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.EAST,0,10,35,10,GridBagConstraints.REMAINDER,0,0,0,0,1,1,1,6,_positionObject,label);
		
		component[9] = label ;
		
		bouttonUpDown.setComponent(component);
		bouttonUpDown.setSelected(false);
	
	}
	
	private void initPanel(){
		JLabel label;
		
		_generalParametersObject = new JPanel(new GridBagLayout());
	
		label = new JLabel("<html><h3>Object Pixel Value<h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,3,1,0,0,_generalParametersObject,label);
	
	//Pixel valueForm
	
		label = new JLabel("<html><ul><li>Grey Value :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,_generalParametersObject,label);

		_pixelValue = new VTextField_NEW("100",3,-2,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,1,_generalParametersObject,_pixelValue);

		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,25,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,1,_generalParametersObject,label);
		
	//Layer Value
		label = new JLabel("<html><h3>Object Layer Position<h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,0,0,0,0,3,1,0,2,_generalParametersObject,label);
		
		label = new JLabel("<html><ul><li>Layer Value :</li></ul></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,25,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,3,_generalParametersObject,label);
		
		label = new JLabel("0");

		_layerValue = new SliderLayer(JSlider.HORIZONTAL,0,9,0,label,this);
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,1,3,_generalParametersObject,_layerValue);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,0,10,0,0,GridBagConstraints.HORIZONTAL,0,0,0,0,1,1,2,3,_generalParametersObject,label);
		
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,25,0,GridBagConstraints.BOTH,1,0,0,0,1,1,2,3,_generalParametersObject,label);	
		
		
		setRotPanel();
		setPosPanel();
		setDimPanel();
	}
	
	private void setDimPanel(){
		_dimensionObject = new BasicInterfaceComponent_DimensionPanelObj_NEW();
		((BasicInterfaceComponent_DimensionPanelObj_NEW)_dimensionObject).addListener(this);
	}	
	
	//****************************************************************************************************************************************************************
	//Public methode
	
	public void setUpdatePositionO(int id, int value){
		this._basicInterface.FireObjectPosChange(id,value,this.ori);
	}

	public void setUpdateLayerO(int layer){
		this._basicInterface.FireObjectLayerChange(layer);
	}
	
	public void setUpdatePixelValueO(int pixelValue){
		this._basicInterface.FireObjectPixelValueChange(pixelValue);
	}
	
	public void setUpdateRotationO(){
		int[] newAngle = {phi.getValue(),theta.getValue(),psi.getValue()};
		this._basicInterface.FireObjectRotationChange(newAngle,this.angle,this.ori,this.center);
	}
	
	public void updateSliderRotationObjCenterofRotation(int id){
		this._basicInterface.FireObjectRotationCenterChange(id);
	}
	
	public void setCenterRotationChangeVisible(boolean bol, int id){
		_PanelChooseCenterObj.build(nameForm);
		_PanelChooseCenterObj.setVisible(bol);
		_PanelChooseCenterObj.getParent().validate();
		this.updateSliderRotationObjCenterofRotation(-1);
	}
	
	
	public void setChangeObjDim(int transfo,int[][] dimension){
		this._basicInterface.FireObjectDimChange(transfo,ori,dimension);
	}
	
	public void initSlider(int[] min, int[] max){
		slider_change_posX.setMaximum_(max[0]);
		slider_change_posY.setMaximum_(max[1]);
		slider_change_posZ.setMaximum_(max[2]);
		
		slider_change_posX.setMinimum_(min[0]);
		slider_change_posY.setMinimum_(min[1]);
		slider_change_posZ.setMinimum_(min[2]);
		
		slider_change_posX.setValue_(0);
		slider_change_posY.setValue_(0);
		slider_change_posZ.setValue_(0);	
	}
	
	public void setOri(int[][] ori){
		this.ori = ori ;
	}
	
	public int[][] getOri(){
		return this.ori;
	}
	
	public void setCenter(int[] center){
		this.center = center ;
	}
	
	public int[] getCenter(){
		return this.center;
	}
	
	public void setAngle(int[][] angle){
		this.angle = angle ;
	}
	
	public int[][] getAngle(){
		return this.angle;
	}
	
	public void setObjectInfo(int[][] dim, int[][] ori,ArrayList<String> nameForm,float pv,int l,String name){
		this.ori = ori;
		this.nameForm = nameForm;
		((BasicInterfaceComponent_DimensionPanelObj_NEW)this._dimensionObject).setDimension(dim);
		_layerValue.setValue(l);
		_pixelValue.setText(((int)pv)+"");
		_labelName.setText("<html><h1>Object : "+name+"</h1></html>");
		
	}
	
	public void setObjectInfoRot(int[][] angle, int[] center){
		this.angle = angle;
		this.center = center;
	}
	
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}

}