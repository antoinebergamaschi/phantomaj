/**
* BasicInterfaceComponent_Second_FormPanel.java
* 15/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.phantomaj.button.ButtonUpnDown;

public class BasicInterfaceComponent_Second_FormPanel extends JPanel{

	private BasicInterface basicInterface;
	private JLabel nameFormTab2;
	private JLabel nameFormObjTab2;
	
	private BasicInterfaceComponent_rotPanel _transfo_form_r;
	private BasicInterfaceComponent_posPanel _transfo_form_p;
	private BasicInterfaceComponent_dimFormPanelRange _transfo_form_d;
	private JPanel _Transfo_form_d;
	
	private ButtonUpnDown[] bouttonUpDown= new ButtonUpnDown[3];
	//Constructor
	
	public BasicInterfaceComponent_Second_FormPanel(BasicInterface basicInterface,int type){
		super(new GridBagLayout());
		this.basicInterface = basicInterface;
		this.build(type);
		
		_transfo_form_d.addListener(this);
		_transfo_form_p.addListener(this);
		_transfo_form_r.addListener(this);
	}
/**********************************************************************************************************************************************************/
	//public methode
	public void setFormInfo(int ID,String nameForm, String nameObj){
		this.nameFormTab2.setText("<html><h1>Form : "+nameForm+"</h1></html>");
		this.nameFormObjTab2.setText("<html><h3>"+nameObj+"</h3></html>");
		
		this._Transfo_form_d.removeAll();
		_transfo_form_d = new BasicInterfaceComponent_dimFormPanelRange(ID);
		_transfo_form_d.addListener(this);
	
		bouttonUpDown[2] = new ButtonUpnDown("",null);
		JComponent[] component = new JComponent[2];
		
		JLabel label = new JLabel("<html><h3>Dimension Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_form_d,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_form_d,bouttonUpDown[2]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_form_d,_transfo_form_d);
		component[1] = _transfo_form_d;
		
		bouttonUpDown[2].setComponent(component);
		bouttonUpDown[2].setSelected(false);
		
		this.validate();
		this.repaint();
	
	}
	
	public void setInfo(int[][] valTransfo,int[][] valTransfo_dim){
		for (int i=0 ; i < 3 ; i++ ){
			_transfo_form_p.setValueSlider(i,valTransfo[i]);
			_transfo_form_r.setValueSlider(i,valTransfo[(i+3)]);
		}
		for (int z=0 ; z < valTransfo_dim.length ; z++ ){
			_transfo_form_d.setValueSlider(z,valTransfo_dim[z]);
		}
	}
	
	public void fireInfoSelected(int ID, int secondID){
		basicInterface.FireInfoSelected(ID,secondID);
	}
	
	public void fireStatChanged(int[] nv, int ID, int secondID){
		basicInterface.FireUpdateSliderForm(nv,ID,secondID);
	}
	
	public void fireUpdateSliderRange(int[] nv, int ID){
		basicInterface.FireUpdateSliderForm(nv,ID,0);
	}
	
	public void setSelected(boolean[][] selected){
		_transfo_form_d.setSelected(selected[2]);
		_transfo_form_p.setSelected(selected[0]);
		_transfo_form_r.setSelected(selected[1]);
		
		for ( int i = 0 ; i < selected.length ; i++ ){
			for ( int z = 0 ; z < selected[i].length ; z++ ){
				if ( selected[i][z] ){
					bouttonUpDown[i].setSelected(selected[i][z]);
				}
			}
		}
	}
	
	
/**********************************************************************************************************************************************************/
	//private building methode
	
	private void build(int type){
		JLabel label;
	
		nameFormTab2 = new JLabel("<html><h1>Form : Sphere 1</h1></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,0,this,nameFormTab2);
		
		nameFormObjTab2 = new JLabel("<html><h3>Object 1</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.NONE,1,0,0,0,1,1,0,1,this,nameFormObjTab2);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,2,this,this.getTransfoDim(type));	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,3,this,this.getTransfoPos());	
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,10,10,0,10,GridBagConstraints.BOTH,1,0,0,0,1,1,0,4,this,this.getTransfoAngle());	
		
		label = new JLabel(" ");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2			
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,5,this,label);	
		
	}
	
	private JPanel getTransfoDim(int type){
		_transfo_form_d = new BasicInterfaceComponent_dimFormPanelRange(type);
		
		_Transfo_form_d = new JPanel(new GridBagLayout());
	
		bouttonUpDown[2] = new ButtonUpnDown("",null);
		JComponent[] component = new JComponent[2];
		
		JLabel label = new JLabel("<html><h3>Dimension Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_form_d,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_form_d,bouttonUpDown[2]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_form_d,_transfo_form_d);
		component[1] = _transfo_form_d;
		
		TitledBorder title = BorderFactory.createTitledBorder("Modify Dimension Parameters");
		_Transfo_form_d.setBorder(title);	
		
		bouttonUpDown[2].setComponent(component);
		bouttonUpDown[2].setSelected(false);
		
		return _Transfo_form_d;
	}
	
	private JPanel getTransfoPos(){
		
		_transfo_form_p = new BasicInterfaceComponent_posPanel(BasicInterface.SLIDER_POSX_N,BasicInterface.SLIDER_POSY_N,BasicInterface.SLIDER_POSZ_N);
		
		JPanel _Transfo_form_p = new JPanel(new GridBagLayout());
		bouttonUpDown[0] = new ButtonUpnDown("",null);
		JComponent[] component = new JComponent[2];
		
		JLabel label = new JLabel("<html><h3>Translation Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_form_p,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_form_p,bouttonUpDown[0]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_form_p,_transfo_form_p);
		component[1] = _transfo_form_p;
		
		TitledBorder title = BorderFactory.createTitledBorder("Modify Translation Parameters");
		_Transfo_form_p.setBorder(title);	
		
		bouttonUpDown[0].setComponent(component);
		bouttonUpDown[0].setSelected(false);
		
		return _Transfo_form_p;
	}
	
	private JPanel getTransfoAngle(){
		
		_transfo_form_r = new BasicInterfaceComponent_rotPanel(BasicInterface.SLIDER_PHI_N,BasicInterface.SLIDER_THETA_N,BasicInterface.SLIDER_PSI_N);
		
		JPanel _Transfo_form_r = new JPanel(new GridBagLayout());
	
		bouttonUpDown[1] = new ButtonUpnDown("",null);
		JComponent[] component = new JComponent[2];
		
		JLabel label = new JLabel("<html><h3>Rotation Parameters</h3></html>");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.WEST,10,0,0,0,GridBagConstraints.BOTH,1,0,0,0,2,1,0,0,_Transfo_form_r,label);
		
		component[0] = label ;
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTHEAST,0,0,10,10,GridBagConstraints.REMAINDER,1,1,0,0,1,1,1,0,_Transfo_form_r,bouttonUpDown[1]); 
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,1,_Transfo_form_r,_transfo_form_r);
		component[1] = _transfo_form_r;
		
		TitledBorder title = BorderFactory.createTitledBorder("Modify Rotation Parameters");
		_Transfo_form_r.setBorder(title);	
		
		bouttonUpDown[1].setComponent(component);
		bouttonUpDown[1].setSelected(false);
		
		return _Transfo_form_r;
	}
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}

	
	
	
}