/**
* BasicInterfaceComponent_RandPanel.java
* 29/10/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.component;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
// import PhantoMaJ.select.SliderAngle_NEW;


public class BasicInterfaceComponent_RandPanel extends JPanel{

	private BasicInterfaceComponent_dimFormPanelRange _transfo_form_d;
	private BasicInterface basicInterface;
	private JPanel _Transfo_form_d;
	
//********************************************************************
//Constructor
	public BasicInterfaceComponent_RandPanel(BasicInterface bi){
		super(new GridBagLayout());
		this.basicInterface = bi;
		this.build();
	}

//********************************************************************
// Public Methode
	public void fireInfoSelected(int ID,int secondID, boolean bol){
		basicInterface.fireInfoSelected_Rand(ID,secondID,bol);
	}
	
	public void fireStatChanged(int[] nv, int ID, int secondID){
		basicInterface.fireStatChanged_Rand(nv,ID,secondID);
	}
	
//********************************************************************
//Private Methode	
	
	private void build(){		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,0,this,this.getDimPanel(0)); 
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,1,this,this.getRotPanel()); 
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,0,0,0,0,1,1,0,2,this,this.getIntensityPanel()); 
		
		JLabel label = new JLabel();
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,3,this,label); 
	}
	
	private JPanel getRotPanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		BasicInterfaceComponent_rotPanel _rotPanel = new BasicInterfaceComponent_rotPanel(BasicInterface.SLIDER_PHI_N,BasicInterface.SLIDER_THETA_N,BasicInterface.SLIDER_PSI_N);
		_rotPanel.addListener(this);
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,panel,_rotPanel);
		
		TitledBorder title = BorderFactory.createTitledBorder("Rotation");
		panel.setBorder(title);	
		
		return panel;
	}	
	
	private JPanel getIntensityPanel(){
		JPanel panel = new JPanel(new GridBagLayout());
		BasicInterfaceComponent_IntensityPanel _intensityPanel = new BasicInterfaceComponent_IntensityPanel(BasicInterface.SLIDER_RAND_INT);
		_intensityPanel.addListener(this);
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,panel,_intensityPanel);
		
		TitledBorder title = BorderFactory.createTitledBorder("Intensity");
		panel.setBorder(title);	
		
		return panel;
	}
	
	/**
	* int , ID, -1 is an object ID
	**/
	private JPanel getDimPanel(int ID){
		_transfo_form_d = new BasicInterfaceComponent_dimFormPanelRange(ID);
		_transfo_form_d.addListener(this);
		_Transfo_form_d = new JPanel(new GridBagLayout());
	
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.NORTH,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_Transfo_form_d,_transfo_form_d);
		
		TitledBorder title = BorderFactory.createTitledBorder("Dimension");
		_Transfo_form_d.setBorder(title);	
		
		
		return _Transfo_form_d;
	}
	
	
	public void setForm(int ID){
		this._Transfo_form_d.removeAll();
		_transfo_form_d = new BasicInterfaceComponent_dimFormPanelRange(ID);
		_transfo_form_d.addListener(this);
	
		
		// anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,0,0,0,0,GridBagConstraints.BOTH,1,1,0,0,1,1,0,0,_Transfo_form_d,_transfo_form_d);
	
		
		this.validate();
		this.repaint();
		
	}
	
	
	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
}

	
	
	
