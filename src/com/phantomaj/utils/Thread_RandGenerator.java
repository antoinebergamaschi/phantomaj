/**
* Thread_RandGenerator.java
* 20/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

import java.util.ArrayList;

import com.phantomaj.form.CreateForm;
import com.phantomaj.obj3D.Object3D;
import com.phantomaj.randomGenerator.LoadingFrame;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;
import com.phantomaj.form.CreateForm;
import com.phantomaj.obj3D.Object3D;
import com.phantomaj.randomGenerator.RandomGeneratorFrame;

public class Thread_RandGenerator implements Runnable{	
	private RandomGeneratorFrame rand_frame;
	private int nb_try;
	private int nb_form;
	private int[] dim_stack;
	private ArrayList<ArrayList<CreateForm>> pre_phantom;
	@SuppressWarnings("unused")
	private boolean Continuer;
	private Thread t;
	
	private Compute_Intensity Cint;
	private Thread_option Toption;
	
	private boolean isOk;
	
	public Thread_RandGenerator(RandomGeneratorFrame rand_frame,int nb_try, int nb_form, int[] dim_stack, ArrayList<ArrayList<CreateForm>> pre_phantom,Compute_Intensity Cint, Thread_option Toption){	
		t = new Thread (this,"Thread_RandGenerator");
		
		this.pre_phantom = pre_phantom;
		this.rand_frame = rand_frame;
		this.nb_try = nb_try;
		this.nb_form = nb_form;
		this.Cint = Cint;
		this.Toption = Toption;
		this.dim_stack = dim_stack;
		
		Continuer = true;	
	}

	//Runnable Fonction
	
	public void run (){	
		Chrono c = new Chrono();
		c.start();

		this.rand_frame.setEnable_butDraw(false);

		LoadingFrame lf = new LoadingFrame(this.nb_form);
		

		Object3D obj = new Object3D(this.rand_frame.creatRandomPhantom(this.nb_form));
	
		//If user want to repeat the same structure in the stack
		if ( this.rand_frame.getIsStructureSelected() ){
			obj.setIsStructure(true);
		}
	
		obj.setComputeIntensity(this.Cint);
		obj.setThreadOption(this.Toption);
		


		if ( pre_phantom != null ){
			System.out.println("Pre-Phantom Up");
			obj.setPhantom_toString(this.pre_phantom,this.dim_stack);
		}
		
		obj.setDimStack(this.dim_stack);


		int todo_form = obj.drawWithoutCollision(true);
		int todo_form_ = 0 ;
		int stop_try = 0;
		boolean break_while = true ;

		if ( todo_form == 0 ){
			break_while = false ;
		}

		int i = 1;

		while (  break_while && !lf.getStop() ){
			//update the loading Frame
			lf.update(todo_form);

			todo_form_ = todo_form;
			obj.setPhantom(this.rand_frame.creatRandomPhantom(todo_form));
			todo_form = obj.drawWithoutCollision(false);

			if ( todo_form_ == todo_form ){
				stop_try++;
				if ( stop_try == 500*i ){
					System.out.println("Stop Try ........... "+stop_try);
					i++;
				}
			}
			
			else{
				stop_try = 0;
				i = 1;
			}


			if ( stop_try > nb_try && break_while ){
				break_while = false ;
			}


			if ( todo_form <= 0 ){
				break_while = false;
			}
		}
		
		lf.setStop(true);
		lf.end_load(todo_form);

		System.out.println("Writing File .......");

		c.stop();
		//Writing the results file
		this.rand_frame.creatWorkFile((this.nb_form - todo_form),c.delayString());
		obj.creaRegisterFile(this.rand_frame.getPath());
		obj.creatObjectFile(this.dim_stack,this.rand_frame.getPath());
		obj.saveImage(this.rand_frame.getPath(),0);
		obj.drawForced();
		

		this.rand_frame.setEnable_butDraw(true);
	}
	
	public void Arret (){
		Continuer = false;
	}
	
	public String getName(){
		return t.getName();
	}
	
	public void start(){	
		t.start();
	}
	
	public boolean isAlive() {
		return t.isAlive();
	}		
	
	public void	join()	throws InterruptedException{
		t.join();
	}
	
	//Public Methode
	
	public boolean getIsOk(){
		return this.isOk;
	}
	
}