/**
* Compute_Intensity.java
* 10/07/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

public class Compute_Intensity{

	private static final int COSINUS_F = 0;
	private static final int LOGARITHM_F = 1;
	private static final int EXPONENTIAL_F = 2;
	private static final int LINEAR_F = 3;
	private static final int BINARY_F = 4;

	private static final int MOD_MOY = 0;
//	private static final int MOD_LASTIN = 1;
//	private static final int MOD_SUM = 2;
//	private static final int MOD_NEG = 3;
	
	private boolean isDouble = false;
	private boolean	inverted = false;
	private boolean isCstSelected = false;
	
	private float cstValue = 0.0f;
	private int functionID= Compute_Intensity.COSINUS_F;
	private int modIntersection = Compute_Intensity.MOD_MOY;
	
	private float backgroudValue = 0.0f;
	
	public Compute_Intensity(){
	
	}

	/**
	* Compute the value between 0 and 1 corresponding to the relative grey value to be drew
	* @param x the floating point between 0 and 1
	* @param bol the boolean corresponding to the test if the inverted function is asked
	**/
	public float compute(double x,boolean bol){
		float it = 0.0f;
		switch(this.functionID){
				
			case COSINUS_F:
				if ( bol ){
					it = (float)Math.cos((Math.PI/2)*(x-1));
				}
				else {
					it = (float)Math.cos((Math.PI/2)*x);
				}
				break;
			case LOGARITHM_F:
				if ( bol ){
					it = (float)(1-Math.exp(6*(-x)));
				}
				else {
					it = (float)(1-Math.exp(-6*(1-x)));
				}
				break;
			case EXPONENTIAL_F:
				if ( bol ){
					it = (float)Math.exp(6.0d*(x-1));
				}
				else {
					it = (float)Math.exp(-6.0d*x);
				}
				break;
			case LINEAR_F:
				if ( bol ){
					it = (float)x;
				}
				else {
					it = (float)(1-x);
				}
				break;
			case BINARY_F:
				if ( bol ){
					it = 0.0f;
				}
				else {
					it = 1.0f;
				}
				break;
		}
		return it;		
	}
	

	/**
	* Compute the value in the interval [0,1] in the function implemented in the programs
	* @param x the relative position between 0 and 1 ( 0 center and 1 border ) in a geometrical shape
	**/
	public float compute_creatForm(float x){
		// System.out.println("Passage Compute_CreatForm");
		float it = 0.0f;
		//seems to works fine
		if ( this.isDouble ){
			if ( this.isCstSelected ){
				if ( x >= (0.5f - this.cstValue/2.0f) && x <= (0.5f + this.cstValue/2.0f) ){
					return this.inverted ? 1.0f : 0.0f ;
				}
				else if ( x < (0.5f - this.cstValue/2.0f) ){
					x *= (1/(0.5f - this.cstValue/2.0f));
					return compute(x,this.inverted);
				}
				else if ( x > (0.5f + this.cstValue/2.0f) ){
					x -= (0.5f + this.cstValue/2.0f);
					x *= (1/(0.5f - this.cstValue/2.0f));
					
					//Cas particulier de la fonction binaire
					if ( this.functionID == BINARY_F ){
						return compute(x,this.inverted);
					}
					
					return compute(x,!this.inverted);
				}
			}
			else{
				if ( x <= 0.5f ){
					return compute(2*x,this.inverted);
				}
				else if ( x > 0.5f ){
					return compute((2*x)-1,!this.inverted);
				}
			}
		}
		//this works fine
		else {
			if ( this.isCstSelected ){
				
				if ( x <= this.cstValue ){
					return this.inverted ? 0.0f : 1.0f ;
				}
				
				x *= 1/(1-this.cstValue);
				x -= (1/(1-this.cstValue)-1);
				
				//cas particulier de la fonction binaire ( inverse des autres fonctions )
				if ( this.functionID == BINARY_F ){
					return compute(x,!this.inverted);
				}
				
				return compute(x,this.inverted);
			}
			else{
				return compute(x,this.inverted);
			}
		}
		
		return it;
	}
	
	public void changeFunction(int idFunction){
		this.functionID = idFunction;
	}
	
	public void setRadiusCst(boolean bol){
		this.isCstSelected = bol;
	}
	
	public void updateCstValue(float cst){
		this.cstValue = cst;
		System.out.println("Update Cst Value"+this.cstValue);
	}
	
	public void changeInverted(boolean bol){
		this.inverted = bol;
	}
	
	public void changeDouble(boolean bol){
		this.isDouble = bol;
	}
	
	
	public int getFunctionId(){
		return this.functionID;
	}
	
	public float getCstValue(){
		return this.cstValue;
	}
	
	public boolean getIsCstSel(){
		return this.isCstSelected;
	}
	
	public boolean getIsDouble(){
		return this.isDouble;
	}
	
	public boolean getIsInverted(){
		return this.inverted;
	}
	
	public int getModIntersection(){
		return this.modIntersection;
	}
	
	public void setModIntersection(int mod){
		this.modIntersection = mod;
	}
	
	public void setBackgroundValue(float bv){
		this.backgroudValue = bv;
	}
	
	public float getBackgroundValue(){
		return this.backgroudValue;
	}
	
}