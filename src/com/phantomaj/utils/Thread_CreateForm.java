/**
* Thread_CreateForm.java
* 15/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

import com.phantomaj.form.CreateForm;
import com.phantomaj.form.CreateForm;

public class Thread_CreateForm implements Runnable{	
	private CreateForm form;
	private int type;//0=> build , 1 try_build
	@SuppressWarnings("unused")
	private boolean Continuer;
	private int[] x_section;
	private int[] y_section;
	private int[] z_section;
	private Thread t;
	
	private boolean isOk=false;
	
	public Thread_CreateForm (CreateForm form,int[] x_section,int[] y_section, int[] z_section, int type){	
		t = new Thread (this,"thread_CreatForm");
		this.form = form;
		this.type = type;
		this.x_section=x_section;
		this.y_section=y_section;
		this.z_section=z_section;
		Continuer = true;	
	}

	//Runnable Fonction
	
	public void run (){	
		if ( this.type == 0 ){
			form.build_Stack_XYZ_multi(this.x_section,this.y_section,this.z_section);
		}
		else if ( this.type == 1 ){
			if( !form.try_build_Stack_XYZ_multi(this.x_section,this.y_section,this.z_section) ){
				//Stop condition
				this.form.setBolTry(false);
			}
		}
	}
	
	public void Arret (){
		Continuer = false;
	}
	
	public String getName(){
		return t.getName();
	}
	
	public void start(){	
		t.start();
	}
	
	public boolean isAlive() {
		return t.isAlive();
	}		
	
	public void	join()	throws InterruptedException{
		t.join();
	}
	
	//Public Methode
	
	public boolean getIsOk(){
		return this.isOk;
	}
	
}
