/**
* LoadingBar.java
* 13/09/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.phantomaj.PhantoMaJ;
import com.phantomaj.obj3D.Object3D;


@SuppressWarnings("serial")
public class LoadingBar extends JFrame{

	private JLabel loading = null;
	private static final String[] T_loading = {"Loading...",
												"Loading..",
												"Loading.",
												"Loading.."};											
	
	private JLabel progressLabel = null;
	private BarPanel barPanel = null;
	private PhantoMaJ principal_window = null;
	private Object3D obj3D = null;
	private Thread_LoadingFrame t = null;
	
	//Loadtype
	private int type = 0;
//	private final static int BOUCLE = 0;
//	private final static int AVANCE = 1;
	// private boolean fin=false;
	
/********************************************/
//Constructor
	
	public LoadingBar(){
		super();
		this.build();
	}

	/**
	* Private Method, continue the building process of the Frame
	*/
	private void build(){ 

		setResizable(false); 
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		setContentPane(buildContentPane());
		this.pack();
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension windowSize = this.getSize();

		int windowX = Math.max(0, (screenSize.width  - windowSize.width ) / 2);
		int windowY = Math.max(0, (screenSize.height - windowSize.height) / 2);

		setLocation(windowX, windowY); 
		this.setVisible(true);
	}

	// public boolean finished(){
		// return fin;
	// }

	/**
	* Final method use to build up this frame.Add all the elements constituting the 
	* default Frame.
	*@return JPanel the default contentPane to be display
	*/
	private JPanel buildContentPane(){
	
//		TitledBorder title;
		Border raisedbevel;

//		blackline = BorderFactory.createLineBorder(Color.black);
//		raisedetched = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);
//		loweredetched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		raisedbevel = BorderFactory.createRaisedBevelBorder();
//		loweredbevel = BorderFactory.createLoweredBevelBorder();
//		empty = BorderFactory.createEmptyBorder();
//	
		JPanel principal = new JPanel(new GridBagLayout());
		
		loading = new JLabel(LoadingBar.T_loading[0]);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,0,0,0,0,1,1,0,0,principal,loading);
		
		barPanel = new BarPanel();
		barPanel.setBorder(raisedbevel);
		
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.BOTH,1,0,0,0,1,1,0,1,principal,barPanel);
		
		
		progressLabel = new JLabel("Test");
		//anchor,insetsTop, insetLeft,insetBottom,insetRight, fill,weightx,weighty,ipadx,ipady,gridwidth,gridheight,gridx,gridy,JComponent container, JComponent 2	
		addGridBag(GridBagConstraints.CENTER,5,5,5,5,GridBagConstraints.NONE,0,0,0,0,1,1,0,2,principal,progressLabel);
		
		return principal;
	}
	
	
//private Methode

	private void addGridBag(int anchor,int insetTop,int insetLeft,int insetBottom, int insetRight,int fill,double weightx,double weighty,int ipadx,int ipady,int width, int height, int gridx,int gridy, JComponent container, JComponent component){
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(insetTop,insetLeft,insetBottom,insetRight);
		c.fill = fill;
		c.anchor = anchor;
		c.weightx = weightx;
		c.weighty = weighty;
		c.ipadx = ipadx;
		c.ipady = ipady;
		c.gridx = gridx;
		c.gridy = gridy;
		c.gridwidth = width;
		c.gridheight = height;
		container.add(component, c);
	}
	
	
	public void setInfo(int type,PhantoMaJ principal_window){
		this.principal_window = principal_window;
		this.type = type;
	}
	
	public void setInfo_obj(int type, Object3D obj3D){
		this.obj3D = obj3D;
		this.type = type;
	}
	
	
	public void start(){
		t = new Thread_LoadingFrame(this,this.type);
		t.start();
	}
	
	public void setText(int com){
		loading.setText(T_loading[com]);
		// this.repaint();
	}
	
	public void setProgressText(String text){
		progressLabel.setText(text);
	}	
	
	public void ask_update(){
		if ( obj3D == null ){
			principal_window.ask_update_loading(this);
		}
		else{
			obj3D.ask_update_loading(this);
		}
	}
	
	public void updateThread(int x, String text){
		if ( x < 0 ){
			setTypeBar(0);
			t.setX(100);
		}
		else{
			t.setX(x);
		}
		t.setText(text);
	}
	
	public Graphics getGraphics_bar(){
		return barPanel.getGraphics();
	}
	
	public void repaint_(int x){
		barPanel.setX(x);
		barPanel.repaint();
	}
	
	public void setTypeBar(int type){
		t.setTypeBar(type);
	}
	
	public void setLoadTitle(String title){
		loading.setText(title);
	}
	//INNER class
	

	class BarPanel extends JPanel{
		// private Graphics g = null;
		private int x=0;
		
		
		public BarPanel(){
			super();
			this.setLayout(null);
			this.setPreferredSize(new Dimension(200,30));
			this.setBackground(Color.GRAY);
		}
		
		private void build(Graphics g){
			g.setColor(Color.red);
			g.fill3DRect(0,0,x,30,true); 
		}
		
		public void setX(int x){
			this.x = x ;
		}
		
		
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			build(g);
		}
	}
	
	/**
	* override
	**/
	public void dispose(){
		t.Arret();
		super.dispose();
	}
	
	
}