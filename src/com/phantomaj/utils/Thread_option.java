/**
* Thread_option.java
* 23/07/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

public class Thread_option{

	@SuppressWarnings("unused")
	private final static int MIXTE=0;
	private final static int FULL=1;
	@SuppressWarnings("unused")
	private final static int CLASSIC=2;

	private int numbOfProcessor = Runtime.getRuntime().availableProcessors();
	// private int numbOfProcessor = 1;
	private int computeType = Thread_option.FULL;
	
	public Thread_option(){
	}
	
	public void setNumberOfProcessor(int numb){
		this.numbOfProcessor = numb;
	}
	
	public int getNumberOfProcessor(){
		return this.numbOfProcessor;
	}
	
	public int getComputingType(){
		return this.computeType;
	}
	
	public void setComputingType(int type){
		this.computeType = type;
	}
	
}