/**
* ThreadSetTree.java
* 19/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

import com.phantomaj.tree.NodeForm;
import com.phantomaj.tree.TreeDragAndDrop;

public class ThreadSetTree implements Runnable{	
	private TreeDragAndDrop tree;
	private int posObj;
	private NodeForm obj;
	@SuppressWarnings("unused")
	private boolean Continuer;
	private Thread t;
	
	private boolean isOk;
	
	public ThreadSetTree(TreeDragAndDrop tree,NodeForm obj, int posObj){	
		t = new Thread (this,"thread_setTree");
		this.tree = tree;
		this.obj = obj;
		this.posObj = posObj;
		Continuer = true;	
	}

	//Runnable Fonction
	
	public void run (){	
		this.tree.add_form(this.obj,this.posObj);
	}
	
	public void Arret (){
		Continuer = false;
	}
	
	public String getName(){
		return t.getName();
	}
	
	public void start(){	
		t.start();
	}
	
	public boolean isAlive() {
		return t.isAlive();
	}		
	
	public void	join()	throws InterruptedException{
		t.join();
	}
	
	//Public Methode
	
	public boolean getIsOk(){
		return this.isOk;
	}
	
}