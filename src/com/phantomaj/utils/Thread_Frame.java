/**
* Thread_Frame.java
* 13/09/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;

import com.phantomaj.PhantoMaJ;


public class Thread_Frame implements Runnable{	
	private PhantoMaJ principal_window;
	private LoadingBar l;
//	private boolean Continuer;
	private Thread t;
	private int type;
	private boolean isOk;

	
	public Thread_Frame(PhantoMaJ principal_window,int type){
		t = new Thread (this,"Thread_Frame");
		this.principal_window = principal_window;
		this.type = type;
//		Continuer = true;	
	}

	//Runnable Function
	
	public void run (){	
		l = new LoadingBar();
		l.setInfo(type,principal_window);
	}
	
	
	
	public void Arret (){
		l.dispose();
	}
	
	public String getName(){
		return t.getName();
	}
	
	public void start(){	
		t.start();
	}
	
	public boolean isAlive() {
		return t.isAlive();
	}		
	
	public void	join()	throws InterruptedException{
		t.join();
	}
	
	//Public Methode
	
	public boolean getIsOk(){
		return this.isOk;
	}
	
}