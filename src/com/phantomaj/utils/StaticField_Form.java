/**
* StaticField_Form.java
* 15/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;
import com.phantomaj.form.CreateForm;
import com.phantomaj.form.*;
import com.phantomaj.view3D.Fiber3d;
import com.phantomaj.view3D.Form3D;
import com.phantomaj.view3D.Pyramid3d;

public class StaticField_Form{	
//*******************************************************************************
//Do not Modified                                                              //
//*******************************************************************************
	public static final int _3DFORM = 0;
	public static final int _2DFORM = 1;
	public static final int _1DFORM = 2;
	// public static final int _3DFORMX = -1;
	
	public static final int PHI = 0;
	public static final int THETA = 1;
	public static final int PSI = 2;
//********************************************************************************
//********************************************************************************

//Add here the identifier of a new form to add
//Default ID
	public static final int BOX=0;
	public static final int CONE=1;
	public static final int PYRAMID=2;
	public static final int SPHERE=3;
	public static final int TUBE=4;
	public static final int ELLIPSE=5;
	public static final int FIBER = 6;
//Added ID	
	public static final int test = 7;
	public static final int testi = 8;
	

//Add the name of the form to add	
	public static final String[] FORM_NAME = {"Box","Cone","Pyramid","Sphere","Tube","Ellipsoid","Fiber"};

//Add the name of every dimension modifiable by your form geometry
	public static final String[][] FORMDIMENSION_NAME = {{"Width","Height","Size"},//0
														  {"Diameter","Height"},//1
														  {"Base Width","Base Height","Size"},//0
														  {"Diameter"},//2
														  {"Diameter","Height"},//1
														  {"Width","Height","Size"},//0
														  {"Diameter","Height","Angle of Flexion"}};//0
	
	//Specified the 3D information of the form
	//_3DFORM => Basic Dimension == Dimension 3D
	//_2DFORM => Basic Dimension Lack 1 D to be 3D (Base dimension x == y )
	//_1DFORM => Basic Dimension lack 2D to be 3D (base dimension x == y == z)

	//Warning :: Be aware that every Form have 3D representation !!
	public static final int[] CODE_3D_FORM = {_3DFORM,_2DFORM,_3DFORM,_1DFORM,_2DFORM,_3DFORM,_2DFORM};
	
	//Specified the Type of the additionnal parameters
	//null if there isn't any
	//0=> The additionnal parameters is a dimension
	//1=> the additionnal parameters is an angle
	//!!!
	// Warning if there is additional parameters ( not x,y or z transformation ) please add here an entry
	//!!!
	public static final int[][] CODE_ADDITIONAL_FORM = {null,
														null,
														null,
														null,
														null,
														null,
														{1}};
	
	// add the default parameters for the new form
	// The default dimension taken by the form :: dimX,dimY,dimZ,posX,posY,posZ,phi,theta,psi
	public static final int[][] BASICDIMENSION = {{100,100,100,100,100,100,0,0,0},
												  {100,100,150,100,100,100,0,0,0},
												  {100,100,100,100,100,100,0,0,0},
												  {100,100,100,100,100,100,0,0,0},
												  {100,100,100,100,100,100,0,0,0},
												  {150,100,50,100,100,100,0,0,0},
												  {100,100,100,100,100,100,0,0,0}};
	
	// DEFAULT Constructor													  
	public StaticField_Form(){}
	
//*********************************************************************************************************************************
// Function to modified	                                                                                                         //
//*********************************************************************************************************************************

	/**
	* get the CreateForm object constituing of the parameters of this NodeForm.
	* @return obj, CreateForm an AbstractForm who could be draw
	**/
	public static final CreateForm getCreatForm(int formID, float pixelValue, int layer, int[] centerRotation, int[] dimension, int[] origine, int[] angle){
		CreateForm obj;
		// int[] centerRotation = getCenterRotation();
		switch(formID){
			case StaticField_Form.BOX:
				obj = new Box(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.CONE:
				obj = new Cone(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.PYRAMID:
				obj = new Pyramid(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.SPHERE:
				obj = new Sphere(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.TUBE:
				obj = new Tube(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.ELLIPSE:
				obj = new Ellipse(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;
			case StaticField_Form.FIBER:
				obj = new Fiber(dimension,origine,angle,pixelValue,layer);
				//set the center of rotation of this form
				obj.setCenterOfRotation(centerRotation[0],centerRotation[1],centerRotation[2]);
				break;

			//Add here the new form 
			
			// case StaticField_Form.new_form:
				// obj = new PhantoMaJ.form.new_form(dimension,origine,angle,pixelValue,layer);
				// break;
			
			default:
				obj = null;
				System.out.println("WARNING :::: The NodeForm to Draw have unidentified Form ID ......... StaticField_Form.getCreatForm");
				break;
		}
		return obj;	
	}
	
	public static final boolean addForm3D(int formID,Form3D form3D){
		boolean bol = true;
		switch(formID){
			case StaticField_Form.BOX:
				form3D.addChild(new com.sun.j3d.utils.geometry.Box(1.0f,1.0f,1.0f,form3D.getAppearance()));
				break;
			case StaticField_Form.CONE:
				form3D.addChild(new com.sun.j3d.utils.geometry.Cone(1.0f,1.0f,form3D.getAppearance()));
				break;
			case StaticField_Form.PYRAMID:
				form3D.addChild(new Pyramid3d());
				
				break;
			case StaticField_Form.SPHERE:
				form3D.addChild(new com.sun.j3d.utils.geometry.Sphere(1.0f,form3D.getAppearance()));
				break;
			case StaticField_Form.TUBE:
				form3D.addChild(new com.sun.j3d.utils.geometry.Cylinder(1.0f,1.0f,form3D.getAppearance()));
				break;
			case StaticField_Form.ELLIPSE:
				form3D.addChild(new com.sun.j3d.utils.geometry.Sphere(1.0f,form3D.getAppearance()));
				break;
			case StaticField_Form.FIBER:
				form3D.addChild(new Fiber3d(new float[1]));
				break;
			//Add here the new 3Dform
			// case StaticField_Form.new_form:
				// this.addChild(new new_form);
				// break;
			default:
				System.out.println("WARNING :::: The Form3D to add in the 3DView is missing ("+formID+")........ StaticField_Form.addForm3D");
				return false;
		}
		return bol;
	}
	
//************************************************************************************************************************************
//Do not modified	                                                                                                                //
//************************************************************************************************************************************
	
	public static final int getIDfromName(String name){
		for ( int i = 0 ; i < FORM_NAME.length ; i++ ){
			if ( FORM_NAME[i] == name ){
				return i;
			}
		}
		System.out.println("WARNING :::: THE NAME LIST IS INCOMPLETED");
		return -1;
	}
	
//*************************************************************************************************************************************	
}		
