/**
* Thread_LoadingFrame.java
* 13/09/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.utils;


public class Thread_LoadingFrame implements Runnable{	
	private LoadingBar loading_frame;
	private boolean Continuer;
	private Thread t;

	private int x=0;
	private int type;
	private String text="";
	
	
	private boolean _sens = true;
	
	private final static int BOUCLE = 0;
	private final static int AVANCE = 1;
	
	public Thread_LoadingFrame(LoadingBar loading_frame,int type){	
		this.t = new Thread (this,"Thread_LoadingFrame");
		this.type = type;
		this.loading_frame = loading_frame;
		this.Continuer = true;	
	}

	//Runnable Fonction
	
	public void run (){	
		// int i = 0;
		while ( Continuer ){
			//Ask for update

			this.loading_frame.setProgressText(text);
		
			if ( this.type == BOUCLE ){
				drawGraphics_boucle();
			}
			else if ( this.type == AVANCE ){
				this.loading_frame.ask_update();
				drawGraphics();
			}
			
			
			try{
				Thread.sleep(500L);
			}
			catch(InterruptedException e){
			
			}

		}
	}
	
	public void drawGraphics(){
		//get entier correspondant a l'avancement
		
		
		
		this.loading_frame.repaint_(x);
	}
	
	
	public void drawGraphics_boucle(){
		if (_sens){
			x+=20;
		}
		else if ( !_sens){
			x-=20;
		}
		
		this.loading_frame.repaint_(x);
		
		if ( x == 200 ){
			_sens = false;
		}
		else if ( x == 0 && !_sens ){
			_sens = true;
		}
	}
	
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setText(String text){
		this.text = text;
	}
	
	public void setTypeBar(int type){
		this.type = type;
	}
	
	public void Arret (){
		this.Continuer = false;
	}
	
	public String getName(){
		return t.getName();
	}
	
	public void start(){	
		t.start();
	}
	
	public boolean isAlive() {
		return t.isAlive();
	}		
	
	public void	join()	throws InterruptedException{
		t.join();
	}
	
	
}