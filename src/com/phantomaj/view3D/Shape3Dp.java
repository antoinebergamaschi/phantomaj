package com.phantomaj.view3D;

import javax.media.j3d.Appearance;
import javax.media.j3d.Geometry;
import javax.media.j3d.Shape3D;

public abstract class Shape3Dp extends Shape3D{

	protected float[] scale = new float[3];
	protected float[] additionalParameters;

	public Shape3Dp(){
		super();
		this.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
	}

	abstract void updateAdditionalParameters(float[] dim);

	abstract Appearance buildAppearance();
	
	abstract Geometry buildGeometry(float[] additionalParameters,float[] scale);
	
	abstract void updateScale(float[] scale);
	
	abstract void updateGeometry(float[] additionalParameters, float[] scale);
	
}