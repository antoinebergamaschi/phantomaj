/**
* Pyramid3d.java
* 25/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.Geometry;
import javax.media.j3d.Material;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TransparencyAttributes;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;

public class Pyramid3d extends Shape3D{

	public Pyramid3d(){
		super();
		this.setAppearance(buildAppearance());
		this.setGeometry(buildGeometry());
	}
	
	private Appearance buildAppearance(){
		Appearance appearance = new Appearance();
		appearance.setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
		
		//Color overrid by lightning
		Color3f color = new Color3f(0.5f,0.5f,0.5f);
		// Color3f color = new Color3f(1.0f,1.0f,1.0f);
		appearance.setColoringAttributes(new ColoringAttributes(color, ColoringAttributes.NICEST));
		appearance.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.NONE,0.0f) );
		

		RenderingAttributes render = new RenderingAttributes(true,true,1.0f,RenderingAttributes.EQUAL);
		// render.setIgnoreVertexColors(true); 

		appearance.setRenderingAttributes(render);
		
		
		Material mat = new Material();
		mat.setCapability(Material.ALLOW_COMPONENT_WRITE);
		// No ambient light
		mat.setAmbientColor(new Color3f(0.0f,0.0f,0.0f));
		mat.setEmissiveColor(new Color3f(0.5f,0.5f,0.5f));
		mat.setDiffuseColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setSpecularColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setShininess(30.0f);
		
		appearance.setMaterial(mat);
		return appearance;
	}
	
	private Geometry buildGeometry(){
		int nbPointSommet = 6; 
		int nbPointBase = 6; 
		int nbFan = 2;
		
		int[] stripVertexCount = new int[nbFan];
		stripVertexCount[0] = nbPointSommet;
		stripVertexCount[1] = nbPointBase;
		
		Point3f[] points = new Point3f[nbPointSommet+nbPointBase];
		
		
		points[0] = new Point3f(0.0f,-1.0f,0.0f);
		points[1] = new Point3f(-1.0f,1.0f,1.0f);
		points[2] = new Point3f(-1.0f,1.0f,-1.0f);
		points[3] = new Point3f(1.0f,1.0f,-1.0f);
		points[4] = new Point3f(1.0f,1.0f,1.0f);
		points[5] = new Point3f(-1.0f,1.0f,1.0f);

		
		points[6] = new Point3f(0.0f,1.0f,0.0f);
		points[7] = new Point3f(-1.0f,1.0f,-1.0f);
		points[8] = new Point3f(-1.0f,1.0f,1.0f);
		points[9] = new Point3f(1.0f,1.0f,1.0f);
		points[10] = new Point3f(1.0f,1.0f,-1.0f);
		points[11] = new Point3f(-1.0f,1.0f,-1.0f);

		GeometryInfo triangleFanArray = new GeometryInfo(GeometryInfo.TRIANGLE_FAN_ARRAY);
		triangleFanArray.setStripCounts(stripVertexCount);
		// new TriangleFanArray(nbPointSommet+nbPointBase,TriangleFanArray.COORDINATES,stripVertexCount);
		triangleFanArray.setCoordinates(points);
			
		new NormalGenerator().generateNormals(triangleFanArray);
		
		return triangleFanArray.getGeometryArray();
	}
}