/**
* Form3D.java
* 12/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;
import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.Material;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.StaticField_Form;


public class Form3D extends TransformGroup{
	private int formID;
	private Appearance appearance;
	private Transform3D transform;
	
	public Form3D(){
		super();
		this.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
		this.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	}

	public void addForm(int formID){
		this.formID = formID;
		if ( !StaticField_Form.addForm3D(formID, this) ){
			System.out.println("In order to not Bug this program we add a Shpere instead of your form");
			StaticField_Form.addForm3D(StaticField_Form.SPHERE, this);
		}
	}
	
	/**
	*@deprecate
	**/
	public void setFiberInfo(float AngleFlex){
		System.out.println("DEPRECATE :: Form3D.setFiberInfo");
		// ((Fiber3d)this.getChild(0)).updateGeometry(-AngleFlex);
	}
	
	public void setAdditionalParameters(float[] addDim){
		((Shape3Dp)this.getChild(0)).updateAdditionalParameters(addDim);
	}
	
	public void remove(int position){
		this.removeChild(position);
	}

	public int getFormID(){
		return this.formID;
	}
	
	/**
	* Creat a new Transform3D used to Move or Modify dynamically a Shape3D, Warning Inverted Axe between ImageJ and j3d, x=x, y=-y,z=-z
	* @param angle, int[] phi,theta,psi
	* @param position, float[] X,Y,Z position in the stack, Warning : relative position where X = X(real)/(dimStackX/2)...
	* @param dimension, float[] dimX,dimY,dimZ the three dimension of this Shape3D, Warning : relative dimension where dimX = dimX(real)/dimStackX ...
	* @param rotate, Quat4f the rotation currently display by the view
	* @param scale, float the scale currently use in the view
	**/
	public void setTransformation(int[] angle, float[] position, float[] dimension , Matrix3f rotate, float scaleAll){
	
		transform = new Transform3D();
		Transform3D tempRotate = new Transform3D();
		
		switch(this.formID){
			case 0:
				tempRotate = new Transform3D();
				//transform.rotZ(Math.toRadians(angle[0]));
				tempRotate.rotZ(Math.toRadians(-angle[0]));
				//multiplie x et Y
				transform.mul(tempRotate);

				tempRotate = new Transform3D();
				tempRotate.rotY(Math.toRadians(-angle[1]));

				//multiplie les 3 angles
				transform.mul(tempRotate);

				tempRotate = new Transform3D();
				tempRotate.rotZ(Math.toRadians(-angle[2]));

				transform.mul(tempRotate);
				break;
			default:
				//First Rotation use to display the same object as in the ImageJ stack
				transform.rotX(Math.toRadians(90));
				tempRotate = new Transform3D();
				//transform.rotZ(Math.toRadians(angle[0]));
				tempRotate.rotY(Math.toRadians(-angle[0]));
				//multiplie x et Y
				transform.mul(tempRotate);
				
				tempRotate = new Transform3D();
				tempRotate.rotZ(Math.toRadians(angle[1]));
				//multiplie les 3 angles
				transform.mul(tempRotate);
			
				tempRotate = new Transform3D();
				tempRotate.rotY(Math.toRadians(-angle[2]));
				
				transform.mul(tempRotate);
				Quat4f q1 = new Quat4f();
				transform.get(q1); 
				// System.out.println(q1.toString());
				break;
		}

		// Warning inversion of the Y/Z axes in the ImageJ stack and j3D representation
		Vector3f pos = new Vector3f(((position[0])-1.0f)*scaleAll,(((position[1])-1.0f)*-1.0f)*scaleAll,(((position[2])-1.0f)*-1.0f)*scaleAll);
		// add the translation
		transform.setTranslation(pos);
	
		Vector3d scale = new Vector3d ();
		// set this form relative dimension 
		switch(this.formID){
			case 0:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[1]*(double)scaleAll,(double)dimension[2]*(double)scaleAll);
				break;
			case 1:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[2]*2*(double)scaleAll,(double)dimension[1]*(double)scaleAll);
				break;
			case 2:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[2]*(double)scaleAll,(double)dimension[1]*(double)scaleAll);
				// System.out.println("Pyramid");
				break;
			case 3:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[1]*(double)scaleAll,(double)dimension[2]*(double)scaleAll);
				break;
			case 4:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[2]*2*(double)scaleAll,(double)dimension[0]*(double)scaleAll);
				break;
			case 5:
				scale = new Vector3d ((double)dimension[0]*(double)scaleAll,(double)dimension[2]*(double)scaleAll,(double)dimension[1]*(double)scaleAll);
				break;
			default:
				scale = new Vector3d ((double)scaleAll,(double)scaleAll,(double)scaleAll);
				if ( StaticField_Form.CODE_ADDITIONAL_FORM[this.formID] != null ){
					((Shape3Dp)this.getChild(0)).updateScale(dimension);
				}
				break;
		}
		transform.setScale(scale);
		
		this.setTransform(transform); 
	}
	
	/**
	* Build the Basic Appearance for Each Shape3D
	**/
	public Appearance getAppearance(){
		appearance = new Appearance();
		// appearance.setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
		
		//Color overrid by lightning
		// Color3f color = new Color3f(0.5f,0.5f,0.5f);
		Color3f color = new Color3f(1.0f,1.0f,1.0f);
		appearance.setColoringAttributes(new ColoringAttributes(color, ColoringAttributes.NICEST));
		appearance.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.NONE,0.0f) );
		

		RenderingAttributes render = new RenderingAttributes(true,true,1.0f,RenderingAttributes.EQUAL);
		appearance.setRenderingAttributes(render);
		
		
		Material mat = new Material();
		mat.setCapability(Material.ALLOW_COMPONENT_WRITE);
		// No ambient light
		mat.setAmbientColor(new Color3f(0.0f,0.0f,0.0f));
		mat.setEmissiveColor(new Color3f(0.5f,0.5f,0.5f));
		mat.setDiffuseColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setSpecularColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setShininess(30.0f);
		
		appearance.setMaterial(mat);
		return appearance;
	}
	
	public void updateAppearance(Color3f newColor){
		if ( this.formID == 2 ){
			((Pyramid3d)this.getChild(0)).getAppearance().getMaterial().setEmissiveColor(newColor);
		}
		else if ( this.formID == 6 ){
			((Fiber3d)this.getChild(0)).getAppearance().getMaterial().setEmissiveColor(newColor);
		}
		else{
			this.appearance.getMaterial().setEmissiveColor(newColor);
		}
	}
	
}