/**
* Fiber3d.java
* 25/06/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.Geometry;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.Material;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.TransparencyAttributes;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;

public class Fiber3d extends Shape3Dp {

	private float angleFlex=0.0f;
	
	public Fiber3d(float[] additionalParameters){
		super();
		// this.setCapability(Shape3D.ALLOW_GEOMETRY_WRITE);
		this.setGeometry(buildGeometry(additionalParameters,new float[3]));
		this.setAppearance(buildAppearance());
	}
	
	public Appearance buildAppearance(){
		Appearance appearance = new Appearance();
		appearance.setCapability(Appearance.ALLOW_COLORING_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
		appearance.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
		
		//Color overrid by lightning
		Color3f color = new Color3f(0.5f,0.5f,0.5f);
		// Color3f color = new Color3f(1.0f,1.0f,1.0f);
		appearance.setColoringAttributes(new ColoringAttributes(color, ColoringAttributes.NICEST));
		appearance.setTransparencyAttributes(new TransparencyAttributes(TransparencyAttributes.NONE,0.0f) );
		

		RenderingAttributes render = new RenderingAttributes(true,true,1.0f,RenderingAttributes.EQUAL);
		// render.setIgnoreVertexColors(true); 

		appearance.setRenderingAttributes(render);
		
		
		Material mat = new Material();
		mat.setCapability(Material.ALLOW_COMPONENT_WRITE);
		// No ambient light
		mat.setAmbientColor(new Color3f(0.0f,0.0f,0.0f));
		mat.setEmissiveColor(new Color3f(0.5f,0.5f,0.5f));
		mat.setDiffuseColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setSpecularColor(new Color3f(1.0f,1.0f,1.0f));
		mat.setShininess(30.0f);
		
		appearance.setMaterial(mat);
		return appearance;
	}
	
	public Geometry buildGeometry(float[] additionalParameters,float[] scale){
		this.angleFlex = -additionalParameters[0];
		this.additionalParameters = additionalParameters;
		this.scale = scale;
		
		float diameterScale = this.scale[0];
		float lenghtScale = this.scale[1];
	
	//parametrage de la finesse de dessin
		int nbSection = 50;
		int nbPointBand = 4*nbSection + 2;
		int nbBand = 18;

		double _radius;

		//null angle exception
		if ( angleFlex == 0.0f ){
			angleFlex = 0.001f;
		}
		
		//Scale compute parameters
		//0=> scale min, 1=>scale max, basic Scale => 0.5
		// diameterScale = 1 + diameterScale;
		lenghtScale = 2*lenghtScale;
		
		
		_radius = ((360/angleFlex)*lenghtScale)/(Math.PI*2) ;

		
		int[] stripVertexCount = new int[nbBand];

		for ( int i = 0 ; i < nbBand ; i++ ){
			stripVertexCount[i] = nbPointBand;
		}

		
		Point3f[] points = new Point3f[nbPointBand*nbBand];
		double rad = Math.toRadians(360/(nbBand));
//		double dimSection = 2.0/nbSection;
		// boolean bol = true;
		
//		double cosAngleFlex = Math.cos(Math.toRadians(angleFlex/2));
//		double sinAngleFlex = Math.sin(Math.toRadians(angleFlex/2));
		
		double posCenterEcartX_b = ( -_radius* Math.cos(Math.toRadians(-angleFlex/2)) ) + _radius;
		double posCenterEcartZ_b =  - ( -_radius*Math.sin(Math.toRadians(-angleFlex/2)) );
		
		double posCenterEcartX_e = ( -_radius*Math.cos(Math.toRadians(angleFlex/2)) ) + _radius;
		double posCenterEcartZ_e =  - ( -_radius*Math.sin(Math.toRadians(angleFlex/2)) );
		
		// Building band around the Tube
		for ( int band = 0 ; band < nbBand ; band++ ){ 
			double posZ = Math.cos(rad*band)*(diameterScale);
			double posX = Math.sin(-rad*band)*(diameterScale);
			double posZ_2 = Math.cos(rad*(band+1))*(diameterScale);
			double posX_2 = Math.sin(-rad*(band+1))*(diameterScale);
		
			int position = band * nbPointBand;
			
			points[position] = new Point3f((float)posCenterEcartX_b,(float)posCenterEcartZ_b,0.0f);
			double toAddx = 0;
			double toAddz = 0;
			double toAddx_2 = 0;
			double toAddz_2 = 0;
			
			
			for ( int section = 0 ; section < nbSection*2  ; section++ ){
				position = band * nbPointBand + section*2;//section*4
				
				double angle = Math.toRadians(((angleFlex/nbSection)*((section + section%2)/2))-angleFlex/2);
				

				
				toAddx = ((posX - _radius)*Math.cos(angle)) + _radius ;
				toAddz = -( (posX - _radius)*Math.sin(angle) ) ;
				
				toAddx_2 = ( (posX_2 - _radius)*Math.cos(angle) ) + _radius ;
				toAddz_2 = -( (posX_2 - _radius)*Math.sin(angle) ) ;
				
				
				points[position+1] = new Point3f((float)toAddx,(float)toAddz, (float)posZ);
				points[position+2] = new Point3f((float)toAddx_2,(float)toAddz_2, (float)posZ_2);
				
			}
			
			points[((band+1) * nbPointBand)-1] = new Point3f((float)posCenterEcartX_e,(float)posCenterEcartZ_e,0.0f);

		}
		
		GeometryInfo triangleArray = new GeometryInfo(GeometryInfo.TRIANGLE_STRIP_ARRAY);
		triangleArray.setStripCounts(stripVertexCount);
		triangleArray.setCoordinates(points);

		new NormalGenerator().generateNormals(triangleArray);
		
		GeometryArray geom = triangleArray.getGeometryArray(true,false,false);

		return geom;
	}
	
	
	public void updateGeometry(float[] additionalParameters, float[] scale){
		this.setGeometry(buildGeometry(additionalParameters,scale));
	}

	

	public void updateAdditionalParameters(float[] additionalParameters){
		this.setGeometry(buildGeometry(additionalParameters,this.scale));
	}
	
	
	public void updateScale(float[] scale){
		float[] addDim = new float[2];
		addDim[0] = scale[0];
		addDim[1] = scale[2];
		this.setGeometry(buildGeometry(this.additionalParameters,addDim));
	}
	
	
}