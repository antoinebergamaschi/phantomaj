/**
* CubeStack.java
* 12/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.Appearance;
import javax.media.j3d.Geometry;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineArray;
import javax.media.j3d.Shape3D;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;

public class CubeStack extends Shape3D{

	public CubeStack(){
		super();
		this.setGeometry(buildGeometry());
		this.setAppearance(buildAppearance());
	}

	private Geometry buildGeometry(){
        IndexedLineArray stack = new IndexedLineArray(8, GeometryArray.COORDINATES | GeometryArray.COLOR_3 ,24);
		Color3f red = new Color3f(1.0f, 0.0f, 0.0f);
		
		//DataArray
	    stack.setCoordinate( 0, new Point3f(-1.0f, 1.0f, 1.0f));
	    stack.setCoordinate( 1, new Point3f( 1.0f, 1.0f, 1.0f));
	    stack.setCoordinate( 2, new Point3f( 1.0f, -1.0f, 1.0f));
	    stack.setCoordinate( 3, new Point3f( -1.0f,-1.0f, 1.0f));
	    stack.setCoordinate( 4, new Point3f( -1.0f, 1.0f,-1.0f));
	    stack.setCoordinate( 5, new Point3f( 1.0f,1.0f,-1.0f));
	    stack.setCoordinate( 6, new Point3f( 1.0f,-1.0f, -1.0f));
	    stack.setCoordinate( 7, new Point3f( -1.0f, -1.0f, -1.0f));

		stack.setColor( 0, red);
		stack.setColor( 1, red);
		stack.setColor( 2, red);
		stack.setColor( 3, red);
		stack.setColor( 4, red);
		stack.setColor( 5, red);
		stack.setColor( 6, red);
		stack.setColor( 7, red);
		
		// IndexArray
		stack.setCoordinateIndex( 0, 0);
		stack.setCoordinateIndex( 1, 1);
		stack.setCoordinateIndex( 2, 2);
		stack.setCoordinateIndex( 3, 3);
		
		stack.setCoordinateIndex( 4, 0);
		stack.setCoordinateIndex( 5, 3);
		stack.setCoordinateIndex( 6, 7);
		stack.setCoordinateIndex( 7, 4);
		
		stack.setCoordinateIndex( 8, 0);
		stack.setCoordinateIndex( 9, 4);
		stack.setCoordinateIndex(10, 5);
		stack.setCoordinateIndex(11, 1);
		
		stack.setCoordinateIndex(12, 1);
		stack.setCoordinateIndex(13, 2);
		stack.setCoordinateIndex(14, 6);
		stack.setCoordinateIndex(15, 5);
		
		stack.setCoordinateIndex(16, 4);
		stack.setCoordinateIndex(17, 5);
		stack.setCoordinateIndex(18, 6);
		stack.setCoordinateIndex(19, 7);
		
		stack.setCoordinateIndex(20, 3);
		stack.setCoordinateIndex(21, 7);
		stack.setCoordinateIndex(22, 6);
		stack.setCoordinateIndex(23, 2);
		
		
		return stack;
	} 
	
	private Appearance buildAppearance(){
		Appearance appearance = new Appearance();
		return appearance;
	}
	
}