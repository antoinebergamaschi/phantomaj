/**
* TextAxe3D.java
* 12/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import java.awt.Font;

import javax.media.j3d.Appearance;
import javax.media.j3d.Font3D;
import javax.media.j3d.FontExtrusion;
import javax.media.j3d.Geometry;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Text3D;
import javax.vecmath.Point3f;

public class TextAxe3D extends Shape3D{


	public TextAxe3D(int coordonate){
		super();
		this.setGeometry(buildGeometry(coordonate));
		this.setAppearance(buildAppearance());
	}


	private Geometry buildGeometry(int coordonate){
		Font3D font =new Font3D(new Font("Helvetica",Font.PLAIN,1),new FontExtrusion());
		String text = null;
		Point3f pos = new Point3f(1.0f,1.0f,1.0f);
		switch(coordonate){
			case 0:
				pos = new Point3f(0.90f*10,0.90f*10,1.0f*10);
				text = "X";
				break;
			case 1:
				pos = new Point3f(-0.90f*10,-0.90f*10,1.0f*10);
				text = "Y";
				break;
			case 2:
				pos = new Point3f(-0.90f*10,0.90f*10,-1.0f*10);
				text = "Z";
				break;
			case 3:
				pos = new Point3f(-0.90f*10,0.90f*10,1.0f*10);
				text ="O";
				break;
		}
		Text3D coor = new Text3D(font , text , pos);
		return coor;
	}
	
	private Appearance buildAppearance(){
		Appearance appearance = new Appearance();
		return appearance;
	}

}