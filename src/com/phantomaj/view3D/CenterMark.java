/**
* CenterMark.java
* 20/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.Appearance;
import javax.media.j3d.Geometry;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedLineArray;
import javax.media.j3d.Shape3D;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;

public class CenterMark extends Shape3D{


	public CenterMark(){
		super();
		this.setGeometry(buildGeometry());
		this.setAppearance(buildAppearance());
	}

	private Geometry buildGeometry(){
        IndexedLineArray stack = new IndexedLineArray(8, GeometryArray.COORDINATES | GeometryArray.COLOR_3 ,8);
		Color3f blue = new Color3f(0.0f, 0.0f, 1.0f);
		
		//DataArray
	    stack.setCoordinate( 0, new Point3f(-1.0f, 1.0f, 1.0f));
	    stack.setCoordinate( 1, new Point3f( 1.0f, 1.0f, 1.0f));
	    stack.setCoordinate( 2, new Point3f( 1.0f, -1.0f, 1.0f));
	    stack.setCoordinate( 3, new Point3f( -1.0f,-1.0f, 1.0f));
	    stack.setCoordinate( 4, new Point3f( -1.0f, 1.0f,-1.0f));
	    stack.setCoordinate( 5, new Point3f( 1.0f,1.0f,-1.0f));
	    stack.setCoordinate( 6, new Point3f( 1.0f,-1.0f, -1.0f));
	    stack.setCoordinate( 7, new Point3f( -1.0f, -1.0f, -1.0f));

		stack.setColor( 0, blue);
		stack.setColor( 1, blue);
		stack.setColor( 2, blue);
		stack.setColor( 3, blue);
		stack.setColor( 4, blue);
		stack.setColor( 5, blue);
		stack.setColor( 6, blue);
		stack.setColor( 7, blue);
		
		// IndexArray
		stack.setCoordinateIndex( 0, 0);
		stack.setCoordinateIndex( 1, 6);
		
		stack.setCoordinateIndex( 2, 1);
		stack.setCoordinateIndex( 3, 7);
		
		stack.setCoordinateIndex( 4, 2);
		stack.setCoordinateIndex( 5, 4);
		
		stack.setCoordinateIndex( 6, 3);
		stack.setCoordinateIndex( 7, 5);
		
		
		
		return stack;
	} 
	
	private Appearance buildAppearance(){
		Appearance appearance = new Appearance();
		return appearance;
	}

}