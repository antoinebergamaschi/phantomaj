/**
* Center3D.java
* 13/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.behaviors.mouse.MouseBehaviorCallback;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;

public class Center3D extends BranchGroup implements MouseBehaviorCallback{
	
	private float scaleAll;
	private TransformGroup centerTransfo;
	
	public Center3D(float[] center,float scaleAll){
		super();
		this.scaleAll = scaleAll;
		this.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		this.setCapability(BranchGroup.ALLOW_DETACH);
		setTransformGroup(center);
		this.compile();
	}

	private void setTransformGroup(float [] center){
		centerTransfo = new TransformGroup();
		this.addChild(centerTransfo);
		
		TransformGroup centerR = new TransformGroup();
		centerR.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		
		
		this.centerTransfo.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		this.centerTransfo.setCapability( TransformGroup.ALLOW_TRANSFORM_READ );
		this.centerTransfo.setCapability(TransformGroup.ENABLE_PICK_REPORTING);

		BoundingSphere bounds = new BoundingSphere();
		MouseRotate behavior = new MouseRotate(centerTransfo);
		centerTransfo.addChild(behavior);
		behavior.setSchedulingBounds(bounds);
		MouseZoom behavior2 = new MouseZoom(centerTransfo);
		centerTransfo.addChild(behavior2);
		behavior2.setSchedulingBounds(bounds);
		
		behavior.setupCallback(this);		
		behavior2.setupCallback(this);
		
		//sphere pour le moment peut-etre une croix ?
		centerR.addChild(new CenterMark());
		
		Transform3D transform = new Transform3D();
		Vector3f pos = new Vector3f(((center[0])-1.0f)*scaleAll,(((center[1])-1.0f)*-1.0f)*scaleAll,(((center[2])-1.0f)*-1.0f)*scaleAll);

		// add the translation
		transform.setTranslation(pos);
		transform.setScale(0.02f);
		centerR.setTransform(transform);
		this.centerTransfo.addChild(centerR);

	}
	
	
	
	
	/** 
	* Override MouseBehaviorCallback.transformChanged
	**/
	public void transformChanged(int type, Transform3D transform){
		if ( type == MouseBehaviorCallback.ROTATE ){ 
			// System.out.println("Rotate");
		}
		if ( type == MouseBehaviorCallback.ZOOM ){
			// System.out.println("Zoom");
		}
	}
	
	public void updateAllRotation(Matrix3f rot,Matrix3f rotate){
		Transform3D transform = new Transform3D();
		transform.setIdentity();
		Transform3D rotT = new Transform3D();

		rotT = new Transform3D();
		rotT.setRotation(rot);
		
		transform.mul(rotT);
		this.centerTransfo.setTransform(transform);
	}
	
}