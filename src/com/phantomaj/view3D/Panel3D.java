
/**
* Panel3D.java
* 10/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import com.phantomaj.utils.StaticField_Form;
import com.sun.j3d.utils.behaviors.mouse.MouseBehaviorCallback;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;
import com.sun.j3d.utils.universe.SimpleUniverse;

import javax.media.j3d.*;
import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Point3f;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Panel3D extends JPanel implements MouseBehaviorCallback{


	// private Form3D obj;
	private Obj3D obj_rand = null;
	
	private Center3D center;
	private ArrayList<Obj3D> phantomArray ;
	private TransformGroup phantom;
	private Transform3D phantomTransform;
	private SimpleUniverse simpleU;
	private BranchGroup root;
	private PointLight light;
	private boolean isInBuild = false ;
	private float scale=0.5f;
	private Matrix3f rotate = new Matrix3f(0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f);
	// private DirectionalLight  light ;
	
	private long time=0;
	
	public Panel3D(){
		super();
		this.setPreferredSize(new Dimension(400,500));
		// this.setMaximumSize(new Dimension(500,10000));
		this.setLayout(new BorderLayout());
		// this.setBackground(Color.red);
		GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration();
		Canvas3D canvas3D = new Canvas3D(config);
		this.add(canvas3D,BorderLayout.CENTER);
		root = createSceneGraph();
		root.compile();

		// SimpleUniverse is a Convenience Utility class
		simpleU = new SimpleUniverse(canvas3D);

		// This moves the ViewPlatform back a bit so the
		// objects in the root can be viewed.
		simpleU.getViewingPlatform().setNominalViewingTransform();
		simpleU.addBranchGraph(root);
		
		
		phantomArray = new ArrayList<Obj3D>();
	} 

	
	
     public BranchGroup createSceneGraph() {
		// Create the root of the branch graph
		BranchGroup objRoot = new BranchGroup();
		objRoot.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		objRoot.setCapability(BranchGroup.ALLOW_DETACH); 
		
		//Color of the light, position, attenuation ( 1 , 0 , 0) = constant
		light=new PointLight(new Color3f(Color.GRAY),new Point3f(2.0f,2.0f,2.0f),new Point3f(1f,0f,0f));
		light.setInfluencingBounds(new BoundingSphere());
		objRoot.addChild(light); 
		
		light.setCapability(PointLight.ALLOW_POSITION_WRITE); 
		
		// Create the transform group node and initialize it to the
		// identity.  Enable the TRANSFORM_WRITE capability so that
		// our behavior code can modify it at runtime.  Add it to the
		// root of the subgraph.
		phantom = new TransformGroup();
		phantom.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	
		objRoot.addChild(phantom);
	 
		
		//Add Red Cube, representing the Stack size
		
		phantom.addChild(new CubeStack());
		// phantom.addChild(new Pyramid3d());
		
		//Set the scale for the cube to be entierly view in the window
		phantomTransform = new Transform3D();
		phantomTransform.set(this.scale);
		phantom.setTransform(phantomTransform);
		
		//Add Axe's name
		TransformGroup axeName = new TransformGroup();
		Transform3D scaleText = new Transform3D();
		scaleText.set(0.1f);
		
		axeName.addChild(new TextAxe3D(0));
		axeName.addChild(new TextAxe3D(1));
		axeName.addChild(new TextAxe3D(2));
		axeName.addChild(new TextAxe3D(3));
		axeName.setTransform(scaleText);
		
		phantom.addChild(axeName);
		// // this.obj = new Form3D();

		// // objSpin.addChild(obj);
		// // obj.addForm(0);
		
		// // Create a new Behavior object that will perform the desired
		// // operation on the specified transform object and add it into
		// // the scene graph.
		// Transform3D yAxis = new Transform3D();
		// Alpha rotationAlpha = new Alpha(-1, 10000);

		// RotationInterpolator rotator = new RotationInterpolator(rotationAlpha, phantom, yAxis,0.0f, (float) Math.PI*2.0f);

		// // a bounding sphere specifies a region a behavior is active
		// // create a sphere centered at the origin with radius of 1
		BoundingSphere bounds = new BoundingSphere();
		// rotator.setSchedulingBounds(bounds);
		// phantom.addChild(rotator);
		MouseRotate behavior = new MouseRotate(phantom);
		phantom.addChild(behavior);
		behavior.setSchedulingBounds(bounds);
		MouseZoom behavior2 = new MouseZoom(phantom);
		phantom.addChild(behavior2);
		behavior2.setSchedulingBounds(bounds);
		
		behavior.setupCallback(this);		
		behavior2.setupCallback(this);
		
		return objRoot;
	}
	

	/**
	* Update the Value of the selected Form3D, Warning if the transformation result in a change of geometry a new Form3D will be creat
	**/
	public void update(int[] angle, float[] position,float[] dimension,int formID, int posObj, int posForm){
		if ( formID != ((Form3D)(this.phantomArray.get(posObj)).getChildren(posForm)).getFormID() ){
			//detach and Delete the old form
			this.phantomArray.get(posObj).detach();
			this.phantomArray.get(posObj).delete(posForm);
			// Creat the new Form3D
			Form3D form = new Form3D();
			form.addForm(formID);
			
			if ( StaticField_Form.CODE_ADDITIONAL_FORM[formID] != null ){
				float[] addDim = new float[StaticField_Form.CODE_ADDITIONAL_FORM[formID].length];
				System.arraycopy(dimension,3,addDim,0,addDim.length);
				// System.out.println(addDim[0]);
				form.setAdditionalParameters(addDim);
			}
			
			form.setTransformation(angle,position,dimension,rotate,scale);
			//Add and attach the new Form3D
			this.phantomArray.get(posObj).add(form,posForm);
			this.simpleU.addBranchGraph(this.phantomArray.get(posObj));
			//Add the color for this Form3D
			this.updateColorForm(posObj,posForm,0);
		}
		else{
			if ( StaticField_Form.CODE_ADDITIONAL_FORM[formID] != null ){
				float[] addDim = new float[StaticField_Form.CODE_ADDITIONAL_FORM[formID].length];
				System.arraycopy(dimension,3,addDim,0,addDim.length);
				((Form3D)(this.phantomArray.get(posObj)).getChildren(posForm)).setAdditionalParameters(addDim);
			}	
			((Form3D)(this.phantomArray.get(posObj)).getChildren(posForm)).setTransformation(angle,position,dimension,rotate,scale);
		}
	}
	
	
	/**
	* Add a new Form3D in the canvas3D
	**/
	public void addForm(int[] angle, float[] position,float[] dimension,int identifier, int posObj){
	
		//for simplicity axe are reset to the matrix identity
		this.reset(0);
		Form3D form = new Form3D();
		// System.out.println(identifier);
		form.addForm(identifier);
		form.setTransformation(angle,position,dimension,rotate,scale);
		this.phantomArray.get(posObj).detach();
		this.phantomArray.get(posObj).add(form,-1);
		if ( ! isInBuild ){
			this.simpleU.addBranchGraph(this.phantomArray.get(posObj));
		}
		
	}
	
	/**
	* Add a new Form3D in the canvas3D
	**/
	public void addForm_randS(int[] angle, float[] position,float[] dimension,int identifier,float angleFlex, int posObj){
	
		//for simplicity axe are reset to the matrix identity
		this.reset(0);
		Form3D form = new Form3D();
		// System.out.println(identifier);
		form.addForm(identifier);
		form.setTransformation(angle,position,dimension,rotate,scale);
		if ( identifier == 6 ){
			form.setFiberInfo(angleFlex);
		}
		this.phantomArray.get(posObj).detach();
		this.phantomArray.get(posObj).add(form,-1);
		if ( ! isInBuild ){
			this.simpleU.addBranchGraph(this.phantomArray.get(posObj));
		}
		
	}
	
	/**
	* Add a new Form3D in the canvas3D ( use in RandomGeneratorFrame )
	**/
	public void addForm_rand(int[] angle, float[] position,float[] dimension,int identifier,float angleFlex, int posObj){
		
		//for simplicity axe are reset to the matrix identity
		this.reset(0);
		Form3D form = new Form3D();
		// System.out.println(identifier);
		form.addForm(identifier);
		form.setTransformation(angle,position,dimension,rotate,scale);
		this.obj_rand.detach();
		if ( identifier == 6 ){
			form.setFiberInfo(angleFlex);
		}
		this.obj_rand.add(form,-1);
		if ( ! isInBuild ){
			this.simpleU.addBranchGraph(this.obj_rand);
		}
		
	}
	
    //Debug Time
    public void printDelay(){
		System.out.println("Temps Passez pour l'ajout Pane3D  L195 Panel3D:: "+time);
    }
	
	/**
	* Add a new TransformGroup as an Object containing form
	**/
	public void addObject(){
		Obj3D obj = new Obj3D();
		this.phantomArray.add(obj);
		this.simpleU.addBranchGraph(obj);
	}
	
	/**
	* Add and save a new TransformGroup as an Object containing form ( use in RandomGeneratorFrame ) 
	**/
	public void addObject_rand(){
		this.obj_rand = null;
		this.obj_rand = new Obj3D();
		this.simpleU.addBranchGraph(obj_rand);
	}
	
	/**
	*
	**/
	public void initializeArrayListObject(int size){
		this.phantomArray = new ArrayList<Obj3D>(size);
	}
	
	/**
	* Clear the view3D
	**/
	public void clearView(){
		for ( int i=0 ; i < phantomArray.size() ; i++ ){
			this.phantomArray.get(i).detach();
		}
		this.phantomArray = new ArrayList<Obj3D>();
		
		if ( obj_rand != null ){
			obj_rand.detach();
		}
	}
	
	public void clearView_rand(){
		for ( int i=0 ; i < phantomArray.size() ; i++ ){
			this.phantomArray.get(i).detach();
		}
		if ( obj_rand != null ){
			obj_rand.detach();
		}
	}
	
	public void printS(){
		System.out.println("Size of phantomArray :: "+this.phantomArray.size());
	}
	
	/**
	* Update the color of this selected Form3D
	**/
	public void updateColorForm(int posObj, int posForm, int color){
		this.updateColorObj(posObj);
		this.phantomArray.get(posObj).updateColor(posForm,color);
	}
	
	/**
	* Update the color of the selected Object
	**/
	public void updateColorObj(int posObj){
		for ( int i=0 ; i < phantomArray.size() ; i++ ){
			if ( i == posObj ){
				this.phantomArray.get(posObj).updateColorAll(1);
			}
			else{
				this.phantomArray.get(i).updateColorAll(2);
			}
		}
	}
	
	/**
	* Delete an Obj3D object
	**/
	public void deleteObj(int posObj){
		this.phantomArray.get(posObj).detach();
		this.phantomArray.remove(posObj);
	}
	
	/**
	* Delete an Form3D object in this Obj3D
	**/
	public void deleteForm(int posObj, int posForm){
		this.phantomArray.get(posObj).detach();
		this.phantomArray.get(posObj).delete(posForm);
		this.simpleU.addBranchGraph(this.phantomArray.get(posObj));
	}
	
	
	/**
	* Reset the rotation position to the plan x/y,0 ; x/z,1 ; y/z,2
	**/
	public void reset(int axe){
		Matrix3f mat = new Matrix3f();
		switch(axe){
			case 0:
				//the Matrix identity
				mat.setIdentity();
				break;
			case 1:
				//the Matrix identity multiply by 90 vers y
				// mat.rotX(90.0f);
				mat.set(new Matrix3f(1.0f,0.0f,0.0f,0.0f,-0.0f,-1.0f,-0.0f,1.0f,-0.0f));
				break;
			case 2:
				//mat.rotY(90.0f)
				mat.set(new Matrix3f(-0.0f,-0.0f,1.0f,0.0f,1.0f,0.0f,-1.0f,0.0f,-0.0f));
				break;
		}
		this.phantomTransform.setRotation(mat);
		this.phantom.setTransform(this.phantomTransform);
		try{
			this.center.updateAllRotation(mat,this.rotate);
		}
		catch(NullPointerException e){
			// System.out.println("No center to update L 405 Panel3D.reset");
		}
		for ( int i = 0 ; i  < this.phantomArray.size() ; i++ ){
			this.phantomArray.get(i).updateAllRotation(mat,this.rotate);
		}
		if ( this.obj_rand != null ){
			this.obj_rand.updateAllRotation(mat,this.rotate);
		}
		
		
	}
	
	/**
	* Add a point3D corresponding white the coordinate of this center
	**/
	public void addCenter(float[] center, int formID ,int posObj){
		if ( formID == -1 ){
			this.reset(0);
			try{
				//delete the center and reset the form Color of this object
				this.center.detach();
				this.updateColorObj(posObj);
			}
			//if there is no center instanced yet
			catch(NullPointerException e){
				System.out.println("NullPointerException : addCenter 289, first initialization");
			}
			//create a new center and add it in the Universe
			this.center = new Center3D(center,this.scale);
			this.simpleU.addBranchGraph(this.center);
		}
		else{
			try{
				this.center.detach();
			}
			//if there is no center instanced yet
			catch(NullPointerException e){
			
			}
			// System.out.println("Update Color");
			this.updateColorForm(posObj,formID,3);
		}
	}
	
	/**
	* remove the center
	**/
	public void removeCenter(){
		try{
			this.center.detach();
		}
		catch(NullPointerException e){
			// System.out.println("NullPointerException : removeCenter 316, No Center to remove");
		}
		finally{
			this.updateColorObj(-1);
		}
	}
	
	/** 
	* @Override MouseBehaviorCallback.transformChanged
	**/
	public void transformChanged(int type, Transform3D transform){
		if ( type == MouseBehaviorCallback.ROTATE ){ 
			rotate.setIdentity();
			transform.get(rotate);
		}
		if ( type == MouseBehaviorCallback.ZOOM ){
//			double scale = transform.getScale() ;
			// System.out.println(scale);
		}
	}
	
	public void setIsInBuild(boolean bol){
		this.isInBuild = bol;
		if ( ! bol ){
			if ( obj_rand != null ){
				this.simpleU.addBranchGraph(obj_rand);
			}
			else{
				for ( int i=0; i < this.phantomArray.size() ; i++ ){
					this.simpleU.addBranchGraph(this.phantomArray.get(i));
				}
			}
		}
	}
	
	public void setView3D(int id){
		this.clearView_rand();
		if ( id == 0){
			if ( obj_rand != null ){
				obj_rand.detach();
			}
			for ( int i=0; i< this.phantomArray.size() ; i++ ){
				this.simpleU.addBranchGraph(this.phantomArray.get(i));
			}
		}
		// else if ( obj_rand )
		else if ( id == 1 && obj_rand!=null){
			this.simpleU.addBranchGraph(obj_rand);
		}
		reset(0);
	}
	
}