/**
* Obj3D.java
* 13/04/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.view3D;

import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Matrix3f;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseZoom;


public class Obj3D extends BranchGroup{

	private TransformGroup form;
	private static final Color3f red = new Color3f(0.5f,0.0f,0.0f);
	private static final Color3f basicGrey = new Color3f(0.5f,0.5f,0.5f);
	private static final Color3f yellow = new Color3f(0.5f, 0.5f, 0.0f);
	private static final Color3f blue = new Color3f(0.0f, 0.0f, 0.5f);
	
	public Obj3D(){
		super();
		this.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		this.setCapability(BranchGroup.ALLOW_DETACH); 
		setTransformGroup();
		this.addChild(this.form);
	}

	private void setTransformGroup(){
		this.form = new TransformGroup();
		
		this.form.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		this.form.setCapability( TransformGroup.ALLOW_TRANSFORM_READ );
		this.form.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
		
		BoundingSphere bounds = new BoundingSphere();
		
		MouseRotate behavior = new MouseRotate(this.form);
		this.form.addChild(behavior);
		behavior.setSchedulingBounds(bounds);
		MouseZoom behavior2 = new MouseZoom(this.form);
		this.form.addChild(behavior2);
		behavior2.setSchedulingBounds(bounds);
	}
	
	/**
	* add this Form3D form in this transformGroup
	* @param form the Form3D
	* @param posForm int the position where to add this Form3D, Warning +2 because there already is 2 childs
	**/
	public void add(Form3D form, int posForm){
		if ( posForm == -1 ){
			this.form.addChild(form);
		}
		else{
			this.form.insertChild(form,posForm+2);
		}
	}
	
	public void delete(int index){
		//warning +2 because there is already 2 child in this TransformGroup
		this.form.removeChild(index+2);
	}
	
	public Form3D getChildren(int pos){
		//warning +2 because there is already 2 child in this TransformGroup
		return (Form3D)this.form.getChild(pos+2);
	}
	
	public void updateColor(int index, int color){
		switch(color){
			case 0:
				((Form3D)this.form.getChild(index+2)).updateAppearance(Obj3D.red);
				break;
			case 1:
				((Form3D)this.form.getChild(index+2)).updateAppearance(Obj3D.yellow);
				break;
			case 2:
				((Form3D)this.form.getChild(index+2)).updateAppearance(Obj3D.basicGrey);
				break;
			case 3:
				((Form3D)this.form.getChild(index+2)).updateAppearance(Obj3D.blue);
				break;	
		}
	}
		
	public void updateColorAll(int color){
		for ( int i = 2 ; i < this.form.numChildren() ; i++ ){
			switch(color){
				case 0:
					((Form3D)this.form.getChild(i)).updateAppearance(Obj3D.red);
					break;
				case 1:
					((Form3D)this.form.getChild(i)).updateAppearance(Obj3D.yellow);
					break;
				case 2:
					((Form3D)this.form.getChild(i)).updateAppearance(Obj3D.basicGrey);
					break;
				case 3:
					((Form3D)this.form.getChild(i)).updateAppearance(Obj3D.blue);
					break;	
			}
		}
	}
	
	public void updateAllRotation(Matrix3f rot,Matrix3f rotate){
		Transform3D transform = new Transform3D();
		transform.setIdentity();
		Transform3D rotT = new Transform3D();

		rotT = new Transform3D();
		rotT.setRotation(rot);
		
		transform.mul(rotT);
		this.form.setTransform(transform);
	}
}