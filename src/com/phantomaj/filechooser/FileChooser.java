package com.phantomaj.filechooser;


import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;


public class FileChooser extends JPanel{
	private JFileChooser fc;
	private String name;
	private File file;

	public FileChooser(int whitch) {
		super(new BorderLayout());
		if ( whitch == 0){
			//Create a file chooser
			fc = new JFileChooser();
			fc.addChoosableFileFilter(new Objfilter());
			fc.setAcceptAllFileFilterUsed(false);
			int returnVal = fc.showOpenDialog(FileChooser.this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				file = fc.getSelectedFile();
				this.name = file.getName();
			}
		}
		else if ( whitch == 1){
			fc = new JFileChooser();
			fc.addChoosableFileFilter(new Objfilter());
			fc.setAcceptAllFileFilterUsed(false);
			fc.setSelectedFile(new File("fichier.obj"));
			int returnVal = fc.showSaveDialog(FileChooser.this);
			if ( returnVal == JFileChooser.APPROVE_OPTION){
				file = fc.getSelectedFile();
				name = file.getName();
			}	
		}
		else if ( whitch == 2){
			fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fc.addChoosableFileFilter(new Objfilter());
			fc.setAcceptAllFileFilterUsed(false);
			int returnVal = fc.showOpenDialog(FileChooser.this);
			if ( returnVal == JFileChooser.APPROVE_OPTION){
				file = fc.getSelectedFile();
				name = file.getName();
			}	
		}
	}

	public File getFile(){
		return file;
	}

	public String getName(){
		return name;
	}

}






