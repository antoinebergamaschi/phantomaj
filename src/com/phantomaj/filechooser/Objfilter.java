package com.phantomaj.filechooser;


import java.io.File;

import javax.swing.filechooser.FileFilter;

public class Objfilter extends FileFilter{
	  //Accept all directories and all gif, jpg, tiff, or png files.
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}

		String extension = Utils.getExtension(f);
		if (extension != null) {
			if (extension.equals(Utils.obj)){
					return true;
			} else {
				return false;
			}
		}

		return false;
	}

	//The description of this filter
	public String getDescription() {
		return "Uniquement les obj3D";
	}
}
