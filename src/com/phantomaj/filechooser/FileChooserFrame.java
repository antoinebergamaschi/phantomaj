package com.phantomaj.filechooser;


import java.io.File;

import javax.swing.JFrame;

public class FileChooserFrame extends JFrame{
		//Create and set up the window.
	private FileChooser f;

	public FileChooserFrame(int witch){
		super("FileChooser");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		//Add content to the window.
		f = new FileChooser(witch);
		this.add(f);

		//Display the window.
		this.pack();
		this.setVisible(true);
	}

	public String getNameFile(){
		return f.getName();
	
	}

	
	
	public File getFile(){
		return f.getFile();
	}
}
