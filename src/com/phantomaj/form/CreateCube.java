package com.phantomaj.form;



public abstract class CreateCube extends CreateForm {

	protected int[][] _shape;

	/**
	* Comput the Shape of the Box where no rotation is apply
	**/
	protected void shape_comput(){
		this._shape = new int[3][2];
		for ( int coor=0 ; coor<3 ; coor++ ){
			this._shape[coor][0] = (this.origine[coor]) - (dimension[coor]/2 + dimension[coor]%2);
			this._shape[coor][1] = (this.origine[coor]) + (dimension[coor]/2 + dimension[coor]%2);
		}
	}


	/**	
	* Comput the start - end of the zone in a 3D environment containing the box to draw
	* @Override CreateForm.init_zone
	**/
	protected void init_zone(boolean coor_ZYX){
		// Creat a size limitation of the zone scanned to draw the box
		double [] stack_x_limit = {(double)(- dimension[0]/2), (double)(dimension[0]/2 + this.stack_width_is_pair)};
		double [] stack_y_limit = {(double)(- dimension[1]/2), (double)(dimension[1]/2 + this.stack_height_is_pair)};
		double [] stack_z_limit = {(double)(- dimension[2]/2), (double)(dimension[2]/2 + this.stack_size_is_pair)};

		double _tmp;
		int [][] _selection = new int[3][2];
		//initialze _selection with impossible values
		for (int i=0 ; i<3 ; i++){
			_selection[i][0] = 10000000 ; 
			_selection[i][1] = -10000000 ;
		}
		
		int[] oriNew = new int[3];
		
		for( int coor = 0 ; coor < 3 ; coor++){
			// if ( coor_ZYX ){
			
			// }
			// else{
				oriNew[coor] = (int)((-this.centerOfRotation[0])*op_matrice_rotation_euler[coor][0] + (-this.centerOfRotation[1])*op_matrice_rotation_euler[coor][1] + (-this.centerOfRotation[2])*op_matrice_rotation_euler[coor][2] + this.origine[coor]+ this.centerOfRotation[coor]);
				// System.out.println("Coor :"+oriNew[coor]);
			// }
		}
		
		//parcourt pour 4 sommets
 		// position x ( 0 ou 1 ) 
		for ( int x=0; x<2 ; x++){
			// position y ( 0 ou 1 )
			for ( int y=0 ; y<2 ; y++){
				// position z (0 ou 1)
				for ( int z=0 ; z<2 ; z++){
					// calcule de la rotation pour chaque coordonne x=0,y=1,z=2
					for ( int coor=0 ; coor < 3 ; coor++){
						//inversion des coordonnee Y/X
						if(coor_ZYX){
							_tmp = (stack_y_limit[y]-this.centerOfRotation[1])*op_matrice_rotation_euler[coor][1] + (stack_x_limit[x]-this.centerOfRotation[0])*op_matrice_rotation_euler[coor][0] + (stack_z_limit[z]-this.centerOfRotation[2])*op_matrice_rotation_euler[coor][2] + (this.origine[coor]+(this.centerOfRotation[coor])); 
						}
						else{
							// _tmp = stack_x_limit[x]*matrice_rotation_euler[coor][0] + stack_y_limit[y]*matrice_rotation_euler[coor][1] + stack_z_limit[z]*matrice_rotation_euler[coor][2] + (this.origine[coor]+(this.centerOfRotation[coor])); 
							_tmp = (stack_x_limit[x]-this.centerOfRotation[0])*op_matrice_rotation_euler[coor][0] + (stack_y_limit[y]-this.centerOfRotation[1])*op_matrice_rotation_euler[coor][1] + (stack_z_limit[z]-this.centerOfRotation[2])*op_matrice_rotation_euler[coor][2] + (this.origine[coor]+(this.centerOfRotation[coor])); 
							// System.out.println("Coor"+coor+" : "+_tmp+"\tOrigine :"+this.origine[coor]+"\tCentre : "+this.centerOfRotation[coor]);
						} 
						// test si les coordonne sont maximum ou minimum (avec arrondi a 1)
						if ( _tmp < _selection[coor][0]){
							_selection[coor][0] = (int)_tmp;
						}
						if( _tmp > _selection[coor][1]){
							_selection[coor][1] = (int)_tmp;
						}
					}
				}
			}
		}

		//Cut the zone if its dont fit in the stack size
		if ( _selection[0][1] > stack.getWidth()){
			_selection[0][1] = stack.getWidth();
		}
		
		if( _selection[0][0] < 0){
			_selection[0][0] = 0;
		}

		if ( _selection[1][1] > stack.getHeight()){
			_selection[1][1] = stack.getHeight();
		}

		if( _selection[1][0] < 0){
			_selection[1][0] = 0;
		}

		if ( _selection[2][1] > stack.getSize()){
			_selection[2][1] = stack.getSize();
		}
		if ( _selection[2][1] < 0 ){
			_selection[2][1] = 0;
		}
		
		if ( _selection[2][0] < 0 ){
			_selection[2][0] = 0;
		}
		if ( _selection[2][0] > stack.getSize() ){
			_selection[2][0] = stack.getSize();
		}
		
		this.zone_selection = _selection;
	}

//debug
	public void print_debug(){
		System.out.println("Shape");
		for (int i=0 ; i<3 ; i++){
			System.out.println("***"+i);
			System.out.println(_shape[i][0]);
			System.out.println(_shape[i][1]);
		}
		System.out.println("Zone");
		for (int i=0 ; i<3 ; i++){
			System.out.println("***"+i);
			System.out.println(zone_selection[i][0]);
			System.out.println(zone_selection[i][1]);
		}
		System.out.println("Matrice_euler");
		for(int i=0 ; i<3 ; i++){
			for(int j=0 ; j<3 ; j++){
				System.out.print("("+i+","+j+") , "+this.matrice_rotation_euler[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}

}
