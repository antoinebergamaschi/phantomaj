/**
* CreateForm.java
* 07/03/2012
* @author Antoine Bergamaschi
**/

package com.phantomaj.form;


import com.phantomaj.obj3D.Object3D.DataID;
import com.phantomaj.utils.Compute_Intensity;
import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.Thread_CreateForm;
import com.phantomaj.utils.Thread_option;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.FileSaver;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
//debug

/**
* Abstract class that regroup every function used to draw the form that herite his methode.
**/
public abstract class CreateForm implements WindowListener{

	private static final int MOD_MOY = 0;
	private static final int MOD_LASTIN = 1;
	private static final int MOD_SUM = 2;
	private static final int MOD_NEG = 3;


//debug
	protected static ImagePlus Imp=new ImagePlus();
//findebug

	protected boolean try_init_done = false;
	protected boolean bol_try = true;
	
	protected String name;
	protected boolean change_coor;
	
	
	protected static ImageStack mask_layer;
	protected static ImageStack stack;
	
	protected static int [] stack_dimension = new int[3];
	protected int [] origine = new int[3];
	protected int [] true_origine = new int[3];
	protected int [] dimension = new int[3];
	protected int [] true_dimension = new int[3];
	protected int [][] zone_selection;
	protected int [] angle= new int[3];
	protected int [] true_angle= new int[3];
	protected float pixel_value;
	protected int [] centerOfRotation = {0,0,0};
	protected double angle_flex; 
	//layer position corresponding to the superposition of this form
	protected int layer=0;

	// 0 ou 1 pour ajuste quand le centre est +1
	protected int stack_width_is_pair;
	protected int stack_height_is_pair;
	protected int stack_size_is_pair;


//	protected Matrix matrice_rotation;
	protected double [][] matrice_rotation_euler;
	protected double [][] op_matrice_rotation_euler;

	
	//protected cst use in isIn
	protected double cst_x_1;
	protected double cst_x_2;
	protected double cst_x_3;
	
	protected double cst_y_3;
	protected double cst_y_2;
	protected double cst_y_1;
	
	protected double cst_z_1;
	protected double cst_z_2;
	protected double cst_z_3;
	
	protected double dimXsquare_;
	protected double dimYsquare_;
	protected double dimZsquare_;
	
	protected double dimX_cst;
	protected double dimY_cst;
	protected double dimZ_cst;
	
	
	protected static Compute_Intensity Cinten=new Compute_Intensity();
	protected static Thread_option Toption = new Thread_option();
	// protected static
	
	private final static int MIXTE=0;
	private final static int FULL=1;
	private final static int CLASSIC=2;
	
	private static ExecutorService exec=null;
	private static int processors=0;
	
	public void init_parameters(){
		this.matrice_rotation_euler = init_matrice_rotation(angle);
		this.stack_width_is_pair = CreateForm.stack.getWidth()%2;
		this.stack_height_is_pair = CreateForm.stack.getHeight()%2;
		this.stack_size_is_pair = CreateForm.stack.getSize()%2;
		

		if ( !try_init_done ){		
			this.updateInfo();
			shape_comput();
			init_zone(this.change_coor);
			init_const();
			// System.out.println("Init Done .......................................................L135 CreateForm");
		}
		

		
		//MultiThreading
		ArrayList<Thread_CreateForm> _threadArray = new ArrayList<Thread_CreateForm>();
		if( processors != Toption.getNumberOfProcessor()){
			if(exec!=null){
				exec.shutdown();
			}
			processors=Toption.getNumberOfProcessor();
		}
		
		/************************************************************************************************************/
		//divide the X dimension by the number of process
		// int z_section_ = (int)( ( (this.zone_selection[2][1] - this.zone_selection[2][0]) / processors ) + 1 );
		/************************************************************************************************************/
		int z_section_ = 0;
		int pro = processors+1;


		switch(Toption.getComputingType()){
			case CLASSIC:
				/**************Version Classic*******************************************************************************/
				
				// int z_section_ = 0;
				// int pro = processors+1;
				
				while( z_section_ <= 1){
					pro--;
					z_section_ = (int)( ( (this.zone_selection[2][1] - this.zone_selection[2][0]) / pro ) + 1 );
				}
				
				//for every usable process
				for(int i=0; i < pro ; i++) {
					int[] z_section_pf = {this.zone_selection[2][0] + z_section_ * i,this.zone_selection[2][0] + z_section_ * ( i+1) } ;
					
					if ( z_section_pf[1] > CreateForm.stack_dimension[2] ){
						z_section_pf[1] = CreateForm.stack_dimension[2] ;
					}
					
					int[] x_section = { this.zone_selection[0][0] , this.zone_selection[0][1] };
					int[] y_section = { this.zone_selection[1][0] , this.zone_selection[1][1] };
					int[] z_section	= { z_section_pf[0] , z_section_pf[1] };

					//New Thread 
					Thread_CreateForm t = new Thread_CreateForm(this,x_section,y_section,z_section,0);
					t.start();
					_threadArray.add(t);
				}
				
				
				/*for ( int z =0 ; z<_threadArray.size() ; z++ ){
					_threadArray.get(z).start();
				}*/

				while(_threadArray.size() > 0){
					if ( !_threadArray.get(0).isAlive() ){
						_threadArray.remove(0);
						// System.out.println("Boucle ........................... L94");
					}
				}
				
				// System.out.println("Version Classic Used");
				break;
				/**************      FIN      *******************************************************************************/
				/**************Version Classic*******************************************************************************/
		
			case FULL:
				/**************Version Full Thread***************************************************************************/
				
				if ( exec == null || exec.isShutdown() ){
					exec = Executors.newFixedThreadPool(processors);
				}
				
				// System.out.println((this.zone_selection[2][1] - this.zone_selection[2][0]));
				// System.out.println(this.zone_selection[2][1]+" , "+this.zone_selection[2][0]);
				if( this.zone_selection[2][1] < 0 || this.zone_selection[2][0] < 0 || (this.zone_selection[2][1] - this.zone_selection[2][0]) < 0){
					System.out.println(this.zone_selection[2][1]+" , "+this.zone_selection[2][0]+"................................................... L214 CreateForm");
				}
				else{
					//1Thread par slice
					@SuppressWarnings("rawtypes")
					Future[] future=new Future[(this.zone_selection[2][1] - this.zone_selection[2][0])];
					
					// System.out.println(this.zone_selection[0][1] +" , "+ this.zone_selection[0][0] +"....................................................... L220 CreateForm");
					
					for (int i=0; i < (this.zone_selection[2][1] - this.zone_selection[2][0]) ; i++){
						int[] x_section = { this.zone_selection[0][0] , this.zone_selection[0][1] };
						int[] y_section = { this.zone_selection[1][0] , this.zone_selection[1][1] };
						int[] z_section	= { this.zone_selection[2][0]+i , this.zone_selection[2][0]+i+1 };
						future[i] = exec.submit(new Thread_CreateForm(this,x_section,y_section,z_section,0));
					}
					//Fin des threads
					for(int z=0;z<future.length;z++){
						try{
							future[z].get();
						}
						catch(ExecutionException ex){
							
						}
						catch(InterruptedException e){
						}
					}
				}
				// System.out.println("Version Full Thread");
				break;
				/**************      FIN      *******************************************************************************/
				/**************Version Full Thread***************************************************************************/

			case MIXTE:
				/**************Version Mixte*********************************************************************************/
				
				
				// int z_section_ = 0;
				// int pro = processors+1;
				while( z_section_ <= 1){
					pro--;
					z_section_ = (int)( ( (this.zone_selection[2][1] - this.zone_selection[2][0]) / pro ) + 1 );
				}
				
				if ( exec == null || exec.isShutdown() ){
					exec = Executors.newFixedThreadPool(pro);
				}
				
				@SuppressWarnings("rawtypes")
				Future[] future2 = new Future[pro];
				for(int i=0; i < processors ; i++) {
					int[] z_section_pf = {this.zone_selection[2][0] + z_section_ * i,this.zone_selection[2][0] + z_section_ * ( i+1) } ;
					
					if ( z_section_pf[1] > CreateForm.stack_dimension[2] ){
						z_section_pf[1] = CreateForm.stack_dimension[2] ;
					}
					
					int[] x_section = { this.zone_selection[0][0] , this.zone_selection[0][1] };
					int[] y_section = { this.zone_selection[1][0] , this.zone_selection[1][1] };
					int[] z_section	= { z_section_pf[0] , z_section_pf[1] };

					//New Thread 
					future2[i] = exec.submit(new Thread_CreateForm(this,x_section,y_section,z_section,0));
				}
				
				for(int z=0;z<future2.length;z++){
					try{
						future2[z].get();
					}
					catch(ExecutionException ex){
						
					}
					catch(InterruptedException e){
					}
				}
				// System.out.println("Version Mixte");
				
				/**************      FIN      *******************************************************************************/
				/**************Version Mixte*********************************************************************************/
		}
		//Old system
		// build_Stack_XYZ();
	}

	/**
	*Use in the RandomGenerator to avoid collision
	*@return boolean, true if the shape can be draw without collision else false
	**/
	public boolean try_init_parameters(){
	
		if( processors != Toption.getNumberOfProcessor()){
			if(exec!=null){
				exec.shutdown();
			}
			processors=Toption.getNumberOfProcessor();
		}
	
		try_init_done = true;

		this.matrice_rotation_euler = init_matrice_rotation(angle);
		this.stack_width_is_pair = CreateForm.stack.getWidth()%2;
		this.stack_height_is_pair = CreateForm.stack.getHeight()%2;
		this.stack_size_is_pair = CreateForm.stack.getSize()%2;
		
		
		this.updateInfo();
		shape_comput();
		init_zone(this.change_coor);		
		init_const();
		
		
		
		
		if ( exec == null || exec.isShutdown() ){
			exec = Executors.newFixedThreadPool(processors);
		}
		
		if( this.zone_selection[2][1] < 0 || this.zone_selection[2][0] < 0 || (this.zone_selection[2][1] - this.zone_selection[2][0]) < 0){
			// System.out.println(this.zone_selection[2][1]+","+this.zone_selection[2][0]);
		}
		//1Thread par slice
		else {
			@SuppressWarnings("rawtypes")
			Future[] future=new Future[(this.zone_selection[2][1] - this.zone_selection[2][0])];
		
		
			for (int i=0; i < (this.zone_selection[2][1] - this.zone_selection[2][0]) ; i++){
				int[] x_section = { this.zone_selection[0][0] , this.zone_selection[0][1] };
				int[] y_section = { this.zone_selection[1][0] , this.zone_selection[1][1] };
				int[] z_section	= { this.zone_selection[2][0]+i , this.zone_selection[2][0]+i+1 };
				future[i] = exec.submit(new Thread_CreateForm(this,x_section,y_section,z_section,1));
			}
			//Fin des threads
			for(int z=0;z<future.length;z++){
				try{
					future[z].get();
					if ( !bol_try ){
						exec.shutdown();
						return false;
					}
				}
				catch(ExecutionException ex){
					
				}
				catch(InterruptedException e){
					
				}
			}
		}
		
		return this.bol_try;
	}

	abstract void shape_comput();



	abstract void init_zone(boolean coor_change);



	/**
	* initialize the matrix (3x3) of rotation with the Euler angle
	* @param angle_rotation int [] angle of rotation in degree ( phi , theta , psi )
	* @return matrice double[][] the matrice of rotation
	**/
	protected double [][] init_matrice_rotation(int [] angle_rotation){

		this.change_coor = false;

		if ( angle_rotation[0] != 0 && angle_rotation[1] != 0 || angle_rotation[1]!=0 && angle_rotation[2]!=0 /*|| angle_rotation[0]!=0 && angle_rotation[1]!=0 && angle_rotation[2]!=0*/){
			this.change_coor = true;
		}
		double [][] matrice=new double[3][3];

		double phi = Math.toRadians(angle_rotation[0]);
		double theta = Math.toRadians(angle_rotation[1]);
		double psi = Math.toRadians(angle_rotation[2]);

		double cphi = Math.cos(phi);
		double sphi = Math.sin(phi);
		double ctheta = Math.cos(theta);
		double stheta = Math.sin(theta);
		double cpsi = Math.cos(psi);
		double spsi = Math.sin(psi);

		matrice[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
		matrice[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
		matrice[0][2] = -cpsi * stheta;
		matrice[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
		matrice[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
		matrice[1][2] = ( spsi * stheta );
		matrice[2][0] = stheta * cphi;
		matrice[2][1] = stheta * sphi;
		matrice[2][2] =	ctheta;

		//Opposite matrix of ratotion
		this.op_matrice_rotation_euler = new double[3][3];
 		phi = Math.toRadians(-angle_rotation[2]);
 		theta = Math.toRadians(-angle_rotation[1]);
 		psi = Math.toRadians(-angle_rotation[0]);
		
		
 		cphi = Math.cos(phi);
 		sphi = Math.sin(phi);
 		ctheta = Math.cos(theta);
 		stheta = Math.sin(theta);
 		cpsi = Math.cos(psi);
 		spsi = Math.sin(psi);

 		this.op_matrice_rotation_euler[0][0] = ( cpsi * ctheta * cphi ) -spsi * sphi ;
 		this.op_matrice_rotation_euler[0][1] =  ( cpsi * ctheta * sphi ) +  (spsi * cphi );
 		this.op_matrice_rotation_euler[0][2] = -cpsi * stheta;
 		this.op_matrice_rotation_euler[1][0] = ( -spsi * ctheta * cphi ) -cpsi * sphi ;
		this.op_matrice_rotation_euler[1][1] = ( -spsi * ctheta * sphi ) + ( cpsi * cphi );
 		this.op_matrice_rotation_euler[1][2] = ( spsi * stheta );
 		this.op_matrice_rotation_euler[2][0] = stheta * cphi;
 		this.op_matrice_rotation_euler[2][1] = stheta * sphi;
 		this.op_matrice_rotation_euler[2][2] = ctheta;

		return matrice;
	}

    /**
     * Scan the stack in order to find the pixel whose position is inside the form to be draw
     * MultiThread
     * @see CreateForm isIn
     */
	public final void build_Stack_XYZ_multi(int[] x_section,int[] y_section, int[] z_section ){
		//for condition on the Z-axis

		for (int z=z_section[0]; z<z_section[1] ; z++){
			float[] pixel = (float[])CreateForm.stack.getPixels(z+1);
			int[] mask = (int[])CreateForm.mask_layer.getPixels(z+1);
			//for condition on the Y-axis
			for ( int y=y_section[0]; y<y_section[1] ; y++){
				int tmp = y * stack.getWidth();
				//for condition on the X-axis
				for (int x=x_section[0] ; x<x_section[1] ; x++){
					int pos=x+tmp;
					int [] position = {x,y,z};
					float isIn_ = isIn(position);
					
					
					//Test if the pixel at the position pos in a 1D array of 2D image is in the box
					if ( isIn_ > -1 && isIn_ <= 1){
						// If this pixel is part of this object as one unique object ( like the combine of different form )

						if ( this._isTheSameObject(mask[pos],this.layer) ){
							pixel[pos] = Math.max((this.pixel_value * Cinten.compute_creatForm(isIn_)),pixel[pos]);
						}
						
						// Else there is a super-position of two object then the value have to be added
						else{

							if ( this._isTheSameLayer(mask[pos],this.layer,0) ){
								
								pixel[pos] = this.pixel_value * Cinten.compute_creatForm(isIn_);
								
								mask[pos] = this.layer;
		
							}
							else if( this._isTheSameLayer(mask[pos],this.layer,1) ){
							
								pixel[pos] = modIntersec(this.pixel_value*Cinten.compute_creatForm(isIn_),pixel[pos]);
							}
						}
					}
				}
			}
		}
		
	}
	

	
	/**
	* Scan the stack in order to find the pixel whose position is inside the form to be draw
	* MultiThread
	* @see CreateForm isIn
	**/
	public boolean try_build_Stack_XYZ_multi(int[] x_section,int[] y_section, int[] z_section){

		//for condition on the Z-axis
		for (int z=z_section[0]; z<z_section[1] ; z++){
			int[] mask = (int[])mask_layer.getPixels(z+1);
			//for condition on the Y-axis
			for ( int y=y_section[0]; y<y_section[1] ; y++){
				int tmp = y * stack.getWidth();
				
				//for condition on the X-axis
				for (int x=zone_selection[0][0] ; x<zone_selection[0][1] ; x++){
					
					int pos=x+tmp;
					int [] position = {x,y,z};
					
					if (isIn(position) > -1.0f){
						if ( mask[pos] >= 0 && !this._isTheSameObject(mask[pos],this.layer)){
							return false;
						}
					}
					
				}
			}
		}
		return true;
	}


	private float modIntersec(float newVal, float oldVal){
		float pixel = 0.0f;
		switch(CreateForm.Cinten.getModIntersection()){
			case MOD_MOY:
				pixel = (newVal + oldVal)/2.0f;
				break;
			case MOD_LASTIN:
				pixel = newVal;
				break;
			case MOD_NEG:
				pixel = -100.0f;
				break;
			case MOD_SUM:
				pixel = newVal + oldVal;
				break;
		}
		// System.out.println("pixel Val ..L554 CreateForm ... "+pixel);
		return pixel;
	}
	
	
	/**
	* Test if the pixel at this position is inside the form to be draw or not
	* @param position int[] the 3D vector (x,y,z)
	**/
	abstract float isIn(int [] position);

	/**
	* Update Every information necessary for computation, called before initialing parameters
	**/
	abstract void updateInfo();

	/**
	* Initialize every constante use in the isIn fonction ConstName : cst_x_1 cst_x_2 cst_x_3 // cst_y_1 cst_y_2 cst_y_3 // cst_z_1 cst_z_2 cst_z_3 // dimXsquare_ dimYsquare_ dimZsquare_
	**/
	public void init_const(){
		this.cst_x_1 = -(origine[0]+centerOfRotation[0])*matrice_rotation_euler[0][0];
		this.cst_x_2 = -(origine[1]+centerOfRotation[1])*matrice_rotation_euler[0][1];
		this.cst_x_3 = -(origine[2]+centerOfRotation[2])*matrice_rotation_euler[0][2] + origine[0]+centerOfRotation[0];
	
		this.cst_y_1 = -(origine[0]+centerOfRotation[0])*matrice_rotation_euler[1][0] ;
		this.cst_y_2 = -(origine[1]+centerOfRotation[1])*matrice_rotation_euler[1][1] ;
		this.cst_y_3 = -(origine[2]+centerOfRotation[2])*matrice_rotation_euler[1][2] + origine[1]+centerOfRotation[1] ;
		
		this.cst_z_1 = -(origine[0]+centerOfRotation[0])*matrice_rotation_euler[2][0] ;
		this.cst_z_2 = -(origine[1]+centerOfRotation[1])*matrice_rotation_euler[2][1] ;
		this.cst_z_3 = -(origine[2]+centerOfRotation[2])*matrice_rotation_euler[2][2] + origine[2]+centerOfRotation[2] ;
	
		this.dimXsquare_ = (((double)dimension[0])/2.0d)*(((double)dimension[0])/2.0d);
		this.dimYsquare_ = (((double)dimension[1])/2.0d)*(((double)dimension[1])/2.0d);
		this.dimZsquare_ = (((double)dimension[2])/2.0d)*(((double)dimension[2])/2.0d);
		
		this.dimX_cst = ((double)dimension[0])/2.0d;
		this.dimY_cst = ((double)dimension[1])/2.0d;
		this.dimZ_cst = ((double)dimension[2])/2.0d;
	}

//#######################################################################
//debug

	/**
	* Debug fonction. Construct a new ImagePlus containing the stack created in this
	* debug constructor
	**/
	public void build_ImagePlus(String name,double noise){

		Imp.setTitle(name);
		Imp.show();

	}

	public void save(String path,int type){
		
		FileSaver fs = new FileSaver(CreateForm.Imp);
		if ( type == 0 ){
			fs.saveAsTiffStack(path); 
		}
		
	}
	
	/**
	* @deprecate
	**/
	public void addNoise(double noiseSD){
		Random r = new Random();
		//for condition on the Z-axis
		for (int z = 1 ; z < CreateForm.stack.getSize()+1 ; z++){
			float[] pixel = (float[])CreateForm.stack.getPixels(z);
			//for condition on the Y-axis
			for ( int y=1; y < CreateForm.stack.getHeight() ; y++){
				int tmp = y * stack.getWidth();
				//for condition on the X-axis
				for (int x=1 ; x < CreateForm.stack.getWidth() ; x++){
					int pos = x+tmp;
					// Add Noise
					pixel[pos] += noiseSD * r.nextGaussian();
				}
			}
		}
	}


	
	
	/**
	* Construct the stack use in the build_Stack fonction, build in Parallel a mask stack where the layer of each pixel are saved.
	* @param width int Xdim of the Stack to be draw
	* @param height int Ydim of the Stack to be draw
	* @param size int Zdim of the Stack to be draw
	* @param pixelValueFont float the grey value of the background
	**/
	public static void creatStack(int width, int height, int size, float pixelValueFont){
	
		stack_dimension[0] = width;
		stack_dimension[1] = height;
		stack_dimension[2] = size;
		//Ajout
//		ArrayList<Thread_CreateForm> _threadArray = new ArrayList<Thread_CreateForm>();
		if( processors != Toption.getNumberOfProcessor()){
			if(exec!=null){
				exec.shutdown();
			}
			processors=Toption.getNumberOfProcessor();
		}
		
		if ( exec == null || exec.isShutdown() ){
			exec = Executors.newFixedThreadPool(processors);
		}
		

		stack = new ImageStack(width,height,size);
		
		
		final float[] pixel_init_S = new float[width*height];
		Arrays.fill(pixel_init_S,pixelValueFont);

		
		mask_layer = new ImageStack(width,height,size);
		final int[] pixel_init_M = new int[width*height];
		Arrays.fill(pixel_init_M,-1);
		
		//1Thread par slice
		@SuppressWarnings("rawtypes")
		Future[] future=new Future[size];

		
		for ( int i=1 ; i <= size ; i++){
			final int k = i;
			future[i-1] = exec.submit(new Thread( new Runnable(){
				public void run(){
					float[] pixel_init = new float[pixel_init_S.length];
					int[] pixel_init_m = new int[pixel_init_S.length];
					//stack
					System.arraycopy(pixel_init_S,0,pixel_init,0,pixel_init_S.length);
					stack.setPixels(pixel_init,k);
					//mask
					System.arraycopy(pixel_init_M,0,pixel_init_m,0,pixel_init_S.length);
					mask_layer.setPixels(pixel_init_m,k);
				}
				}));
		}
		//Fin des threads
		for(int z=0;z<future.length;z++){
			try{
				future[z].get();
			}
			catch(ExecutionException ex){
				ex.printStackTrace();
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
		Imp = new ImagePlus(null,stack);
	}


	public static void reset(){
		mask_layer = null;
		Imp = null;
	}
	
//debug

	 public void windowActivated(WindowEvent e){}

	 public void windowClosed(WindowEvent e){
	 }

	 public void windowClosing(WindowEvent e){
		CreateForm.Imp.close();
	}

	 public void windowDeactivated(WindowEvent e){}

	 public void windowDeiconified(WindowEvent e){}

	 public void windowIconified(WindowEvent e){}

	 public void windowOpened(WindowEvent e){}

	 public void windowStateChanged(WindowEvent e){}

	/**
	* Debug Printing.
	**/
	public void print_debug(){
		System.out.println("Zone");
		for (int i=0 ; i<3 ; i++){
			System.out.println("***"+i);
			System.out.println(zone_selection[i][0]);
			System.out.println(zone_selection[i][1]);
		}
		System.out.println("Matrice_euler");
		for(int i=0 ; i<3 ; i++){
			for(int j=0 ; j<3 ; j++){
				System.out.print("("+i+","+j+") , "+this.matrice_rotation_euler[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}


    /**
    * Set the pixel value of this form. 0 < value < 256.
    * @deprecate
    **/
    public void setPixel_value(int value){
    	this.pixel_value = value;
    }

	public float getPixel_value(){
		return this.pixel_value;
	}
	
	public void setLayer(int value){
		this.layer += value;
	}

	public void addToXori(int number){
		this.origine[0] = this.true_origine[0] + number ;
	}	
	
	public void addToYori(int number){
		this.origine[1] = this.true_origine[1] + number ;
	}
	
	public void addToZori(int number){
		this.origine[2] = this.true_origine[2] + number ;
	}
	
	public void addToPhi(int number){
		this.angle[0] = this.true_angle[0] + number ;
	}
	
	public void addToTheta(int number){
		this.angle[1] = this.true_angle[1] + number ;
	}
	
	public void addToPsi(int number){
		this.angle[2] = this.true_angle[2] + number ;
	}
	
	
	public void addToDim(int number, int ID){
		if ( StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] == StaticField_Form._2DFORM && ID >= 1 ){
			this.dimension[ID+1] = this.true_dimension[ID+1] + number ;
		}
		else if ( StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] == StaticField_Form._1DFORM && ID >= 1 ){
			this.dimension[ID+2] = this.true_dimension[ID+2] + number ;
		}
		else if ( StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] == StaticField_Form._3DFORM ){
			this.dimension[ID] = this.true_dimension[ID] + number ;
		}
		else{
			for ( int z = 0 ; z  <= StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] ; z++ ){
				this.dimension[ID + z] = this.true_dimension[ID + z] + number ;
			}
		}
	}
	
	public void setDimensionInit(int newinit, int ID){
		if ( StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] == StaticField_Form._2DFORM && ID >= 1 ){
			this.true_dimension[ID+1] = newinit;
		}
		else if ( StaticField_Form.CODE_3D_FORM[StaticField_Form.getIDfromName(this.name)] == StaticField_Form._1DFORM && ID >= 1 ){
			this.true_dimension[ID+2] = newinit;
		}
		else {
			this.true_dimension[ID] = newinit;
		}
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setPhiInitial(int newinit){
		if ( newinit < this.angle[0] ){
			this.true_angle[0] = newinit;
		}
	}
	
	public void setThetaInitial(int newinit){
		if( newinit < this.angle[1] ){
			this.true_angle[1] = newinit;
		}
	}
	
	public void setPsiInitial(int newinit){
		if ( newinit < this.angle[2] ){
			this.true_angle[2] = newinit;
		}
	}
	
	public void setOriXInitial(int newinit){
		if ( newinit < this.origine[0] ){
			this.true_origine[0] = newinit;
		}
	}
	
	public void setOriYInitial(int newinit){
		if ( newinit < this.origine[1] ){
			this.true_origine[1] = newinit;
		}
	}
	
	public void setOriZInitial(int newinit){
		if ( newinit < this.origine[2] ){
			this.true_origine[2] = newinit;
		}
	}
	
	
	/**
	* Set the new Center of Rotation for this form, in relative coordonate (0,0,0) is the center of the form.
	* @param XCenter int the X coordonate of the Center of rotation
	* @param YCenter int the Y coordonate of the Center of rotation
	* @param ZCenter int the Z coordonate of the Center of rotation
	**/
	public void setCenterOfRotation(int XCenter, int YCenter, int ZCenter){
		this.centerOfRotation[0] = XCenter;
		this.centerOfRotation[1] = YCenter;
		this.centerOfRotation[2] = ZCenter;
	}
	
	
	/**
	* Copy this form parameters in a new form
	**/
	public CreateForm copy(){
		CreateForm form = null ;
		int[] new_dimension = new int[this.dimension.length];
		int[] new_origine = new int[3];
		int[] new_angle = new int[3];
		
		System.arraycopy(this.dimension,0,new_dimension,0,this.dimension.length);
		System.arraycopy(this.origine,0,new_origine,0,3);
		System.arraycopy(this.angle,0,new_angle,0,3);
		
		form = StaticField_Form.getCreatForm(StaticField_Form.getIDfromName(this.name), this.pixel_value, this.layer, this.centerOfRotation , new_dimension, new_origine, new_angle);
		
		return (CreateForm)form;
	}
	

	
	
	public void setObjectChange(int[] newOri, int[] newAngle){
		for ( int i=0 ; i<3 ; i++ ){
			this.origine[i] = newOri[i];
			this.angle[i] = newAngle[i];
		}
	}

	public void setObjectChange(int[][] ori_dim){
		for ( int i = 0 ; i < 3 ; i++ ){
			this.origine[i] = ori_dim[0][i];
			this.dimension[i] = ori_dim[1][i];
		}
	}
	
	public static void setComputeIntensity(Compute_Intensity Cint){
		if ( Cint != null ){
			Cinten = Cint;
		}
		else {
			System.out.println("WARNING :: Compute_Intensity NULL   ....   L 872 CreateForm");
		}
	}
	
	public static void setThreadOption(Thread_option Toptiony){
		if ( Toptiony !=null ){
			Toption = Toptiony;
		}
		else{
			System.out.println("WARNING :: Thread_option NULL  ....  L 881 CreateForm");
		}
	}
	
	public void setImageStack(ImageStack img){
		CreateForm.stack = img;
	}
	
	public void setMask(ImageStack mask){
		CreateForm.mask_layer = mask;
	}
	
	public ImageStack getImageStack(){
		return CreateForm.stack;
	}
	
	public ImageStack getMask_layer(){
		return CreateForm.mask_layer;
	}
	
	public void setBolTry(boolean b){
		this.bol_try = b;
	}
	
	
	// public static void setLayerObjectPos(int id){
		// layer = id;
	// }
	
	public void setLayerObject(int id){
		this.layer += id;
	}
	
	public int getLayer_value(){	
		return Integer.parseInt(String.valueOf(this.layer).substring((String.valueOf(this.layer)).length()-1,(String.valueOf(this.layer)).length()));
	}
	
	/**
	* Test if the drawing form is at the same object with the currently selected pixel
	* @param layer_old the layer value in the pixel
	* @param layer_new the layer value of the current form
	**/
	private boolean _isTheSameObject(int layer_old, int layer_new){

		if ( layer_old >= 0 ){

			int idObj_old = Integer.parseInt(String.valueOf(layer_old).substring(0,(String.valueOf(layer_old)).length()-1));
			int idObj_new = Integer.parseInt(String.valueOf(layer_new).substring(0,(String.valueOf(layer_new)).length()-1));
			
			if ( idObj_old == idObj_new ){
				return true;
			}
		}
		return false;
	}
	
	
	/**
	* Test if the drawing form is at the same layer with the currently selected pixel
	* @param layer_old the layer value in the pixel
	* @param layer_new the layer value of the current form
	* @param test 0,1,3 < , == , !=
	**/
	private boolean _isTheSameLayer(int layer_old, int layer_new, int test){

		int idLayer_old;
		
		if ( layer_old < 0 ){
			idLayer_old = layer_old;
		}
		else{
			idLayer_old = Integer.parseInt(String.valueOf(layer_old).substring((String.valueOf(layer_old)).length()-1,(String.valueOf(layer_old)).length()));
		}
		
		int idLayer_new = Integer.parseInt(String.valueOf(layer_new).substring((String.valueOf(layer_new)).length()-1,(String.valueOf(layer_new)).length()));
		
		//Test old == new
		if ( test == 1){
			//test if the Last part of the int is the same ( 100-1 == 200-1 ) (100-2 != 100-1)
			if ( idLayer_old == idLayer_new ){
				return true;
			}
		}
		//Test old < new 
		else if ( test == 0 ){
			//test if the Last part of the int is the same ( 100-1 == 200-1 ) (100-2 != 100-1)
			if ( idLayer_old < idLayer_new ){
				return true;
			}
		}
		//Test old != new
		else if ( test == 3 ){
			if ( idLayer_old != idLayer_new ){
				return true;
			}
		}
		
		return false;
	}
	
	/**
	* Print the principal information about this form.
	**/
	public String toString(){
		String formString="";
		formString += DataID.FORM.getName()+"\t"+name+"\n";
		formString += DataID.ORIGIN.getName()+"\t"+origine[0]+"\t"+origine[1]+"\t"+origine[2]+"\n";
		formString += DataID.DIMENSION.getName()+"\t"+dimension[0]+"\t"+dimension[1]+"\t"+dimension[2]+"\n";
		formString += DataID.ANGLE.getName()+"\t"+angle[0]+"\t"+angle[1]+"\t"+angle[2]+"\n";
		formString += DataID.PIXEL_VALUE.getName()+"\t"+pixel_value+"\n";
		return formString;
	}
	
}
