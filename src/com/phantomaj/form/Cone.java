package com.phantomaj.form;


import com.phantomaj.utils.StaticField_Form;


public class Cone extends CreateCube{

	private double cos_angle_cone;

//#######################################################################
//Constructor

	/**
	* Default Constructor used to create a new cone.
	* @param dimension int[] the widht, height and size of this cone.
	* @param origine int[] the cartesian X-axis, Y-axis and Z-axis coordonate of the cone center.
	* @param pixel_value float the grey value used to draw this pixel.
	* @param layer int the layer position of this form.
	**/
	public Cone(int[] dimension, int[] origine, int[] angle, float pixel_value , int layer){
		this.origine[0] = origine[0] ;
		this.origine[1] = origine[1] ;
		this.origine[2] = origine[2] ;	
		
		this.true_origine[0] = origine[0] ;
		this.true_origine[1] = origine[1] ;
		this.true_origine[2] = origine[2] ;		
		
		this.dimension[0] = dimension[0];
		this.dimension[1] = dimension[1];
		this.dimension[2] = dimension[2];
		
		this.true_dimension[0] = dimension[0];
		this.true_dimension[1] = dimension[1];
		this.true_dimension[2] = dimension[2];
		
		this.angle[0] = angle[0];
		this.angle[1] = angle[1];
		this.angle[2] = angle[2];
	
		this.true_angle[0] = angle[0];
		this.true_angle[1] = angle[1];
		this.true_angle[2] = angle[2];
		this.dimension = dimension;
		this.angle = angle;
		this.pixel_value = pixel_value;

		this.name = StaticField_Form.FORM_NAME[StaticField_Form.CONE];
		this.layer = layer;
	}
	

//fonction


	public void updateInfo(){}

	/**
	* Compute and test if the point (x,y,z) is in or out of the diameter
	* Override Create_form.isIn
	* @param position int[] the array containing the (x,y,z) vector of coordonate
	* @return float an Integer between 0 and 1 if the test is true, -1 if false
	**/
	public float isIn(int [] position){	
		double tmp;
		double x;
		double y;
		double z;

		x = (position[0]*matrice_rotation_euler[0][0]) + this.cst_x_1 + (position[1]*matrice_rotation_euler[0][1]) + this.cst_x_2 + (position[2]*matrice_rotation_euler[0][2]) + this.cst_x_3 ;
		if( (int)x >= this._shape[0][0] && (int)x <= _shape[0][1] ){
			
			//position en y
			y = (position[0]*matrice_rotation_euler[1][0]) + this.cst_y_1 + (position[1]*matrice_rotation_euler[1][1]) + this.cst_y_2 + (position[2]*matrice_rotation_euler[1][2]) + this.cst_y_3 ;			
			if ( (int)y >= this._shape[1][0] && (int)y <= this._shape[1][1] ){
				
				//position en z
				z = (position[0]*matrice_rotation_euler[2][0]) + this.cst_z_1 + (position[1]*matrice_rotation_euler[2][1]) + this.cst_z_2 + (position[2]*matrice_rotation_euler[2][2]) + this.cst_z_3 ;
				if ( (int)z >= this._shape[2][0] && (int)z <= this._shape[2][1] ){
					// Compute the radius
					tmp = ((z-this._shape[2][0])/dimension[2])*(dimension[0]/2);
					

					// double tmp_cercle = ((x-origine[0])*(x-origine[0]) + (y-origine[1])*(y-origine[1]))/((tmp)*(tmp));
					double tmp_cercle = Math.sqrt((x-origine[0])*(x-origine[0]) + (y-origine[1])*(y-origine[1]))/(tmp);
					if ( tmp_cercle <= 1){
						return (float)Math.max(Math.sqrt((z-origine[2])*(z-origine[0]))/this.dimZsquare_,tmp_cercle);
					}
				}
			}
		}
		return -1.0f;
	}

}
