/**
*Fiber.java
*12/06/2012
*@author Antoine Bergamaschi
*/

package com.phantomaj.form;

import com.phantomaj.utils.StaticField_Form;



public class Fiber extends CreateCube{


	private double radius;

	private int dimZ_reel;
	private int dimY_reel;
	private int[] centerCircle;


//#######################################################################
//Constructor

	/**
	* Default Constructor used to create a new fiber.
	* @param dimension, int[] the widht, height and size of this tube.
	* @param origine, int[] the cartesian X-axis, Y-axis and Z-axis coordonate of the tube center.
	* @param pixel_value, float the grey value used to draw this pixel.
	* @param layer, int the layer position of this form.
	**/
	public Fiber(int[] dimension, int[] origine, int[] angle, float pixel_value,int layer){
		this.dimension = new int[4];
		this.true_dimension = new int[4];
		
		this.origine[0] = origine[0] ;
		this.origine[1] = origine[1] ;
		this.origine[2] = origine[2] ;	
		
		this.true_origine[0] = origine[0] ;
		this.true_origine[1] = origine[1] ;
		this.true_origine[2] = origine[2] ;		
		
		System.arraycopy(dimension,0,this.dimension,0,dimension.length);
		
		this.true_dimension[0] = dimension[0];
		this.true_dimension[1] = dimension[1];
		this.true_dimension[2] = dimension[2];
		//Additional Parameter
		this.true_dimension[3] = dimension[3];
		
		this.angle[0] = angle[0];
		this.angle[1] = angle[1];
		this.angle[2] = angle[2];
	
		this.true_angle[0] = angle[0];
		this.true_angle[1] = angle[1];
		this.true_angle[2] = angle[2];
		
		this.pixel_value = pixel_value;	
		// this.matrice_rotation_euler = init_matrice_rotation(angle);
		this.name = StaticField_Form.FORM_NAME[StaticField_Form.FIBER];
		this.layer = layer;
		
		// this.setInfoFiber(0);
	}
	
	//Note pour moi meme :: au debut dim[0] == dimension de la fibre reel, changement pour dim[0] classique pour la formation du cube perte de l'information premiere
	
	//set this angle in degree and compute the radius corresponding
	/**
	* @deprecate
	**/
	public void setInfoFiber(double deprecateVar){
		this.angle_flex = this.dimension[3];
		
		dimZ_reel = this.dimension[2];
		dimY_reel = this.dimension[0];
		
		if ( this.angle_flex == 180.0 ){
			this.angle_flex = 179.99999;
		}
		
		else if ( this.angle_flex == 0.0 ){
			this.angle_flex = 0.0001;
		}
		
		this.radius = (double)(((360.0d/this.angle_flex) * (double)dimZ_reel)/(2*Math.PI));

		this.dimension[2] = (int)( Math.tan(Math.toRadians(this.angle_flex/2.0d)) * (double)(this.radius + (double)dimY_reel/2.0d));

		this.dimension[0] = (int)((double)(this.radius ) - ( Math.cos(Math.toRadians(this.angle_flex/2.0d)) * (double)( this.radius - (double)dimY_reel/2.0d)) ); 
		
		this.dimension[2] = this.dimension[2]*2;
		if ( this.angle_flex > 90 ){
			this.dimension[0] = (int)(dimY_reel + this.radius) ; 
		}
		else{
			this.dimension[0] = this.dimension[0]*2; 
		}
		this.dimension[1] = dimY_reel;
		//Arbitrairement
		centerCircle = new int[3];
		centerCircle[0] = this.origine[0] - (int)this.radius;
		centerCircle[1] = this.origine[1];
		centerCircle[2] = this.origine[2];
	}
	

	public void updateInfo(){
		this.angle_flex = this.dimension[3];
		
		dimZ_reel = this.dimension[2];
		dimY_reel = this.dimension[0];
		
		if ( this.angle_flex == 180.0 ){
			this.angle_flex = 179.99999;
		}
		
		else if ( this.angle_flex == 0.0 ){
			// System.out.println("Test");
			this.angle_flex = 0.0001;
		}
		
		this.radius = (double)(((360.0d/this.angle_flex) * (double)dimZ_reel)/(2*Math.PI));


		this.dimension[2] = (int)( Math.tan(Math.toRadians(this.angle_flex/2.0d)) * (double)(this.radius + (double)dimY_reel/2.0d));

		this.dimension[0] = (int)((double)(this.radius ) - ( Math.cos(Math.toRadians(this.angle_flex/2.0d)) * (double)( this.radius - (double)dimY_reel/2.0d)) ); 

		
		this.dimension[2] = this.dimension[2]*2;
		if ( this.angle_flex > 90 ){
			this.dimension[0] = (int)(dimY_reel + this.radius) ; 
		}
		else{
			this.dimension[0] = this.dimension[0]*2; 
		}
		this.dimension[1] = dimY_reel;

		//Arbitrairement
		centerCircle = new int[3];
		centerCircle[0] = this.origine[0] - (int)this.radius;
		centerCircle[1] = this.origine[1];
		centerCircle[2] = this.origine[2];
	}
	
//fonction

	/* Methode Antoine Angle bisot�
	/**
	* Compute and test if the point (x,y,z) is in or out of the radius
	* @param position, int[] the array containing the (x,y,z) vector of coordonate
	* @return Boolean, true if in, false else
	* @Override Create_form.isIn
	**/
		// public Boolean isIn(int [] position){
		// double tmp;
		// //position en x
		// tmp = (position[0]-(origine[0]+centerOfRotation[0]))*matrice_rotation_euler[0][0] + (position[1]-(origine[1]+centerOfRotation[1]))*matrice_rotation_euler[0][1] + (position[2]-(origine[2]+centerOfRotation[2]))*matrice_rotation_euler[0][2] + origine[0] + centerOfRotation[0];
		// double x = tmp;
		// if( (int)tmp >= this._shape[0][0] && (int)tmp <= _shape[0][1] ){
			// // System.out.println("shape 1 ");
			// //position en y
			// tmp = (position[0]-(origine[0]+centerOfRotation[0]))*matrice_rotation_euler[1][0] + (position[1]-(origine[1]+centerOfRotation[1]))*matrice_rotation_euler[1][1] + (position[2]-(origine[2]+centerOfRotation[2]))*matrice_rotation_euler[1][2] + origine[1] + centerOfRotation[1];
			// double y = tmp;
			// if ( (int)tmp >= this._shape[1][0] && (int)tmp <= this._shape[1][1] ){
				// //position en z
				// // System.out.println("shape 2 ");
				// tmp = (position[0]-(origine[0]+centerOfRotation[0]))*matrice_rotation_euler[2][0] + (position[1]-(origine[1]+centerOfRotation[1]))*matrice_rotation_euler[2][1] + (position[2]-(origine[2]+centerOfRotation[2]))*matrice_rotation_euler[2][2] + origine[2] + centerOfRotation[2];
				// double z = tmp;
				// if ( (int)tmp >= this._shape[2][0] && (int)tmp <= this._shape[2][1] ){
					// //calcule la position du fil central ( supose Y fixe , Z donne )
					// double x_rad = Math.sqrt((double)(radius*radius) - ((double)(position[2]-centerCircle[2])*(double)(position[2]-centerCircle[2]))) + centerCircle[0];
					// // System.out.println(x);
					// // x = x/3;
					// tmp = (( x - x_rad )*( x - x_rad )) + ((y - origine[1])*(y - origine[1]));
					// // System.out.println(tmp);
					// if ( tmp <= (dimY_reel/2 * dimY_reel/2)){
						
						// double distanceEuclidian = Math.sqrt( ( x - centerCircle[0] )*( x - centerCircle[0] ) + ( y - centerCircle[1] )*( y - centerCircle[1]  ) + ( z - centerCircle[2] )*( z - centerCircle[2] ) );
						// double distanceFromOri = Math.abs( y - centerCircle[1] );
						// double finalDist = Math.sqrt( distanceEuclidian*distanceEuclidian + distanceFromOri*distanceFromOri );
						
						// tmp = Math.toDegrees(Math.acos(Math.abs(x-centerCircle[0])/finalDist));
						
					
						// if ( tmp <= this.angle_flex/2 ){
							// return true;
						// }
					// }
				// }
			// }
		// }
		// return false;
	// }
	
	/**
	* Compute and test if the point (x,y,z) is in or out of the radius
	* Override Create_form.isIn
	* @param position int[] the array containing the (x,y,z) vector of coordonate
	* @return float an Integer between 0 and 1 if the test is true, -1 if false
	**/
	public float isIn(int [] position){
		double tmp;
		double x;
		double y;
		double z;
		

		x = (position[0]*matrice_rotation_euler[0][0]) + this.cst_x_1 + (position[1]*matrice_rotation_euler[0][1]) + this.cst_x_2 + (position[2]*matrice_rotation_euler[0][2]) + this.cst_x_3 ;
		if( (int)x >= this._shape[0][0] && (int)x <= _shape[0][1] ){
			
			//position en y
			y = (position[0]*matrice_rotation_euler[1][0]) + this.cst_y_1 + (position[1]*matrice_rotation_euler[1][1]) + this.cst_y_2 + (position[2]*matrice_rotation_euler[1][2]) + this.cst_y_3 ;			
			if ( (int)y >= this._shape[1][0] && (int)y <= this._shape[1][1] ){
				
				//position en z
				z = (position[0]*matrice_rotation_euler[2][0]) + this.cst_z_1 + (position[1]*matrice_rotation_euler[2][1]) + this.cst_z_2 + (position[2]*matrice_rotation_euler[2][2]) + this.cst_z_3 ;
				if ( (int)z >= this._shape[2][0] && (int)z <= this._shape[2][1] ){
				
					//calcule la position du fil central ( supose Y fixe , Z donne )
					double distanceEuclidian =  Math.sqrt(( x - centerCircle[0] )*( x - centerCircle[0] ) + ( z - centerCircle[2] )*( z - centerCircle[2])) ;
					
					tmp = Math.toDegrees(Math.acos((x - centerCircle[0])/distanceEuclidian));


					if ( tmp <= this.angle_flex/2 && tmp >= 0){
						if ( (this.origine[2] - z) > 0 ){
							tmp = - tmp;
						}
					
						double cosTmp = Math.cos(Math.toRadians(tmp));
						double sinTmp = Math.sin(Math.toRadians(tmp));
						
						double centerToDrawX = ( (this.origine[0]-centerCircle[0]) * cosTmp ) - ( (this.origine[2]-centerCircle[2]) * sinTmp ) + centerCircle[0] ; 
						double centerToDrawZ = ( (this.origine[0]-centerCircle[0]) * sinTmp ) + ((this.origine[2]-centerCircle[2]) * cosTmp ) + centerCircle[2] ;
						
						
						distanceEuclidian = Math.sqrt(( x - centerToDrawX )*( x - centerToDrawX ) + ( y - this.origine[1] )*( y - this.origine[1] ) + ( z - centerToDrawZ )*( z - centerToDrawZ ));

						distanceEuclidian = distanceEuclidian/(this.dimY_reel/2);
						if ( distanceEuclidian <=1.0d ){
							return (float)Math.max(Math.abs((tmp/(this.angle_flex/2))),distanceEuclidian);
						}
					}
				}
			}
		}
		return -1.0f;
	}
	
	/**
	* Print the principal information about this form.
	* @override toString Create_form
	**/
	public String toString(){
		String formString="";
		formString += "NAME\t"+name+"\n";
		formString += "ORI\t"+origine[0]+"\t"+origine[1]+"\t"+origine[2]+"\n";
		formString += "DIM\t"+dimY_reel+"\t"+dimZ_reel+"\t"+dimZ_reel+"\t"+angle_flex+"\n";
		formString += "ANG\t"+angle[0]+"\t"+angle[1]+"\t"+angle[2]+"\n";
		formString += "PV\t"+pixel_value+"\n";
		return formString;
	}
	
}