package com.phantomaj.form;


import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.StaticField_Form;


public class Sphere extends CreateForm{


//#######################################################################
// Constructor	



//TODO Constructeur final
	public Sphere(int[] dimension, int[] origine, int[] angle,float pixel_value,int layer){
		this.origine[0] = origine[0] ;
		this.origine[1] = origine[1] ;
		this.origine[2] = origine[2] ;	
		
		this.true_origine[0] = origine[0] ;
		this.true_origine[1] = origine[1] ;
		this.true_origine[2] = origine[2] ;		
		
		this.dimension[0] = dimension[0];
		this.dimension[1] = dimension[1];
		this.dimension[2] = dimension[2];
		
		this.true_dimension[0] = dimension[0];
		this.true_dimension[1] = dimension[1];
		this.true_dimension[2] = dimension[2];
		
		this.angle[0] = angle[0];
		this.angle[1] = angle[1];
		this.angle[2] = angle[2];
	
		this.true_angle[0] = angle[0];
		this.true_angle[1] = angle[1];
		this.true_angle[2] = angle[2];	
		this.dimension = dimension;
		this.pixel_value = pixel_value;
		this.angle = angle;
		this.name = StaticField_Form.FORM_NAME[StaticField_Form.SPHERE];
		this.layer = layer;

	}


//#######################################################################
// Private method
	public void updateInfo(){}

	protected void shape_comput(){}

	/**
	* @override CreateForm.init_zone
	**/
	protected void init_zone(boolean test){
		// Creat a size limitation of the zone scanned to draw the sphere
		int [] stack_x_limit = {(origine[0]-(dimension[0]/2)),(origine[0]+(dimension[0]/2)+this.stack_width_is_pair)};
		int [] stack_y_limit = {(origine[1]-(dimension[0]/2)),(origine[1]+(dimension[0]/2)+this.stack_height_is_pair)};
		int [] stack_z_limit = {(origine[2]-(dimension[0]/2)),(origine[2]+(dimension[0]/2)+this.stack_size_is_pair)};

		int[][] _selection = new int[3][2];
		
		_selection[0]=stack_x_limit;
		_selection[1]=stack_y_limit;
		_selection[2]=stack_z_limit;

		
		if ( _selection[0][1] > stack.getWidth()){
			_selection[0][1] = stack.getWidth();
		}
		
		if(_selection[0][0] < 0){
			_selection[0][0] = 0;
		}

		if ( _selection[1][1] > stack.getHeight()){
			_selection[1][1] = stack.getHeight();
		}

		if(_selection[1][0] < 0){
			_selection[1][0] = 0;
		}

		if ( _selection[2][1] > stack.getSize()){
			_selection[2][1] = stack.getSize();
		}

		if ( _selection[2][0] < 0){
			_selection[2][0] = 0;
		}
		
		this.zone_selection = _selection;
	}



	/**
	* Compute and test if the point (x,y,z) is in or out of the diameter
	* @param pos int[] the 3D vector (x,y,z)
	* @return float an Float between 0 and 1 if the test is true, -1 if false
	* @Override CreateForm.isIn
	**/
	public float isIn(int[] pos){
		double tmp = (pos[0]-origine[0])*(pos[0]-origine[0]) + (pos[1]-origine[1])*(pos[1]-origine[1]) + (pos[2]-origine[2])*(pos[2]-origine[2]);
		if ( tmp <= this.dimXsquare_ ){
			tmp = Math.sqrt(tmp);
			return (float) (tmp/this.dimX_cst);
		}
		return -1.0f;
	}


}


