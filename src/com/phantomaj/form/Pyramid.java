package com.phantomaj.form;


import com.phantomaj.utils.StaticField_Form;
import com.phantomaj.utils.StaticField_Form;


public class Pyramid extends CreateCube{



//#######################################################################
//Constructor

	/**
	* Default Constructor used to create a new pyramid.
	* @param dimension int[] the widht, height and size of this pyramid.
	* @param origine int[] the cartesian X-axis, Y-axis and Z-axis coordonate of the pyramid center.
	* @param pixel_value float the grey value used to draw this pixel.
	* @param layer int the layer position of this form.
	**/
	public Pyramid(int[] dimension, int[] origine, int[] angle,float pixel_value, int layer){
		this.origine[0] = origine[0] ;
		this.origine[1] = origine[1] ;
		this.origine[2] = origine[2] ;	
		
		this.true_origine[0] = origine[0] ;
		this.true_origine[1] = origine[1] ;
		this.true_origine[2] = origine[2] ;		
		
		this.dimension[0] = dimension[0];
		this.dimension[1] = dimension[1];
		this.dimension[2] = dimension[2];
		
		this.true_dimension[0] = dimension[0];
		this.true_dimension[1] = dimension[1];
		this.true_dimension[2] = dimension[2];
		
		this.angle[0] = angle[0];
		this.angle[1] = angle[1];
		this.angle[2] = angle[2];
	
		this.true_angle[0] = angle[0];
		this.true_angle[1] = angle[1];
		this.true_angle[2] = angle[2];
		this.pixel_value = pixel_value;
		this.dimension = dimension;
		this.angle = angle;

		this.name = StaticField_Form.FORM_NAME[StaticField_Form.PYRAMID];
		this.layer = layer;
	}

// #################################################################################################
//fonction


	public void updateInfo(){}
	
	/**
	* Compute and test if the point (x,y,z) is in or out of the diameter
	* Override Create_form.isIn
	* @param position int[] the array containing the (x,y,z) vector of coordonate
	* @return float an Integer between 0 and 1 if the test is true, -1 if false
	**/
	public float isIn(int [] position){
		double tmp;
		double x;
		double y;
		double z;
		double intenity_test_x;
		double intenity_test_y;

		x = (position[0]*matrice_rotation_euler[0][0]) + this.cst_x_1 + (position[1]*matrice_rotation_euler[0][1]) + this.cst_x_2 + (position[2]*matrice_rotation_euler[0][2]) + this.cst_x_3 ;
		if( (int)x >= this._shape[0][0] && (int)x <= _shape[0][1] ){
			
			//position en y
			y = (position[0]*matrice_rotation_euler[1][0]) + this.cst_y_1 + (position[1]*matrice_rotation_euler[1][1]) + this.cst_y_2 + (position[2]*matrice_rotation_euler[1][2]) + this.cst_y_3 ;			
			if ( (int)y >= this._shape[1][0] && (int)y <= this._shape[1][1] ){
				
				//position en z
				z = (position[0]*matrice_rotation_euler[2][0]) + this.cst_z_1 + (position[1]*matrice_rotation_euler[2][1]) + this.cst_z_2 + (position[2]*matrice_rotation_euler[2][2]) + this.cst_z_3 ;
				if ( (int)z >= this._shape[2][0] && (int)z <= this._shape[2][1] ){

					
					double proportion = (z-this.origine[2])/dimension[2];
					tmp = proportion;
					
					
					if ( proportion == 0){
						proportion = 0.5;
					}
					else{
						proportion = (.5 - proportion);
					}
					
					// intenity_test_x = ((x-this.origine[0])*(x-this.origine[0])) / ((this.dimX_cst*proportion)*(this.dimX_cst*proportion));
					// intenity_test_y = ((y-this.origine[1])*(y-this.origine[1])) / ((this.dimY_cst*proportion)*(this.dimY_cst*proportion));
				
					intenity_test_x = Math.sqrt((x-this.origine[0])*(x-this.origine[0])) / (this.dimX_cst*proportion);
					intenity_test_y = Math.sqrt((y-this.origine[1])*(y-this.origine[1])) / (this.dimY_cst*proportion);
				
					if ( intenity_test_x <= 1 && intenity_test_y <= 1){
						return (float)Math.max(Math.max(intenity_test_x,intenity_test_y),Math.abs(tmp*2));
					}
				}
			}
		}
		return -1.0f;
	}

}
