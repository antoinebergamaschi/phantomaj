# README #

Supported OS:
Windows, Linux
Download the Phanto_maJ-1.0.0.one-jar.jar file to have a stand alone software.
To use Phanto_maJ as a ImageJ plugin follow instruction here.
If used as an ImageJ plug-in :
For windows users, there seems to be some bugs between ogl.dll and imageJ. to fix the bug add -Dj3d.rend=d3d option in the ImageJ.cfg file and make sure you have the j3dcore-d3d.dll

######

PhantomaJ is a software which aims to help users designing complexe three dimensional structures and building phantom banks. This software, implemented as an ImageJ plug-in, has been designed to be intuitive. PhantomaJ is thus "ready to use" for most users, but advanced users can choose to modify specific parameters such as intersection mode, intensity decrease function or virtual real three dimension. In addition, adding a new "basic form" consiste for advanced users to write two short java file ( one describing the new form geometry and the other transcripting this geometry in java3D world ). PhantomaJ is the indispensable tool for who want to create phantom bank of simply an unique complexe structure very easily.

* Version : 1.0.0
* [site](http://cmib.curie.fr/fr/t-l-chargements/softwares/phantomaj/phantomaj-00899)

######

Supported OS:
Windows, Linux

Download the Phanto_maJ-1.0.0.one-jar.jar file to have a stand alone software.
To use Phanto_maJ as a ImageJ plugin follow instruction here.

If used as an ImageJ plug-in :

For windows users, there seems to be some bugs between ogl.dll and imageJ. to fix the bug add -Dj3d.rend=d3d option in the ImageJ.cfg file and make sure you have the j3dcore-d3d.dll

######

* Antoine Bergamaschi